# README #

This project is currently in early development.


# BUILDING #

To run a project, run `../gradlew :run` in the specific project's directory. This will build any modified source files and run the project.

To edit the projects in Eclipse, run `./gradlew eclipse` in the root directory. This will generate Eclipse project files for each project, so you can now open the projects from Eclipse.

To build distributable archives of the projects, run `./gradlew distZip` in the root directory. This will generate zip files in `<project>/build/distributions/<project>.zip`, which when extracted can be run by running the `bin/<project>` script.

To view a list of other possible build targets, you can run `./gradlew tasks` in the root directory (or `../gradlew tasks` in a project directory to see targets for that project).


# DEVELOPMENT #

Development should be done in the [`dev`](https://bitbucket.org/nautdrafter/nautdrafter/branch/dev) branch, or (for new features or non-trivial changes) a feature branch for the specific feature (which should be merged into `dev` once working). Long-living feature branches should be frequently rebased off `dev` to avoid problems when merging.

The `dev` branch will be merged into `master` by a maintainer at milestone releases. Important fixes may also be merged into `master` whenever necessary.
