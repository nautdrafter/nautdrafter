/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.callbacks;

import java.nio.channels.SelectionKey;
import java.util.UUID;

/**
 * Callbacks for Client login requests
 */
public interface ClientIdentityCallback {

	/**
	 * An unknown client has requested login
	 *
	 * @param clientId
	 *            The client's private unique id
	 * @param key
	 *            The client's connection
	 */
	public void onClientUnknown(UUID clientId, SelectionKey key);

	/**
	 * A client with a known clientId and steamId has requested their information
	 *
	 * @param clientId
	 *            The client's private unique id
	 * @param steamId
	 *            The client's unique Steam id
	 * @param key
	 *            The client's connection
	 * @return true if the client information was successfully retrieved
	 */
	public boolean onClientIdentify(UUID clientId, Long steamId, SelectionKey key);
}
