/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network;

import java.util.UUID;

/**
 * Keeps track of a user's pending authentication
 */
public class AuthPendingConnectionInfo extends ConnectionInfo {

	/**
	 * UUID of the user we represent
	 */
	public final UUID         uuid;

	/**
	 * Upgrades a ConnectionInfo to an AuthPendingConnectionInfo
	 *
	 * @param upgrade
	 *            The ConnectionInfo to upgrade
	 * @param uuid
	 *            UUID of the user we represent
	 */
	public AuthPendingConnectionInfo(ConnectionInfo upgrade, UUID uuid) {
		super(upgrade);
		this.uuid = uuid;
	}

}
