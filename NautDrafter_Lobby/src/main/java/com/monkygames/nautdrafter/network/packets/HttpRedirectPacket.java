/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.monkygames.nautdrafter.Constants;
import com.monkygames.nautdrafter.network.packets.segments.Segment;

/**
 * Sends an HTTP redirect<br>
 * Used for redirecting a client to our success or failure page after they authenticate with Steam
 */
public class HttpRedirectPacket extends Packet {

	/**
	 * Creates a new HttpRedirectPacket
	 *
	 * @param redirectTo
	 *            The URL to redirect to
	 * @throws PacketEncoderException
	 */
	public HttpRedirectPacket(String redirectTo) throws PacketEncoderException {
		byte[] content = ("<html><head><title>301 Moved Permanently</title></head><body><center><a href=\""
		        + redirectTo + "\"><h1>301 Moved Permanently</h1></a></center></body></html>").getBytes(Packet.charset);
		byte[] headers = ("HTTP/1.1 301 Moved Permanently\r\n" + "Server: nautdrafter_lobby/" + Constants.VERSION
		        + "\r\n" + "Date: " + new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z").format(new Date()) + "\r\n"
		        + "Content-Type: text/html\r\n" + "Content-Length: " + content.length + "\r\n"
		        + "Connection: close\r\n" + "Location: " + redirectTo + "\r\n\r\n").getBytes(Packet.charset);
		this.data = Segment.concat(headers, content);
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		throw new UnsupportedOperationException("Not supported");
	}

	@Override
	protected void handleImpl() {
		throw new UnsupportedOperationException("Not supported");
	}

}
