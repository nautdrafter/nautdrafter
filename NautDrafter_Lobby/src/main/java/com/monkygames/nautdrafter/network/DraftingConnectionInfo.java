/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network;

import java.nio.channels.SelectionKey;

/**
 * Extends the common ServerInfo class to include a buffer and SelectionKey
 */
public class DraftingConnectionInfo extends ConnectionInfo {

	/**
	 * The SelectionKey of the connection
	 */
	public final SelectionKey key;
	/**
	 * Info about the Drafting Server we represent
	 */
	public final ServerInfo   serverInfo;
	/**
	 * Whether this server will be removed next timeout
	 */
	public boolean            defunct = false;

	/**
	 * Upgrades a ConnectionInfo to a DraftingConnectionInfo
	 *
	 * @param upgrade
	 *            The ConnectionInfo to upgrade
	 * @param key
	 *            The SelectionKey of the connection
	 * @param serverInfo
	 *            Info about the Drafting Server we represent
	 */
	public DraftingConnectionInfo(ConnectionInfo upgrade, SelectionKey key, ServerInfo serverInfo) {
		super(upgrade);
		this.key = key;
		this.serverInfo = serverInfo;
	}

}
