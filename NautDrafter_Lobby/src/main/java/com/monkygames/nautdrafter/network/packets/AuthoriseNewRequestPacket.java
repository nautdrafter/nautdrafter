/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.monkygames.nautdrafter.network.packets;

import java.nio.channels.SelectionKey;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.monkygames.nautdrafter.network.callbacks.ClientIdentityCallback;

/**
 * This packet is used to tell the client to authorise a new client
 */
public class AuthoriseNewRequestPacket extends Packet {

	/**
	 * The constant used for the specific packet ID
	 */
	public static final int              PACKET_ID = Packet.NEWAUTH_PACKET_ID;

	private final SelectionKey           key;
	private final ClientIdentityCallback receiver;

	/**
	 * Constructor taking a byte[] and creating a new packet form this
	 *
	 * @param data
	 * @param key
	 * @param reciever
	 */
	public AuthoriseNewRequestPacket(byte[] data, SelectionKey key, ClientIdentityCallback receiver) {
		super(data);
		this.key = key;
		this.receiver = receiver;
	}

	/**
	 * How to decode this packet
	 *
	 * @return
	 * @throws PacketDecoderException
	 */
	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		return true;
	}

	/**
	 * What you receive this packet, create a new ClientID Packet
	 */
	@Override
	protected void handleImpl() {
		UUID clientId = UUID.randomUUID();
		try {
			this.response = new ClientIdPacket(clientId.toString());
		} catch (PacketEncoderException ex) {
			Logger.getLogger(AuthoriseNewRequestPacket.class.getName()).log(Level.SEVERE, null, ex);
			this.disconnect = true;
		}
		this.receiver.onClientUnknown(clientId, this.key);
	}
}
