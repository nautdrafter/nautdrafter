package com.monkygames.nautdrafter.network;

import com.monkygames.nautdrafter.Constants;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Contains information relating to a draft server for managing dedicated servers.
 */
public class DraftServerHolder {

    /**
     * The host.
     */
    public String host;

    /**
     * The port for this dedicated server.
     */
    public int port;

    /**
     * The drafting server associated with this holder.
     */
    public DraftingServer draftingServer;

    /**
     * The player that requested this server to be created.
     */
    public Player player;

    /**
     * The time this server was created.
     */
    private long serverCreateTime;

    /**
     * The address for this server.
     */
    private InetSocketAddress addr;

    /**
     * The lobby server address.
     */ 
    private InetSocketAddress lobbyAddr;

    /**
     * 5 minute timeout peroid for once the match has ended.
     */
    private static final long MATCH_END_TIMEOUT = 5*60*1000;
    /**
     * True if we are in the end match state.
     */
    private boolean isEndMatchState = false;

    /**
     * The time in millis that the end of the match was noticed.
     */
    private long endMatchStartTime;

    /**
     * Creates a new drafting server holder.
     * @param port
     * @port a unique port that will be used for creating a drafting server.
     */
    public DraftServerHolder(String host, int port){
        this.host = host;
        this.port = port;
        addr      = new InetSocketAddress(host,port);
        lobbyAddr = new InetSocketAddress(Constants.LOBBY_ADDRESS, Constants.LOBBY_PORT);
        /*
        try {
            //InetAddress addrI = InetAddress.getByName(host);
            //addr = new InetSocketAddress(addrI,port);
        } catch (UnknownHostException ex) {
            Logger.getLogger(DraftServerHolder.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
        draftingServer = null;
    }
    public boolean createDraftingServer(
        CreateServerInfo createServerInfo, 
        Player player
    ){
        return createDraftingServer(
            createServerInfo.name,
            createServerInfo.description,
            (byte)createServerInfo.mapId,
            createServerInfo.isAutoRandomMap,
            createServerInfo.schedule,
            addr,
            lobbyAddr,
            createServerInfo.banBidirectional,
            createServerInfo.isSequentialDraft,
            createServerInfo.isCaptainsMode,
            createServerInfo.isPickBan,
            player
        );
    }

    /**
     * True on success and false on failure.
     * @param name
     * @param description
     * @param mapId
     * @param isAutoRandomMap
     * @param addr
     * @param schedule
     * @param lobbyAddress
     * @param banBidirectional
     * @param isSequentialDraft
     * @param isCaptainsMode
     * @param isPickBan
     * @param player
     * @return true if able to create the server and false otherwise.
     */
    public boolean createDraftingServer(
        String name, 
        String description, 
        byte mapId, 
        boolean isAutoRandomMap, 
        int[] schedule, 
        InetSocketAddress addr,
        InetSocketAddress lobbyAddress, 
        boolean banBidirectional,
        boolean isSequentialDraft,
        boolean isCaptainsMode,
        boolean isPickBan,
        Player player
    ){
        if(draftingServer != null){
            return false;
        }
        serverCreateTime = Calendar.getInstance().getTimeInMillis();
        try {
            // null - set password to null as passwords are not allowed on dedicated servers
            // false - do not allow lan (doesn't make sense as its on the dedicated server
            // true - export all games
            draftingServer = new DraftingServer(
                name, 
                description, 
                null, 
                mapId, 
                isAutoRandomMap, 
                addr,
                host,
                port, 
                schedule, 
                lobbyAddress,
                isPickBan,
                banBidirectional, 
                isSequentialDraft, 
                false, 
                true, 
                isCaptainsMode,
                true
            );
            this.player = player;
            // need to start the drafting server in its own thread.
            new Thread(() -> draftingServer.run()).start();
        } catch (IOException ex) {
            Logger.getLogger(DraftServerHolder.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    /**
     * Checks if this server has timed out.  If so, it kills the server.
     * Also checks if the server has been killed due to end of game.
     * If its been killed by the game, it simply frees up the server as if it was a timeout.
     * @param timeout the timeout period to check.
     * @return true if the server has timed out and false if its not timed out.
     */
    public boolean doTimeout(long timeout){
        if(draftingServer != null){
            // check if the drafting server has already been killed
            if(draftingServer.isShutdown()){
                // free it up, no need to check
                draftingServer = null;
                player = null;
                return true;
            }

            // get current time
            long currentTime = Calendar.getInstance().getTimeInMillis();

            if(this.isEndMatchState){
                if(currentTime - this.endMatchStartTime > MATCH_END_TIMEOUT){
                    draftingServer.shutdown();
                    draftingServer = null;
                    player = null;
                    this.isEndMatchState = false;
                    return true;
                }

            }else if(draftingServer.isCompletedMatch() ){
                this.isEndMatchState = true;
            }else{
                if((currentTime - serverCreateTime) > timeout){
                    if(!draftingServer.isShutdown()){
                        draftingServer.shutdown();
                    }
                    draftingServer = null;
                    player = null;
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns the status of the drafting server.
     * @return true if in use and false otherwise.
     */
    public boolean isInUse(){
        if(draftingServer != null){
            return true;
        }
        return false;
    }
    
}