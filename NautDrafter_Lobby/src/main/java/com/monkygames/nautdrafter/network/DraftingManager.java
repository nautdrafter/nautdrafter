package com.monkygames.nautdrafter.network;

import com.monkygames.nautdrafter.network.callbacks.DedicatedServerRequestCallback;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Manages the dedicated drafting servers.
 * This is a threaded class that ensures the drafting servers do not
 * live indefeniatly (right now timeout is set to 10 minutes of inactivity).
 * 
 * This class also uses syncronization in order to manage servers being created
 * and destroyed.  A ServerHolder class manages the actual initialization of
 * a DraftingServer and also the deletion of said drafting server when timed out.
 */
public class DraftingManager implements Runnable{
    /**
     * The defaults for pool size.
     */
    public int DEFAULT_POOL_SIZE = 100;
    /**
     * The total number of dedicated drafting servers.
     */
    public int poolSize = DEFAULT_POOL_SIZE;

    /**
     * A collection of servers currently in use.
     */
    private List<DraftServerHolder> usedServers;

    /**
     * A collection of servers that are currently free to use.
     */
    private List<DraftServerHolder> availableServers;

    public static final int START_PORT = 500001;
    public int startPort = START_PORT;

    /**
     * The time to check to see if a server has timed out.
     * In milliseconds (every 2 minutes)
     */
    private static final long CHECK_TIME = 120000;

    /**
     * The timeout for this server.
     * This is the maximum amount of time the server can live.
     * 45 minutes 
     */
    private static final long SERVER_TIMEOUT = 45*60*1000;
    private boolean checkTimeOuts;

    /**
     * Creates a drafting manager.
     * @param host The server host name.
     * @param startPort the starting port.
     * @param poolSize the size of the dedicated server pool.
     */
    public DraftingManager(String host, int startPort, int poolSize){
        this.startPort = startPort;
        this.poolSize = poolSize;
        initializeServers(host);

        // start timeout thread
        Thread thread = new Thread(this);
        thread.start();
    }

    // === public methods === //

    public synchronized int createDedicatedServer(CreateServerInfo createServerInfo, Player player){
        synchronized(availableServers){
           if(availableServers.size() == 0){
                Logger.getLogger(LobbyServer.class.getName()).log(
                    Level.INFO, 
                    "Failed to create drafting server: no enough free servers",
                    new Object[] { player.toString() }
                );
                return DedicatedServerRequestCallback.RESPONSE_FAILED_NO_SERVERS;
           } 
           // check if the user already has a server created
           if(isServerOwnedByPlayer(player)){
                Logger.getLogger(LobbyServer.class.getName()).log(
                    Level.INFO, 
                    "Failed to create drafting server: player already has an existing server",
                    new Object[] { player.toString() }
                );
               return DedicatedServerRequestCallback.RESPONSE_FAILED_EXISTING_SERVER;
           }

            // get the drafting server
            DraftServerHolder serverHolder = availableServers.remove(0);
            serverHolder.createDraftingServer(createServerInfo,player);
            synchronized(usedServers){
                usedServers.add(serverHolder);
            }
            Logger.getLogger(LobbyServer.class.getName()).log(
                Level.INFO, 
                "Create drafting server: ",
                new Object[] { player.toString() }
            );
        }
        return DedicatedServerRequestCallback.RESPONSE_SUCCESS;
    }

    @Override
    public void run(){
        checkTimeOuts = true;
        while(checkTimeOuts){
            try {
                // check
                synchronized(usedServers){
                    Iterator<DraftServerHolder> iterator = usedServers.iterator();
                    DraftServerHolder serverHolder;
                    while (iterator.hasNext()) {
                        serverHolder = iterator.next();
                        if(serverHolder.doTimeout(SERVER_TIMEOUT)){
                            // remove from used servers
                            iterator.remove();
                            synchronized(availableServers){
                                // add back to available servers
                                availableServers.add(serverHolder);
                            }
                        }
                    }
                }
                Thread.sleep(CHECK_TIME);
            } catch (InterruptedException ex) {
                Logger.getLogger(DraftingManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    // === private methods === //
    /**
     * Initialize the servers.
     * Thread safe.
     */
    private synchronized void initializeServers(String host){
        usedServers = Collections.synchronizedList(new ArrayList<DraftServerHolder>());
        availableServers = Collections.synchronizedList(new ArrayList<DraftServerHolder>());

        for(int i = 0; i < poolSize; i++){
            availableServers.add(new DraftServerHolder(host,startPort+i));
        }
    }

    /**
     * Checks used servers's owner for a match with the passed in player.
     * @param player the player to check.
     * @return true if the player ownes an in use server and false otherwise.
     */
    private synchronized boolean isServerOwnedByPlayer(Player player){
        synchronized(usedServers){
            Iterator<DraftServerHolder> iterator = usedServers.iterator();
            DraftServerHolder serverHolder;
            while (iterator.hasNext()) {
                serverHolder = iterator.next();
                if(serverHolder.player.equals(player)){
                    return true;
                }
            }
        }
        return false;
    }
}