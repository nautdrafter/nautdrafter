/*
 * Copyright (C) 2014 Nautdrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import com.monkygames.nautdrafter.network.packets.Packet;
import com.monkygames.nautdrafter.network.packets.UserDetailsPacket;

/**
 * Used to handle all of the interaction with Steam's API, OpenID and Community
 */
public class SteamAPI {

	/**
	 * Reads the API key from a file. If the file is missing the program will exit with an error message telling the
	 * user where to put the file.
	 *
	 * @return The contents of the STEAM_API_KEY file, with whitespace trimmed from either end.
	 */
	private static String getKey() {
		try {
			return new String(Files.readAllBytes(Paths.get("STEAM_API_KEY")), Packet.charset).trim();
		} catch (IOException ex) {
			System.err
			        .println("You need to create a file named \"STEAM_API_KEY\" in the same folder as \"NautDrafter_Lobby.jar\", containing your Steam API Key");
			System.exit(-1);
			return null;
		}
	}

	private static final String API_KEY           = SteamAPI.getKey();
	private static final String PROFILE_API_URL   = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key="
	                                                      + SteamAPI.API_KEY + "&steamids=";
	private static final String USERSTATS_API_URL = "http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=204300&key="
	                                                      + SteamAPI.API_KEY + "&steamid=";

	/**
	 * This will be called once Steam has finished authenticating and will finish off the process by obtaining your
	 * avatar, name and various statistics from the Steam Web API. These statistics are used to compute the tie break
	 * value that is also returned in the UserDetailsPacket.<br>
	 *
	 * @param clientId
	 *            The unique ID that should be stored in the users preferences file that is created by the database
	 *            software on the server
	 * @param steamId
	 *            The ID returned by the initial Steam OAuth request
	 * @return A UserDetailsPacket containing a Player which has the SteamID, Steam Name, Steam Avatar and the tie break
	 *         value included in it
	 */
	public static UserDetailsPacket getDetails(UUID clientId, Long steamId) {
		// Insert into database
		new UserDetails(clientId, steamId);
		Logger.getLogger(LobbyServer.class.getName()).log(Level.INFO,
		        "Obtaining Steam username and Profile picture for {0}", steamId);
		try {
			// Get username and icon using Steam API
			URL JSONUrl = new URL(SteamAPI.PROFILE_API_URL + steamId);
			BufferedReader br = new BufferedReader(new InputStreamReader(JSONUrl.openStream()));
			StringBuilder sb = new StringBuilder();
			int count;
			while ((count = br.read()) != -1) {
				sb.append((char) count);
			}
			String jsonText = sb.toString();
			JSONObject parsed = new JSONObject(jsonText).getJSONObject("response").getJSONArray("players")
			        .getJSONObject(0);
			String steamName = parsed.getString("personaname");
			String avatarImg = parsed.getString("avatarmedium");

			long tieBreak = 0;
			try {
				// Get the ranking for the TieBreak from a different Steam API
				JSONUrl = new URL(SteamAPI.USERSTATS_API_URL + steamId);
				br = new BufferedReader(new InputStreamReader(JSONUrl.openStream()));
				sb = new StringBuilder();
				count = 0;
				while ((count = br.read()) != -1) {
					sb.append((char) count);
				}
				jsonText = sb.toString();
				JSONArray names = new JSONObject(jsonText).getJSONObject("playerstats").getJSONArray("stats");
				double wincount = 0;
				double playcount = 1;
				for (int i = 0; i < names.length(); i++) {
					parsed = (JSONObject) names.get(i);
					if (parsed.getString("name").equals("GST_SEASON_GAMES_WON")) {
						wincount = parsed.getInt("value");
					}
					if (parsed.getString("name").equals("GST_SEASON_GAMES_STARTED")) {
						playcount = parsed.getInt("value");
					}
				}

				// Calculate what the tiebreak value is
				if (playcount > 0) {
					tieBreak = (long) ((wincount / playcount) * Math.log10(wincount) * 1000000);
				}
			} catch (IOException ex) {
				// failed to get user stats
			}

			// Send all the details back
			return new UserDetailsPacket(new Player(new PlayerIdentifier(true, steamId), steamName, avatarImg, tieBreak));
		} catch (IOException ex) {
			Logger.getLogger(SteamAPI.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	/**
	 * THIS METHOD SHOULD NOT CONTAIN ANY CODE<br>
	 * Called to touch this class in order to load static members at startup<br>
	 * (they take awhile to load so its better to do it on startup than when we receive our first login request)
	 */
	public static void touch() {
	}
}
