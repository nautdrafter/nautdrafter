/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.monkygames.nautdrafter.Constants;
import com.monkygames.nautdrafter.model.Summary;
import com.monkygames.nautdrafter.network.callbacks.ClientIdentityCallback;
import com.monkygames.nautdrafter.network.callbacks.DedicatedServerRequestCallback;
import com.monkygames.nautdrafter.network.callbacks.SaveSummaryCallback;
import com.monkygames.nautdrafter.network.callbacks.ServerAnnounceCallback;
import com.monkygames.nautdrafter.network.callbacks.ServerListRequestCallback;
import com.monkygames.nautdrafter.network.callbacks.VersionCallback;
import com.monkygames.nautdrafter.network.packets.AuthoriseNewRequestPacket;
import com.monkygames.nautdrafter.network.packets.ClientIdPacket;
import com.monkygames.nautdrafter.network.packets.DedicatedServerRequestPacket;
import com.monkygames.nautdrafter.network.packets.LobbyKeepalivePacket;
import com.monkygames.nautdrafter.network.packets.OpenIdHttpGetRequestPacket;
import com.monkygames.nautdrafter.network.packets.Packet;
import com.monkygames.nautdrafter.network.packets.PacketEncoderException;
import com.monkygames.nautdrafter.network.packets.ServerAnnouncePacket;
import com.monkygames.nautdrafter.network.packets.ServerListPacket;
import com.monkygames.nautdrafter.network.packets.ServerListRequestPacket;
import com.monkygames.nautdrafter.network.packets.ServerUpdatePacket;
import com.monkygames.nautdrafter.network.packets.SummaryPacket;
import com.monkygames.nautdrafter.network.packets.UserDetailsPacket;
import com.monkygames.nautdrafter.network.packets.VersionPacket;

/**
 * Lobby Server<br>
 * A public server which handles client login, and maintains a list of Drafting Servers
 */
public class LobbyServer extends Server implements 
    ServerAnnounceCallback, 
    ServerListRequestCallback,
    ClientIdentityCallback, 
    VersionCallback, 
    SaveSummaryCallback ,
    DedicatedServerRequestCallback
        {
	
	private static final Logger logger = Logger.getLogger(LobbyServer.class.getName());

	private final Set<DraftingConnectionInfo> drafting_servers = new HashSet<>();

	private final Map<UUID, SelectionKey>     pending_auths    = new HashMap<>();
	/**
	 * The location to write the summary's exported. If null, summaries are not saved.
	 */
	private String                            summaryExportPath;
	/**
	 * The directory to save file.
	 */
	private File                              summaryDir;
	/**
	 * true if the server is setup to save exports && if path is valid.
	 */
	private boolean                           doSaveExport     = false;

    /**
     * Manages dedicated drafting servers.
     */
    private DraftingManager draftingManager;

	/**
	 * Sends keepalive requests to DraftingServers, and removes them from the Lobby listing if they do not respond
	 * before the next keepalive
	 */
	private final class KeepaliveThread extends Thread {

		private boolean isShutdown = false;

		public KeepaliveThread() {
			super("Drafting Server Keepalive");
			this.start();
		}

		@Override
		public void run() {
			while (true) {
				try {
					Thread.sleep(Constants.KEEPALIVE_INTERVAL);
				} catch (InterruptedException e) {
					if (this.isShutdown) {
						break;
					}
					e.printStackTrace();
				}
				synchronized (LobbyServer.this.drafting_servers) {
					Set<DraftingConnectionInfo> current_servers = new HashSet<>(LobbyServer.this.drafting_servers);
					for (DraftingConnectionInfo server : current_servers) {
						if (server.defunct) {
							logger.log(Level.INFO, "Server Timed Out: {0}@{1}:{2}",
								new Object[] {
									server.serverInfo.name,
									server.serverInfo.address,
									server.serverInfo.port
								}
							);
							LobbyServer.this.kill(server.key);
						} else {
							server.defunct = true;
							server.sendBuffer.put(Packet.KEEPALIVE_PACKET_ID);
							LobbyServer.this.write(server.key);
						}
					}
				}
			}
		}

		public void shutdown() {
			this.isShutdown = true;
			this.interrupt();
		}
	}

	private final KeepaliveThread timeout_thread = new KeepaliveThread();

	/**
	 * Creates a new LobbyServer listening on addr
	 *
	 * @param addr
	 *            The address to listen on
     * @param summaryExportPath
     * @param dedicatedServerHost
     * @param dedicatedStartPort
     * @param poolSize
	 * @throws IOException
	 */
	public LobbyServer(InetSocketAddress addr, 
            String summaryExportPath, 
            String dedicatedServerHost,
            int dedicatedStartPort, 
            int poolSize,
            String draftingServerHost
        ) throws IOException {
            super(addr);

            // check if the lobby server should export.
            this.summaryExportPath = summaryExportPath;
            if (summaryExportPath != null) {
                this.summaryDir = new File(summaryExportPath);
                if (this.summaryDir.exists()) {
                    this.doSaveExport = true;
                } else {
                    System.out.println("[LobbyServer] path " + summaryExportPath + " does not exists");
                }
            }
            draftingManager = new DraftingManager(draftingServerHost,dedicatedStartPort, poolSize);
	}

	@Override
	protected void kill(SelectionKey key) {
		super.kill(key);
		// if it was a drafting server that disconnected, remove it
		if (key.attachment() instanceof DraftingConnectionInfo) {
			DraftingConnectionInfo info = (DraftingConnectionInfo) key.attachment();
			this.drafting_servers.remove(info);
			Logger.getLogger(LobbyServer.class.getName()).log(Level.INFO, "Removed server: {0}@{1}:{2}",
			        new Object[] { info.serverInfo.name, info.serverInfo.address, info.serverInfo.port });
		}
		// if it was a pending client auth, remove it
		if (key.attachment() instanceof AuthPendingConnectionInfo) {
			this.pending_auths.remove(((AuthPendingConnectionInfo) key.attachment()).uuid);
		}
	}

	@Override
	protected Packet createPacket(SelectionKey key, byte[] data) throws IOException {
		// make sure the timeout gets reset
		if (key.attachment() instanceof DraftingConnectionInfo) {
			((DraftingConnectionInfo) key.attachment()).defunct = false;
		}
		switch (data[0]) {
		case Packet.VERSION_PACKET_ID:
			return new VersionPacket(data, this);
		case Packet.SERVER_ANNOUNCE_PACKET_ID:
			return new ServerAnnouncePacket(data, ((SocketChannel) key.channel()).socket().getInetAddress(), this);
		case Packet.SERVER_UPDATE_PACKET_ID:
			return new ServerUpdatePacket(data, this);
		case Packet.SERVER_LIST_REQUEST_PACKET_ID:
			return new ServerListRequestPacket(data, key, this);
		case Packet.HTTP_GET_REQUEST_ID:
			return new OpenIdHttpGetRequestPacket(data, this);
		case Packet.CLIENTID_PACKET_ID:
			return new ClientIdPacket(data, key, this);
		case Packet.NEWAUTH_PACKET_ID:
			return new AuthoriseNewRequestPacket(data, key, this);
		case Packet.SAVE_SUMMARY_PACKET_ID:
			return new SummaryPacket(data, this);
		case Packet.KEEPALIVE_PACKET_ID:
			return new LobbyKeepalivePacket(data);
                case Packet.DEDICATED_SERVER_REQUEST_PACKET_ID:
                    return new DedicatedServerRequestPacket(data,this);
		}
		return null;
	}

	@Override
	public void onServerAnnounce(ServerInfo serverInfo) {
		DraftingConnectionInfo info = new DraftingConnectionInfo((ConnectionInfo) this.readingKey.attachment(),
		        this.readingKey, serverInfo);
		this.drafting_servers.add(info);
		this.readingKey.attach(info);
		Logger.getLogger(LobbyServer.class.getName()).log(Level.INFO, "Server Announce: {0}@{1}:{2} - {3} players",
		        new Object[] { serverInfo.name, serverInfo.address, serverInfo.port, serverInfo.playerCount });
	}

	@Override
	public void onServerUpdate(int playerCount) {
		DraftingConnectionInfo info = (DraftingConnectionInfo) this.readingKey.attachment();
		ServerInfo serverInfo = info.serverInfo;
		info.serverInfo.playerCount = playerCount;
		Logger.getLogger(LobbyServer.class.getName()).log(Level.INFO, "Server Update: {0}@{1}:{2} - {3} players",
		        new Object[] { serverInfo.name, serverInfo.address, serverInfo.port, serverInfo.playerCount });
	}

	@Override
	public void onServerListRequest(SelectionKey key) {
		try {
			ConnectionInfo info = (ConnectionInfo) key.attachment();
			ServerListPacket packet = new ServerListPacket(this.drafting_servers);
			info.sendBuffer.put(packet.data);
			this.write(key);
		} catch (PacketEncoderException ex) {
			Logger.getLogger(LobbyServer.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	public void onClientUnknown(UUID clientId, SelectionKey key) {
		AuthPendingConnectionInfo info = new AuthPendingConnectionInfo((ConnectionInfo) this.readingKey.attachment(),
		        clientId);
		key.attach(info);
		this.pending_auths.put(clientId, key);
	}

	@Override
	public boolean onClientIdentify(UUID clientId, Long steamId, SelectionKey key) {
		if (key == null) {
			key = this.pending_auths.get(clientId);
		}
		if (key == null) {
			return false;
		}
		UserDetailsPacket packet = SteamAPI.getDetails(clientId, steamId);
		Player player = packet.getPlayer();
		Logger.getLogger(LobbyServer.class.getName()).log(Level.INFO, "Sending user details for {0} ({1})", player);
		((ConnectionInfo) key.attachment()).sendBuffer.put(packet.data);
		this.write(key);
		return true;
	}

	@Override
	public void onVersionReceived(int major, int minor, int patch) {
		ConnectionInfo info = (ConnectionInfo) this.readingKey.attachment();
		info.sendBuffer.put(Constants.VERSION_PACKET);
		// lobby is only incompatible if the major version has changed
		if (Constants.VERSION_MAJOR != major) {
			String version = major + "." + minor + "." + patch;
			info.kill = true;
		}
		this.write(this.readingKey);
	}

        @Override
        public int onDedicatedServerRequestReceived(
            CreateServerInfo createServerInfo,
            Player player
        ){
            // check if this player is authenticated with steam.  If not, do not create.
            if(!player.isSteam()){
                Logger.getLogger(LobbyServer.class.getName()).log(
                    Level.INFO, 
                    "Unauthenticated Player attempting to create a dedicated server: ",
                    new Object[] { player.toString() }
                );
                return DedicatedServerRequestCallback.RESPONSE_FAILED_NO_STEAM;
            }
            // create the dedicated server
            return draftingManager.createDedicatedServer(createServerInfo,player);
        }

	@Override
	public void shutdown() {
		this.timeout_thread.shutdown();
		super.shutdown();
	}

    /**
     * The main loop.
     *
     * @param args
     *            args[0] address to bind args[1] port to bind args[2] path to write game summaries if unspecified
     *            (defaults to the default local address, port 8080, and does not save)
     * 
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        InetAddress addr = null;
        int port = Constants.LOBBY_PORT;
        String dedicatedServerHost = null;
        String draftingServerHost = Constants.LOBBY_ADDRESS;
        int dedicatedServerPort = Constants.LOBBY_PORT+1;
        int dedicatedServerSize = 3;
        String savePath = null;
        if (args.length > 0) {
            addr = InetAddress.getByName(args[0]);
            dedicatedServerHost = args[0];
        }
        if (args.length > 1) {
            port = Integer.parseInt(args[1]);
        }
        if(args.length > 2) {
            draftingServerHost = args[2];
        }
        if(args.length > 3) {
            dedicatedServerPort = Integer.parseInt(args[3]);
        }
        if(args.length > 4){
            dedicatedServerSize = Integer.parseInt(args[4]);
        }
        if (args.length > 5) {
            savePath = args[5];
        }
        SteamAPI.touch();
        Logger.getLogger(LobbyServer.class.getName()).log(Level.INFO, "Loading database...");
        UserDetails.touch();
        Logger.getLogger(LobbyServer.class.getName()).log(Level.INFO, "Database loaded!");
        new LobbyServer(
            new InetSocketAddress(addr, port),
            savePath,
            dedicatedServerHost,
            dedicatedServerPort,
            dedicatedServerSize,
            draftingServerHost
        ).run();
    }

    @Override
    public void saveSummaryToDisk(String summary,
        String redTeamName,
        String blueTeamName,
        String mapName,
        boolean isRandomMap) {
        if (this.doSaveExport) {
            File exportFile = new File(this.summaryExportPath + File.separator
                + Summary.getExportFileName(summary, redTeamName, blueTeamName, mapName, isRandomMap));
            if (!Summary.writeSummaryFile(summary, exportFile)) {
                // if it failed, to something?
                System.out.println("Failed to write file: " + exportFile);
            }
        }
    }
}
