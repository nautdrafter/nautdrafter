/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.monkygames.nautdrafter.network.packets;

import java.util.Collection;

import com.monkygames.nautdrafter.network.DraftingConnectionInfo;
import com.monkygames.nautdrafter.network.packets.segments.Segment;
import com.monkygames.nautdrafter.network.packets.segments.ServerInfoSegment;

/**
 * Lobby implementation of a Server List
 */
public class ServerListPacket extends Packet {

	public static final byte PACKET_ID = Packet.SERVER_LIST_PACKET_ID;

	public ServerListPacket(Collection<DraftingConnectionInfo> servers) throws PacketEncoderException {
		int server_count = servers.size();
		if (server_count > 0xFFFF) {
			throw new PacketEncoderException("Too many servers!");
		}
		byte[][] buffers = new byte[server_count][];
		int i = 0;
		for (DraftingConnectionInfo server : servers) {
			buffers[i++] = ServerInfoSegment.encode(server.serverInfo);
		}
		this.data = Segment.concat(PACKET_ID, (byte) ((server_count >> 8) & 0xFF), (byte) ((server_count) & 0xFF),
		        buffers);
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		throw new UnsupportedOperationException("Not supported");
	}

	@Override
	protected void handleImpl() {
		throw new UnsupportedOperationException("Not supported");
	}

}
