/*
 * Copyright (C) 2014 NautDrafter Lobby
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.callbacks.ServerAnnounceCallback;

/**
 * Updates the player count for a Drafting Server
 */
public class ServerUpdatePacket extends ServerUpdatePacketEncoder {

	private final ServerAnnounceCallback receiver;
	private Integer                      playerCount;

	public ServerUpdatePacket(byte[] data, ServerAnnounceCallback receiver)
	        throws PacketDecoderException {
		super(data);
		this.receiver = receiver;
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		if (this.playerCount == null) {
			this.playerCount = (this.data[this.pos++] & 0xFF);
		}
		return true;
	}

	@Override
	protected void handleImpl() {
		this.receiver.onServerUpdate(this.playerCount);
	}

}
