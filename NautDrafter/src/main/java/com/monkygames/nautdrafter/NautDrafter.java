/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.prefs.Preferences;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import com.monkygames.nautdrafter.audio.AudioPlayer;
import com.monkygames.nautdrafter.controller.Base;
import com.monkygames.nautdrafter.controller.Browser;
import com.monkygames.nautdrafter.controller.LoginScreen;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.network.DraftingClient;
import com.monkygames.nautdrafter.network.DraftingServer;
import com.monkygames.nautdrafter.network.LanServerInfo;
import com.monkygames.nautdrafter.network.LobbyClient;
import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.PlayerIdentifier;
import com.monkygames.nautdrafter.view.PopUp;
import com.monkygames.nautdrafter.view.PopUp.ButtonCreator;
import com.monkygames.nautdrafter.view.SubScreen;
import com.sun.javafx.application.ParametersImpl;

/**
 * The main application class for NautDrafter
 */
public class NautDrafter extends Application {
	private static final String   DOC_URL_BASE = "https://kbmaster.atlassian.net/wiki/display/NAUTW/";
	private static final String   DOC_DOMAIN   = "kbmaster.atlassian.net";

	private static NautDrafter    _instance;
	private static boolean        isPlayer;
	private Preferences           prefs;
	private String                CountryCode;

	static Base                   root;
	public static Stage           stage;
	VBox                          adsVbox;

	private static Scene          mainScene;

	/**
	 * A static AudioPlayer to be used by the entire application to play sounds
	 */
	private static AudioPlayer    audioPlayer;

	/**
	 * Load locales dynamically from the bundles folder
	 */
	private static Locale[]       supportedLocales;
	static {
		List<Locale> locales = new ArrayList<>();
		locales.add(Locale.UK); // default locale
		try {
			for (String locale : Helpers.getResources(Constants.class, "/bundles/")) {
				System.out.println("Locale: " + locale);
				if (!locale.equals("Strings.properties")) {
					// get the language tag by stripping "Strings_" from the start, and ".properties" from the end
					locale = locale.substring(8, locale.length() - 11).replace('_', '-');
					locales.add(Locale.forLanguageTag(locale));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		NautDrafter.supportedLocales = locales.toArray(new Locale[0]);
	}

	@Override
	public void start(Stage stage) throws Exception {

		NautDrafter._instance = this;
		this.prefs = Preferences.userNodeForPackage(NautDrafter.class);

		audioPlayer = new AudioPlayer();

		Application.setUserAgentStylesheet(Application.STYLESHEET_MODENA);

		NautDrafter.root = new Base();
		this.adsVbox = new VBox(new Text("Developed By"), new ImageView("assets/images/monky.png"), new ImageView(
		        "assets/images/arcton.png"));
		NautDrafter.root.setAdArea(this.adsVbox);
		NautDrafter.root.setTitleAreaVisable(false);

		stage.setTitle("Nautdrafter");
		this.CountryCode = NautDrafter.getPreferences().get("countryCode", null);
		System.out.println("Read From Prefs: " + this.CountryCode);
		if (this.CountryCode == null) {
			this.CountryCode = "en-GB";
			System.out.println("No Code!");
			NautDrafter.getPreferences().put("countryCode", "en-GB");
		}
		LanguageResource.get().reload(this.CountryCode);

		NautDrafter.root.setMain(new LoginScreen());
		NautDrafter.stage = stage;
		NautDrafter.mainScene = new Scene(NautDrafter.root);

		stage.setScene(NautDrafter.mainScene);
		stage.sizeToScene();
		stage.getIcons().addAll((new Image(this.getClass().getResourceAsStream("/assets/images/app-icons/16px.png"))),
		        (new Image(this.getClass().getResourceAsStream("/assets/images/app-icons/32px.png"))),
		        (new Image(this.getClass().getResourceAsStream("/assets/images/app-icons/64px.png"))),
		        (new Image(this.getClass().getResourceAsStream("/assets/images/app-icons/128px.png"))));

		stage.setOnCloseRequest(event -> {

			// No servers running, so it's safe to just close
			if (this.runningServers.isEmpty()) {
				return;
			}

			// otherwise, prevent closing until there are no servers running by consuming the event
			event.consume();

			// Show a popup that shows the user their options about closing
			PopUp shutdownPopup = new PopUp(LanguageResource.getString("server_shutdown.message"), 
			    LanguageResource.getString("server_shutdown.title"));

			shutdownPopup.setResizable(true);

			shutdownPopup.setButtons(new ButtonCreator(LanguageResource.getString("server_shutdown.keep"), () -> {
				// Do nothing - return to the main window
				    shutdownPopup.close();
			    }), new ButtonCreator(LanguageResource.getString("server_shutdown.shutdown"), () -> {
				// Shutdown all the servers, then re-call close() (which will call this function, but since there server
				// list should now be empty, it will actually close the window)
				    this.runningServers.stream().forEach((d) -> {
					    d.shutdown();
				    });
				    NautDrafter.stage.close();
			    }));

			shutdownPopup.showPopup();
		});

		stage.show();
		stage.setResizable(false);
	}

	/**
	 * Return the Base for the application
	 *
	 * @return
	 */
	public static Base getBase() {
		return NautDrafter.root;
	}

	/**
	 * Gets the AudioPlayer that should be used by the entire application to play sounds
	 * 
	 * @return AudioPlayer that should be used to play sounds
	 */
	public static AudioPlayer getAudioPlayer() {
		return NautDrafter.audioPlayer;
	}

	/**
	 * This attempt to alert the user that something in the application has changed, for example Steam is finished
	 * authenticating or a new chat message has arrived. On Windows, this will make the application flash on the
	 * taskbar. The user is able disable the effect of this function from the UI, so it may not have any effect.
	 *
	 * @see Stage#toFront()
	 */
	public static void takeAttention() {
		if (NautDrafter.getPreferences().getBoolean("allowFocusStealing", false)) {
			NautDrafter.stage.toFront();
		}
	}

	/**
	 * Set the size and resizable property in on go
	 *
	 * @param width
	 * @param height
	 * @param isResizable
	 */
	public void resizeScreen(int width, int height, boolean isResizable) {

		// Extra values are sizes of window borders
		NautDrafter.stage.setWidth(width + 16);
		NautDrafter.stage.setHeight(height + 64 + 38);
		NautDrafter.stage.setResizable(isResizable);
	}

	/**
	 * Center the screen in the window
	 */
	public void centerScreen() {
		stage.centerOnScreen();
	}

	/**
	 * Switch to another screen
	 *
	 * @param screen
	 *            SubScreen to show
	 * @param stack
	 *            Set true if you want to add onto the screen stack rather than just replace the current screen
	 */
	public static void setScreen(SubScreen screen, boolean stack) {

		if (!Platform.isFxApplicationThread()) {
			Platform.runLater(() -> NautDrafter.setScreen(screen, stack));
			return;
		}
		if (stack) {
			NautDrafter.root.addMain(screen);
		} else {
			NautDrafter.root.setMain(screen);
		}
	}

	/**
	 * Start steam in default browser
	 */
	public void startSteam() {
		this.openBrowser("http://steampowered.com");
	}

	/**
	 * Opens a browser and displays the given wiki page. If the pageIdentifier is null, the wiki's home/main page will
	 * be shown
	 * 
	 * @param pageIdentifier
	 *            Title of the wiki page to show (does not need to be URL encoded)
	 */
	public void showDocumentation(String pageIdentifier) {
		this.openBrowser(DOC_URL_BASE + pageIdentifier, DOC_DOMAIN);
	}

	/**
	 * Opens the given URL in a web browser, either using the built-in {@link Browser} in a new window or using the
	 * default system browser, depending on the user's settings.
	 * 
	 * @param url
	 *            URL to load
	 * @param allowedDomains
	 *            Only applies if the built-in browser is used. See {@link Browser#Browser(String...)}
	 */
	public void openBrowser(String url, String... allowedDomains) {
		if (Platform.isFxApplicationThread()) {
			if (!NautDrafter.getPreferences().getBoolean("useExternalBrowser", false)) {
				Stage bs = new Stage();
				Browser browser = new Browser(allowedDomains);
				bs.setScene(new Scene(browser));

				bs.initOwner(NautDrafter.stage);
				bs.sizeToScene();
				bs.centerOnScreen();

				bs.show();

				browser.load(url);
			} else {
				this.getHostServices().showDocument(url);
			}
		} else {
			Platform.runLater(() -> this.openBrowser(url, allowedDomains));
		}
	}

	/**
	 * Get the preferences of the Nautdrafter application
	 *
	 * @return Preferences object for the application
	 */
	public static Preferences getPreferences() {
		return NautDrafter._instance.prefs;
	}

	/**
	 * Get the instance of the Nautdrafter application
	 *
	 * @return The running instance of the application
	 */
	public static NautDrafter getInstance() {
		return NautDrafter._instance;
	}

	/**
	 * Get the country code drawn from the preferences
	 *
	 * @return
	 */
	public static String getCountryCode() {
		return NautDrafter._instance.CountryCode;
	}

	/**
	 * Get the array of Locales that are currently supported
	 *
	 * @return
	 */
	public static Locale[] getSupportedLocales() {
		return NautDrafter.supportedLocales;
	}

	private void reload() {
	    LanguageResource.get().reload(NautDrafter.getCountryCode());
	    NautDrafter.root.setMain(new LoginScreen());
	}

	/**
	 * Sets the country code and reloads the login screen with this new language
	 *
	 * @param CountryCode
	 */
	public static void setCountryCode(String CountryCode) {
		NautDrafter._instance.CountryCode = CountryCode;
		NautDrafter._instance.prefs.put("countryCode", CountryCode);
		System.out.println("Wrote To Prefs: " + CountryCode);
		NautDrafter._instance.reload();
	}

        /**
         * Clear the client id to force a new authentication with the steam server.
         */
        public static void resetLogin(){
            NautDrafter._instance.prefs.remove(LobbyClient.CLIENT_ID);
        }

	/**
	 * Check if the player is a spectator or not
	 *
	 * @return True if player, false if spectator
	 */
	public static boolean isPlayer() {
		return NautDrafter.isPlayer;
	}

	/**
	 * Set whether the player is a player or spectator
	 *
	 * @param isPlayer
	 *            true if a player, false otherwise
	 */
	public static void setIsPlayer(boolean isPlayer) {
		NautDrafter.isPlayer = isPlayer;
	}

	/**
	 * @param player
	 *            The player to check
	 * @return true if our player Id is the same as the given player
	 */
	public static boolean isMe(PlayerIdentifier player) {
		Player me = NautDrafter.getBase().getPlayer();
		return me != null && me.equals(player);
	}

	/*
	 * Things to help with killing servers when closing aplication *
	 */
	private final ObservableList<DraftingServer> runningServers = FXCollections.observableArrayList();

	/**
	 * Add the new drafting server to the list of hosted servers to retain a reference to them
	 *
	 * @param server
	 *            the new drafting server that was created
	 */
	public void addNewDraftingServer(DraftingServer server) {
		this.runningServers.add(server);
	}

	/**
	 * @return The list of current servers hosted by the local player
	 */
	public ObservableList<DraftingServer> getRunningServers() {
		return this.runningServers;
	}

	/**
	 * Shutdown any running drafting servers or notify user that there is one running
	 */
	@Override
	public void stop() {
            DraftingClient.disconnect();
            LanServerInfo.abortServerList();
            LobbyClient.abortLogin();
            LobbyClient.abortVersion();
            root.getTitle().stopSteamStats();
            root.getTitle().stopNautStats();
            System.exit(0);
	}

	public static Player fakePlayer = null;

	public static void main(String[] args) {

		Application.Parameters parameters = new ParametersImpl(args);

		Map<String, String> named = parameters.getNamed();

		String fakeName = named.get("name");
		if (fakeName != null) {
			String fakeIcon = named.getOrDefault("icon",
			        "http://fomori.org/blog/wp-content/uploads/2013/02/archlogo.png");
			Random rng = new Random();
			Long fakeScore = Long.parseLong(named.getOrDefault("score", Long.toString(rng.nextLong())));
			Long fakeId = Long.parseLong(named.getOrDefault("id", Long.toString(rng.nextLong())));

			fakePlayer = new Player(false, fakeId, fakeName, fakeIcon, fakeScore);
		}

		Application.launch(args);
	}
}
