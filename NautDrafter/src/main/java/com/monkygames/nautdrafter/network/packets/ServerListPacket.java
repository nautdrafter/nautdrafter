/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.monkygames.nautdrafter.network.packets;

import java.util.LinkedList;
import java.util.List;

import com.monkygames.nautdrafter.network.ServerInfo;
import com.monkygames.nautdrafter.network.packets.segments.ServerInfoSegment;

/**
 * Client implementation of a list of servers
 */
public class ServerListPacket extends Packet {

	public static final int        PACKET_ID   = Packet.SERVER_LIST_PACKET_ID;
	private final List<ServerInfo> servers     = new LinkedList<>();

	private Integer                serverCount = null;

	/**
	 * Creates a new ServerListPacket for decoding
	 *
	 * @param data
	 *            The packet data
	 */
	public ServerListPacket(byte[] data) {
		super(data);
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		if (this.max() < 3) {
			return false;
		}
		if (this.serverCount == null) {
			this.serverCount = ((this.data[this.pos++] & 0xFF) << 8) | (this.data[this.pos++] & 0xFF);
		}
		for (; this.serverCount > 0; this.serverCount--) {
			if (ServerInfoSegment.canDecode(this, true)) {
				this.servers.add(ServerInfoSegment.decode(this));
			} else {
				return false;
			}
		}
		return (this.serverCount <= 0);
	}

	/**
	 * @return The list of servers this packet contains
	 */
	public List<ServerInfo> getServers() {
		return this.servers;
	}

	@Override
	protected void handleImpl() {
		for (ServerInfo server : this.servers) {
			System.out.println("Server: " + server.name + "@" + server.address + ":" + server.port + " - "
			        + server.playerCount + " players" + (server.hasPassword ? " (password protected)" : ""));
		}
	}
}
