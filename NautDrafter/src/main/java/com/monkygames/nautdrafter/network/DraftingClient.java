/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.monkygames.nautdrafter.network;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.monkygames.nautdrafter.Constants;
import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.network.Timeline.TimelineItem;
import com.monkygames.nautdrafter.network.callbacks.ChatCallback;
import com.monkygames.nautdrafter.network.callbacks.ClientDraftingCallback;
import com.monkygames.nautdrafter.network.callbacks.ConnectionCallback;
import com.monkygames.nautdrafter.network.callbacks.DraftingCallback;
import com.monkygames.nautdrafter.network.callbacks.NautChooseCallback;
import com.monkygames.nautdrafter.network.callbacks.PlayerConnectionCallback;
import com.monkygames.nautdrafter.network.callbacks.PlayerIdentifyCallback;
import com.monkygames.nautdrafter.network.callbacks.RoomInfoCallback;
import com.monkygames.nautdrafter.network.callbacks.ServerPlayerConnectionCallback;
import com.monkygames.nautdrafter.network.callbacks.StageChangeCallback;
import com.monkygames.nautdrafter.network.callbacks.TeamCallback;
import com.monkygames.nautdrafter.network.callbacks.VersionCallback;
import com.monkygames.nautdrafter.network.packets.CaptainChooseNautPacket;
import com.monkygames.nautdrafter.network.packets.CaptainSelectNautPacket;
import com.monkygames.nautdrafter.network.packets.ChatPacket;
import com.monkygames.nautdrafter.network.packets.ChatRequestPacket;
import com.monkygames.nautdrafter.network.packets.ConnectionErrorPacket;
import com.monkygames.nautdrafter.network.packets.LockTeamPacket;
import com.monkygames.nautdrafter.network.packets.NautChoosePacket;
import com.monkygames.nautdrafter.network.packets.NautChooseRequestPacket;
import com.monkygames.nautdrafter.network.packets.Packet;
import com.monkygames.nautdrafter.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.network.packets.PacketEncoderException;
import com.monkygames.nautdrafter.network.packets.PasswordPacket;
import com.monkygames.nautdrafter.network.packets.PlayerJoinedPacket;
import com.monkygames.nautdrafter.network.packets.PlayerLeftPacket;
import com.monkygames.nautdrafter.network.packets.RoomInfoPacket;
import com.monkygames.nautdrafter.network.packets.SpectatorJoinedPacket;
import com.monkygames.nautdrafter.network.packets.StageChangePacket;
import com.monkygames.nautdrafter.network.packets.TeamCaptainSwapPacket;
import com.monkygames.nautdrafter.network.packets.TeamChangePacket;
import com.monkygames.nautdrafter.network.packets.TeamChangeRequestPacket;
import com.monkygames.nautdrafter.network.packets.TeamNameChangePacket;
import com.monkygames.nautdrafter.network.packets.VersionPacket;

/**
 * Connection between a client and a Drafting Server<br>
 * Handles communication, and also stores state
 */
public class DraftingClient extends Thread implements ServerPlayerConnectionCallback, RoomInfoCallback,
        PlayerIdentifyCallback, TeamCallback, PlayerConnectionCallback, StageChangeCallback, DraftingCallback,
        NautChooseCallback, VersionCallback {

	private static final Logger                 logger     = Logger.getLogger(DraftingClient.class.getName());
	private static DraftingClient               _instance;
	private static final Object                 lock       = new Object();

	private final InetAddress                   addr;
	private final int                           port;
	private final Player                        playerDetails;
	private final ConnectionCallback            receiver;
	private final String                        password;
	private final Map<PlayerIdentifier, Player> players    = new HashMap<>();
	private TeamCallback                        teamCallback;
	private ChatCallback                        chatCallback;
	private PlayerConnectionCallback            playerConnectionCallback;
	private StageChangeCallback                 stageChangeCallback;
	private ClientDraftingCallback              draftingCallback;
	private NautChooseCallback                  nautSelectionCallback;
	private Socket                              socket     = null;
	private boolean                             running    = true;
	private boolean                             connecting = true;
	private final Team                          teamRed    = new Team();
	private final Team                          teamBlue   = new Team();
	private final Set<Player>                   unassigned = new HashSet<>();
	private Timeline                            timeline;
	// used for minimising inaccuracy in the timeline time if we join late or the frontend is delayed
	private boolean                             offsetPhaseTime;
	private byte                                currentPhaseTime;
	private long                                phaseTimeOffset;
	private byte                                stage      = DraftingServer.STAGE_TEAM_SELECTION;
	private byte                                mapId;
        private boolean                             isPickBan;
	private boolean                             banBidirectional;
        private boolean                             isSequentialDraft;

	private DraftingClient(InetAddress addr, int port, Player playerDetails, ConnectionCallback receiver,
	        String password) {
		super("NautDrafter Drafting Client");
		this.addr = addr;
		this.port = port;
		this.playerDetails = playerDetails;
		this.receiver = receiver;
		this.password = password;
	}

	private Packet createPacket(byte[] data) throws PacketDecoderException {
		switch (data[0]) {
		case Packet.VERSION_PACKET_ID:
			return new VersionPacket(data, this);
		case Packet.ROOM_INFO_PACKET_ID:
			return new RoomInfoPacket(data, this);
		case Packet.PLAYER_JOINED_PACKET_ID:
			return new PlayerJoinedPacket(data, this);
		case Packet.PLAYER_LEFT_PACKET_ID:
			return new PlayerLeftPacket(data, this, this);
		case Packet.TEAM_CHANGE_PACKET_ID:
			return new TeamChangePacket(data, this, this);
		case Packet.TEAM_CAPTAIN_SWAP_PACKET_ID:
			return new TeamCaptainSwapPacket(data, this, this);
		case Packet.TEAM_NAME_CHANGE_PACKET_ID:
			return new TeamNameChangePacket(data, this);
		case Packet.CHAT_PACKET_ID:
			return new ChatPacket(data, this.chatCallback, this);
		case Packet.TEAM_READY_PACKET_ID:
			return new LockTeamPacket(data, this);
		case Packet.STAGE_CHANGE_PACKET_ID:
			return new StageChangePacket(data, this);
		case Packet.CAPTAIN_CHOOSE_NAUT_PACKET_ID:
			return new CaptainChooseNautPacket(data, this);
		case Packet.CAPTAIN_SELECT_NAUT_PACKET_ID:
			return new CaptainSelectNautPacket(data, this);
		case Packet.NAUT_CHOOSE_PACKET_ID:
			return new NautChoosePacket(data, this, this);
		case Packet.SERVER_MESSAGE_PACKET_ID:
			return new ConnectionErrorPacket(data, this.receiver);
		}
		return null;
	}

	@Override
	public void run() {
		// connect to drafting server
		try (Socket s = new Socket()) {
			this.socket = s;
			s.connect(new InetSocketAddress(this.addr, this.port));
			// check version
			this.socket.getOutputStream().write(Constants.VERSION_PACKET);
			// authenticate if necessary
			if (this.password != null) {
				this.socket.getOutputStream().write(new PasswordPacket(this.password).data);
			}
			// handshake with drafting server
			if (this.playerDetails != null) {
				// join as player
				this.socket.getOutputStream().write(new PlayerJoinedPacket(this.playerDetails).data);
			} else {
				// join as spectator
				this.socket.getOutputStream().write(new SpectatorJoinedPacket().data);
			}
			try (InputStream is = this.socket.getInputStream()) {
				byte[] buffer = new byte[Server.BUFFER_SIZE];
				int pos = 0;
				Packet decoding = null;
				while (this.running) {
					try {
						int read = is.read(buffer, pos, buffer.length - pos);
						if (read == -1) {
							break;
						}
						pos += read;
						boolean extra_iter = false;
						do {
							extra_iter = false;
							if (decoding == null) {
								decoding = this.createPacket(buffer);
								if (decoding == null) {
									break;
								}
							}
							decoding.setAvailable(pos);
							if (decoding.decode()) {
								decoding.handle();
								if (decoding.hasResponse()) {
									// send a response
								}
								if (decoding.getDisconnect()) {
									// break out of the main loop
									this.connecting = false;
									this.running = false;
									break;
								}
								// retain any remaining data in the buffer
								if (decoding.pos < pos) {
									System.arraycopy(buffer, decoding.pos, buffer, 0, pos - decoding.pos);
									pos = pos - decoding.pos;
									extra_iter = true;
								} else {
									pos = 0;
								}
								decoding = null;
							}
						} while (extra_iter);

					} catch (IOException ex) {
						if (this.running) {
							logger.log(Level.SEVERE, null, ex);
						} else {
							logger.log(Level.INFO, "Disconnecting client from server");
						}
						break;
					}
				}
			} catch (IOException ex) {
				logger.log(Level.SEVERE, null, ex);
			}
			try {
				this.socket.close();
			} catch (IOException e) {
				// nom nom nom
			}
		} catch (IOException ex) {
			logger.log(Level.SEVERE, null, ex);
		}
		if (this.connecting) {
			this.receiver.onConnectionFailed(DraftingServer.ERROR_UNKNOWN);
		}
		if (this.running) {
			System.out.println("DISCONNECTED. SHOULD GO BACK TO LOBBY!!!");
			NautDrafter.getBase().popMain(LanguageResource.getString("server_browser.screen_name"));
		}
	}

	@Override
	public void onPlayerJoin(Player player) {
		synchronized (lock) {
			if (player != null) {
				this.players.put(player, player);
				this.unassigned.add(player);
				System.out.println("PLAYER JOINED: " + player.name);
				if (this.playerConnectionCallback != null) {
					this.playerConnectionCallback.onPlayerJoin(player);
				}
			}
		}
	}

	@Override
	public void onPlayerLeave(Player player) {
		synchronized (lock) {
			if (player != null) {
				this.players.remove(player);
				this.removePlayer(player);
				System.out.println("PLAYER LEFT: " + player.name);
				if (this.playerConnectionCallback != null) {
					this.playerConnectionCallback.onPlayerLeave(player);
				}
			}
		}
	}

	@Override
	public Player getPlayer(PlayerIdentifier pid) {
		return this.players.get(pid);
	}

	@Override
	public void onConnected(byte stage,
	        byte mapId,
                boolean isPickBan,
	        boolean banBidirectional,
                boolean isSequentialDraft,
	        Team teamRed,
	        Team teamBlue,
	        Set<Player> unassigned,
	        Timeline timeline,
	        byte currentPhaseTime) {
		synchronized (lock) {

			this.connecting = false;

			this.stage = stage;
			System.out.println("On stage " + this.stage);

			this.mapId = mapId;
			System.out.println("On map " + this.mapId);

			this.timeline = timeline;
			this.currentPhaseTime = currentPhaseTime;
			this.offsetPhaseTime = this.stage == DraftingServer.STAGE_DRAFTING;
			if (this.offsetPhaseTime) {
				this.phaseTimeOffset = System.currentTimeMillis();
			}

                        this.isPickBan         = isPickBan;
			this.banBidirectional  = banBidirectional;
                        this.isSequentialDraft = isSequentialDraft;
			System.out.println("Picks are " + (isPickBan ? "ban" : "no_ban"));
			System.out.println("Bans are " + (banBidirectional ? "bi" : "uni") + "directional");

			this.teamRed.name = teamRed.name;
			this.teamRed.lockedIn = teamRed.lockedIn;
			System.out.println(this.teamRed.name + ":");
			for (int i = 0; i < 3; i++) {
				Player player = teamRed.players[i];
				this.teamRed.players[i] = player;
				this.teamRed.nautSelections[i] = teamRed.nautSelections[i];
				if (player != null) {
					System.out.println("\t" + player.name);
					this.players.put(player, player);
				}
				if (teamRed.bannedNauts[i] != -1) {
					this.teamRed.addBan(teamRed.bannedNauts[i]);
				}
				if (teamRed.pickedNauts[i] != -1) {
					this.teamRed.addPick(teamRed.pickedNauts[i]);
				}
			}

			this.teamBlue.name = teamBlue.name;
			this.teamBlue.lockedIn = teamBlue.lockedIn;
			System.out.println(this.teamBlue.name + ":");
			for (int i = 0; i < 3; i++) {
				Player player = teamBlue.players[i];
				this.teamBlue.players[i] = player;
				this.teamBlue.nautSelections[i] = teamBlue.nautSelections[i];
				if (player != null) {
					System.out.println("\t" + player.name);
					this.players.put(player, player);
				}
				if (teamBlue.bannedNauts[i] != -1) {
					this.teamBlue.addBan(teamBlue.bannedNauts[i]);
				}
				if (teamBlue.pickedNauts[i] != -1) {
					this.teamBlue.addPick(teamBlue.pickedNauts[i]);
				}
			}

			this.unassigned.addAll(unassigned);
			System.out.println("Unassigned:");
			for (Player player : this.unassigned) {
				System.out.println("\t" + player.name);
				this.players.put(player, player);
			}

			this.receiver.onConnectionSucceeded(stage);
		}
	}

	private void removePlayer(Player player) {
		Team.removePlayer(player, this.teamRed, this.teamBlue);
		this.unassigned.remove(player);
	}

	@Override
	public void onChangedTeam(Player player, int teamId, int position) {
		synchronized (lock) {
			// remove the player from their old position
			this.removePlayer(player);
			switch (teamId) {
			case Team.RED:
				this.teamRed.players[position] = player;
				break;
			case Team.BLUE:
				this.teamBlue.players[position] = player;
			case Team.NONE:
				this.unassigned.add(player);
			}
			if (this.teamCallback != null) {
				this.teamCallback.onChangedTeam(player, teamId, position);
			}
		}
	}

	@Override
	public void onCaptainSwap(int teamId, Player newCaptain, Player oldCaptain, int oldCaptainNewPosition) {
		synchronized (lock) {
			// remove the newCaptain from their old position
			// old captain is being replaced so we don't need to remove them
			this.removePlayer(newCaptain);
			switch (teamId) {
			case Team.RED:
				this.teamRed.players[0] = newCaptain;
				this.teamRed.players[oldCaptainNewPosition] = oldCaptain;
				break;
			case Team.BLUE:
				this.teamBlue.players[0] = newCaptain;
				this.teamBlue.players[oldCaptainNewPosition] = oldCaptain;
			}
			if (this.teamCallback != null) {
				this.teamCallback.onCaptainSwap(teamId, newCaptain, oldCaptain, oldCaptainNewPosition);
			}
		}
	}

	@Override
	public void onChangedTeamName(int teamId, String name) {
		synchronized (lock) {
			switch (teamId) {
			case Team.RED:
				this.teamRed.name = name;
				break;
			case Team.BLUE:
				this.teamBlue.name = name;
			}
			if (this.teamCallback != null) {
				this.teamCallback.onChangedTeamName(teamId, name);
			}
		}
	}

	@Override
	public void onLockTeam(int teamId) {
		synchronized (lock) {
			switch (this.stage) {
			case DraftingServer.STAGE_TEAM_SELECTION:
				System.out.println("Locking Team " + teamId);
				switch (teamId) {
				case Team.RED:
					this.teamRed.lockedIn = true;
					break;
				case Team.BLUE:
					this.teamBlue.lockedIn = true;
					break;
				}
				break;
			case DraftingServer.STAGE_NAUT_SELECTION:
				System.out.println("Locking Team " + teamId);
				switch (teamId) {
				case Team.RED:
					this.teamRed.finished = true;
					break;
				case Team.BLUE:
					this.teamBlue.finished = true;
					break;
				}
				break;
			}
			if (this.teamCallback != null) {
				this.teamCallback.onLockTeam(teamId);
			}
		}
	}

	@Override
	public void onStageChanged(int stage) {
		synchronized (lock) {
			this.stage = (byte) stage;
			System.out.println("Changed to stage " + stage);
			if (this.stageChangeCallback != null) {
				this.stageChangeCallback.onStageChanged(stage);
			}
		}
	}

	@Override
	public void onCaptainChooseNaut(byte nautId, byte bonusTimeRemaining) {
		synchronized (lock) {
			TimelineItem step = this.timeline.items.get(this.timeline.pos++);
			Team team;
			switch (step.team) {
			case Team.RED:
				team = this.teamRed;
				break;
			case Team.BLUE:
				team = this.teamBlue;
				break;
			default:
				return;
			}
			this.timeline.setBonus(bonusTimeRemaining);
			this.currentPhaseTime = Timeline.PHASE_LENGTH;
			this.phaseTimeOffset = System.currentTimeMillis();
			switch (step.type) {
			case Timeline.PICK:
				team.addPick(nautId);
				break;
			case Timeline.BAN:
				team.addBan(nautId);
				break;
			}
			if (this.draftingCallback != null) {
				this.draftingCallback.onCaptainChooseNaut(nautId, bonusTimeRemaining);
			}
		}
	}

	@Override
	public void onCaptainSelectNaut(byte nautId) {
		if (this.draftingCallback != null) {
			this.draftingCallback.onCaptainSelectNaut(nautId);
		}
	}

	@Override
	public void onPlayerChoose(Player player, byte teamId, byte position) {
		Team team = teamId == Team.RED ? this.teamRed : this.teamBlue;
		team.selectNaut(player, position);
		if (this.nautSelectionCallback != null) {
			this.nautSelectionCallback.onPlayerChoose(player, teamId, position);
		}
	}

	@Override
	public void onVersionReceived(int major, int minor, int patch) {
		String version = major + "." + minor + "." + patch;
		if (Constants.VERSION_MAJOR < major || (Constants.VERSION_MAJOR == major && Constants.VERSION_MINOR < minor)) {
			System.out.println("Our version is too old: " + Constants.VERSION + " < " + version);
			this.receiver.onConnectionFailed(DraftingServer.ERROR_CLIENT_OUTDATED);
			this.connecting = false;
			this.running = false;
		} else if (Constants.VERSION_MAJOR > major
		        || (Constants.VERSION_MAJOR == major && Constants.VERSION_MINOR > minor)) {
			System.out.println("Server version is too old: " + Constants.VERSION + " > " + version);
			this.receiver.onConnectionFailed(DraftingServer.ERROR_SERVER_OUTDATED);
			this.connecting = false;
			this.running = false;
		} else {
			System.out.println("Versions compatible: " + Constants.VERSION + " ~ " + version);
			if (Constants.VERSION_PATCH < patch) {
				System.out.println("There is a new patch out!");
			}
		}
	}

	/**
	 * Attempts to connect to a Drafting Server<br>
	 * Automatically disconnects from any other server before trying to connect
	 *
	 * @param addr
	 *            Address of the Drafting Server
	 * @param port
	 *            Port of the Drafting Server
	 * @param playerDetails
	 *            Our player details (or null for spectator)
	 * @param receiver
	 *            Callback to be called when the connection succeeds or fails
	 * @param versionCallback
	 *            Callback to be called with the server version
	 * @param password
	 *            Password to use for authentication
	 */
	public static void connectTo(InetAddress addr,
	        int port,
	        Player playerDetails,
	        ConnectionCallback receiver,
	        String password) {
		DraftingClient.disconnect();
		_instance = new DraftingClient(addr, port, playerDetails, receiver, password);
		_instance.start();
	}

	/**
	 * Disconnects from a Drafting Server (if we are connected to one)
	 */
	public static void disconnect() {
		try {
			DraftingClient i = _instance;
			if (i != null) {
				i.running = false;
				i.connecting = false;
				Socket s = i.socket;
				if (s != null) {
					s.close();
				}
				i.interrupt();
				i.join();
				if (_instance == i) {
					_instance = null;
				}
			}
		} catch (IOException | InterruptedException ex) {
			logger.log(Level.WARNING, null, ex);
		}
	}

	/**
	 * Checks if we are a player or not<br>
	 * Required to ensure the UI doesn't get us booted from the Drafting Server when it makes requests it shouldn't
	 *
	 * @return
	 */
	public static boolean isPlayer() {
		return _instance != null && _instance.playerDetails != null;
	}

	private static void send(byte[] data) {
		if (_instance != null && _instance.socket != null) {
			try {
				_instance.socket.getOutputStream().write(data);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Sends a request to change team and/or position on the team
	 *
	 * @param team
	 *            The new team we want
	 * @param position
	 *            The new position we want
	 */
	public static void changeTeam(int team, int position) {
		try {
			if (DraftingClient.isPlayer()) {
				DraftingClient.send(new TeamChangeRequestPacket(team, position).data);
			}
		} catch (PacketEncoderException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sends a request to change team name
	 *
	 * @param team
	 *            The team we want to affect
	 * @param name
	 *            The new name for the team
	 */
	public static void changeTeamName(int team, String name) {
		try {
			if (DraftingClient.isPlayer()) {
				DraftingClient.send(new TeamNameChangePacket(team, name).data);
			}
		} catch (PacketEncoderException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sends a chat message
	 *
	 * @param message
	 *            The message
	 */
	public static void sendChatMessage(String message) {
		try {
			if (DraftingClient.isPlayer()) {
				DraftingClient.send(new ChatRequestPacket(message).data);
			}
		} catch (PacketEncoderException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sends a request to lock the team (valid once the team is full and the captain clicks ready)
	 */
	public static void lockTeam() {
		if (DraftingClient.isPlayer()) {
			DraftingClient.send(new LockTeamPacket().data);
		}
	}

	/**
	 * Chooses a naut (pick/ban)<br>
	 * The server will check whether this was valid, and if so it will advance the timeline
	 *
	 * @param nautId
	 *            The naut that was chosen
	 */
	public static void captainChooseNaut(byte nautId) {
		if (DraftingClient.isPlayer()) {
			DraftingClient.send(new CaptainChooseNautPacket(nautId).data);
		}
	}

	/**
	 * Selects a naut<br>
	 * The selection is not locked in, this just lets the team members see what their captain has selected
	 *
	 * @param nautId
	 *            The naut that was selected
	 */
	public static void captainSelectNaut(byte nautId) {
		if (DraftingClient.isPlayer()) {
			DraftingClient.send(new CaptainSelectNautPacket(nautId).data);
		}
	}

	/**
	 * Requests to select a naut in the naut selection screen
	 *
	 * @param position
	 *            position of the naut to select
	 */
	public static void selectNaut(byte position) {
		if (DraftingClient.isPlayer()) {
			DraftingClient.send(new NautChooseRequestPacket(position).data);
		}
	}

	/**
	 * @return The red team, or null if we are not connected to a server
	 */
	public static Team getRedTeam() {
		return (_instance != null ? _instance.teamRed : null);
	}

	/**
	 * @return The blue team, or null if we are not connected to a server
	 */
	public static Team getBlueTeam() {
		return (_instance != null ? _instance.teamBlue : null);
	}

	/**
	 * @return The unassigned players, or null if we are not connected to a server
	 */
	public static Set<Player> getUnassignedPlayers() {
		return (_instance != null ? _instance.unassigned : null);
	}

	/**
	 * @return The id of the map this server will play on, or 0 if unknown
	 */
	public static byte getMapId() {
		return (_instance != null ? _instance.mapId : 0);
	}

	/**
	 * Sends callbacks to setup the state of a screen<br>
	 * Must set the callbacks before using this method
	 */
	public static void requestInitCallbacks() {
		if (_instance != null) {
			synchronized (lock) {
				if ((_instance.stage == DraftingServer.STAGE_DRAFTING || _instance.stage == DraftingServer.STAGE_NAUT_SELECTION)
				        && _instance.draftingCallback != null && _instance.timeline != null) {
					byte phaseTime = _instance.currentPhaseTime;
					if (_instance.offsetPhaseTime) {
    					phaseTime -= ((System.currentTimeMillis() - _instance.phaseTimeOffset) / 1000);
					}
					_instance.draftingCallback.onTimelineReceived(_instance.timeline, (phaseTime >= 0 ? phaseTime : 0));
				}

				if (_instance.teamCallback != null) {
					_instance.teamCallback.onChangedTeamName(Team.RED, _instance.teamRed.name);
					_instance.teamCallback.onChangedTeamName(Team.BLUE, _instance.teamBlue.name);
					if (_instance.teamRed.lockedIn) {
						_instance.teamCallback.onLockTeam(Team.RED);
					}
					if (_instance.teamBlue.lockedIn) {
						_instance.teamCallback.onLockTeam(Team.BLUE);
					}
				}

				if (_instance.playerConnectionCallback != null || _instance.teamCallback != null) {
					for (int i = 0; i < 3; i++) {
						Player player = _instance.teamRed.players[i];
						if (player != null) {
							if (_instance.playerConnectionCallback != null) {
								_instance.playerConnectionCallback.onPlayerJoin(player);
							}
							if (_instance.teamCallback != null) {
								_instance.teamCallback.onChangedTeam(player, Team.RED, i);
							}
						}
						player = _instance.teamBlue.players[i];
						if (player != null) {
							if (_instance.playerConnectionCallback != null) {
								_instance.playerConnectionCallback.onPlayerJoin(player);
							}
							if (_instance.teamCallback != null) {
								_instance.teamCallback.onChangedTeam(player, Team.BLUE, i);
							}
						}
					}
				}

				if (_instance.playerConnectionCallback != null) {
					for (Player player : _instance.unassigned) {
						_instance.playerConnectionCallback.onPlayerJoin(player);
					}
				}
			}
		}
	}

	/**
	 * Removes any callbacks
	 */
	public static void clearCallbacks() {
		if (_instance != null) {
			_instance.teamCallback = null;
			_instance.chatCallback = null;
			_instance.playerConnectionCallback = null;
			_instance.stageChangeCallback = null;
			_instance.draftingCallback = null;
		}
	}

	/**
	 * Adds callbacks
	 *
	 * @param callback
	 *            an Object that implements any number of the callbacks known by DraftingClient
	 */
	public static void addCallbacks(Object callback) {
		if (_instance != null) {
			if (callback instanceof TeamCallback) {
				_instance.teamCallback = (TeamCallback) callback;
			}
			if (callback instanceof ChatCallback) {
				_instance.chatCallback = (ChatCallback) callback;
			}
			if (callback instanceof PlayerConnectionCallback) {
				_instance.playerConnectionCallback = (PlayerConnectionCallback) callback;
			}
			if (callback instanceof StageChangeCallback) {
				_instance.stageChangeCallback = (StageChangeCallback) callback;
			}
			if (callback instanceof ClientDraftingCallback) {
				_instance.draftingCallback = (ClientDraftingCallback) callback;
			}
			if (callback instanceof NautChooseCallback) {
				_instance.nautSelectionCallback = (NautChooseCallback) callback;
			}
		}
	}

	/**
	 * Clears the existing callbacks, then tries to set the callbacks
	 *
	 * @param callback
	 *            an Object that implements any number of the callbacks known by DraftingClient
	 */
	public static void setCallbacks(Object callback) {
		DraftingClient.clearCallbacks();
		DraftingClient.addCallbacks(callback);
	}

	/**
	 * @return The {@link Timeline} for the current draft
	 */
	public static Timeline getTimeline() {
		return _instance.timeline;
	}


        /**
         * @return true if the pick means a ban for the opponent team.
         */
        public static boolean isPickBan(){
            return _instance == null ? false : _instance.isPickBan;
        }

	/**
	 * @return returns true if the bans are to be for both teams.
	 */
	public static boolean isBanBidirectional() {
		return _instance == null ? false : _instance.banBidirectional;
	}

	/**
	 * @return returns true if the bans are to be for both teams
	 */
	public static boolean isSequentialDraft() {
		return _instance == null ? false : _instance.isSequentialDraft;
	}
}
