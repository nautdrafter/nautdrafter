/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Platform;

import com.monkygames.nautdrafter.Constants;
import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.controller.Browser;
import com.monkygames.nautdrafter.network.callbacks.DedicatedServerResponseCallback;
import com.monkygames.nautdrafter.network.callbacks.LoginDetailsCallback;
import com.monkygames.nautdrafter.network.callbacks.ServerBrowserCallback;
import com.monkygames.nautdrafter.network.callbacks.VersionCallback;
import com.monkygames.nautdrafter.network.packets.ClientIdPacket;
import com.monkygames.nautdrafter.network.packets.DedicatedServerRequestPacket;
import com.monkygames.nautdrafter.network.packets.DedicatedServerResponsePacket;
import com.monkygames.nautdrafter.network.packets.Packet;
import com.monkygames.nautdrafter.network.packets.ServerListPacket;
import com.monkygames.nautdrafter.network.packets.UserDetailsPacket;
import com.monkygames.nautdrafter.network.packets.VersionPacket;

/**
 * Provides methods for communicating with the Lobby Server <br>
 */
public class LobbyClient{

	private static final Logger logger = Logger.getLogger(LobbyClient.class.getName());
        /**
         * The string used to identify the key for the client's ID.
         */
        public static final String CLIENT_ID = "clientID";

	private LobbyClient() {

	}

	private static Thread  serverListThread = null;
	private static Socket  serverListSocket = null;
        private static Thread  createServerThread = null;
        private static Socket  createServerSocket = null;
	private static boolean isKilling;
	private static boolean isRestarting;

	/**
	 * Asks the Lobby Server to give us the current list of Drafting Servers
	 *
	 * @param receiver
	 */
	public static final void getServerList(final ServerBrowserCallback receiver) {
		LobbyClient.abortGetServerList(true);
		serverListThread = new Thread(() -> {
			try (Socket s = new Socket()) {
				serverListSocket = s;
				s.connect(new InetSocketAddress(InetAddress.getByName(Constants.LOBBY_ADDRESS), Constants.LOBBY_PORT));
				// send request
				s.getOutputStream().write(Packet.SERVER_LIST_REQUEST_PACKET_ID);
				// get response
				byte[] buf = new byte[10000];
				int pos = 0;
				do {
					int read = s.getInputStream().read(buf, 0, 10000 - pos);
					if (read == -1) {
						throw new IOException("Read failed");
					}
					pos += read;
				} while (pos == 0);
				if (buf[0] != Packet.SERVER_LIST_PACKET_ID) {
					throw new IOException("Invalid packet ID");
				}
				ServerListPacket packet = new ServerListPacket(buf);
				while (!packet.decode()) {
					int read = s.getInputStream().read(buf, 0, 10000 - pos);
					if (read == -1) {
						throw new IOException("Read failed");
					}
					pos += read;
				}
				// handle
				packet.handle();
				receiver.addServers(packet.getServers());
			} catch (IOException ex) {
				if (!isRestarting) {
					receiver.addServers(null);
				}
				if (!isKilling) {
					logger.log(Level.SEVERE, null, ex);
				}
			}
			serverListSocket = null;
			serverListThread = null;
		});
		serverListThread.start();
	}

    /**
     * Asks the Lobby Server to create a dedicated Drafting Server.
     *
     * @param receiver handles the response from the request.
     * @param serverInfo contains the information to ask the lobby server to create a dedicated server.
     * @param player the player that is making the request.
     */
    public static final void createDedicatedServer(
        final DedicatedServerResponseCallback receiver, 
        CreateServerInfo serverInfo,
        Player player
    ) {
        createServerThread = new Thread(() -> {
            try (Socket s = new Socket()) {
                createServerSocket = s;
                s.connect(new InetSocketAddress(InetAddress.getByName(Constants.LOBBY_ADDRESS), Constants.LOBBY_PORT));
                DedicatedServerRequestPacket packet = new DedicatedServerRequestPacket(serverInfo,player);
                s.getOutputStream().write(packet.data);
                byte[] buf = new byte[10000];
                int pos = 0;
                do {
                    int read = s.getInputStream().read(buf, 0, 10000 - pos);
                    if (read == -1) {
                        throw new IOException("Read failed");
                    }
                    pos += read;
                } while (pos == 0);
                if (buf[0] != Packet.DEDICATED_SERVER_RESPONSE_PACKET_ID) {
                    throw new IOException("Invalid packet ID");
                }
                DedicatedServerResponsePacket response = new DedicatedServerResponsePacket(buf);
                while (!packet.decode()) {
                    int read = s.getInputStream().read(buf, 0, 10000 - pos);
                    if (read == -1) {
                        throw new IOException("Read failed");
                    }
                    pos += read;
                }
                response.decode();
                receiver.onDedicatedServerResponseReceived(response.success);
            } catch (IOException ex) { }
            createServerSocket = null;
            createServerThread = null;
        });
        createServerThread.start();
    }

	/**
	 * Aborts the attempt to get the server list if the thread is still running
	 * 
	 * @param restarting
	 *            set to true if we a re restarting a search, false if just aborting
	 */
	public static void abortGetServerList(boolean restarting) {
		try {
			isKilling = true;
			isRestarting = restarting;
			Socket s = serverListSocket;
			if (s != null) {
				s.close();
			}
			Thread t = serverListThread;
			if (t != null) {
				t.interrupt();
				t.join();
			}
			isRestarting = false;
			isKilling = false;
		} catch (InterruptedException | IOException e) {
			// om nom nom
		}
	}

	private static final String STEAM_AUTH_URL = "https://steamcommunity.com/openid/login/?openid.ns=http://specs.openid.net/auth/2.0&openid.mode=checkid_setup&openid.realm=http://"
	                                                   + Constants.LOBBY_ADDRESS
	                                                   + "&openid.identity=http://specs.openid.net/auth/2.0/identifier_select&openid.claimed_id=http://specs.openid.net/auth/2.0/identifier_select&openid.return_to=http://"
	                                                   + Constants.LOBBY_ADDRESS + ":" + Constants.LOBBY_PORT + "/";

	private static Thread       loginThread    = null;
	private static Socket       loginSocket    = null;
	private static boolean      loginAborting  = false;

	/**
	 * Aborts a login attempt, killing the thread that was attempting to login<br>
	 * Will be called automatically before every attempt to login (to ensure we don't start breeding Threads)
	 */
	public static final void abortLogin() {
		try {
			loginAborting = true;
			Socket s = loginSocket;
			if (s != null) {
				s.close();
			}
			Thread t = loginThread;
			if (t != null) {
				t.interrupt();
				t.join();
			}
		} catch (InterruptedException | IOException e) {
			// om nom nom
		}
	}

	/**
	 * Asks the Lobby Server to log us in with Steam<br>
	 * Automatically aborts any outstanding login attempts before trying to login
	 *
	 * @param reciever
	 */
	public static final void login(final LoginDetailsCallback reciever) {
		if (NautDrafter.fakePlayer != null) {
			reciever.onLoginDetailsReturn(NautDrafter.fakePlayer);
		} else {
			LobbyClient.abortLogin();
			loginAborting = false;
			loginThread = new Thread(
			        () -> {
				        try (Socket s = new Socket()) {
					        loginSocket = s;
					        s.connect(new InetSocketAddress(InetAddress.getByName(Constants.LOBBY_ADDRESS),
					                Constants.LOBBY_PORT));
					        // test for config
					        String clientID = NautDrafter.getPreferences().get(CLIENT_ID, null);
					        if (clientID == null) {
						        s.getOutputStream().write(Packet.NEWAUTH_PACKET_ID);
					        } else {
						        s.getOutputStream().write((new ClientIdPacket(clientID)).data);
					        }
					        // get response
					        byte[] buf = new byte[10000];
					        int pos = 0;
					        do {
						        int read = s.getInputStream().read(buf, 0, 10000 - pos);
						        if (read == -1) {
							        throw new IOException("Read failed");
						        }
						        pos += read;
					        } while (pos == 0);
					        // deal with the ClientID packet
					        if (buf[0] == Packet.CLIENTID_PACKET_ID) {
						        // can safely give it a null callback as long as we don't try to decode it
						        ClientIdPacket packet = new ClientIdPacket(buf, null);
						        while (!packet.decode()) {
							        int read = s.getInputStream().read(buf, 0, 10000 - pos);
							        if (read == -1) {
								        throw new IOException("Read failed");
							        }
							        pos += read;
						        }
						        clientID = packet.getClientId();
						        
						        String url = LobbyClient.STEAM_AUTH_URL + packet.getClientId();

						        if (!NautDrafter.getPreferences().getBoolean("useExternalBrowser", false)) {
							        // Open the Steam login page in an overlay
							        Platform.runLater(() -> {
								        Browser b = new Browser(() -> {
									        LobbyClient.abortLogin();
									        NautDrafter.getBase().stopLoadingOverlay();
								        }, "steamcommunity.com", "www.steamcommunity.com",
								                "server.nautdrafter.monky-games.com", "nautdrafter.bitbucket.org");

								        // Faking the user agent in order to get the mobile Steam site
								        if (NautDrafter.getBase().getWidth() < 960) {
									        b.setUserAgent("Android: Mozilla/5.0 (Linux; Android 4.4.2; Nexus 4 Build/KOT49H) AppleWebKit/537.44 (KHTML, like Gecko) JavaFX/8.0 Mobile Safari/537.44");
								        }

								        b.load(url);

								        NautDrafter.getBase().startOverlay(b);
							        });
						        } else {
							        NautDrafter.getInstance().getHostServices().showDocument(url);
						        }

						        pos = 0;
						        do {
							        int read = s.getInputStream().read(buf, 0, 10000 - pos);
							        if (read == -1) {
								        throw new IOException("Read failed");
							        }
							        pos += read;
						        } while (pos == 0);
					        }
					        // And now deal with the User Details packet
					        if (buf[0] == Packet.USERDETAILS_PACKET_ID) {
						        UserDetailsPacket packet = new UserDetailsPacket(buf);
						        while (!packet.decode()) {
							        int read = s.getInputStream().read(buf, 0, 10000 - pos);
							        if (read == -1) {
								        throw new IOException("Read failed");
							        }
							        pos += read;
						        }
						        NautDrafter.getPreferences().put(CLIENT_ID, clientID);
						        reciever.onLoginDetailsReturn(packet.getPlayer());
					        } else {
						        throw new IOException("FAIL");
					        }
				        } catch (IOException | IllegalArgumentException ex) {
					        if (!loginAborting) {
						        reciever.onLoginDetailsReturn(null);
						        logger.log(Level.SEVERE, null, ex);
					        }
				        } catch (ExceptionInInitializerError e) {
					        e.printStackTrace();
					        System.exit(0);
				        }
				        loginSocket = null;
				        loginThread = null;
			        });
			loginThread.start();
		}
	}

	private static Thread  versionThread   = null;
	private static Socket  versionSocket   = null;
	private static boolean versionAborting = false;

	/**
	 * Aborts a version check attempt, killing the thread that was attempting to get the lobby version<br>
	 * Will be called automatically before every attempt to check version (to ensure we don't start breeding Threads)
	 */
	public static final void abortVersion() {
		try {
			versionAborting = true;
			Socket s = versionSocket;
			if (s != null) {
				s.close();
			}
			Thread t = versionThread;
			if (t != null) {
				t.interrupt();
				t.join();
			}
		} catch (InterruptedException | IOException e) {
			// om nom nom
		}
	}

	/**
	 * Asks the lobby server for its version number<br>
	 * If the major version is different, the versions are incompatible
	 * 
	 * @param receiver
	 *            The callback to call with the lobby's version numbe
	 */
	public static final void checkVersion(final VersionCallback receiver) {
		LobbyClient.abortVersion();
		versionAborting = false;
		(versionThread = new Thread(() -> {
			try (Socket s = new Socket()) {
				versionSocket = s;
				s.connect(new InetSocketAddress(InetAddress.getByName(Constants.LOBBY_ADDRESS), Constants.LOBBY_PORT));
				// send request
				s.getOutputStream().write(Constants.VERSION_PACKET);
				// get response
				byte[] buf = new byte[10000];
				int pos = 0;
				do {
					int read = s.getInputStream().read(buf, 0, 10000 - pos);
					if (read == -1) {
						throw new IOException("Read failed");
					}
					pos += read;
				} while (pos == 0);
				if (buf[0] != Packet.VERSION_PACKET_ID) {
					throw new IOException("Invalid packet ID");
				}
				VersionPacket packet = new VersionPacket(buf, receiver);
				while (!packet.decode()) {
					int read = s.getInputStream().read(buf, 0, 10000 - pos);
					if (read == -1) {
						throw new IOException("Read failed");
					}
					pos += read;
				}
				// handle
				packet.handle();
			} catch (IOException ex) {
				if (!versionAborting) {
					ex.printStackTrace();
				}
			}
			versionSocket = null;
		})).start();
	}

}
