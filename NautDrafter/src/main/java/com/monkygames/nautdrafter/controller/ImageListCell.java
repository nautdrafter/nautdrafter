/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.IOException;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.effect.Glow;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

/**
 * A simple node structure that contains a text label on top of a background image. Intended to be used as the graphic
 * element of a {@link ListCell}. The background image has a glow effect that pulses when the node is hovered over.
 */
public class ImageListCell extends StackPane {
	@FXML
	private Label     text;
	@FXML
	private StackPane stack;
	@FXML
	private Region    image;

	private Glow      glowEffect;
	private Timeline  pulse;

	/**
	 * Creates a new image list cell with no image or text set.
	 */
	public ImageListCell() {
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
		        "/com/monkygames/nautdrafter/view/ImageListCellInner.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		this.glowEffect = new Glow(0.5);
		this.pulse = new Timeline();

		this.pulse.getKeyFrames().addAll(
		        new KeyFrame(Duration.ZERO, new KeyValue(this.glowEffect.levelProperty(), 0.4)),
		        new KeyFrame(new Duration(1500), new KeyValue(this.glowEffect.levelProperty(), 0.8)));

		this.pulse.setCycleCount(Animation.INDEFINITE);
		this.pulse.setAutoReverse(true);

		this.addEventHandler(MouseEvent.MOUSE_ENTERED, event -> {
			this.image.setEffect(this.glowEffect);
			this.pulse.play();
		});

		this.addEventHandler(MouseEvent.MOUSE_EXITED, event -> {
			this.image.setEffect(null);
			this.pulse.stop();
		});
	}

	/**
	 * Sets an image as the background of the node. (Note: this does not actually set the background property of the
	 * calling object, but rather sets the background property of a child node of the object)
	 *
	 * @param img
	 *            Image to be displayed as the background of the cell
	 */
	public void setImage(Image img) {
		this.image.setBackground(new Background(new BackgroundImage(img, BackgroundRepeat.NO_REPEAT,
		        BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(BackgroundSize.AUTO,
		                BackgroundSize.AUTO, false, false, false, true))));
	}

	/**
	 * Sets the text that is displayed on top of the image.
	 *
	 * @param string
	 *            Text to display in the cell
	 */
	public void setText(String text) {
		this.text.setText(text);
	}
}
