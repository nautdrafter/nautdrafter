/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.IOException;
import java.util.ArrayList;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.model.Character;
import com.monkygames.nautdrafter.model.Character.IconType;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.network.Team;
import com.monkygames.nautdrafter.network.Timeline;
import com.monkygames.nautdrafter.network.Timeline.TimelineItem;

/**
 * UI element that shows the currently selected Nauts information on the players drafting screen <br/>
 * Also includes the pick/ban button that enables/disables for the captains when it is there turn
 */
public class CharacterDisplay extends HBox {

	@FXML
	private ImageView       icon;
	@FXML
	private Label           name;
	@FXML
	private Label           health;
	@FXML
	private Label           attack;
	@FXML
	private Label           speed;
	@FXML
	private Label           role;
	@FXML
	private Button          actionButton;
	private final String[]  buttonText;
	private String[]        scheduledText;
	private int             index;
	private PickBanListener listener;
	private int             myTeam;
	private int[]           scheduledTeam;
	private boolean         isCaptain;
        private boolean         isSequentialDraft;
	{
		this.buttonText = new String[2];
		this.buttonText[0] = LanguageResource.getString("pick");
		this.buttonText[1] = LanguageResource.getString("ban");
	}

	/**
	 * Create the element and initialize the UI
	 */
	public CharacterDisplay() {
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
		        "/com/monkygames/nautdrafter/view/CharacterDisplay.fxml"), LanguageResource.getLanguageResource());
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		// Set up pick/ban button
		this.actionButton.setOnAction(event -> {
			Character c;
			if ((c = this.getCharacter()) == Character.UNKNOWN) {
				return;
			}
			if (this.listener != null) {
				this.listener.onPickBan(c);
			}
		});
		this.updateUI();
	}

	/**
	 * Set the selection schedule so we don't have to manually update the UI on each selection
	 *
	 * @param schedule
	 *            The schedule to follow. Use {@link #updateSchedule()} to set the UI to the next selection
	 */
	public void schedule(ArrayList<TimelineItem> schedule, int currentPos) {
		this.scheduledText = new String[schedule.size() + 1];
		this.scheduledTeam = new int[this.scheduledText.length];
		for (int i = 0; i < this.scheduledText.length - 1; i++) {
			TimelineItem item = schedule.get(i);
			this.scheduledText[i] = this.buttonText[item.type];
			this.scheduledTeam[i] = item.team;
		}
		this.scheduledText[this.scheduledText.length - 1] = "Finished";
		this.scheduledTeam[this.scheduledText.length - 1] = 0;

		this.index = currentPos;
		this.updateSchedule();
		this.index = currentPos;
	}

	/**
	 * Set the prompt text on the pick/ban button according to the type of action defined in {@link Timeline}
	 *
	 * @param type
	 *            PICK or BAN as defined in {@link Timeline}. If any other int is supplied, the button will be disabled
	 *            and show "Finished" (Useful to show when no more selections ready)
	 */
	public void setButtonText(int type) {
		if (type != 0 || type != 1) {
			this.actionButton.setDisable(true);
			this.actionButton.setText("Finished");
		} else {
			this.actionButton.setDisable(false);
			this.actionButton.setText(this.buttonText[type]);
		}

	}

	/**
	 * Set whether the player can interact with the pick/ban button<br/>
	 * the button will only be enabled if this player is the captain of a team and its their turn to pick/ban
	 *
	 * @param isCaptain
	 *            true if the player is the captain of any team
	 * @param team
	 *            Constants defined in {@link Team} and {@link Timeline}
	 */
	public void setCaptainMode(boolean isCaptain, int team) {
		this.myTeam = team;
		this.isCaptain = isCaptain;
	}

        /**
         * Sets if this is sequential draft mode.
         * @param isSequentialDraft true if sequential draft and false for concurrent.
         */
        public void setDraftMode(boolean isSequentialDraft){
            this.isSequentialDraft = isSequentialDraft;
        }

	/**
	 * Check if this displayer is in captain mode
	 *
	 * @return true if the local player is the captain of any team
	 */
	public boolean isCaptain() {
		return this.isCaptain;
	}

	/**
	 * Update the buttons schedule when a selection occurs. This will show the next action and disable/enable depending
	 * on whether it has been set to captain mode or not and if it is this captains turn<br/>
	 * Will do nothing if a schedule was not defined
	 */
	public void updateSchedule() {
            this.index++;
            if (this.scheduledText != null && this.index < this.scheduledText.length) {
                this.actionButton.setText(this.scheduledText[this.index]);
                updateActionButton();
            }
	}

	/**
	 * Register the listener for the Pick/Ban action
	 *
	 * @param listener
	 *            The listener to register
	 */
	public void setPickBanListener(PickBanListener listener) {
		this.listener = listener;
	}

	/**
	 * The character has been change and we should display the new information
	 */
	private void updateUI() {

		Character naut = this.getCharacter();

		// Disable the button if no character is selected or if this is not the captain and the correct team
                updateActionButton();

		// Set the display data
		this.icon.setImage(naut.getImage(IconType.RENDER));
		this.name.setText(naut.name);
		this.health.setText(naut.getDisplayHealth());
		this.speed.setText(naut.movementSpeed + "");
		this.attack.setText(naut.attack.displayName);
		String roles = "";
		int i = 0;
		for (; i < naut.roles.length - 1; i++) {
			roles += naut.roles[i] + ", ";
		}
		roles += naut.roles[i];
		this.role.setText(roles);
		this.requestLayout();
	}

	/**
	 * The {@link Character} the displayer is currently displaying.<br/>
	 * If not set specifically, it will be Character.UNKNOWN
	 */
	public final ObjectProperty<Character> characterProperty() {
		if (this.character == null) {
			this.character = new SimpleObjectProperty<Character>(Character.UNKNOWN) {
				@Override
				public void invalidated() {
					CharacterDisplay.this.updateUI();
				}

				@Override
				public Object getBean() {
					return CharacterDisplay.this;
				}

				@Override
				public String getName() {
					return "character";
				}

			};
		}
		return this.character;
	}

	private ObjectProperty<Character> character;

	/**
	 * @param value
	 *            The Character that you want to be displayed in this CharacterDisplay
	 */
	public final void setCharacter(Character value) {
		this.characterProperty().set(value);
	}

	/**
	 * @return The character that is being displayed in this CharacterDisplay
	 */
	public final Character getCharacter() {
		return this.character == null ? Character.UNKNOWN : this.character.get();
	}

	/**
	 * A listener that is called when a valid selection occurs<br/>
	 * The event that fires this listener is when the pick/ban button is clicked(button.fire() is called)
	 */
	public interface PickBanListener {
		/**
		 * Called when a valid selection occurs
		 *
		 * @param selection
		 *            The currently selected character when the button was pushed
		 */
		public void onPickBan(Character selection);
	}

        /**
         * Sets the state of the action button.
         */
    private void updateActionButton(){
        if (this.getCharacter() == Character.UNKNOWN) {
                this.actionButton.setDisable(true);
        } else {
            // if your not the captain, always disabled
            if(!this.isCaptain){
                this.actionButton.setDisable(true);
                return;
            }

            // you are the captain, is normal drafting, and its not your turn
            if(this.isSequentialDraft && (this.scheduledTeam[this.index] != this.myTeam)){
                this.actionButton.setDisable(true);
                return;
            }

            // you are the captain, its sequential drafting but it is your turn
            if(this.isSequentialDraft){
                this.actionButton.setDisable(false);
                return;
            }


            // its concurrent drafting so enable
            this.actionButton.setDisable(false);
            //this.actionButton.setDisable(!this.isCaptain || this.scheduledTeam[this.index] != this.myTeam);
        }
    }
}
