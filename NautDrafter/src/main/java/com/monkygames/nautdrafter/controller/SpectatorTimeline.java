/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.model.Planet;
import com.monkygames.nautdrafter.network.Team;
import com.monkygames.nautdrafter.network.Timeline;
import com.monkygames.nautdrafter.network.Timeline.TimelineItem;

/**
 * This is the timeline object to show in the spectators drafting screen <br/>
 * The internal {@link AnimationTimer} automatically counts down and switches to bonus time upon reaching 0 <br/>
 * TO keep the timing in sync with the server, the exact bonus time remaining is sent with each update selection<br/>
 * Most of the styling has been done via css
 */
public class SpectatorTimeline extends HBox {

	private static final long        S_to_NS       = 1000000000;
	private static final double      NS_to_S       = 1d / S_to_NS;
	protected static final double    MAX_PICK_TIME = 30 * S_to_NS;

	@FXML
	private Label                    redBonus;
	@FXML
	private Label                    blueBonus;
	@FXML
	private ImageView                endIcon;
	@FXML
	private HBox                     timeLineArea;

	private ArrayList<TimelineItem>  schedule;
	private IntegerProperty          positionIndex;
	private long                     currentTime;
	private final long[]             bonusTimes;
	private static String[]          types;
	private final Label[]            bonuses;
	private CountDownTimer           timer;

	private StringProperty           timeString    = new SimpleStringProperty("0:30");
	private DoubleProperty           ratio;
	private double                   width         = 64d;

	private ArrayList<TimelinePiece> pieces        = new ArrayList<>();

	/**
	 * Create an empty timeline. The timeline can then be filled by calling {@link SpectatorTimeline#init}
	 */
	public SpectatorTimeline() {
		this.getStyleClass().setAll("large-timeline");
		ResourceBundle res;
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
		        "/com/monkygames/nautdrafter/view/Timeline.fxml"), res = LanguageResource.getLanguageResource());
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		this.bonusTimes = new long[2];
		this.bonuses = new Label[] { this.redBonus, this.blueBonus };
		SpectatorTimeline.types = new String[] { res.getString("pick"), res.getString("ban") };
		this.timer = new CountDownTimer();
		this.positionIndex = new SimpleIntegerProperty(0);

		// Creative way to get the ratio for the clip shape on the timeline item. (Hope it works. Edit: It works)
		this.ratio = new SimpleDoubleProperty(MAX_PICK_TIME) {
			@Override
			public double get() {
				return 1 - super.get() / MAX_PICK_TIME;
			}
		};
	}

	@Override
	protected void layoutChildren() {

		// Check if the timeline needs resizing
		if (this.width != this.timeLineArea.getWidth()) {
			this.refreshTimeline();
		}
		super.layoutChildren();
	}

	/**
	 * Get the property for the index into the schedule
	 *
	 * @return the {@link IntegerProperty} that corresponds to the position in the schedul
	 */
	public IntegerProperty getPosition() {
		return this.positionIndex;
	}

	/**
	 * Initialize the UI with the current state of the drafting process
	 *
	 * @param timeline
	 *            list of TimelineItem that represent the drafting process
	 * @param pos
	 *            current position into the drafting process with 0 = start and timeline.length-1 = last position
	 * @param time
	 *            current events time counter value
	 * @param bonus
	 *            currently picking teams bonus time
	 * @param otherBonus
	 *            other teams bonus time
	 */
	public void init(ArrayList<TimelineItem> schedule, int pos, int time, int bonus, int otherBonus) {
		TimelineItem current = schedule.get(pos);
		ArrayList<TimelineItem> myList = new ArrayList<>();
		for (TimelineItem i : schedule) {
			myList.add(new TimelineItem(i.team, i.type));
		}
		this.schedule = myList;
		this.positionIndex.set(pos);
		this.currentTime = time * S_to_NS;
		int team = current.team - 1;
		this.bonusTimes[team] = bonus * S_to_NS;
		this.bonusTimes[team == 0 ? 1 : 0] = otherBonus * S_to_NS;
		this.ratio.set(this.bonusTimes[team]);
		this.bonuses[team].setText(this.minuteFormat(this.bonusTimes[team]));
		this.bonuses[team == 0 ? 1 : 0].setText(this.minuteFormat(this.bonusTimes[team == 0 ? 1 : 0]));
	}

	/**
	 * Sets the map that is displayed as being where the Awesomenauts battle will be played. This information is used 
	 * for display only and does not affect the drafting process/logic in any way. The map details are displayed in the 
	 * battle icon tooltip, at the end of the timeline.
	 * 
	 * @param map
	 *            Map that the Awesomenauts battle will be played on.
	 */
	public void setMap(Planet map) {
		Tooltip mapTooltip = new Tooltip(LanguageResource.getString("drafting.battle_info.tooltip.map")
		        + " "
		        + (map.getId() != Planet.UNKNOWN.getId() ? map.getName() : LanguageResource.getString("drafting.battle_info.tooltip.map.random")));
		mapTooltip.setGraphic(new ImageView(map.getImage(Planet.ImageType.MAP)));
		mapTooltip.setContentDisplay(ContentDisplay.TOP);
		mapTooltip.setAutoHide(false);
		
		Tooltip.install(this.endIcon, mapTooltip);
	}

	/**
	 * Notify the UI animation timer to start counting down bonus time
	 */
	public void setBonusTimeActive() {
		this.currentTime = 0; // Make sure the picking times is 0
		this.timer.isBonus = true;
	}

	/**
	 * A selection has occurred so update the ui and start the next round of selections
	 *
	 * @param remainingBonus
	 *            the remaining bonus time for the last team to select (to keep times in check), or -1 to keep the same
	 */
	public void notifyOfSelection(long remainingBonus) {
		if (Platform.isFxApplicationThread()) {
			this.update(remainingBonus);
		} else {
			Platform.runLater(() -> this.update(remainingBonus));
		}
	}

	/**
	 * A selection has occurred so update the ui and start the next round of selections
	 *
	 * @param remainingBonus
	 *            the remaining bonus time for the last team to select (to keep times in check), or -1 to keep the same
	 */
	private void update(long remainingBonus) {
		int pos;
		if ((pos = this.positionIndex.get()) >= this.schedule.size()) {
			return;
		}

		// Fill out most recent action
		TimelineItem current = this.schedule.get(pos++);
		if (remainingBonus != -1) {
			this.bonusTimes[current.team - 1] = remainingBonus * S_to_NS;
			this.updateLabel(current.team - 1);
		}
		this.positionIndex.set(pos);

		// Check for last item
		if (pos >= this.schedule.size()) {
			this.timer.stop();
			System.out.println("Finished drafting, now time to go to vs screen");
			return;
		}

		current = this.schedule.get(pos);

		this.refreshTimeline();
		this.currentTime = 30 * S_to_NS;
		this.startCountDown(current.team - 1);
	}

	/**
	 * Start the countdown timer
	 *
	 * @param teamIndex
	 *            index of the team (basically the {@link Team} constant - 1)
	 */
	private void startCountDown(int teamIndex) {
		this.timer.currentTeam = teamIndex;
		this.timer.isBonus = false;
		this.timer.start();
	}

	/**
	 * Recreates the timeline. Re-sizes and lays out all elements of the timeline area hbox.<br/>
	 * Used when laying out children of this node
	 */
	private void refreshTimeline() {
		// Do we need this layout?
		if (this.width == this.timeLineArea.getWidth()) {
			return;
		}

		// Make the pieces if none yet made
		if (this.pieces.size() == 0) {
			if (this.schedule == null) {
				return;
			}
			TimelineItem prev = null;
			for (int i = 0; i < this.schedule.size(); i++) {
				TimelineItem item = this.schedule.get(i);
				item.pos = i;
				this.pieces.add(new TimelinePiece(item, prev));
				prev = item;
			}

			this.pieces.add(new TimelinePiece());

			int old = this.positionIndex.get();
			this.positionIndex.set(-1);
			this.positionIndex.set(old);
			this.timeLineArea.getChildren().addAll(this.pieces);
			this.startCountDown(this.schedule.get(this.positionIndex.get()).team - 1);
		}

		// Resize
		double parentWidth;
		if ((parentWidth = this.timeLineArea.getWidth()) == 0) {
			System.out.println("NOT YET");
			Platform.runLater(() -> this.refreshTimeline());
		} else {
			this.width = parentWidth;
			System.out.println("NOW " + this.timeLineArea.getWidth());
			double each = parentWidth - this.pieces.size() * 10 - 10;
			each /= this.pieces.size();
			for (TimelinePiece piece : this.pieces) {
				piece.resize(each);
			}
		}
	}

	/**
	 * Formats number to minute structure<br/>
	 * Regards anything over ten minutes (l > 600) as nanoseconds and converts it to seconds
	 */
	private String minuteFormat(long l) {
		// Convert any large numbers (600 = ten minutes) into seconds
		if (l > 600) {
			l = (long) (l * NS_to_S);
		}
		return String.format("%d:%02d", (int) (l / 60), (int) (l % 60));
	}

	/**
	 * Pieces to fill up the timeline
	 */
	private class TimelinePiece extends VBox {
		private DoubleProperty currentWidth   = new SimpleDoubleProperty(0d);
		final Rectangle        CLIP_RECTANGLE = new Rectangle(0, -5, 0, 15);
		private Polygon        shape, shapeClip;
		@FXML
		private Label          top;
		@FXML
		private Label          bottom;
		@FXML
		private StackPane      shapeHolder;

		private int            position, team;
		private boolean        normal;

		/**
		 * timeline piece that uses the previous piece to work out what to display
		 *
		 * @param data
		 *            the timeline item for this selection. Make sure to set the data.pos int to the index into the
		 *            schedule
		 * @param previous
		 *            the previous item in the schedule
		 */
		public TimelinePiece(TimelineItem data, TimelineItem previous) {
			this.getStyleClass().setAll("item");
			FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
			        "/com/monkygames/nautdrafter/view/TimelinePiece.fxml"), LanguageResource.getLanguageResource());
			fxmlLoader.setRoot(this);
			fxmlLoader.setController(this);

			try {
				fxmlLoader.load();
			} catch (IOException exception) {
				throw new RuntimeException(exception);
			}

			// Determine what if any text to display for the item
			String text = previous == null || previous.type != data.type ? types[data.type] : "";

			this.shape = new Polygon(this.getShape(data.team, SpectatorTimeline.this.width));
			this.shape.getStyleClass().setAll("shape", data.type == Timeline.PICK ? "pick" : "ban");
			this.shapeClip = new Polygon(this.getShape(data.team, SpectatorTimeline.this.width));
			this.shapeClip.setClip(this.CLIP_RECTANGLE);
			this.shapeClip.getStyleClass().setAll("clip", data.type == Timeline.PICK ? "pick" : "ban");
			this.CLIP_RECTANGLE.widthProperty()
			        .bind(Bindings.multiply(this.currentWidth, SpectatorTimeline.this.ratio));

			if (data.team == Team.RED) {
				this.top.textProperty().bind(SpectatorTimeline.this.timeString);
				this.top.getStyleClass().add("counter");
				this.bottom.setText(text);
				this.shapeHolder.getStyleClass().add("red-team");
				this.shapeHolder.setAlignment(Pos.TOP_CENTER);
			} else {
				this.bottom.textProperty().bind(SpectatorTimeline.this.timeString);
				this.bottom.getStyleClass().add("counter");
				this.top.setText(text);
				this.shapeHolder.getStyleClass().add("blue-team");
				this.shapeHolder.setAlignment(Pos.BOTTOM_CENTER);
			}

			/**
			 * To change the appearance of the item when it is currently selected, when this property is changed the
			 * item will check to see if it is now the current selection in the timeline schedule and give itself the
			 * style class "current" else get rid of it.
			 */
			SpectatorTimeline.this.positionIndex.addListener((ov, oldVal, newVal) -> {
				if (newVal.intValue() == this.position) {
					this.getStyleClass().add("current");
				} else {
					this.getStyleClass().remove("current");
					if (newVal.intValue() > this.position) {
						this.getStyleClass().add("old");
					}
				}
			});

			// Put the stuff in the HBox
			this.shapeHolder.getChildren().addAll(this.shape, this.shapeClip);

			this.position = data.pos;
			this.team = data.team;
			this.normal = true;
		}

		/**
		 * Create the arrow that is at the end of the timeline
		 */
		private TimelinePiece() {
			FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
			        "/com/monkygames/nautdrafter/view/TimelinePiece.fxml"), LanguageResource.getLanguageResource());
			fxmlLoader.setRoot(this);
			fxmlLoader.setController(this);

			try {
				fxmlLoader.load();
			} catch (IOException exception) {
				throw new RuntimeException(exception);
			}
			this.getStyleClass().setAll("item", "end");
			this.position = SpectatorTimeline.this.schedule == null ? 0 : SpectatorTimeline.this.schedule.size();
			SpectatorTimeline.this.positionIndex.addListener((ov, oldVal, newVal) -> {
				if (newVal.intValue() == this.position) {
					this.getStyleClass().add("current");
				}
			});

			this.shape = new Polygon(this.endArrow(64));
			this.shape.getStyleClass().setAll("arrow");
			this.shapeHolder.getChildren().addAll(this.shape);
			this.top.setVisible(false);
			this.bottom.setVisible(false);
			this.normal = false;
		}

		/**
		 * Get the shape of the piece that goes into the timeline
		 *
		 * @param team
		 *            the {@link Team} that the item belongs to
		 * @param itemWidth
		 *            preferred width of each piece
		 * @return an array of points for use like: new {@link Polygon}(points)
		 */
		public double[] getShape(int team, double itemWidth) {
			if (team == Team.RED) {
				return new double[] { 0, 0, itemWidth / 2 - 4, 0, itemWidth / 2, -5, itemWidth / 2 + 4, 0, itemWidth,
				        0, itemWidth, 5, 0, 5 };
			} else {
				return new double[] { 0, 0, itemWidth, 0, itemWidth, 5, itemWidth / 2 + 4, 5, itemWidth / 2, 10,
				        itemWidth / 2 - 4, 5, 0, 5 };
			}
		}

		/**
		 * Resize the shape to fit the new width of the timeline
		 *
		 * @param width
		 *            width of each piece
		 */
		public void resize(double width) {
			this.currentWidth.set(width);
			this.shape.getPoints().clear();
			if (!this.normal) {
				for (double point : this.endArrow(width)) {
					this.shape.getPoints().add(point);
				}
				return;
			}
			this.shapeClip.getPoints().clear();
			for (double point : this.getShape(this.team, width)) {
				this.shape.getPoints().add(point);
				this.shapeClip.getPoints().add(point);
			}
		}

		// Shape for end arrow
		private double[] endArrow(double width) {
			return new double[] { 0, 0, width - 8, 0, width - 8, -5, width, 2.5, width - 8, 10, width - 8, 5, 0, 5 };
		}
	}

	private void updateLabel(int i) {
		this.bonuses[i].setText(this.minuteFormat(this.bonusTimes[i]));
		this.timeString.set(this.minuteFormat(this.currentTime));
	}

	/**
	 * {@link AnimationTimer} that is used for the countdowns for the bonus time and normal time
	 */
	private class CountDownTimer extends AnimationTimer {

		private long    last;
		private boolean isBonus;
		public int      currentTeam;

		/*
		 * (non-Javadoc)
		 * 
		 * @see javafx.animation.AnimationTimer#handle(long)
		 */
		@Override
		public void handle(long now) {
			if (this.last == 0) {
				this.last = now;
			}
			long delta = now - this.last;
			this.last = now;

			if (this.isBonus) {
				SpectatorTimeline.this.bonusTimes[this.currentTeam] -= delta;
				if (SpectatorTimeline.this.bonusTimes[this.currentTeam] < 0) {
					SpectatorTimeline.this.bonusTimes[this.currentTeam] = 0;
				}
			} else {
				SpectatorTimeline.this.currentTime -= delta;
				if (SpectatorTimeline.this.currentTime < 0) {
					SpectatorTimeline.this.setBonusTimeActive();
				}
			}

			SpectatorTimeline.this.updateLabel(this.currentTeam);

			// Update the clip ratio
			SpectatorTimeline.this.ratio.set(SpectatorTimeline.this.currentTime);
		}

		// Convenience method to reset the last time to 0
		@Override
		public void start() {
			super.start();
			this.last = 0;
		}
	}
}
