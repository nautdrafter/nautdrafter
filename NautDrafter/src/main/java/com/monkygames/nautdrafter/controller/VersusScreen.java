/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.model.Character;
import com.monkygames.nautdrafter.model.Character.IconType;
import com.monkygames.nautdrafter.model.Formatter;
import com.monkygames.nautdrafter.model.Summary;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.network.DraftingClient;
import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.Team;
import com.monkygames.nautdrafter.network.callbacks.NautChooseCallback;
import com.monkygames.nautdrafter.view.SubScreen;

/**
 * This represents the Versus screen mainly to be seen by spectators<br/>
 * It is used to show the selected Nauts and which players choose each Nauts as it happens<br/>
 * Players will be directed to this screen also when they have finished selecting their Nauts
 */
public class VersusScreen implements SubScreen, NautChooseCallback {

	private VBox             root;

	@FXML
	private TeamArea         redTeam;
	@FXML
	private TeamArea         blueTeam;
	@FXML
	private Label            redName;
	@FXML
	private Label            blueName;
	@FXML
	private Button           leaveButton;

	private final TeamArea[] teams;
	private final MenuItem[] menus;

	private KeyCombination copyToClipboard = new KeyCodeCombination(KeyCode.C, KeyCombination.SHORTCUT_DOWN);

	/**
	 * Creates the versus screen for all players
	 */
	public VersusScreen() {
		// Load xml
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
		        "/com/monkygames/nautdrafter/view/VersusScreen.fxml"), LanguageResource.getLanguageResource());

		fxmlLoader.setController(this);
		try {
			this.root = fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		this.redTeam.getStyleClass().add("team");
		this.blueTeam.getStyleClass().add("team");

		this.blueTeam.setEqual(this.redTeam);

		this.teams = new TeamArea[] { this.redTeam, this.blueTeam };

		// Exit button to return to server browser
		this.leaveButton.setOnAction(action -> {
			DraftingClient.disconnect();
			NautDrafter.getBase().popMain();
		});

		this.fillData(DraftingClient.getRedTeam(), DraftingClient.getBlueTeam());

		DraftingClient.setCallbacks(this);

		// copy order to clip-board the data on ctrl-c (on Mac it should be cmd-c)
		NautDrafter.getBase().getScene().getAccelerators()
		        .put(this.copyToClipboard , () -> {
		            System.out.println("Caught ctrl-c key bind");
                            Summary.exportSummary(true, Formatter.PlainText, 
                                DraftingClient.getRedTeam(), DraftingClient.getBlueTeam(),
                                DraftingClient.getMapId(), DraftingClient.getTimeline());
		        });

		Menu saveMenu = new Menu(LanguageResource.getString("versus.save_menu"));
		
		// Adding all the formats to the export menu
		for(Formatter format : Formatter.values()) {
			MenuItem menuItem = new MenuItem(LanguageResource.getString(format.nameKey));
			Button clipboard = new Button("", new ImageView("assets/images/icons/clipboard-16px.png"));
			clipboard.getStyleClass().add("icon");
			Platform.runLater(() -> clipboard.setTooltip(new Tooltip(LanguageResource.getString("versus.save.copy_tooltip"))));
			menuItem.setGraphic(new HBox(clipboard, new Separator(Orientation.VERTICAL)));
			
			menuItem.setOnAction(e -> this.saveSummary(format));
			clipboard.setOnAction(e -> {
                            Summary.exportSummary(true, format, 
                                DraftingClient.getRedTeam(), DraftingClient.getBlueTeam(),
                                DraftingClient.getMapId(), DraftingClient.getTimeline());
                            e.consume();
			});
			
			saveMenu.getItems().add(menuItem);
		}

		this.menus = new MenuItem[] { saveMenu };
	}

	/**
	 * Populate the picked naut images and team names
	 *
	 * @param red
	 *            Red teams data
	 * @param blue
	 *            Blue teams data
	 */
	public void fillData(Team red, Team blue) {
		for (int i = 0; i < 3; i++) {
			Character naut = Character.getById(red.pickedNauts[i]);
			this.redTeam.setPick(naut.getImage(IconType.RENDER), naut.name, i);
			naut = Character.getById(blue.pickedNauts[i]);
			this.blueTeam.setPick(naut.getImage(IconType.RENDER), naut.name, i);
			if (red.nautSelections[i] != null) {
				this.redTeam.setPlayer(red.nautSelections[i], i);
			}
			if (blue.nautSelections[i] != null) {
				this.blueTeam.setPlayer(blue.nautSelections[i], i);
			}
		}

		this.redName.setText(red.name);
		this.blueName.setText(blue.name);
	}

	/**
	 * A player has selected the naut they will use, display their name at the bottom of the naut image<br/>
	 *
	 * @param player
	 *            Player that made the selection
	 * @param team
	 *            Team that the player belongs to
	 * @param position
	 *            index of the naut that was selected (0 = left,1 = middle, 2 = right) or -1 = deselection
	 */
	@Override
	public void onPlayerChoose(Player player, byte team, byte position) {
		if (!Platform.isFxApplicationThread()) {
			Platform.runLater(() -> this.onPlayerChoose(player, team, position));
			return;
		}

		this.teams[team - 1].setPlayer(player, position);
	}

	@Override
	public Node getRoot() {
		return this.root;
	}

	@Override
	public String getTitle() {
		return LanguageResource.getString("versus.title");
	}

	@Override
	public MenuItem[] onShow() {
		return this.menus;
	}

	@Override
	public MenuItem[] onHide() {
		NautDrafter.getBase().getScene().getAccelerators()
        .remove(this.copyToClipboard);
		return this.menus;
	}

	/* (non-Javadoc)
	 * @see com.monkygames.nautdrafter.view.SubScreen#getDocIdentifier()
	 */
    @Override
    public String getDocIdentifier() {
	    return "How to use the Versus Screen";
    }
    
    /**
     * Writes the summary to disk.
     */
    private void saveSummary(Formatter format) {
    	FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(LanguageResource.getString("versus.save_menu"));
        File file = fileChooser.showSaveDialog(NautDrafter.stage);
        String summary = Summary.exportSummary(false, format, 
                            DraftingClient.getRedTeam(), DraftingClient.getBlueTeam(),
                            DraftingClient.getMapId(), DraftingClient.getTimeline());
        if(Summary.writeSummaryFile(summary, file)){
            // TODO notify user that export failed
        }
    }
}
