/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.IOException;
import java.util.ArrayList;

import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.view.NotificationPopUp;

/**
 * This creates a drag and drop UI to enable a user to customize the drafting schedule
 */
public class TimelineConfig extends HBox {

	@FXML
	private HBox        pickUp;
	@FXML
	private HBox        dropBox;
	@FXML
	private PickupLabel redPick;
	@FXML
	private PickupLabel redBan;
	@FXML
	private PickupLabel bluePick;
	@FXML
	private PickupLabel blueBan;
	@FXML
	private StackPane   pane;
	@FXML
	private Button      checkButton;

	private int[]       schedule;
	static final int    RED_PICK = 0, RED_BAN = 1, BLUE_PICK = 2, BLUE_BAN = 3;

	/**
	 * Create the element and set the initial drag labels data
	 */
	public TimelineConfig() {
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
		        "/com/monkygames/nautdrafter/view/TimelineConfig.fxml"));

		fxmlLoader.setController(this);
		fxmlLoader.setRoot(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		this.redPick.setHolder(this);
		this.bluePick.setHolder(this);
		this.blueBan.setHolder(this);
		this.redBan.setHolder(this);

		this.redPick.setData(RED_PICK);
		this.bluePick.setData(BLUE_PICK);
		this.blueBan.setData(BLUE_BAN);
		this.redBan.setData(RED_BAN);

		this.checkButton.setOnAction(event -> {
			if (this.isValid(true)) {
				System.out.println("Timeline valid");
			} else {
				System.out.println("Timeline invalid");
			}
		});

		Tooltip.install(this.pickUp, new Tooltip(LanguageResource.getString("drafting_config.pick.tooltip")));
		Tooltip.install(this.dropBox, new Tooltip(LanguageResource.getString("drafting_config.drop.tooltip")));
	}

	/**
	 * Set the dropBox area to a preset
	 *
	 * @param preset
	 *            preset in the form of: (nth action)...(2nd action) (first action) (action count), where the action
	 *            count is 4 bits and the actions are 2 bit using the constant defined in this class
	 */
	public void setToPreset(int preset) {
		int len = preset & 0xf;
		int rest = preset >> 4;

		ObservableList<Node> list = this.dropBox.getChildren();
		list.clear();

		for (int i = 0; i < len; i++) {
			int teamAndType = (rest >> (i * 2)) & 0b11;
			switch (teamAndType) {
			case RED_PICK:
				list.add(this.redPick.cloneMe());
				break;
			case RED_BAN:
				list.add(this.redBan.cloneMe());
				break;
			case BLUE_PICK:
				list.add(this.bluePick.cloneMe());
				break;
			case BLUE_BAN:
				list.add(this.blueBan.cloneMe());
				break;
			}
		}
	}

	/**
	 * Check for a valid schedule
	 *
	 * @param notifyOfSuccess
	 *            set to true if it is to display a notification if succeeds
	 * @return true if valid, false otherwise (will show error popups)
	 */
	public boolean isValid(boolean notifyOfSuccess) {
		int[] counts = new int[4];
		int[] finalData = new int[this.dropBox.getChildren().size()];

		// First check uneven
		if (finalData.length % 2 == 1) {
			new NotificationPopUp(LanguageResource.getString("drafting_config.error.uneven"),
			        NotificationPopUp.LENGTH_MEDIUM).showNotification(this.getScene());
			return false;
		}

		int index = 0;

		// Do counts
		for (Node item : this.dropBox.getChildren()) {
			if (item instanceof PickupLabel) {
				PickupLabel thing = (PickupLabel) item;
				counts[thing.data]++;
				finalData[index++] = thing.data;
			}
		}

		// Check 3 picks
		if (counts[RED_PICK] != 3 || 3 != counts[BLUE_PICK]) {
			new NotificationPopUp(LanguageResource.getString("drafting_config.error.pick_count"),
			        NotificationPopUp.LENGTH_MEDIUM).showNotification(this.getScene());
			return false;
		}

		// Check uneven bans
		if (counts[RED_BAN] != counts[BLUE_BAN]) {
			new NotificationPopUp(LanguageResource.getString("drafting_config.error.uneven_bans"),
			        NotificationPopUp.LENGTH_MEDIUM).showNotification(this.getScene());
			return false;
		}

		// Check max of 3 bans, already know they are even so only check one team
		if (counts[RED_BAN] > 3) {
			new NotificationPopUp(LanguageResource.getString("drafting_config.error.too_many_bans"),
			        NotificationPopUp.LENGTH_MEDIUM).showNotification(this.getScene());
			return false;
		}

		boolean redOK = false, blueOK = false;

		// Check picks are last
		for (int i = finalData.length - 1; i >= 0; i--) {
			if (!redOK && finalData[i] == BLUE_BAN) {
				new NotificationPopUp(LanguageResource.getString("drafting_config.error.bans_last.blue"),
				        NotificationPopUp.LENGTH_MEDIUM).showNotification(this.getScene());
				return false;
			}
			if (!blueOK && finalData[i] == RED_BAN) {
				new NotificationPopUp(LanguageResource.getString("drafting_config.error.bans_last.red"),
				        NotificationPopUp.LENGTH_MEDIUM).showNotification(this.getScene());
				return false;
			}
			if (finalData[i] == BLUE_PICK) {
				blueOK = true;
			}
			if (finalData[i] == RED_PICK) {
				redOK = true;
			}
		}
		this.schedule = finalData;
		String print = "";
		for (int i : this.schedule) {
			print = (Long.toBinaryString(4 + i).substring(1)) + print;
		}
		System.out.println(print + Long.toBinaryString(finalData.length + 16).substring(1));
		if (notifyOfSuccess) {
			new NotificationPopUp(LanguageResource.getString("drafting_config.success"),
			        NotificationPopUp.LENGTH_SHORT).showNotification(this.getScene());
		}
		return true;
	}

	/**
	 * Get the schedule as separate ints
	 *
	 * @return the array if ints corresponding to each action
	 */
	public int[] getSchedule() {
		return this.schedule;
	}

	/**
	 * {@link PseudoClass} for being able to drop into the box
	 */
	private static final PseudoClass ACCEPTING = PseudoClass.getPseudoClass("accepting");

	/**
	 * Draggable label class
	 */
	public static class PickupLabel extends Label {

		private double          startDragX;
		private double          startDragY;
		private Point2D         dragAnchor;
		private TimelineConfig  thingy;
		private boolean         in;
		private ArrayList<Pair> listMap     = new ArrayList<>();
		private double          dist;
		private int             placeholder = -1;
		ObservableList<Node>    droppy;
		private double          x;
		private int             data;

		/**
		 * Create a new label and set up the mouse listeners
		 */
		public PickupLabel() {
			this.setOnMousePressed(me -> {

				this.toFront();
				this.startDragX = this.getTranslateX();
				this.startDragY = this.getTranslateY();
				this.dragAnchor = new Point2D(me.getSceneX(), me.getSceneY());
				Point2D p = this.thingy.dropBox.localToScene(0, 0);
				this.bx = p.getX();
				this.by = p.getY();

				// Moving from drop box
				boolean early = false;
				this.x = 0;
				if (this.droppy.contains(this)) {
					this.x = this.getLayoutX();
					this.droppy.remove(this);
					this.thingy.pane.getChildren().add(this);
					this.thingy.layout();
					this.setLayoutX(this.x);
					System.out.println(this.x);
					early = true;
				}
				this.listMap.clear();

				int index = 0;
				for (Node n : this.droppy) {
					this.listMap.add(new Pair(n.getLayoutX() + n.prefWidth(-1) / 2, index++));
					if (index == this.droppy.size()) {
						this.listMap.add(new Pair(n.getLayoutX() + n.prefWidth(-1) + n.prefWidth(-1) / 2, index++));
					}
				}
				if (this.droppy.size() == 0) {
					this.listMap.add(new Pair(20, 0));
				}
				if (early) {
					this.trackMovement(me);
				}

			});
			this.setOnMouseReleased(me -> {

				this.setTranslateX(0);
				this.setTranslateY(0);

				if (this.thingy.dropBox.contains(me.getSceneX() - this.bx, me.getSceneY() - this.by)) {

					if (this.placeholder != -1) {
						this.droppy.remove(this.placeholder);
						this.thingy.dropBox.getChildren().add(this.placeholder, this.cloneMe());
					} else {
						this.thingy.dropBox.getChildren().add(this.cloneMe());
					}

					System.out.println("Added");

				} else {
					this.placeholder = -1;
					System.out.println("Cancled");
				}
				this.placeholder = -1;
				this.thingy.dropBox.pseudoClassStateChanged(ACCEPTING, this.in = false);

				this.thingy.pane.getChildren().remove(this);

			});

			this.setOnMouseDragged(me -> {
				this.trackMovement(me);

			});
		}

		/**
		 * Set the type of action this label represents. for use when getting whole timeline schedules data
		 *
		 * @param i
		 *            one of the four constants in this class
		 */
		public void setData(int i) {
			this.data = i;
		}

		/**
		 * Method that tracks the labels movement and the logic that adds the placeholder and stuff
		 *
		 * @param me
		 */
		private void trackMovement(MouseEvent me) {

			double newTranslateX = this.startDragX + me.getSceneX() - this.dragAnchor.getX();
			double newTranslateY = this.startDragY + me.getSceneY() - this.dragAnchor.getY();

			this.setTranslateX(newTranslateX + this.x);
			this.setTranslateY(newTranslateY);

			if (this.thingy.dropBox.contains(me.getSceneX() - this.bx, me.getSceneY() - this.by)) {
				double local = me.getSceneX() - this.bx;
				double d;
				this.dist = 1000;
				int closest = -1;
				for (Pair p : this.listMap) {
					if ((d = Math.abs(local - p.value)) < this.dist) {
						this.dist = d;
						closest = p.id;
					}
				}

				if (this.placeholder != closest) {
					if (this.placeholder != -1) {
						this.droppy.remove(this.placeholder);
					}
					this.placeholder = closest;
					if (this.placeholder != -1) {
						this.droppy.add(this.placeholder, this.cloneMePlaceholder());
					}

				}

				if (!this.in) {
					this.thingy.dropBox.pseudoClassStateChanged(ACCEPTING, this.in = true);
				}
			} else {
				if (this.in) {
					if (this.placeholder != -1) {
						this.droppy.remove(this.placeholder);
					}
					this.placeholder = -1;
					this.thingy.dropBox.pseudoClassStateChanged(ACCEPTING, this.in = false);
				}
			}

		}

		private PickupLabel(String text) {
			this();
			this.setText(text);
		}

		/**
		 * Clones this label
		 *
		 * @return a copy of this label with all its styling and text
		 */
		private Node cloneMe() {
			PickupLabel clone = new PickupLabel(this.getText());
			clone.getStyleClass().addAll(this.getStyleClass());
			clone.data = this.data;
			clone.setHolder(this.thingy);
			return clone;
		}

		/**
		 * Placeholder node
		 *
		 * @return placeholder to display
		 */
		private Node cloneMePlaceholder() {
			Pane clone = new Pane();
			clone.setMaxHeight(this.getHeight());
			clone.setPrefWidth(this.getWidth());
			clone.getStyleClass().add("placeholder");
			return clone;
		}

		private double bx, by;

		/**
		 * Give reference to the parent class so it can access the drop box
		 *
		 * @param thingy
		 *            the {@link TimelineConfig} parent
		 */
		public void setHolder(TimelineConfig thingy) {
			this.thingy = thingy;
			this.droppy = thingy.dropBox.getChildren();
		}
	}

	/**
	 * Class to hold the key values for determining where to put the placeholder
	 */
	private static class Pair {
		double value;
		int    id;

		public Pair(double value, int id) {
			this.value = value;
			this.id = id;
		}
	}

	/**
	 * @return the integer representation of the config schedule
	 */
    public int getIntRepresentation() {
    	int result=this.schedule.length;
    	for(int i = 0 ; i < this.schedule.length ; i++){
    		result |= this.schedule[i] <<(2*i+4);
    	}
	    return result;
    }
}
