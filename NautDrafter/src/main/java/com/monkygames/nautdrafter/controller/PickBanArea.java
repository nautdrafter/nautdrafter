/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.IOException;
import java.util.ArrayList;

import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.model.Character;
import com.monkygames.nautdrafter.model.Character.IconType;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.network.Team;
import com.monkygames.nautdrafter.network.Timeline;
import com.monkygames.nautdrafter.network.Timeline.TimelineItem;

/**
 * UI element for the player drafting screen that shows the current times (bonus and normal), selection of nauts and
 * bans
 */
public class PickBanArea extends HBox {

	private static final long       S_to_NS   = 1000000000;
	private static final double     NS_to_S   = 1d / S_to_NS;

	@FXML
	private Label                   redTypeLabel;
	@FXML
	private Label                   redNoLabel;
	@FXML
	private Label                   redTime;
	@FXML
	private Label                   redBonus;
	@FXML
	private ImageView               redPick1;
	@FXML
	private ImageView               redPick2;
	@FXML
	private ImageView               redPick3;
	@FXML
	private HBox                    redBansHolder;
	@FXML
	private Separator               seperator;
	@FXML
	private Label                   blueTypeLabel;
	@FXML
	private Label                   blueNoLabel;
	@FXML
	private Label                   blueTime;
	@FXML
	private Label                   blueBonus;
	@FXML
	private ImageView               bluePick1;
	@FXML
	private ImageView               bluePick2;
	@FXML
	private ImageView               bluePick3;
	@FXML
	private HBox                    blueBansHolder;

	private ImageView[][][]         sel       = new ImageView[2][2][];
	private Label[]                 type, number, time, bonus;
	static String[]                 types     = new String[2];
	static String[]                 positions = new String[3];
	private ArrayList<TimelineItem> timeline;
	private int                     timelinePosition;
	private long[]                  pickTimes = new long[2], bonusTimes = new long[2];
	private final InternalTimer     timer;

	static {
		types[0] = LanguageResource.getString("team.select.pick");
		types[1] = LanguageResource.getString("team.select.ban");
		positions[0] = LanguageResource.getString("team.1");
		positions[1] = LanguageResource.getString("team.2");
		positions[2] = LanguageResource.getString("team.3");
	}

	{
		this.timer = new InternalTimer();
	}

	/**
	 * Create the element and sets up arrays for easier UI modifications
	 */
	public PickBanArea() {
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
		        "/com/monkygames/nautdrafter/view/PickBanArea.fxml"), LanguageResource.getLanguageResource());

		fxmlLoader.setController(this);
		fxmlLoader.setRoot(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		this.sel[0][0] = new ImageView[] { this.redPick1, this.redPick2, this.redPick3 };
		this.sel[0][1] = new ImageView[] { this.bluePick1, this.bluePick2, this.bluePick3 };
		this.type = new Label[] { this.redTypeLabel, this.blueTypeLabel };
		this.number = new Label[] { this.redNoLabel, this.blueNoLabel };
		this.time = new Label[] { this.redTime, this.blueTime };
		this.bonus = new Label[] { this.redBonus, this.blueBonus };

		this.heightProperty().addListener((obv, n, o) -> {
			this.seperator.setPrefHeight(o.doubleValue() - 20);
		});

	}

	/**
	 * Designate the amount of bans to use for this draft <br/>
	 * Must be called before trying to do anything with the bans
	 *
	 * @param banCount
	 *            should be from 1 - 3 inclusive
	 */
	public void setBanCount(int banCount) {

		this.redBansHolder.getChildren().addAll(this.makeBans(banCount, Team.RED));
		this.blueBansHolder.getChildren().addAll(this.makeBans(banCount, Team.BLUE));
	}

	/**
	 * Initialize the UI with the current state of the drafting process
	 *
	 * @param timeline
	 *            list of TimelineItem that represent the drafting process
	 * @param pos
	 *            current position into the drafting process with 0 = start and timeline.length-1 = last position
	 * @param time
	 *            current events time counter value
	 * @param bonus
	 *            currently picking teams bonus time
	 * @param otherBonus
	 *            other teams bonus time
	 */
	public void init(ArrayList<TimelineItem> timeline, int pos, int time, int bonus, int otherBonus, int banCount) {
		this.timeline = timeline;
		this.timelinePosition = pos;

		// Create number of bans
		this.setBanCount(banCount);

		// fill out old data
		int index = 0;
		TimelineItem current = null;
		for (TimelineItem i : timeline) {
			if (i.type == Timeline.BAN && i.team == Team.RED) {
				banCount++;
			}
			if (index < pos) {
				this.setData(i);
			} else if (index == pos) {
				current = i;

			}
			index++;
		}

		// Set current selection
		this.time[current.team - 1].setText(this.minuteFormat(time));
		this.pickTimes[current.team - 1] = time * S_to_NS;
		this.bonusTimes[current.team - 1] = bonus * S_to_NS;
		this.bonusTimes[(current.team == Team.RED ? Team.BLUE : Team.RED) - 1] = otherBonus * S_to_NS;
		this.bonus[current.team - 1].setText(this.minuteFormat(this.bonusTimes[current.team - 1]));
		this.bonus[(current.team == Team.RED ? Team.BLUE : Team.RED) - 1].setText(this
		        .minuteFormat(this.bonusTimes[(current.team == Team.RED ? Team.BLUE : Team.RED) - 1]));
		this.disable(current.team == Team.RED ? Team.BLUE : Team.RED);
		this.type[current.team - 1].setText(types[current.type]);
		// Notify timer to start
		this.startCountDown(current.team - 1);

		// Set next selection to "next-pick" class
		this.sel[current.type][current.team - 1][current.pos].getParent().getStyleClass().add("next-pick");
	}

	/**
	 * Notify the UI animation timer to start counting down bonus time
	 */
	public void setBonusTimeActive() {
		this.pickTimes[this.timer.currentTeam] = 0; // Make sure the picking times is 0
		this.timer.isBonus = true;
	}

	/**
	 * A selection has occurred so update the ui and start the next round of selections
	 *
	 * @param lastPick
	 *            the {@link Character} that was selected in the last round
	 * @param remainingBonus
	 *            the remaining bonus time for the last team to select (to keep times in check)
	 */
	public void notifyOfSelection(Character lastPick, long remainingBonus) {
		if (Platform.isFxApplicationThread()) {
			this.update(lastPick, remainingBonus);
		} else {
			Platform.runLater(() -> this.update(lastPick, remainingBonus));
		}
	}

	/**
	 * A selection has occurred so update the ui and start the next round of selections
	 *
	 * @param pick
	 *            the {@link Character} that was selected in the last round
	 * @param remainingBonus
	 *            the remaining bonus time for the last team to select (to keep times in check)
	 */
	private void update(Character pick, long remainingBonus) {
		if (this.timelinePosition >= this.timeline.size()) {
			return;
		}

		// Fill out most recent action
		TimelineItem current = this.timeline.get(this.timelinePosition++);
		current.charId = pick.id;
		int i = current.team - 1;
		if (remainingBonus != -1) {
			this.bonusTimes[i] = remainingBonus * S_to_NS;
			this.updateLabel(i);
		}
		this.setData(current);

		// Remove style class "next-pick" from the last selection
		this.sel[current.type][i][current.pos].getParent().getStyleClass().remove("next-pick");

		// Check for last item
		if (this.timelinePosition >= this.timeline.size()) {
			this.timer.stop();
			System.out.println("Finished drafting, now time to pick characters");
			return;
		}

		// Set next positions data
		current = this.timeline.get(this.timelinePosition);
		i = current.team - 1;
		this.disable(current.team == Team.RED ? Team.BLUE : Team.RED); // set disabled state of other team
		this.time[i].setDisable(false); // enable this team
		this.bonus[i].setDisable(false);
		this.type[i].setText(types[current.type]); // set type label
		this.number[i].setText(positions[current.pos]); // set pick/ban number label
		this.time[i].setText("0:30"); // reset time to 30 secs
		this.pickTimes[i] = 30 * S_to_NS;

		// Add style class "next-pick" to next selection
		this.sel[current.type][i][current.pos].getParent().getStyleClass().add("next-pick");
		this.startCountDown(i);
	}

	/**
	 * Show that the other team is not the active team by changing there side of the area
	 *
	 * @param team
	 *            team that is not picking currently
	 */
	private void disable(int team) {
		int i = team - 1;
		this.type[i].setText("");
		this.number[i].setText("");
		this.time[i].setText("0:30");
		this.time[i].setDisable(true);
		this.bonus[i].setDisable(true);
	}

	/**
	 * Set old data to be filled out
	 *
	 * @param data
	 *            Data to fill the picks/bans
	 */
	private void setData(TimelineItem data) {

		this.sel[data.type][data.team - 1][data.pos].setImage(Character.getById(data.charId).getImage(IconType.ICON));

	}

	/**
	 * Start the countdown timer
	 *
	 * @param teamIndex
	 *            index of the team (basically the {@link Team} constant - 1)
	 */
	private void startCountDown(int teamIndex) {
		this.timer.currentTeam = teamIndex;
		this.timer.isBonus = false;
		this.timer.start();
	}

	/**
	 * Formats number to minute structure<br/>
	 * Regards anything over ten minutes (l > 600) as nanoseconds and converts it to seconds
	 */
	private String minuteFormat(long l) {
		// Convert any large numbers (600 = ten minutes) into seconds
		if (l > 600) {
			l = (long) (l * NS_to_S);
		}
		return String.format("%d:%02d", (int) (l / 60), (int) (l % 60));
	}

	private StackPane[] makeBans(int banCount, int team) {
		StackPane[] result = new StackPane[banCount];
		team--;
		this.sel[1][team] = new ImageView[banCount];
		for (int i = 0; i < banCount; i++) {
			this.sel[1][team][i] = new ImageView();
			this.sel[1][team][i].setFitHeight(48);
			this.sel[1][team][i].setFitWidth(48);
			result[i] = new StackPane(this.sel[1][team][i]);
			result[i].getStyleClass().add("picker");
		}
		return result;
	}

	private void updateLabel(int i) {
		this.time[i].setText(this.minuteFormat(this.pickTimes[i]));
		this.bonus[i].setText(this.minuteFormat(this.bonusTimes[i]));
	}

	/**
	 * Timer class that updates the time counters and displays the new time
	 */
	private class InternalTimer extends AnimationTimer {
		private long   last;
		public int     currentTeam;
		public boolean isBonus;

		@Override
		public void handle(long now) {
			if (this.last == 0) {
				this.last = now;
			}
			long delta = now - this.last;
			this.last = now;
			if (this.isBonus) {
				PickBanArea.this.bonusTimes[this.currentTeam] -= delta;
				if (PickBanArea.this.bonusTimes[this.currentTeam] < 0) {
					PickBanArea.this.bonusTimes[this.currentTeam] = 0;
				}
			} else {
				PickBanArea.this.pickTimes[this.currentTeam] -= delta;
				if (PickBanArea.this.pickTimes[this.currentTeam] <= 0) {
					PickBanArea.this.setBonusTimeActive();
				}
			}

			PickBanArea.this.updateLabel(this.currentTeam);
		}

		// Convenience method to reset the last time to 0
		@Override
		public void start() {
			super.start();
			this.last = 0;
		}
	}

}
