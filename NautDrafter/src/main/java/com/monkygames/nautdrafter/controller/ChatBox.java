/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.util.ArrayDeque;

import javafx.application.Platform;
import javafx.concurrent.Worker.State;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlendMode;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.FontSmoothingType;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.network.Player;

/**
 * Re usable area for chat
 *
 * @author agkf1
 */
public class ChatBox extends VBox {

	public final WebView         textArea    = new WebView();
	private JSObject             webWindow;
	private final TextField      textInput;
	private NewMessageListener   listener;
	private static final int     MAX_LENGTH  = 255;

	// Things to deal with message buffering when not loaded
	private boolean              isLoaded;
	private boolean              hasBuffered;
	private ArrayDeque<String[]> buffer      = new ArrayDeque<>();
	/**
	 * CSS class that should be used to style strings that have come from a human (i.e. messages that a human has
	 * entered), allowing such strings to be automatically styled to stand out.
	 */
	public static final String   CLASS_HUMAN = "human";
	private boolean              empty       = true;

	/**
	 * Create a new instance of the chat
	 */
	public ChatBox() {
		this.textInput = new TextField();
		this.textInput.setPromptText(LanguageResource.getString("chat.prompt"));

		VBox.setVgrow(this.textInput, Priority.NEVER);

		this.textArea.setBlendMode(BlendMode.LIGHTEN);
		this.textArea.setContextMenuEnabled(false);
		this.textArea.setFontSmoothingType(FontSmoothingType.LCD);

		// Checking the state of the webview and if not ready will buffer the messages
		this.textArea.getEngine().getLoadWorker().stateProperty().addListener((ov, o, n) -> {
			if (n == State.SUCCEEDED) {
				this.isLoaded = true;
				System.out.println("Webview loaded and ready");

				// Send the buffered messages on to the webview
				if (this.hasBuffered) {
					this.hasBuffered = false;
					if (Platform.isFxApplicationThread()) {
						this.clearBuffer();
					} else {
						Platform.runLater(() -> this.clearBuffer());
					}
				}
			}
		});
		this.textArea.getEngine().load(
		        this.getClass().getResource("/com/monkygames/nautdrafter/view/ChatBox.html").toExternalForm());

		// Getting the DOM/JavaScript window object so its members can be set from Java code
		this.webWindow = (JSObject) this.textArea.getEngine().executeScript("window");

		VBox.setVgrow(this.textArea, Priority.ALWAYS);
		this.getChildren().addAll(this.textArea, this.textInput);

		// Check for length greater than the maximum for a message
		this.textInput.textProperty().addListener((obv, oldVal, newVal) -> {
			if (newVal.length() > ChatBox.MAX_LENGTH) {

				this.textInput.setText(oldVal);
			}
		});

		// Fire message when players hits enter
		this.textInput.setOnAction(action -> {

			// Check if the string is empty first
			    String text;
			    if ((text = this.textInput.getText()) == null || text.isEmpty()) {
				    return;
			    }

			    // Clear input field
			    this.textInput.clear();

			    // Fire notification to the listener
			    if (this.listener != null) {
				    this.listener.onNewMessage(text);

				    // testing
				    // this.addPlayerChatMessage(NautDrafter.getBase().getPlayer(), text);
			    }
		    });
	}

	/**
	 * Displays a message in the chat box. Will always run on the FXThread<br/>
	 * If the webview is not loaded the messages will be buffered and added when it loads
	 *
	 * @param messages
	 *            String pairs corresponding to (message,style class)<br/>
	 *            If the number of strings is odd, the last string is given no style class<br/>
	 *            For a default styling use "" as the style for that string
	 */
	public void addNewMessage(String... message) {

		// Deal with buffering messages when webview is unloaded
		if (!this.isLoaded) {
			this.buffer.add(message);
			this.hasBuffered = true;
			return;
		}

		if (Platform.isFxApplicationThread()) {
			this.webWindow.setMember("msg", message);
			try {
				this.textArea.getEngine().executeScript("addMessage()");

				// Alert the user if they have this minimized of the new message
				NautDrafter.takeAttention();

				// Set flag saying there is stuff in the chat area
				if (this.empty) {
					this.empty = false;
				}
			} catch (netscape.javascript.JSException ex) {
				// May be throw if the chat page has not loaded yet
				System.err.println(ex);
			}
		} else {
			Platform.runLater(() -> this.addNewMessage(message));
		}
	}

	/**
	 * Clears out the buffer and adds the messages to the webview
	 */
	private void clearBuffer() {
		for (String[] message : this.buffer) {
			this.addNewMessage(message);
		}
		this.buffer.clear();
	}

	/**
	 * Adds a new message to the chat box, including a label to identify the player who sent the message
	 *
	 * @param player
	 *            The sender of the message
	 * @param message
	 *            The text content of the message
	 */
	public void addPlayerChatMessage(Player player, String message) {
		this.addNewMessage("[" + player.name + "]: ", "name", message, CLASS_HUMAN);
	}

	/**
	 * Adds a message to the chat box indicating that the specified player has performed an action of some sort (or had
	 * an action performed on them). The message will be in the format "Player [player.name] [event]".
	 *
	 * @param player
	 *            The player who the event applies to
	 * @param event
	 *            The event/action that has taken place. In general, this should start with a linking verb or verb.
	 */
	public void addPlayerEventMessage(Player player, String event) {
		this.addNewMessage("Player ", "", "[" + player.name + "] ", "name", event);
	}

	/**
	 * Set the new message listener for this chat box. Use to be notified when a new message is created by the user
	 *
	 * @param listener
	 *            Listener to call when a new message is created
	 */
	public void setOnNewMessageListener(NewMessageListener listener) {
		this.listener = listener;
	}

	/**
	 * Set to true to disable UI but not show any visible effects of disabling UI
	 *
	 * @param isSpectator
	 */
	public void setSpectatorMode(boolean isSpectator) {
		this.textInput.setDisable(isSpectator);

		if (isSpectator) {
			this.getStyleClass().add("spectator");
			this.getChildren().remove(this.textInput);
		}
	}

	/**
	 * Listener interface for new messages<br/>
	 * When the user hits enter, the current text in the input field will be sent to the listener if it has been set.
	 */
	public interface NewMessageListener {

		/**
		 * Will be called when a new message is ready to be added to the chat box<br/>
		 * Activated when the user hits enter
		 *
		 * @param message
		 *            The string representation of the message taken from the input field
		 */
		public void onNewMessage(String message);
	}
}
