/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.IOException;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.network.DraftingClient;
import com.monkygames.nautdrafter.network.DraftingServer;
import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.Team;
import com.monkygames.nautdrafter.network.callbacks.ChatCallback;
import com.monkygames.nautdrafter.network.callbacks.PlayerConnectionCallback;
import com.monkygames.nautdrafter.network.callbacks.StageChangeCallback;
import com.monkygames.nautdrafter.network.callbacks.TeamCallback;
import com.monkygames.nautdrafter.view.PlayerDisplayer;
import com.monkygames.nautdrafter.view.SubScreen;

/**
 * FXML Controller class for the Team Selection Screen
 *
 * @author Adam
 */
public class TeamSelectionScreen extends VBox implements SubScreen, TeamCallback, ChatCallback,
        PlayerConnectionCallback, StageChangeCallback {

	private static final String NAME_STYLE = "name";
	private static final String RED_TEXT   = "red";
	private static final String BLUE_TEXT  = "blue";
	@FXML
	private Button              leaveButton;
	@FXML
	private Button              proceedButton;

	@FXML
	private TeamPicker          redTeam;
	@FXML
	private TeamPicker          blueTeam;

	@FXML
	private ChatBox             chatBox;

	@FXML
	private FlowPane            playerList;

	private boolean[]           isReady    = new boolean[2];

	/**
	 * Create a new instance of the screen and load the fxml
	 */
	public TeamSelectionScreen() {

		// Load xml
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
		        "/com/monkygames/nautdrafter/view/TeamSelectionScreen.fxml"), LanguageResource.getLanguageResource());
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		// Exit button to return to server browser
		this.leaveButton.setOnAction(action -> {
			DraftingClient.disconnect();
			NautDrafter.getBase().popMain();
		});

		/*
		 * Create listeners for events
		 */

		this.blueTeam.setValidSelectionListener((position, type) -> {
			System.out.println("player " + (type ? "selected" : "de-selected") + " position " + position
			        + " on blue team ");
			DraftingClient.changeTeam(type ? Team.BLUE : Team.NONE, position);
			// Placeholder
			// TeamSelectionScreen.this.onChangedTeam(NautDrafter.getBase().getPlayer(), type ? Team.BLUE : Team.NONE,
			// position);
		    });

		this.blueTeam.setNameChangeListener(name -> DraftingClient.changeTeamName(Team.BLUE, name));

		this.redTeam.setValidSelectionListener((position, type) -> {

			DraftingClient.changeTeam(type ? Team.RED : Team.NONE, position);
			// Placeholder
			// TeamSelectionScreen.this.onChangedTeam(NautDrafter.getBase().getPlayer(), type ? Team.RED : Team.NONE,
			// position);
		    });
		this.redTeam.setNameChangeListener(name -> DraftingClient.changeTeamName(Team.RED, name));

		// Create listener for a new message
		this.chatBox.setOnNewMessageListener(message -> DraftingClient.sendChatMessage(message));

		// Disable if not a player
		if (!NautDrafter.isPlayer()) {
			this.redTeam.setSpectatorMode(true);
			this.blueTeam.setSpectatorMode(true);
			this.chatBox.setSpectatorMode(true);
		}

		// Set up listeners for full teams
		this.redTeam.setFulTeamListener((isFull, isInThisTeam, isCaptain) -> {
			if (isFull && isInThisTeam) {
				this.proceedButton.setText(LanguageResource.getString("team_selection.ready"));
				if (isCaptain) {
					this.proceedButton.setDisable(false);
				}
			} else if (isInThisTeam) {
				this.proceedButton.setDisable(true);
				this.proceedButton.setText(LanguageResource.getString("team_selection.waiting"));
			}
		});
		this.blueTeam.setFulTeamListener((isFull, isInThisTeam, isCaptain) -> {
			if (isFull && isInThisTeam) {
				this.proceedButton.setText(LanguageResource.getString("team_selection.ready"));
				if (isCaptain) {
					this.proceedButton.setDisable(false);
				}
			} else if (isInThisTeam) {
				this.proceedButton.setDisable(true);
				this.proceedButton.setText(LanguageResource.getString("team_selection.waiting"));
			}
		});

		// Sort out proceed button handling
		this.proceedButton.setOnAction(event -> {
			this.proceedButton.setDisable(true);
			DraftingClient.lockTeam();
		});

		DraftingClient.setCallbacks(this);
		DraftingClient.requestInitCallbacks();
	}

	@Override
	public Node getRoot() {
		return this;
	}

	@Override
	public String getTitle() {
		return LanguageResource.getString("team_selection.title");
	}

	@Override
	public MenuItem[] onShow() {
		return null;
	}
	
	@Override
    public MenuItem[] onHide(){
		// Not used
		return null;
	}

	/**
	 * To call when a team is ready, only happens once per team
	 */
	@Override
	public void onLockTeam(int team) {
		System.out.println("should lock team " + (team == Team.RED ? "red" : "blue"));
		if (Platform.isFxApplicationThread()) {
			this.isReady[team - 1] = true;
			System.out.println("We are ready to proceed (Team " + (team == Team.RED ? "red" : "blue") + ")");

			// Notify chat of change
			this.chatBox.addNewMessage("The ", "", (team == Team.RED ? "red " : "blue "), team == Team.RED ? RED_TEXT
			        : BLUE_TEXT, "team have finalised their positions and are now ready", "");

			Player local = NautDrafter.getBase().getPlayer();
			if (team == Team.RED) {
				if (this.isReady[Team.BLUE - 1]) {
					this.proceedButton.setText(LanguageResource.getString("team_selection.starting"));
				} else if (NautDrafter.isPlayer() && this.blueTeam.hasPlayer(local) && this.blueTeam.isFull()) {
					this.proceedButton.setText(LanguageResource.getString("team_selection.ready"));
				} else if (NautDrafter.isPlayer()) {
					this.proceedButton.setText(LanguageResource.getString("team_selection.waiting"));
				} else {
					this.proceedButton.setText(LanguageResource.getString("team_selection.red_fill"));
				}

				// Spectators dont need ui disabled
				if (!NautDrafter.isPlayer()) {
					return;
				}

				// Disable UI
				this.redTeam.setDisable(true);
				if (this.redTeam.hasPlayer(local)) {
					this.blueTeam.setDisable(true);
				}

			} else {
				if (this.isReady[Team.RED - 1]) {
					this.proceedButton.setText(LanguageResource.getString("team_selection.starting"));
				} else if (NautDrafter.isPlayer() && this.redTeam.hasPlayer(local) && this.redTeam.isFull()) {
					this.proceedButton.setText(LanguageResource.getString("team_selection.ready"));
				} else if (NautDrafter.isPlayer()) {
					this.proceedButton.setText(LanguageResource.getString("team_selection.waiting"));
				} else {
					this.proceedButton.setText(LanguageResource.getString("team_selection.blue_fill"));
				}

				// Spectators dont need ui disabled
				if (!NautDrafter.isPlayer()) {
					return;
				}

				// Disable UI
				this.blueTeam.setDisable(true);
				if (this.blueTeam.hasPlayer(local)) {
					this.redTeam.setDisable(true);
				}

			}
		} else {
			Platform.runLater(() -> this.onLockTeam(team));
		}
	}

	/**
	 * A player has joined the server<br/>
	 * Add a player to the current players list and notify chat
	 *
	 * @param player
	 *            The player that has joined
	 */
	@Override
	public void onPlayerJoin(Player player) {
		if (Platform.isFxApplicationThread()) {

			// Add to the list
			this.playerList.getChildren().add(new PlayerDisplayer(player, Team.NONE));

			// Notify chat of the joining player
			this.chatBox.addPlayerEventMessage(player, "has joined the server");

		} else {
			Platform.runLater(() -> this.onPlayerJoin(player));
		}
	}

	/**
	 * A player has left the server<br/>
	 * Removes a player from the current players list and notifies chat
	 *
	 * @param player
	 *            The player that has left
	 */
	@Override
	public void onPlayerLeave(Player player) {
		if (Platform.isFxApplicationThread()) {

			// Remove from the displayed player list
			this.playerList.getChildren().remove(new PlayerDisplayer(player));

			// Notify chat of the joining player
			this.chatBox.addPlayerEventMessage(player, "has left the server");

			this.blueTeam.deselect(player);
			this.redTeam.deselect(player);
		} else {
			Platform.runLater(() -> this.onPlayerLeave(player));
		}
	}

	/**
	 * Receive a new message from the server
	 */
	@Override
	public void onChatReceived(Player sender, String message) {

		// Send it on to the chat box with styling applied
		this.chatBox.addPlayerChatMessage(sender, message);
	}

	@Override
	public void onChangedTeam(Player player, int whichTeam, int whichPosition) {
		if (Platform.isFxApplicationThread()) {

			// Make sure the player is in no team
			this.redTeam.deselect(player);
			this.blueTeam.deselect(player);

			this.updateTeamInList(player, whichTeam);

			// Then allow them to select a position
			switch (whichTeam) {
			case Team.RED:

				// Select the position
				this.redTeam.setSelection(whichPosition, player);
				break;
			case Team.BLUE:

				// Select the position
				this.blueTeam.setSelection(whichPosition, player);
			}
		} else {
			Platform.runLater(() -> this.onChangedTeam(player, whichTeam, whichPosition));
		}

	}

	/**
	 * Receive a changed name event from the server
	 */
	@Override
	public void onChangedTeamName(int whichTeam, String name) {
		if (Platform.isFxApplicationThread()) {
			System.out.println("CHANGED TEAM NAME: " + whichTeam + " = " + name);
			switch (whichTeam) {
			case Team.RED:
				// Set the name for this team
				this.redTeam.setTeamName(name);

				// Notify chat of change
				this.chatBox.addNewMessage("The ", "", "red ", RED_TEXT, "team has changed their name to ", "", name,
				        NAME_STYLE);
				break;
			case Team.BLUE:
				// Set the name for this team
				this.blueTeam.setTeamName(name);

				// Notify chat of change
				this.chatBox.addNewMessage("The ", "", "blue ", BLUE_TEXT, "team has changed their name to ", "", name,
				        NAME_STYLE);
			default:
				return;
			}
		} else {
			Platform.runLater(() -> this.onChangedTeamName(whichTeam, name));
		}
	}

	@Override
	public void onCaptainSwap(int teamId, Player newCaptain, Player oldCaptain, int oldCaptainNewPosition) {
		if (Platform.isFxApplicationThread()) {

			System.out.println("Swapping captain from '" + oldCaptain.name + "' to '" + newCaptain.name + "'");

			// Notify the two players involved with a chat message saying the result of the dispute
			if (NautDrafter.isPlayer()) {
				if (NautDrafter.isMe(newCaptain)) {
					this.chatBox.addNewMessage(
					        "You have successfully challenged the captains position demoting the old captain ", "", "["
					                + oldCaptain.name + "]", NAME_STYLE);
				} else if (NautDrafter.isMe(oldCaptain)) {
					this.chatBox.addNewMessage("You have been challenged for the captains position by ", "", "["
					        + newCaptain.name + "]", NAME_STYLE, " and lost");
				}
			}
			// Make sure the newCaptain is in no team
			this.redTeam.deselect(newCaptain);
			this.blueTeam.deselect(newCaptain);

			// Removing the old captain as well, so that any tasks that need to be run when a captain leaves are run
			this.redTeam.deselect(oldCaptain);
			this.blueTeam.deselect(oldCaptain);

			// Moving the newCaptain and oldCaptain to their new positions
			switch (teamId) {
			case Team.RED:
				// Select the position
				this.redTeam.setSelection(0, newCaptain);
				this.redTeam.setSelection(oldCaptainNewPosition, oldCaptain);
				break;
			case Team.BLUE:
				// Select the position
				this.blueTeam.setSelection(0, newCaptain);
				this.blueTeam.setSelection(oldCaptainNewPosition, oldCaptain);
			}

			// Updating the list of players in case the captain has come from outside his new team
			this.updateTeamInList(newCaptain, teamId);
		} else {
			Platform.runLater(() -> this.onCaptainSwap(teamId, newCaptain, oldCaptain, oldCaptainNewPosition));
		}
	}

	/**
	 * Updates the list of players to show that the given player is now on the given team
	 */
	private void updateTeamInList(Player player, int whichTeam) {
		int playerIndex = -1;
		if ((playerIndex = this.playerList.getChildren().indexOf(new PlayerDisplayer(player))) >= 0) {
			((PlayerDisplayer) (this.playerList.getChildren().get(playerIndex))).setTeam(whichTeam);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.monkygames.nautdrafter.drafting.network.callbacks.StageChangeCallback#onStageChanged(int)
	 */
	@Override
	public void onStageChanged(int stage) {
		if (Platform.isFxApplicationThread()) {
			if (stage == DraftingServer.STAGE_DRAFTING) {
				NautDrafter.setScreen(NautDrafter.isPlayer() ? new DraftingScreenPlayer()
				        : new DraftingScreenSpectator(), false);
			}
		} else {
			Platform.runLater(() -> this.onStageChanged(stage));
		}

	}

	/* (non-Javadoc)
	 * @see com.monkygames.nautdrafter.view.SubScreen#getDocIdentifier()
	 */
    @Override
    public String getDocIdentifier() {
	    return "How to use the Team Selection screen";
    }

}
