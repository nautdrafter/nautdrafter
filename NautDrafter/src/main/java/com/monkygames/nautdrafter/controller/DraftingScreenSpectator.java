/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.IOException;
import java.util.ArrayList;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.model.Character;
import com.monkygames.nautdrafter.model.Character.IconType;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.model.Planet;
import com.monkygames.nautdrafter.network.DraftingClient;
import com.monkygames.nautdrafter.network.DraftingServer;
import com.monkygames.nautdrafter.network.Team;
import com.monkygames.nautdrafter.network.Timeline;
import com.monkygames.nautdrafter.network.Timeline.TimelineItem;
import com.monkygames.nautdrafter.network.callbacks.ClientDraftingCallback;
import com.monkygames.nautdrafter.network.callbacks.StageChangeCallback;
import com.monkygames.nautdrafter.view.SubScreen;

/**
 * This represents the drafting screen for the spectator<br/>
 * It uses a completely different UI layout to the player screen. No UI can be used as it is mainly a display screen
 */
public class DraftingScreenSpectator implements SubScreen, ClientDraftingCallback, StageChangeCallback {

	private VBox                    root;

	@FXML
	private TeamArea                redTeam;
	@FXML
	private TeamArea                blueTeam;
	@FXML
	private SpectatorTimeline       timeline;
	@FXML
	private Button                  leaveButton;

	private ArrayList<TimelineItem> schedule;
	private int                     positionInSchedule;

	/**
	 * Creates the drafting screen for the spectator
	 */
	public DraftingScreenSpectator() {
		// Load xml
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
		        "/com/monkygames/nautdrafter/view/DraftingScreenSpectator.fxml"), LanguageResource.getLanguageResource());

		fxmlLoader.setController(this);
		try {
			this.root = fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		// Exit button to return to server browser
		this.leaveButton.setOnAction(action -> {
			DraftingClient.disconnect();
			NautDrafter.getBase().popMain();
		});

		this.redTeam.getStyleClass().add("team");
		this.blueTeam.getStyleClass().addAll("team");

		this.blueTeam.setEqual(this.redTeam);

		this.setUp(DraftingClient.getRedTeam(), DraftingClient.getBlueTeam(), Planet.getById(DraftingClient.getMapId()));

		DraftingClient.setCallbacks(this);
		DraftingClient.requestInitCallbacks();
	}

	/**
	 * A selection has been made
	 *
	 * @param selection
	 *            character selected in last turn
	 * @param remaingBonus
	 *            remaining bonus time
	 */
	@Override
	public void onCaptainChooseNaut(byte id, byte remaingBonus) {
		if (!Platform.isFxApplicationThread()) {
			Platform.runLater(() -> this.onCaptainChooseNaut(id, remaingBonus));
			return;
		}
		Character selection = Character.getById(id);
		System.out.println("Character selected was " + selection.name);

		if (this.positionInSchedule >= this.schedule.size()) {
			return;
		}
		this.timeline.notifyOfSelection(remaingBonus);

		TimelineItem current;
		if ((current = this.schedule.get(this.positionInSchedule++)).type == Timeline.BAN) {
			TeamArea area = current.team == Team.RED ? this.redTeam : this.blueTeam;
			area.setBan(selection.getImage(IconType.ICON), selection.name, current.pos);
			if(id>=0) {
	            NautDrafter.getAudioPlayer().playSound(selection, false);
            }
		} else {
			TeamArea area = current.team == Team.RED ? this.redTeam : this.blueTeam;
			area.setPick(selection.getImage(IconType.RENDER), selection.name, current.pos);
			if (id >= 0) {
				NautDrafter.getAudioPlayer().playSound(selection, true);
			}
		}
		if (this.positionInSchedule >= this.schedule.size()) {
			return;
		}
		current = this.schedule.get(this.positionInSchedule);
		if (current.type == Timeline.BAN) {
			(current.team == Team.RED ? this.redTeam : this.blueTeam).setNextBan(current.pos);
		} else {
			(current.team == Team.RED ? this.redTeam : this.blueTeam).setNextPick(current.pos);
		}
	}

	
	@Override
	public Node getRoot() {
		return this.root;
	}

	
	@Override
	public String getTitle() {
		return LanguageResource.getString("drafting.title");
	}

	@Override
	public MenuItem[] onShow() {
		return null;
	}

	@Override
	public void onCaptainSelectNaut(byte nautId) {
		// nom nom nom
		// (this callback does nothing because spectators do not have a captain whose selection to view)
	}

	@Override
	public void onTimelineReceived(Timeline timeline, byte currentPhaseTime) {
		if (!Platform.isFxApplicationThread()) {
			Platform.runLater(() -> this.onTimelineReceived(timeline, currentPhaseTime));
			return;
		}

		boolean isRed = timeline.items.get(timeline.pos).team == Team.RED;
		Team red = DraftingClient.getRedTeam();
		Team blue = DraftingClient.getBlueTeam();

		this.schedule = timeline.items;

		int[] pick = new int[2], ban = new int[2];

		// Fill out nautId and pos fields and count bans
		for (TimelineItem i : this.schedule) {

			Team t = i.team == Team.RED ? red : blue;
			if (i.type == Timeline.PICK) {
				i.pos = pick[i.team - 1]++;
				i.charId = t.pickedNauts[i.pos];
			} else {
				i.pos = ban[i.team - 1]++;
				i.charId = t.bannedNauts[i.pos];
			}
		}

		this.positionInSchedule = timeline.pos;

		this.timeline.init(this.schedule, timeline.pos, currentPhaseTime, isRed ? timeline.redBonus
		        : timeline.blueBonus, isRed ? timeline.blueBonus : timeline.redBonus);

		this.redTeam.setBanCount(ban[0]);
		this.blueTeam.setBanCount(ban[0]);

		// Sort out old data filling
		int index = 0;
		for (TimelineItem i : this.schedule) {
			TeamArea area = i.team == Team.RED ? this.redTeam : this.blueTeam;
			if (index < timeline.pos) {
				Character naut = Character.getById(i.charId);
				if (i.type == Timeline.PICK) {
					area.setPick(naut.getImage(IconType.RENDER), naut.name, i.pos);
				} else {
					area.setBan(naut.getImage(IconType.ICON), naut.name, i.pos);
				}
			} else if (index == timeline.pos) {
				if (i.type == Timeline.PICK) {
					area.setNextPick(i.pos);
				} else {
					area.setNextBan(i.pos);
				}
			} else {
				break;
			}
			index++;
		}
	}

	/**
	 * Fills out the team and map data
	 */
	private void setUp(Team red, Team blue, Planet map) {

		for (int i = 0; i < 3; i++) {
			this.redTeam.setPlayer(red.players[i], i);
			this.blueTeam.setPlayer(blue.players[i], i);
		}
		this.redTeam.setTeamName(red.name);
		this.blueTeam.setTeamName(blue.name);
		
		this.timeline.setMap(map);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.monkygames.nautdrafter.network.callbacks.StageChangeCallback#onStageChanged(int)
	 */
	@Override
	public void onStageChanged(int stage) {
		if (stage == DraftingServer.STAGE_NAUT_SELECTION) {
			NautDrafter.setScreen(new VersusScreen(), false);
		}
	}

	@Override
    public MenuItem[] onHide(){
		// Not used
		return null;
	}

	/* (non-Javadoc)
	 * @see com.monkygames.nautdrafter.view.SubScreen#getDocIdentifier()
	 */
    @Override
    public String getDocIdentifier() {
	    return "How to use the Drafting screen (Spectator)";
    }
}
