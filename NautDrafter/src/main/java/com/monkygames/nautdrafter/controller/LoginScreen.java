/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;

import com.monkygames.nautdrafter.Constants;
import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.network.LobbyClient;
import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.callbacks.LoginDetailsCallback;
import com.monkygames.nautdrafter.view.NotificationPopUp;
import com.monkygames.nautdrafter.view.PopUp;
import com.monkygames.nautdrafter.view.SubScreen;
import javafx.scene.control.Label;

/**
 *
 * @author agkf1
 */
public class LoginScreen implements LoginDetailsCallback, SubScreen {

	@FXML
	private SplitMenuButton playerSignIn;
	@FXML
	private MenuItem        fakePlayerSignIn;
        @FXML
        private MenuItem       resetLogin;
	@FXML
	private Button          spectatorSignIn;
	@FXML
	private Hyperlink       steamLink;
	@FXML
	private Menu            languagesMenu;

	@FXML
	private MenuButton      menu;

	@FXML
	private Label		versionL;

	private AnchorPane      login;
	private ResourceBundle  resource;

        /**
         * The width of the overall window once logged in.
         */
        private static final int width = 1200;
        /**
         * The height of the overall window once logged in.
         */
        private static final int height = 720;

	/**
	 * Screen for the Login
	 */
	public LoginScreen() {
		super();

		// Load xml
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
		        "/com/monkygames/nautdrafter/view/Login.fxml"), this.resource = LanguageResource.getLanguageResource());

		fxmlLoader.setController(this);

		try {
			this.login = fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		// Player sign in button
		this.playerSignIn.setOnAction(e -> {

			System.out.println("User clicked \"Sign in as player\" button");

			NautDrafter.getBase().startLoadingOverlay(this.resource.getString("login.loading"),
			        () -> LobbyClient.abortVersion());

			// check lobby version
			    LobbyClient.checkVersion((major, minor, patch) -> {

				    if (Constants.VERSION_MAJOR < major) {
					    Platform.runLater(() -> {
						    NautDrafter.getBase().stopLoadingOverlay();
						    new PopUp(LanguageResource.getString("login.error.too_old"), LanguageResource.getString("login.error.title"), LanguageResource.getString("login.error.too_old.download"), () -> {
						    	NautDrafter.getInstance().getHostServices().showDocument("http://nautdrafter.bitbucket.org/nautdrafter/download/");
						    }).showPopup();
					    });
				    } else if (Constants.VERSION_MAJOR > major) {
			    		Platform.runLater(() -> {
						    NautDrafter.getBase().stopLoadingOverlay();
						    new NotificationPopUp(LanguageResource.getString("login.error.too_new"), 5000).showNotification(this.login.getScene());
					    });
				    } else {
					    // Start the logging in process
					    NautDrafter.getBase().startLoadingOverlay(this.resource.getString("login.loading"),
					            () -> LobbyClient.abortLogin());

					    NautDrafter.setIsPlayer(true);
					    LobbyClient.login(this);

					    return;
				    }
			    });
		    });

		// Specatator sign in button
		this.spectatorSignIn.setOnAction(e -> {
			System.out.println("User clicked \"Continue as spectator\" button");

			NautDrafter.getBase().setPlayer(null);
			NautDrafter.setIsPlayer(false);

			this.showNextScreen(new ServerBrowserScreen(false));
		});

		this.fakePlayerSignIn.setOnAction(event -> {
			Player fake = new FakePlayerLogin().showWindow();
			if(fake == null) {
	            return;
            }
			NautDrafter.fakePlayer = fake;
			NautDrafter.getBase().setPlayer(fake);
			NautDrafter.setIsPlayer(true);

			LobbyClient.login(this);
		});

                // Reset the steam id
		this.resetLogin.setOnAction(event -> {
                    NautDrafter.resetLogin();
		});
		
		// Steam url hyperlink
		this.steamLink.setOnAction(e -> {
			NautDrafter.getInstance().startSteam();
		});

		// Setting up the language drop down menu, based on the locales listed in the NautDrafter class
		ToggleGroup languagesGroup = new ToggleGroup();

		String languageTag = NautDrafter.getCountryCode();

		for (Locale locale : NautDrafter.getSupportedLocales()) {
			RadioMenuItem localeItem = new RadioMenuItem(locale.getDisplayName(locale));
			localeItem.toggleGroupProperty().set(languagesGroup);

			String currentLanguageTag = locale.toLanguageTag();
			localeItem.getProperties().put("locale", currentLanguageTag);
			this.languagesMenu.getItems().add(localeItem);

			if (languageTag != null && languageTag.equals(currentLanguageTag)) {
				localeItem.setSelected(true);
			}
		}

		// Changing the language when a language item is clicked from the menu
		this.languagesMenu.setOnAction(e -> {
			// The event is triggered by clicking on the parent menu item as well, so check that a language was actually
			// clicked
			    if (e.getTarget() instanceof RadioMenuItem) {
				    RadioMenuItem selected = (RadioMenuItem) e.getTarget();
				    NautDrafter.setCountryCode((String) selected.getProperties().get("locale"));
			    }
		    });

		this.menu.getItems().addAll(TitleBar.getLoginMenus());
		// set the version
		versionL.setText("Version: "+Constants.VERSION);
	}

	/**
	 * Set the player details then progress to next screen
	 *
	 * @param player
	 */
	@Override
	public void onLoginDetailsReturn(Player player) {

		if (Platform.isFxApplicationThread()) {
			/*
			 * Since Steam has finished and the user might have minimized the application to complete the web-based //
			 * authentication we should alert to them that we are ready
			 */
			NautDrafter.takeAttention();
			if (player == null) {
				System.out.println("Failed Login");
				new PopUp(this.resource.getString("login.error"), this.resource.getString("login.error.title"))
				        .showPopup();
				NautDrafter.getBase().stopLoadingOverlay();
				return;
			}

			System.out.println("LOGIN DETAILS RETURNED IN CALLBACK: " + player + ", url = " + player.iconUrl
			        + ", tiebreak = " + player.tieBreak);
			NautDrafter.getBase().setPlayer(player);

                        // test if they logged in via steam, if no valid steam id than pop a warning
                        if(!player.isSteam()){
                            new PopUp(
                                this.resource.getString("login.warning.no_steam"), 
                                this.resource.getString("login.warning.no_steam.title")
                            ).showPopup();
                        }

			this.showNextScreen(new ServerBrowserScreen(true));

			NautDrafter.getBase().stopLoadingOverlay();

		} else {
			Platform.runLater(() -> this.onLoginDetailsReturn(player));
		}
	}

	private void showNextScreen(SubScreen screen) {
		NautDrafter.getBase().setTitleAreaVisable(true);
		NautDrafter.getInstance().resizeScreen(width, height, true);
		NautDrafter.setScreen(screen, true);
		NautDrafter.getInstance().centerScreen();
	}

	@Override
	public Node getRoot() {
		return this.login;
	}

	@Override
	public String getTitle() {
		return null;
	}

	@Override
	public MenuItem[] onShow() {
		Platform.runLater(() -> NautDrafter.getBase().stopLoadingOverlay());
		return null;
	}

	@Override
	public MenuItem[] onHide() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.monkygames.nautdrafter.view.SubScreen#getDocIdentifier()
	 */
	@Override
	public String getDocIdentifier() {
		return "How to use the Login screen";
	}
}
