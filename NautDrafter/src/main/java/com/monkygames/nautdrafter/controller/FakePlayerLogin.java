/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.IOException;
import java.util.Random;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.resource.ImageCacher;

/**
 * Window that allows the user to directly connect to a server using the hosts ip address and socket
 */
public class FakePlayerLogin extends Stage {

	private static final Image ERROR  = ImageCacher.get("assets/images/error.png");

	private BorderPane         root;

	@FXML
	private StackPane          imageHolder;
	@FXML
	private TextField          name;
	@FXML
	private TextField          imageUrl;
	@FXML
	private ImageView          icon;
	@FXML
	private ImageView          errorIcon;
	@FXML
	private Button             login;

	private Thread             imageLoader;

	private Player             player;

	private BooleanProperty    isReal = new SimpleBooleanProperty(false);

	public FakePlayerLogin() {

		// Load xml
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
		        "/com/monkygames/nautdrafter/view/FakePlayerLogin.fxml"), LanguageResource.getLanguageResource());

		fxmlLoader.setController(this);
		try {
			this.root = fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		this.initOwner(NautDrafter.stage);
		this.setScene(new Scene(this.root));
		this.sizeToScene();
		this.centerOnScreen();
		this.root.getStylesheets().add("style/Base.css");
		this.errorIcon.setImage(ERROR);
		this.imageUrl.focusedProperty().addListener((ov, o, n) -> {
			if (n) {
				Platform.runLater(() -> {
					this.imageUrl.selectAll();
				});
			}
		});
		BooleanProperty isLoading = new SimpleBooleanProperty(true);

		Platform.runLater(() -> {
			isLoading.set(false);
			String oldURL = NautDrafter.getPreferences().get("fake-player-image-url", "");
			if (!oldURL.isEmpty()) {
				this.imageUrl.setText(oldURL);
			}
			this.name.setText(NautDrafter.getPreferences().get("fake-player-name", ""));
		});

		ProgressIndicator loading = new ProgressIndicator();
		this.imageHolder.getChildren().add(loading);
		loading.visibleProperty().bind(Bindings.and(isLoading, this.isReal.not()));
		this.icon.visibleProperty().bind(this.isReal);
		this.errorIcon.visibleProperty().bind(
		        Bindings.and(isLoading.not(), this.isReal.not()).and(this.imageUrl.textProperty().isEmpty().not()));

		this.login.disableProperty().bind(this.name.textProperty().isEmpty());

		this.imageUrl.textProperty().addListener((ov, o, n) -> {
			if (!isLoading.get()) {
				this.isReal.set(false);
				isLoading.set(true);
			}
			if (n != null) {
				final String newString = n;
				if (this.imageLoader != null) {
					this.imageLoader.interrupt();
				}
				this.imageLoader = new Thread(() -> {
					try {
						Thread.sleep(1000);
						if (newString.equals(this.imageUrl.getText())) {
							try {
								Image i = new Image(n, true);
								i.progressProperty().addListener((ovs, old, newi) -> {
									if (newi.doubleValue() == 1) {
										Platform.runLater(() -> {
											if (!i.isError()) {
												this.icon.setImage(i);
												this.isReal.set(true);
											}
											isLoading.set(false);
										});
									}
								});
							} catch (IllegalArgumentException e) {
								Platform.runLater(() -> {
									isLoading.set(false);
								});
							}
						}
						this.imageLoader = null;
					} catch (InterruptedException e) {
					}
				});
				this.imageLoader.start();
			}
		});

		this.login.setOnAction(event -> {
			this.player = new Player(false, new Random().nextLong(), this.name.getText(),
			        this.isReal.get() ? this.imageUrl.getText() : "assets/ronimo/images/icons/logo.png", 0);
			this.close();
		});
	}

	/**
	 * Show the window and return the Player instance that is made
	 * 
	 * @return the player instance or null if canceled;
	 */
	public Player showWindow() {
		super.showAndWait();
		return this.player;
	}
}
