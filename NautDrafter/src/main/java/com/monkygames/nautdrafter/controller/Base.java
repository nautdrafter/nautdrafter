/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.NoSuchElementException;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.view.LockerPane;
import com.monkygames.nautdrafter.view.SubScreen;

/**
 * The root node for all screens apart from login
 *
 * @author agkf1
 */
public class Base extends BorderPane {

	private boolean               addShowing, titleShowing;
	private Pane                  ad;
	private SubScreen             root;
	private TitleBar              title;
	private ArrayDeque<SubScreen> stack;

	@FXML
	private AnchorPane            top;
	@FXML
	private AnchorPane            right;

	@FXML
	private Base                  base;

	@FXML
	private LockerPane            center;

	{
		this.stack = new ArrayDeque<>();
	}

	/**
	 * Should not really be used
	 */
	public Base() {
		super();

		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass()
		        .getResource("/com/monkygames/nautdrafter/view/Base.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		this.title = new TitleBar();
		this.anchor(this.title);

		this.top.getChildren().add(this.title);
		this.titleShowing = true;

	}

	public SubScreen getCurrentScreen() {
		return this.root;
	}

	/**
	 * Switch main area so the designated screen but does not save last screen on the stack
	 *
	 * @param screen
	 */
	public void setMain(SubScreen screen) {
		if(this.root!=null) {
			this.title.removeExtraMenuOption(this.root.onHide());
        }
		this.root = screen;
		this.setRootScreen(this.root);
	}

	/**
	 * Switch main area so the designated screen and saves last screen on the stack
	 *
	 * @param screen
	 */
	public void addMain(SubScreen screen) {
		if(this.root!=null) {
			this.title.removeExtraMenuOption(this.root.onHide());
        }
		this.stack.addFirst(this.root);
		this.root = screen;
		this.setRootScreen(this.root);
	}

	/**
	 * Return to last saved screen on the stack If nothing is on stack and exception is thrown
	 */
	public void popMain() {
		if (this.stack.isEmpty()) {
			throw new NoSuchElementException("No screen left on stack");
		}
		if(this.root!=null) {
			this.title.removeExtraMenuOption( this.root.onHide());
        }
		this.root = this.stack.removeFirst();
		this.setRootScreen(this.root);
	}

	/**
	 * Return to last saved screen on the stack If nothing is on stack and exception is thrown
	 *
	 * @param notThis
	 *            Do not pop the screen if it has the same title as notThis
	 */
	public void popMain(String notThis) {
		if (notThis.equals(this.root.getTitle())) {
			return;
		}
		if (this.stack.isEmpty()) {
			throw new NoSuchElementException("No screen left on stack");
		}
		if(this.root!=null) {
			this.title.removeExtraMenuOption(this.root.onHide());
        }
		this.root = this.stack.removeFirst();
		this.setRootScreen(this.root);
	}

	// TODO: Need to check if on fx thread before changing ui
	/**
	 * Set whether the title bar at the top exists or not
	 *
	 * @param isVisable
	 */
	public void setTitleAreaVisable(boolean isVisable) {
		if (isVisable) {
			// Only bother showing if root exists and if not showing
			if (this.root != null && !this.titleShowing) {
				this.titleShowing = true;
				this.title.setScreenName(this.root.getTitle());
				this.setTop(this.top);
			}
		} else {
			// Remove title bar by setting top to null
			this.titleShowing = false;
			this.setTop(null);
		}
	}

	/**
	 * Set a node to the ad area
	 *
	 * @param ad
	 */
	public void setAdArea(Pane ad) {
		this.anchor(ad);
		this.ad = ad;
		this.resizeImageViews(ad);
		// Only show if needed
		if (this.addShowing) {
			this.right.getChildren().clear();
			this.right.getChildren().add(ad);
		}

	}

	/**
	 * Set visibility of the ads
	 *
	 * @param isVisable
	 * @return whether the ads were shown/exist
	 */
	public boolean setAdVisable(boolean isVisable) {
		if (isVisable) {
			if (this.ad != null && !this.addShowing) {
				this.addShowing = true;
				this.setRight(this.right);
				this.right.getChildren().clear();
				this.right.getChildren().add(this.ad);
				return true;
			}
		} else {
			this.addShowing = false;
			this.setRight(null);
			this.right.getChildren().clear();
		}
		return false;
	}

	/**
	 * Set the player in the title bar
	 *
	 * @param player
	 */
	public void setPlayer(Player player) {
		this.title.setPlayer(player);
	}

	/**
	 * Get the player
	 *
	 * @return The player
	 */
	public Player getPlayer() {
		return this.title.getPlayer();
	}

	/**
	 * Set the game logo in the title bar VBox
	 *
	 * @param logo
	 */
	public void setAppIcon(Image logo) {
		this.title.setApplicationLogo(new ImageView(logo));
	}

	/**
	 * Start the overlay. Disables all UI
	 *
	 * @param content
	 *            String to display
	 */
	public void startLoadingOverlay(String content) {
		if (Platform.isFxApplicationThread()) {
			System.out.println("Starting loading overlay");
			this.center.lock(content);
		} else {
			Platform.runLater(() -> this.startLoadingOverlay(content));
		}
	}

	/**
	 * Start the overlay. Disables all UI. If canceled the runnable will be called
	 *
	 * @param content
	 *            Message to be displayed on the lock overlay when the screen is locked
	 * @param onUnlock
	 *            Runnable to be called if the user cancels the locking
	 */
	public void startLoadingOverlay(String content, Runnable onUnlock) {
		if (Platform.isFxApplicationThread()) {
			System.out.println("Starting loading overlay");
			this.center.lock(content, onUnlock);
		} else {
			Platform.runLater(() -> this.startLoadingOverlay(content, onUnlock));
		}
	}
	
	/**
	 * Show the locking overlay - disables all UI in main node. The provided Node(s) will be displayed on the overlay,
	 * added as children of the overlay {@link StackPane}.
	 * 
	 * @param graphics
	 *            Node(s) to display on the overlay.
	 */
	public void startOverlay(Node... graphics) {
		if (Platform.isFxApplicationThread()) {
			this.center.lock(graphics);
		} else {
			Platform.runLater(() -> this.startOverlay(graphics));
		}
	}

	/**
	 * Stop the overlay. Re-enables the UI
	 */
	public void stopLoadingOverlay() {
		if (Platform.isFxApplicationThread()) {
			System.out.println("Stopping loading overlay");
			this.center.unlock();
		} else {
			Platform.runLater(() -> this.stopLoadingOverlay());
		}
	}	

	/**
	 * Set the screen to display. Checks if on the fx thread to not cause errors
	 *
	 * @param screen
	 */
	private void setRootScreen(SubScreen screen) {

		if (Platform.isFxApplicationThread()) {
			Node rootNode = screen.getRoot();
			this.anchor(rootNode);
			this.center.setRootContent(rootNode);
			this.title.addExtraMenuOption(screen.onShow());
			this.title.setScreenName(this.root.getTitle());
		} else {
			Platform.runLater(() -> this.setRootScreen(screen));
		}
	}

	/**
	 * Set the node to be expanded in the anchor pane
	 *
	 * @param node
	 */
	private void anchor(Node node) {
		AnchorPane.setBottomAnchor(node, 0d);
		AnchorPane.setTopAnchor(node, 0d);
		AnchorPane.setRightAnchor(node, 0d);
		AnchorPane.setLeftAnchor(node, 0d);
	}

	private void resizeImageViews(Parent n) {
		for (Node child : n.getChildrenUnmodifiable()) {
			if (child instanceof Parent) {
				this.resizeImageViews((Parent) child);
			} else if (child instanceof ImageView) {
				ImageView i;
				if ((i = (ImageView) child).getLayoutBounds().getWidth() > 256) {
					System.out.println("Resized image " + i.toString() + " to fit 256px wide");
					i.setFitWidth(256);
					i.setPreserveRatio(true);
				}
			}

		}
	}
    public TitleBar getTitle(){
        return title;
    }
}
