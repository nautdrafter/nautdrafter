/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.network.DraftingServer;

/**
 * Menu that dynamically adds the currently running servers when it opens
 */
public class ServerMenu extends Menu {

	private final MenuItem         empty;
	ObservableList<DraftingServer> servers;

	public ServerMenu() {
		super(LanguageResource.getString("global.menu.server"));
		this.servers = NautDrafter.getInstance().getRunningServers();
		this.empty = new MenuItem(LanguageResource.getString("global.menu.server.none"));
		this.empty.disableProperty().set(true);
	}

	/**
	 * Look through the server list and add in the items
	 */
	protected void buildItems() {
		this.getItems().clear();
		if (this.servers.isEmpty()) {
			this.getItems().add(this.empty);
			return;
		}
		for (DraftingServer d : this.servers) {
			MenuItem item = new MenuItem(d.name);
			System.out.println("Added server " + d.name);
			item.setOnAction(event -> {
				Stage popout = new ServerDetailsWindow((DraftingServer) item.getProperties().get("server"));
				popout.show();
				Platform.runLater(() -> popout.requestFocus());
			});
			item.getProperties().put("server", d);
			this.getItems().add(item);
		}
	}

	
}
