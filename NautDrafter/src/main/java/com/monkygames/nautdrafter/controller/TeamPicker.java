/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.PlayerIdentifier;
import com.monkygames.nautdrafter.network.Team;
import com.monkygames.nautdrafter.resource.ImageCacher;

/**
 * A UI item that allows the user to pick slots in a team
 *
 * @author Adam
 */
public class TeamPicker extends VBox {

	private TextField           teamNameField;

	private TeamSelectionToggle captainButton;

	private TeamSelectionToggle crewButton1;

	private TeamSelectionToggle crewButton2;

	private ToggleGroup         group;

	private String              name;
	private boolean             isFull;

	private Player[]            players = new Player[3];

	/**
	 * Set up an base for the UI item with default values
	 */
	public TeamPicker() {
		this.getStyleClass().addAll("team", "panel");

		HBox textFieldContainer = new HBox(this.teamNameField = new TextField());

		this.teamNameField.setEditable(false);

		// set the [enter] key action to lose the focus of the textField
		this.teamNameField.setOnAction(action -> {
			String text;
			if ((text = this.teamNameField.getText()) == null || text.isEmpty()) {
				return;
			}

			textFieldContainer.requestFocus();
		});

		// When the textField loses focus fire a name change event
		this.teamNameField.focusedProperty().addListener((ov, oldv, newv) -> {

			String text = this.teamNameField.getText();

			if (!newv && text != null && !text.isEmpty() && !text.equals(this.name)) {
				this.name = text;
				// Fire name change event
				if (this.nameChanger != null) {
					this.nameChanger.onNameChange(text);
				}
			} else {
				this.teamNameField.setText(this.name);
			}
		});

		textFieldContainer.setAlignment(Pos.CENTER);
		VBox.setVgrow(textFieldContainer, Priority.ALWAYS);

		// Container for captain toggle and lable
		VBox captainContainer = new VBox(new Label(LanguageResource.getString("team_picker.captain")),
		        this.captainButton = new TeamSelectionToggle(Team.CAPTAIN));
		this.captainButton.setPrefSize(64, 64);
		captainContainer.setAlignment(Pos.CENTER);
		VBox.setVgrow(captainContainer, Priority.ALWAYS);

		// Container for both crew toggles
		HBox crewIconsBox = new HBox(this.crewButton1 = new TeamSelectionToggle(Team.CREW_1),
		        this.crewButton2 = new TeamSelectionToggle(Team.CREW_2));
		crewIconsBox.setAlignment(Pos.CENTER);
		crewIconsBox.setStyle("-fx-spacing: 16px;");
		this.crewButton1.setPrefSize(64, 64);
		this.crewButton2.setPrefSize(64, 64);

		// Container for crew lable and combined container for the crew toggles
		VBox crewContainer = new VBox(new Label(LanguageResource.getString("team_picker.crew")), crewIconsBox);
		crewContainer.setAlignment(Pos.TOP_CENTER);
		VBox.setVgrow(crewContainer, Priority.ALWAYS);

		// Add children to this node
		this.getChildren().addAll(textFieldContainer, captainContainer, crewContainer);

		// Set up the toggle group
		this.group = new ToggleGroup();
		this.captainButton.setToggleGroup(this.group);
		this.crewButton1.setToggleGroup(this.group);
		this.crewButton2.setToggleGroup(this.group);

	}

	/**
	 * Property to determine what team this picker belongs to
	 */
	private IntegerProperty team;

	/**
	 * get the team Property. The team property determines what team this picker belongs to
	 *
	 * @return the existing team property or a new copy if it didn't exist
	 */
	public final IntegerProperty teamProperty() {
		if (this.team == null) {
			this.team = new SimpleIntegerProperty() {

				@Override
				public Object getBean() {
					return TeamPicker.this;
				}

				@Override
				public String getName() {
					return "team";
				}

				@Override
				public void invalidated() {
					Platform.runLater(() -> {
						TeamPicker.this.teamNameField.setPromptText(TeamPicker.this.getTeamName());
						TeamPicker.this.getStyleClass().remove(this.get() == Team.RED ? "blue-team" : "red-team");
						TeamPicker.this.getStyleClass().add(this.get() == Team.RED ? "red-team" : "blue-team");
					});
				}

			};
		}
		return this.team;
	}

	/**
	 * Set which team this team picker belongs to (determines which style class is associated with this item)
	 *
	 * @param value
	 *            one of three constants from {@link Team} (BLUE,RED,NONE)
	 */
	public final void setTeam(int value) {
		this.teamProperty().set(value);
	}

	/**
	 * Get the constant representing which team this item belongs to (default is {@link Team}.NONE)
	 */
	public final int getTeam() {
		return this.team == null ? 0 : this.team.get();
	}

	/**
	 * Get a string representation of the teams name (used for text prompt)
	 */
	public final String getTeamName() {
		switch (this.getTeam()) {
		case Team.BLUE:
			return LanguageResource.getString("team_picker.blue");
		case Team.RED:
			return LanguageResource.getString("team_picker.red");
		default:
			return "Team not defined";
		}
	}

	/**
	 * Set the valid selection listeners for this item
	 *
	 * @param listener
	 *            listener to be used when a valid event happens
	 */
	public void setValidSelectionListener(ValidSelectionListener listener) {
		this.captainButton.setValidSelectionListener(listener);
		this.crewButton1.setValidSelectionListener(listener);
		this.crewButton2.setValidSelectionListener(listener);
	}

	private NameChangeListener nameChanger;

	/**
	 * Set a listener for when a the name of the team changes (when the text field loses focus or [Enter] is pressed)
	 *
	 * @param listener
	 *            the listener to be used when the event happens
	 */
	public void setNameChangeListener(NameChangeListener listener) {
		this.nameChanger = listener;
	}

	/**
	 * Set a position to be the player specified
	 *
	 * @param position
	 *            see {@link Team} for position constants
	 * @param player
	 *            {@link Player} object representing the player
	 */
	public void setSelection(int position, Player player) {
		this.players[position] = player;
		switch (position) {
		case Team.CAPTAIN:
			this.captainButton.setPlayer(player, player.name, ImageCacher.get(player.iconUrl));
			// Enabling the text box for editing the team name if the local player just became captain
			if (NautDrafter.isMe(player)) {
				this.teamNameField.setEditable(true);
			}

			break;
		case Team.CREW_1:
			this.crewButton1.setPlayer(player, player.name, ImageCacher.get(player.iconUrl));
			break;
		case Team.CREW_2:
			this.crewButton2.setPlayer(player, player.name, ImageCacher.get(player.iconUrl));
			break;
		}

		if (this.isFull()) {
			if (this.isFull) {
				return;
			}

			this.isFull = true;
			if (this.fullTeamListener != null) {
				Player local = NautDrafter.getBase().getPlayer();

				this.fullTeamListener.onFullTeam(true, NautDrafter.isPlayer() && this.hasPlayer(local),
				        NautDrafter.isPlayer() && this.captainButton.player.equals(local));
			}
		}
	}

	/**
	 * Remove this player from any spot
	 *
	 * @param player
	 */
	public void deselect(Player player) {

		this.captainButton.deselectConditional(player);
		this.crewButton1.deselectConditional(player);
		this.crewButton2.deselectConditional(player);
		if (this.captainButton.player == null || this.crewButton1.player == null || this.crewButton2.player == null) {
			Player local = NautDrafter.getBase().getPlayer();
			if (!this.isFull) {
				return;
			} else if (player.equals(local)) {
				if (this.fullTeamListener != null) {
					this.fullTeamListener.onFullTeam(false, true, false);
				}
				this.isFull = false;
				return;
			}
			this.isFull = false;
			if (this.fullTeamListener != null) {
				this.fullTeamListener.onFullTeam(
				        false,
				        NautDrafter.isPlayer() && this.hasPlayer(local),
				        NautDrafter.isPlayer() && this.captainButton.player != null
				                && this.captainButton.player.equals(local));
			}
		}
	}

	/**
	 * Get the {@link Team} object that corresponds to this team picker<br/>
	 * If the team is not full, null will be returned
	 *
	 * @return
	 */
	public Team getFullTeam() {
		if (!this.isFull) {
			return null;
		}
		return new Team(this.name, this.players[0], this.players[1], this.players[2]);
	}

	/**
	 * Check to see if the team is full
	 *
	 * @return true if the team is full
	 */
	public boolean isFull() {
		return this.captainButton.player != null && this.crewButton1.player != null && this.crewButton2.player != null;
	}

	/**
	 * Manually set the team name. Does not fire the {@link NameChangeListener}
	 *
	 * @param name
	 *            The new name to use
	 */
	public void setTeamName(String name) {
		this.teamNameField.setText(name);
		this.name = name;
	}

	/**
	 * Listener for when a valid action occurs in a team selection toggle
	 */
	public interface ValidSelectionListener {
		/**
		 * Called when a valid selection is made<br/>
		 * Allows for the event to be fired when the position is selected if and only if the position being selected is
		 * the captain spot<br/>
		 * Captain conflict resolution is done server side
		 *
		 * @param position
		 *            Position of toggle (CAPTAIN/CREW)
		 * @param type
		 *            true for selection, false for de selection
		 */
		public void onValidSelection(int position, boolean type);

	}

	/**
	 * Listener for a name change
	 */
	public interface NameChangeListener {
		/**
		 * Called when the team name has changed in the team picker
		 *
		 * @param newName
		 */
		public void onNameChange(String newName);
	}

	/**
	 * Listener for when a team becomes full or a full team loses a player
	 */
	public interface FullTeamListener {
		/**
		 * Called when all the spots in a team become selected<br/>
		 * Also gets called when a once full team loses a member
		 *
		 * @param isFull
		 *            true if the team became full on the last selection, false if a player left a full team on the last
		 *            selection
		 * @param inThisTeam
		 *            true if the local player is in this team
		 * @param isCaptain
		 *            true if the local player is the captain of the team, false in all other cases
		 */
		public void onFullTeam(boolean isFull, boolean inThisTeam, boolean isCaptain);
	}

	private FullTeamListener fullTeamListener;

	/**
	 * Set the listener for when a team becomes full or loses a player from a full team
	 *
	 * @param l
	 *            listener to call when a this action occurs
	 */
	public void setFulTeamListener(FullTeamListener l) {
		this.fullTeamListener = l;
	}

	/**
	 * Set to true to disable UI but not show any visible effects of disabling UI
	 *
	 * @param b
	 */
	public void setSpectatorMode(boolean b) {
		this.setDisable(true);
		this.getStyleClass().add("spectator");
	}

	/**
	 * Check to see if the player is already in this team.
	 *
	 * @param player
	 * @return True if in this team, false otherwise
	 */
	public boolean hasPlayer(Player player) {
		return player != null
		        && (player.equals(this.captainButton.player) || player.equals(this.crewButton1.player) || player
		                .equals(this.crewButton2.player));
	}

	/**
	 * A custom toggle button for the team picker
	 *
	 * @author Adam
	 */
	private class TeamSelectionToggle extends ToggleButton {

		private ImageView        icon;
		private PlayerIdentifier player;
		private int              position;
		protected Image          empty;

		/**
		 * Create a new toggle with which position the button is for
		 *
		 * @param position
		 *            use CAPTAIN, CREW_1 or CREW_2
		 */
		public TeamSelectionToggle(int position) {
			super();
			this.position = position;
			this.icon = new ImageView();
			this.setGraphic(this.icon);
			this.icon.setFitHeight(64);
			this.icon.setFitWidth(64);

			this.getStyleClass().add("picker");
			this.getStyleClass().add("empty");
		}

		/**
		 * Set the player for this toggle
		 *
		 * @param id
		 *            Steam id for the player
		 * @param name
		 *            Name of the player
		 * @param i
		 *            Icon for the player
		 */
		public void setPlayer(PlayerIdentifier pid, String name, Image i) {
			this.icon.setImage(i);
			this.player = pid;
			this.setTooltip(new Tooltip(name));
			this.setIsPlayerSet(true);
		}

		/**
		 * Removes all player details from this toggle so that it can be selected again by anyone
		 */
		public void deselect() {
			// Disabling the text box for editing the team name if the local player just left the captain position
			if (this.position == Team.CAPTAIN && NautDrafter.isMe(this.player)) {
				TeamPicker.this.teamNameField.setEditable(false);
			}

			this.setIsPlayerSet(false);
			this.setSelected(false);
			this.setTooltip(null);
		}

		/**
		 * Deselect if and only if this.player == player
		 *
		 * @param player
		 */
		public void deselectConditional(Player player) {
			if (this.player != null && this.player.equals(player)) {
				this.deselect();
			}
		}

		/**
		 * Handles the actions and determines what to do
		 */
		@Override
		public void fire() {

			// Check for being disabled or a spectator
			if (!this.isDisabled()) {

				// Check for existing player that is not the local player
				if (this.getIsPlayerSet() && !NautDrafter.isMe(this.player)) {

					// If this is the captain toggle fire of a captain conflict event
					if (this.position == Team.CAPTAIN) {
						if (this.listener != null) {
							this.listener.onValidSelection(this.position, true);
							System.out
							        .println("Start conflict resolution: Captain position is disputed, current captain = "
							                + this.player + " disputed by " + NautDrafter.getBase().getPlayer());
						}
					} else { // Else consume event
						System.out.println("Cancle slection: another player already there");
					}
					return;
				}

				// If we get to here then it is a valid selection, just check if player is not set for which event is
				// happening
				if (this.listener != null) {
					this.listener.onValidSelection(this.position, this.player == null);
					this.setSelected(this.player == null);
				}

				this.fireEvent(new ActionEvent());
			}
		}

		/**
		 * property for whether the player is set
		 */
		private BooleanProperty setPlayer;

		/**
		 * Get the playerSet property of this toggle
		 *
		 * @return the {@link BooleanProperty} that determines if this player is set
		 */
		private BooleanProperty setPlayerProperty() {
			if (this.setPlayer == null) {
				this.setPlayer = new SimpleBooleanProperty() {

					@Override
					public Object getBean() {
						return TeamSelectionToggle.this;
					}

					@Override
					public String getName() {
						return "team";
					}

					@Override
					public void invalidated() {

						if (!TeamSelectionToggle.this.setPlayer.get()) {
							TeamSelectionToggle.this.icon.setImage(TeamSelectionToggle.this.empty);
							TeamSelectionToggle.this.player = null;
							if (!TeamSelectionToggle.this.getStyleClass().contains("empty")) {
								TeamSelectionToggle.this.getStyleClass().add("empty");
							}
						} else {
							TeamSelectionToggle.this.getStyleClass().remove("empty");
						}

					}
				};
			}
			return this.setPlayer;
		}

		private boolean setIsPlayerSet(boolean value) {
			if (value && this.getIsPlayerSet()) {
				return false;
			}
			this.setPlayerProperty().set(value);
			return true;
		}

		/**
		 * @return true if the player is set for this toggle
		 */
		public final boolean getIsPlayerSet() {
			return this.setPlayer == null ? false : this.setPlayer.get();
		}

		private ValidSelectionListener listener;

		/**
		 * Set the listener for when a valid action occurs
		 *
		 * @param l
		 *            listener to call when a valid action occurs
		 */
		public void setValidSelectionListener(ValidSelectionListener l) {
			this.listener = l;
		}
	}
}
