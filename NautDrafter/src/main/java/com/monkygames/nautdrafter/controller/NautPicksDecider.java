/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.IOException;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.model.Character;
import com.monkygames.nautdrafter.model.Character.IconType;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.network.DraftingClient;
import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.PlayerIdentifier;
import com.monkygames.nautdrafter.network.Team;
import com.monkygames.nautdrafter.network.callbacks.NautChooseCallback;

/**
 * UI element to allow the players to choose which Naut they want to play after drafting has finished
 */
public class NautPicksDecider extends VBox implements NautChooseCallback {

	@FXML
	private ImageView            naut1;
	@FXML
	private Label                name1;
	@FXML
	private ImageView            naut2;
	@FXML
	private Label                name2;
	@FXML
	private ImageView            naut3;
	@FXML
	private Label                name3;
	@FXML
	private Button               actionButton;
	@FXML
	private NautelectionToggle   pos1;
	@FXML
	private NautelectionToggle   pos2;
	@FXML
	private NautelectionToggle   pos3;

	private IntegerProperty      team;

	private ImageView[]          nautIcons;
	private Label[]              playerNames;
	private NautelectionToggle[] buttons;
	private Player               localPlayer;
	private boolean              isCaptain;

	private ToggleGroup          group   = new ToggleGroup();
	private Player[]             players = new Player[3];
	private String[]             buttonValues;
	private SimpleStringProperty buttonStrings;

	/**
	 * Create a new element from the xml
	 *
	 * @param isCaptain
	 *            true if the local player is a captain of a team
	 */
	public NautPicksDecider(boolean isCaptain) {
		ResourceBundle b;
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
				"/com/monkygames/nautdrafter/view/NautPicksDecider.fxml"), b = LanguageResource.getLanguageResource());
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		this.isCaptain = isCaptain;
		this.buttonValues = new String[] { b.getString("drafting.waiting"), b.getString("drafting.ready") };
		this.playerNames = new Label[] { this.name1, this.name2, this.name3 };
		this.nautIcons = new ImageView[] { this.naut1, this.naut2, this.naut3 };
		this.buttons = new NautelectionToggle[] { this.pos1, this.pos2, this.pos3 };
		this.pos1.setToggleGroup(this.group);
		this.pos2.setToggleGroup(this.group);
		this.pos3.setToggleGroup(this.group);

		this.pos1.setOnAction(event -> {
			if (this.localPlayer.equals(this.pos1.taken)) {
				DraftingClient.selectNaut((byte) -1);
			} else {
				DraftingClient.selectNaut((byte) 0);
			}
		});

		this.pos2.setOnAction(event -> {
			if (this.localPlayer.equals(this.pos2.taken)) {
				DraftingClient.selectNaut((byte) -1);
			} else {
				DraftingClient.selectNaut((byte) 1);
			}
		});

		this.pos3.setOnAction(event -> {
			if (this.localPlayer.equals(this.pos3.taken)) {
				DraftingClient.selectNaut((byte) -1);
			} else {
				DraftingClient.selectNaut((byte) 2);
			}
		});

		this.actionButton.setOnAction(event -> {
			System.out.println("READY TO PROCEED TO VS SCREEN");
			DraftingClient.lockTeam();
		});

		this.actionButton.setDisable(true);

		this.buttonStrings = new SimpleStringProperty(this.buttonValues[0]);
		this.actionButton.textProperty().bind(this.buttonStrings);

		DraftingClient.addCallbacks(this);
	}

	/**
	 * Fill out the selected naut
	 *
	 * @param thisTeam
	 *            {@link Team} object that contains the drafting info of the local players team
	 * @param me
	 *            the local player
	 */
	public void init(Team thisTeam, Player me) {
		for (byte i = 0; i < 3; i++) {
			this.nautIcons[i].setImage(Character.getById(thisTeam.pickedNauts[i]).getImage(IconType.RENDER));
			this.buttons[i].local = me;
			if (thisTeam.nautSelections[i] != null) {
				this.onPlayerChoose(thisTeam.nautSelections[i], (byte) this.getTeam(), i);
			}
		}
		this.localPlayer = me;
	}

	/**
	 * Set the player that will be using the Naut when in the actual game
	 *
	 * @param player
	 *            the player that selected this spot
	 * @param position
	 *            which index the selection was for
	 */
	@Override
	public void onPlayerChoose(Player player, byte team, byte position) {
		if (!Platform.isFxApplicationThread()) {
			Platform.runLater(() -> this.onPlayerChoose(player, team, position));
			return;
		}

		if (team != this.getTeam()) {
			return;
		}

		this.deselectPlayer(player);

		if (position == -1) {
			return;
		}

		this.players[position] = player;
		this.playerNames[position].setText(player.name);
		this.playerNames[position].getStyleClass().add("chosen");
		this.buttons[position].taken = player;

		if (this.isFull()) {
			this.buttonStrings.set(this.buttonValues[1]);

			if (this.isCaptain) {
				this.actionButton.setDisable(false);
			}
		}
	}

	/**
	 * Clear a player from the given position
	 *
	 * @param position
	 *            position of the de-selection
	 */
	public void deselectPlayer(Player player) {
		for (int i = 0; i < this.players.length; i++) {
			if (this.players[i] != null && this.players[i].equals(player)) {
				this.playerNames[i].setText(null);
				this.playerNames[i].getStyleClass().remove("chosen");
				this.buttons[i].taken = null;
				this.players[i] = null;
				break;
			}
		}
		if (!this.isFull()) {
			this.actionButton.setDisable(true);
			this.buttonStrings.set(this.buttonValues[0]);
		}
	}

	/**
	 * The team property determines which team this UI element will be styled for
	 *
	 * @return the existing team property or a new copy if it didn't exist
	 */
	public final IntegerProperty teamProperty() {
		if (this.team == null) {
			this.team = new SimpleIntegerProperty() {

				@Override
				public Object getBean() {
					return NautPicksDecider.this;
				}

				@Override
				public String getName() {
					return "team";
				}

				@Override
				public void invalidated() {

					NautPicksDecider.this.getStyleClass().removeAll("blue-team", "red-team");
					if (this.get() == Team.NONE) {
						return;
					}
					NautPicksDecider.this.getStyleClass().add(this.get() == Team.RED ? "red-team" : "blue-team");

				}

			};
		}
		return this.team;
	}

	/**
	 * Set which team this team picker belongs to (determines which style class is associated with this item)
	 *
	 * @param value
	 *            one of three constants from {@link Team} (BLUE,RED,NONE)
	 */
	public final void setTeam(int value) {
		this.teamProperty().set(value);
	}

	/**
	 * Get the constant representing which team this item belongs to (default is {@link Team}.NONE)
	 */
	public final int getTeam() {
		return this.team == null ? 0 : this.team.get();
	}

	/**
	 * Check for a full team
	 *
	 * @return true if all spots are taken, false otherwise
	 */
	private boolean isFull() {
		return this.players[0] != null && this.players[1] != null && this.players[2] != null;
	}

	/**
	 * custom toggle button for choosing the Nauts
	 */
	public static class NautelectionToggle extends ToggleButton {

		/**
		 * Create a new toggle but have no padding
		 */
		public NautelectionToggle() {
			super();
			this.getStyleClass().add("no-padding");
		}

		public PlayerIdentifier taken;
		public PlayerIdentifier local;

		// fire if not already taken or its the local player that is deselecting
		@Override
		public void fire() {
			if (!this.isDisabled() && (this.taken == null || this.taken.equals(this.local))) {
				this.setSelected(!this.isSelected());
				this.fireEvent(new ActionEvent());
			}
		}
	}
}
