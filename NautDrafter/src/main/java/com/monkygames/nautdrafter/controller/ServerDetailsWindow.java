/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.channels.SelectionKey;
import java.util.Map.Entry;
import java.util.MissingResourceException;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.model.Planet;
import com.monkygames.nautdrafter.model.Planet.ImageType;
import com.monkygames.nautdrafter.network.DraftingClient;
import com.monkygames.nautdrafter.network.DraftingServer;
import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.PlayerIdentifier;
import com.monkygames.nautdrafter.network.Team;
import com.monkygames.nautdrafter.network.Timeline;
import com.monkygames.nautdrafter.network.Timeline.TimelineItem;
import com.monkygames.nautdrafter.network.callbacks.ConnectionCallback;
import com.monkygames.nautdrafter.view.NotificationPopUp;
import com.monkygames.nautdrafter.view.PlayerDisplayer;

/**
 * Window to show when selecting a drafting server from the "hosted server" menu <br/>
 * On this window are buttons to be able to kill or reset a server<br/>
 * Also shows most of the details about the running server including a summary of the details and players in the server<br/>
 * Also the controller for the fxml of the root BorderPane
 */
public class ServerDetailsWindow extends Stage implements ConnectionCallback {

	@FXML
	private Label                                         serverName;
	@FXML
	private Label                                         serverDetails;
	@FXML
	private Button                                        resetButton;
	@FXML
	private Button                                        killButton;
	@FXML
	private Label                                         mapName;
	@FXML
	private ImageView                                     mapImage;
	@FXML
	private TableView<PlayerTableItem>                    playerTable;
	@FXML
	private TableColumn<PlayerTableItem, PlayerDisplayer> playerColumn;
	@FXML
	private TableColumn<PlayerTableItem, HBox>            teamColumn;
	@FXML
	private TableColumn<PlayerTableItem, HBox>            positionColumn;
	@FXML
	private Label                                         currentStage;
	@FXML
	private HBox                                          draftingOrder;
	@FXML
	private Label                                         timeElapsed;
	@FXML
	private Label                                         port;
	@FXML
	private Label                                         viewers;
	@FXML
	private ScrollPane                                    mainScrollPane;
	@FXML
	private Label                                         passwordLabel;
	@FXML
	private Label                                         redTeamName;
	@FXML
	private Label                                         blueTeamName;
	@FXML
	private Label                                         banType;
	@FXML
	private Label                                         timeOut;
	@FXML
	private TitledPane                                    passwordDisplay;
	@FXML
	private VBox                                          mainVbox;
	@FXML
	private Button                                        spectateButton;
	@FXML
	private Button                                        playerButton;
	private BorderPane                                    root;

	private DraftingServer                                server;

	/**
	 * Create a new window with the details of the server
	 * 
	 * @param server
	 *            The server whose details should be shown
	 */
	public ServerDetailsWindow(DraftingServer server) {
		// Load xml
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
		        "/com/monkygames/nautdrafter/view/ServerDetailsWindow.fxml"), LanguageResource.getLanguageResource());

		fxmlLoader.setController(this);
		try {
			this.root = fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		
		this.initOwner(NautDrafter.stage);

		this.setScene(new Scene(this.root));
		this.sizeToScene();
		this.centerOnScreen();
		this.root.getStylesheets().add("style/Base.css");

		this.killButton.setOnAction(event -> {
			server.shutdown();
			NautDrafter.getInstance().getRunningServers().remove(server);
			this.close();
		});

		this.resetButton.setOnAction(event -> {
			server.restartServer();
		});

		this.spectateButton.setOnAction(event -> {
			try {
				DraftingClient.connectTo(InetAddress.getByName("localhost"), server.getPort(), null, this,
				        server.password);
				NautDrafter.setIsPlayer(false);

			} catch (UnknownHostException e) {
				new NotificationPopUp(LanguageResource.getString("join_errors.error0"),
				        NotificationPopUp.LENGTH_MEDIUM).showNotification(this.getScene());
			}
			NautDrafter.setIsPlayer(false);
		});

		this.playerButton.setOnAction(event -> {
			try {
				DraftingClient.connectTo(InetAddress.getByName("localhost"), server.getPort(), NautDrafter.getBase()
				        .getPlayer(), this, server.password);
				NautDrafter.setIsPlayer(false);

			} catch (UnknownHostException e) {
				new NotificationPopUp(LanguageResource.getString("join_errors.error0"),
				        NotificationPopUp.LENGTH_MEDIUM).showNotification(this.getScene());
			}
			NautDrafter.setIsPlayer(true);
		});

		this.server = server;
		this.setTitle(server.name);
		this.getIcons().addAll((new Image(this.getClass().getResourceAsStream("/assets/images/app-icons/16px.png"))),
		        (new Image(this.getClass().getResourceAsStream("/assets/images/app-icons/32px.png"))),
		        (new Image(this.getClass().getResourceAsStream("/assets/images/app-icons/64px.png"))),
		        (new Image(this.getClass().getResourceAsStream("/assets/images/app-icons/128px.png"))));

		// Send the scroll event to the main scrollpane to stop the table eating the focus
		this.playerTable.addEventFilter(ScrollEvent.ANY, scrollEvent -> {
			this.mainScrollPane.fireEvent(scrollEvent.copyFor(this.playerTable, this.mainScrollPane));
			scrollEvent.consume();
		});

		// If there was no password, don't show the password box
		if (server.password == null) {
			this.mainVbox.getChildren().remove(this.passwordDisplay);
		}

		// Empty table placeholder
		VBox placeholder = new VBox(new Label(LanguageResource.getString("server_details.no_players")));
		placeholder.setAlignment(Pos.CENTER);
		this.playerTable.setPlaceholder(placeholder);

		// Fill data
		this.fillData();

		// Start the refresher thread
		RefreshWorker refresher = new RefreshWorker();
		refresher.start();

		// When closed kill the refresher
		this.setOnHidden(event -> refresher.kill());
	}

	/**
	 * Fills out the data in the window
	 */
	private void fillData() {
		if (Platform.isFxApplicationThread()) {
			this.serverName.setText(this.server.name);
			this.serverDetails.setText(this.server.description);
			Planet map;
			this.mapName.setText((map = Planet.getById(this.server.mapId)).getName());
			this.mapImage.setImage(map.getImage(ImageType.MAP));

			this.currentStage.setText(this.nameOfStage(this.server.getStage()));
			this.timeElapsed.setText(this.minuteFormat(System.currentTimeMillis() - this.server.initTime));

			this.port.setText("" + this.server.getPort());

			this.viewers.setText(this.server.clients.size() + "");
			this.draftingOrder.getChildren().clear();
			for (TimelineItem item : this.server.timeline.items) {
				Label text = new Label(item.type == Timeline.PICK ? "Pick" : "Ban");
				text.getStyleClass().addAll(item.team == Team.RED ? "red-team" : "blue-team", "colour");
				this.draftingOrder.getChildren().add(text);
			}

			this.passwordLabel.setText(this.server.password);

			this.redTeamName.setText(this.server.teamRed.name);
			this.blueTeamName.setText(this.server.teamBlue.name);

			this.banType.setText(LanguageResource.getString(
			        this.server.banBidirectional ? "server_config.ban_type.bi_directional"
			                : "server_config.ban_type.one_way"));
			
			this.timeOut.setText(LanguageResource.getString("server_config.out_of_time.random"));
			
			PlayerTableItem i = this.playerTable.getSelectionModel().getSelectedItem();

			this.playerTable.getItems().clear();
			for (Entry<SelectionKey, Player> e : this.server.players.entrySet()) {
				Player p = e.getValue();
				this.playerTable.getItems().add(new PlayerTableItem(p, this.server.teamRed, this.server.teamBlue));
			}
			if (i != null) {
				this.playerTable.getSelectionModel().select(this.playerTable.getItems().indexOf(i));
			}

			this.playerColumn.setCellValueFactory((CellDataFeatures<PlayerTableItem, PlayerDisplayer> p) -> p
			        .getValue().playerDispProperty());
			this.playerColumn
			        .setCellFactory((TableColumn<PlayerTableItem, PlayerDisplayer> p) -> new TableCell<PlayerTableItem, PlayerDisplayer>() {
				        @Override
				        public void updateItem(final PlayerDisplayer source, boolean empty) {
					        super.updateItem(source, empty);
					        if (!empty) {
						        this.setGraphic(source);
					        } else {
						        this.setGraphic(null);
					        }
				        }
			        });

			this.teamColumn.setCellValueFactory((CellDataFeatures<PlayerTableItem, HBox> p) -> p.getValue()
			        .teamProperty());
			this.teamColumn
			        .setCellFactory((TableColumn<PlayerTableItem, HBox> p) -> new TableCell<PlayerTableItem, HBox>() {
				        @Override
				        public void updateItem(final HBox source, boolean empty) {
					        super.updateItem(source, empty);
					        if (!empty) {
						        this.setGraphic(source);
					        } else {
						        this.setGraphic(null);
					        }
				        }
			        });

			this.positionColumn.setCellValueFactory((CellDataFeatures<PlayerTableItem, HBox> p) -> p.getValue()
			        .positionProperty());
			this.positionColumn
			        .setCellFactory((TableColumn<PlayerTableItem, HBox> p) -> new TableCell<PlayerTableItem, HBox>() {
				        @Override
				        public void updateItem(final HBox source, boolean empty) {
					        super.updateItem(source, empty);
					        if (!empty) {
						        this.setGraphic(source);
					        } else {
						        this.setGraphic(null);
					        }
				        }
			        });
		} else {
			Platform.runLater(() -> this.fillData());
		}
	}

	private String minuteFormat(long l) {
		l /= 1000;
		return String.format("%d:%02d", (int) (l / 60), (int) (l % 60));
	}

	/**
	 * Gets the name of the stage from the id
	 * 
	 * @param stage
	 *            id of the stage
	 * @return the name
	 */
	private String nameOfStage(byte stage) {
		switch (stage) {
		case DraftingServer.STAGE_TEAM_SELECTION:
			return "Team Selection";
		case DraftingServer.STAGE_DRAFTING:
			return "Drafting";
		case DraftingServer.STAGE_NAUT_SELECTION:
			return "Naut Selection";
		case DraftingServer.STAGE_VERSUS:
			return "Versus";
		default:
			return "Unknown";
		}
	}

	/**
	 * Table item for the Players info table<br/>
	 * Player names and icons are show in the {@link PlayerDisplayer}, the team is coloured and shown in the
	 * {@link #team} HBox, and the position is shown in the {@link #position} HBox
	 */
	private static class PlayerTableItem {

		private ObjectProperty<PlayerDisplayer> playerDisp;
		private ObjectProperty<HBox>            team;
		private ObjectProperty<HBox>            position;

		private PlayerIdentifier                pid;

		PlayerTableItem(Player player, Team red, Team blue) {
			this.playerDisp = new SimpleObjectProperty<>(new PlayerDisplayer(player));
			Label team = new Label(red.hasPlayer(player) ? "Red" : blue.hasPlayer(player) ? "Blue" : "None");
			team.getStyleClass().addAll(
			        red.hasPlayer(player) ? "red-team" : blue.hasPlayer(player) ? "blue-team" : "no-team", "colour");
			HBox holder1 = new HBox(team);
			holder1.getStyleClass().add("team");
			holder1.setAlignment(Pos.CENTER_LEFT);
			this.team = new SimpleObjectProperty<>(holder1);

			Label position = new Label(player.equals(red.players[0]) || player.equals(blue.players[0]) ? "Captain"
			        : team.getText().equals("None") ? "-" : "Crew");
			HBox holder2 = new HBox(position);
			holder2.setAlignment(Pos.CENTER_LEFT);
			this.position = new SimpleObjectProperty<>(holder2);

			this.pid = player;
		}

		ObjectProperty<PlayerDisplayer> playerDispProperty() {
			return this.playerDisp;
		}

		ObjectProperty<HBox> teamProperty() {
			return this.team;
		}

		ObjectProperty<HBox> positionProperty() {
			return this.position;
		}

		@Override
		public boolean equals(Object o) {
			if (o == null) {
				return false;
			}
			if (o == this) {
				return true;
			}
			if (o instanceof PlayerTableItem) {
				PlayerTableItem p = (PlayerTableItem) o;
				return p.pid.equals(this.pid);
			}
			return false;
		}
	}

	/**
	 * Thread that refreshed the window every five seconds
	 */
	private class RefreshWorker extends Thread {

		private boolean showing = true;

		@Override
		public void run() {
			System.out.println("Refresher started");
			while (this.showing) {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					if (this.showing) {
						e.printStackTrace();
					}
				}
				ServerDetailsWindow.this.fillData();
			}
			System.out.println("Refresher stoped");
		}

		/**
		 * Stop the thread cleanly
		 */
		public void kill() {
			this.showing = false;
			this.interrupt();
			try {
				this.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onConnectionSucceeded(int stage) {
		if (Platform.isFxApplicationThread()) {

			// Go back to server browser screen if applicable
			NautDrafter.getBase().popMain(LanguageResource.getString("server_browser.screen_name"));

			switch (stage) {
			case DraftingServer.STAGE_TEAM_SELECTION:
				NautDrafter.setScreen(new TeamSelectionScreen(), true);
				break;
			case DraftingServer.STAGE_DRAFTING:
				NautDrafter.setScreen((NautDrafter.isPlayer() ? new DraftingScreenPlayer()
				        : new DraftingScreenSpectator()), true);
				break;
			case DraftingServer.STAGE_NAUT_SELECTION:
				// player goes to naut selection, spectator drops down to versus
				if (NautDrafter.isPlayer()) {
					DraftingScreenPlayer screen = new DraftingScreenPlayer();
					screen.onStageChanged(stage);
					NautDrafter.setScreen(screen, true);
				} else {
					NautDrafter.setScreen(new VersusScreen(), true);
				}
				break;
			case DraftingServer.STAGE_VERSUS:
				NautDrafter.setScreen(new VersusScreen(), true);
				break;
			default:
				this.onConnectionFailed((byte) 0);
				return;
			}
			System.out.println("Connected!!!");
		} else {
			Platform.runLater(() -> this.onConnectionSucceeded(stage));
		}
	}

	@Override
	public void onConnectionFailed(byte id) {
		if (Platform.isFxApplicationThread()) {
			System.out.println("Error " + id);
			String message;
			try {
				message = LanguageResource.getString("join_errors.error" + id);
			} catch (MissingResourceException e) {
				message = LanguageResource.getString("error.unknown");
			}
			new NotificationPopUp(message, NotificationPopUp.LENGTH_MEDIUM).showNotification(this.getScene());
		} else {
			Platform.runLater(() -> this.onConnectionFailed(id));
		}
	}
}
