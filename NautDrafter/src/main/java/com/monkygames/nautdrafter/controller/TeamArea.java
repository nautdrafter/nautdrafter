/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.IOException;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.Team;
import com.monkygames.nautdrafter.resource.ImageCacher;
import com.monkygames.nautdrafter.view.PlayerDisplayer;
import com.monkygames.nautdrafter.view.XVBox;

/**
 * Re-usable UI for the drafting screen ({@link DraftingScreenSpectator}) and Versus screen ({@link VersusScreen}). <br/>
 * When in normal mode this will show the team on the left (with player names, icons and team name), bans on the left
 * and picks in the middle<br/>
 * The UI for the blue team is a little bit reversed to make it more symmetric<br/>
 * When in versus mode the bans area and team player list/icons are hidden and a name overlay over each pick will be
 * shown when the players choose their nauts
 */
public class TeamArea extends HBox {

	@FXML
	private XVBox             leftBox;
	@FXML
	private XVBox             rightBox;
	@FXML
	private HBox              bansHolder;
	@FXML
	private ImageView         pick1;
	@FXML
	private Label             pickName1;
	@FXML
	private ImageView         pick2;
	@FXML
	private Label             pickName2;
	@FXML
	private ImageView         pick3;
	@FXML
	private Label             pickName3;
	@FXML
	private VBox              teamInfo;
	@FXML
	private Label             teamName;
	@FXML
	private PlayerDisplayer   player1;
	@FXML
	private PlayerDisplayer   player2;
	@FXML
	private PlayerDisplayer   player3;
	@FXML
	private Label             banLabel;
	@FXML
	private Region            filler;
	@FXML
	private StackPane         pickHolder1;
	@FXML
	private StackPane         pickHolder2;
	@FXML
	private StackPane         pickHolder3;

	private ImageView[]       banIcons;
	private StackPane[]       banIconsHolders, pickHolders;
	private ImageView[]       pickIcons;
	private PlayerDisplayer[] players;
	private Label[]           playerNames;
	private boolean           isNew        = true;
	/**
	 * Property to determine what team this picker belongs to
	 */
	private IntegerProperty   team;

	/**
	 * Property to determine how many bans there are
	 */
	private IntegerProperty   banCount;

	/**
	 * Property to determine what is show for the specific screen
	 */
	private BooleanProperty   vsScreen;

	private DoubleProperty    minPrefWidth = new SimpleDoubleProperty();
	private TeamArea          other;

	/**
	 * A reusable UI element that can display the three picks, name and players of a team and can also show bans.
	 */
	public TeamArea() {
		super();
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
		        "/com/monkygames/nautdrafter/view/TeamArea.fxml"), LanguageResource.getLanguageResource());
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		this.pickIcons = new ImageView[] { this.pick1, this.pick2, this.pick3 };
		Image empty = ImageCacher.get("assets/ronimo/images/renders/unknown.png");
		this.pick1.setImage(empty);
		this.pick2.setImage(empty);
		this.pick3.setImage(empty);

		this.players = new PlayerDisplayer[] { this.player1, this.player2, this.player3 };

		this.playerNames = new Label[] { this.pickName1, this.pickName2, this.pickName3 };

		this.pickHolders = new StackPane[] { this.pickHolder1, this.pickHolder2, this.pickHolder3 };
		this.minPrefWidth.addListener((ov, o, n) -> this.layoutChildren());
	}

	// Need to make sure the side bits are evenly spaced
	@Override
	protected void layoutChildren() {
		if (this.isNew) {
			this.isNew = false;
			if (this.leftBox.computeMinWidth(0) > this.rightBox.computeMinWidth(0)) {
				this.minPrefWidth.set(this.leftBox.computeMinWidth(0));
			} else {
				this.minPrefWidth.set(this.rightBox.computeMinWidth(0));
			}
			this.leftBox.prefWidthProperty().bind(this.minPrefWidth);
			this.rightBox.prefWidthProperty().bind(this.minPrefWidth);

			if (this.other != null) {
				double max = Math.max(this.minPrefWidth.get(), this.other.minPrefWidth.get());
				this.minPrefWidth.set(max);
				this.other.minPrefWidth.set(max);
				this.leftBox.setMinWidth(this.minPrefWidth.get());
				this.rightBox.setMinWidth(this.minPrefWidth.get());
				this.other.leftBox.setMinWidth(this.minPrefWidth.get());
				this.other.rightBox.setMinWidth(this.minPrefWidth.get());
				System.out.println("Min width computed as " + this.minPrefWidth.get());

			} else {
				this.leftBox.setMinWidth(this.minPrefWidth.get());
				this.rightBox.setMinWidth(this.minPrefWidth.get());
			}
		}
		super.layoutChildren();
	}

	/**
	 * Make the alignment of this and the "other" Team-area match so they are layed out the same
	 *
	 * @param other
	 *            TeamArea to match alignment with
	 */
	public void setEqual(TeamArea other) {
		this.other = other;
		other.other = this;
	}

	/**
	 * Display the elements in the correct positions <br/>
	 * Elements that need aligning are leftBox and teamInfo
	 */
	protected void reAlign() {
		switch (this.teamProperty().get()) {
		case Team.RED:
			this.teamInfo.getChildren().clear();
			this.teamInfo.getChildren().addAll(this.teamName, this.player1, this.player2, this.player3, this.filler);
			this.leftBox.getChildren().clear();
			this.leftBox.getChildren().addAll(this.banLabel, this.bansHolder);
			break;
		case Team.BLUE:
			this.teamInfo.getChildren().clear();
			this.teamInfo.getChildren().addAll(this.filler, this.player3, this.player2, this.player1, this.teamName);
			this.leftBox.getChildren().clear();
			this.leftBox.getChildren().addAll(this.bansHolder, this.banLabel);
		}

	}

	/**
	 * Set the display icon for a ban, always runs on fx thread
	 *
	 * @param icon
	 *            Image to show
	 * @param name
	 *            Name of the character that has been banned
	 * @param position
	 *            Which position the ban is for. Must be < BanCount property ({@link #banCountProperty()})
	 */
	public void setBan(Image icon, String name, int position) {
		if (this.banIcons == null) {
			return;
		}
		if (Platform.isFxApplicationThread()) {
			if (position < this.banIcons.length) {
				this.banIconsHolders[position].getStyleClass().remove("next-pick");
				this.banIcons[position].setImage(icon);
				Tooltip.install(this.banIcons[position], new Tooltip(name));
			} else {
				throw new IllegalArgumentException(
				        "Not enough room for this ban. Have you forgotten to set the number of bans?");
			}
		} else {
			Platform.runLater(() -> this.setBan(icon, name, position));
		}
	}

	/**
	 * Set the name for the team that is represented by this TeamArea
	 *
	 * @param name
	 *            The name to set for this team
	 */
	public void setTeamName(String name) {
		this.teamName.setText(name);
	}

	/**
	 * Set the display icon for a pick, always runs on fx thread
	 *
	 * @param icon
	 *            Image to show
	 * @param name
	 *            Name of the character that has been picked
	 * @param position
	 *            Which position the pick is for. Must be from 0 - 2
	 */
	public void setPick(Image icon, String name, int position) {
		if (Platform.isFxApplicationThread()) {
			this.pickHolders[position].getStyleClass().removeAll("next-pick");
			this.pickIcons[position].setImage(icon);
			Tooltip.install(this.pickIcons[position], new Tooltip(name));
		} else {
			Platform.runLater(() -> this.setPick(icon, name, position));
		}
	}

	/**
	 * Set the position for the next pick. Used for styling
	 *
	 * @param position
	 *            Position of the next pick (from 0 - 2)
	 */
	public void setNextPick(int position) {
		this.pickHolders[position].getStyleClass().add("next-pick");
	}

	/**
	 * Set the position for the next ban. Used for styling
	 *
	 * @param position
	 *            Position of the next ban (from 0 - 2)
	 */
	public void setNextBan(int position) {
		this.banIconsHolders[position].getStyleClass().add("next-pick");
	}

	/**
	 * Set players for this team
	 *
	 * @param player
	 *            Player to show
	 * @param position
	 *            Which position the player is for. Must be from 0 - 2 (e.g Team.CAPTAIN - CREW_2)
	 */
	public void setPlayer(Player player, int position) {

		if (this.isVsScreen()) {
			for (int i = 0; i < 3; i++) {
				if (i == position) {
					continue;
				}

				if (this.players[i].getPlayer() != null && this.players[i].getPlayer().equals(player)) {
					this.deselectPlayer(i);
					break;
				}
			}
			if (position == -1) {
				return;
			}
			this.playerNames[position].getStyleClass().add("chosen");
		}

		if (position == -1) {
			return;
		}
		this.players[position].setPlayer(player, this.getTeam(), true);
		this.playerNames[position].setText(player.name);
	}

	/**
	 * Deselect the player from the naut if on versus screen
	 *
	 * @param position
	 *            position to deselect
	 */
	public void deselectPlayer(int position) {
		if (this.isVsScreen()) {
			this.playerNames[position].getStyleClass().remove("chosen");
			this.players[position].clearPlayer();
			this.playerNames[position].setText(null);
		}
	}

	/**
	 * Retrieves the team Property from the TeamArea and if one doesn't exist, it will create one and return that new
	 * copy
	 *
	 * @return The existing team property or a new copy if it didn't exist
	 */
	public final IntegerProperty teamProperty() {
		if (this.team == null) {
			this.team = new SimpleIntegerProperty() {

				@Override
				public Object getBean() {
					return TeamArea.this;
				}

				@Override
				public String getName() {
					return "team";
				}

				@Override
				public void invalidated() {

					TeamArea.this.reAlign();
					TeamArea.this.getStyleClass().removeAll("blue-team", "red-team");
					if (this.get() == Team.NONE) {
						return;
					}
					TeamArea.this.getStyleClass().add(this.get() == Team.RED ? "red-team" : "blue-team");

				}

			};
		}
		return this.team;
	}

	/**
	 * Set which team this team picker belongs to (determines which style class is associated with this item)
	 *
	 * @param value
	 *            one of three constants from {@link Team} (BLUE,RED,NONE)
	 */
	public final void setTeam(int value) {
		this.teamProperty().set(value);
	}

	/**
	 * Get the constant representing which team this item belongs to (default is {@link Team#NONE})
	 */
	public final int getTeam() {
		return this.team == null ? 0 : this.team.get();
	}

	/**
	 * Get the Ban Count Property (Property to determine the total number of bans that can be made by this team)
	 *
	 * @return the existing property or a new copy if it doesn't exist
	 */
	public final IntegerProperty banCountProperty() {
		if (this.banCount == null) {
			this.banCount = new SimpleIntegerProperty() {

				@Override
				public Object getBean() {
					return TeamArea.this;
				}

				@Override
				public String getName() {
					return "ban-count";
				}

				@Override
				public void invalidated() {

					TeamArea.this.banIcons = new ImageView[this.get()];
					TeamArea.this.banIconsHolders = new StackPane[this.get()];
					for (int i = 0; i < TeamArea.this.banIcons.length; i++) {
						TeamArea.this.banIconsHolders[i] = new StackPane(TeamArea.this.banIcons[i] = new ImageView(
						        ImageCacher.get("assets/ronimo/images/icons/unknown.png")));
						TeamArea.this.banIconsHolders[i].getStyleClass().setAll("picker");
						TeamArea.this.banIcons[i].setFitHeight(48);
						TeamArea.this.banIcons[i].setFitWidth(48);
						TeamArea.this.bansHolder.getChildren().add(TeamArea.this.banIconsHolders[i]);
					}

				}

			};
		}
		return this.banCount;
	}

	/**
	 * Set how many bans there are
	 *
	 * @param value
	 *            the total number of bans that can be made by this team
	 */
	public final void setBanCount(int value) {
		this.banCountProperty().set(value);
	}

	/**
	 * Get the amount of bans
	 */
	public final int getBanCount() {
		return this.banCount == null ? 0 : this.banCount.get();
	}

	/**
	 * get the vsScreen Property (whether or not this team area is being displayed in Versus mode - see the class
	 * description)
	 *
	 * @return the existing property or a new copy if it didn't exist
	 */
	public final BooleanProperty vsScreenProperty() {
		if (this.vsScreen == null) {
			this.vsScreen = new SimpleBooleanProperty(false) {

				@Override
				public Object getBean() {
					return TeamArea.this;
				}

				@Override
				public String getName() {
					return "vs-screen";
				}

				@Override
				public void invalidated() {

					if (this.get()) {
						TeamArea.this.getChildren().remove(TeamArea.this.leftBox);
						TeamArea.this.getChildren().remove(TeamArea.this.rightBox);
						TeamArea.this.pickName1.setVisible(true);
						TeamArea.this.pickName2.setVisible(true);
						TeamArea.this.pickName3.setVisible(true);
						TeamArea.this.getStyleClass().add("versus");
					} else {
						TeamArea.this.getChildren().add(0, TeamArea.this.leftBox);
						TeamArea.this.getChildren().add(TeamArea.this.rightBox);
						TeamArea.this.pickName1.setVisible(false);
						TeamArea.this.pickName2.setVisible(false);
						TeamArea.this.pickName3.setVisible(false);
						TeamArea.this.getStyleClass().remove("versus");
					}

				}

			};

			TeamArea.this.pickName1.setVisible(false);
			TeamArea.this.pickName2.setVisible(false);
			TeamArea.this.pickName3.setVisible(false);

		}
		return this.vsScreen;
	}

	/**
	 * Set which screen we are on
	 *
	 * @param value
	 *            true for versus screen. false for drafting screen
	 */
	public final void setVsScreen(boolean value) {
		this.vsScreenProperty().set(value);
	}

	/**
	 * Get whether this is set to be in versus screen mode
	 */
	public final boolean isVsScreen() {
		return this.vsScreen == null ? false : this.vsScreen.get();
	}

}
