/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.css.CssMetaData;
import javafx.css.Styleable;
import javafx.css.StyleableDoubleProperty;
import javafx.css.StyleableProperty;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

import com.sun.javafx.css.converters.SizeConverter;

/**
 * A wrapper for a {@link FlowPane} that can have filters that filter the children using given predicates
 *
 * @param <T>
 *            extended {@link Node} class that the children will all have to be
 *
 */
public class FilterFlowPane<T extends Node> extends VBox {

	@FXML
	private FlowPane                flowPane;
	@FXML
	private ScrollPane              scroll;
	@FXML
	private HBox                    filterBox;
	@FXML
	private RadioToggle             allFilter;
	private ToggleGroup             group;
	private ArrayList<ToggleButton> buttons;
	private FilteredList<T>         filteredChildren;
	private ObservableList<T>       children;

	/**
	 * Create an empty filter flow pane with only an "all" filter
	 */
	public FilterFlowPane() {
		super();

		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
		        "/com/monkygames/nautdrafter/view/FilterFlowPane.fxml"));

		fxmlLoader.setController(this);
		fxmlLoader.setRoot(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		// Initialize the button list and the "all" button
		(this.buttons = new ArrayList<>()).add(this.allFilter);
		this.allFilter.setOnAction((event) -> this.filteredChildren.setPredicate((T) -> true));
		this.filteredChildren = new FilteredList<>(this.children = FXCollections.observableArrayList(), (T) -> true);

		// When the filtered list changes re-set the children in the flow pane
		this.filteredChildren.addListener((ListChangeListener<T>) c -> {
			this.flowPane.getChildren().setAll(this.filteredChildren);
		});

		// Set up button groups
		this.group = new ToggleGroup();
		this.allFilter.setToggleGroup(this.group);
		this.allFilter.setSelected(true);

		this.filterBox.getStyleClass().setAll("tab-bar");

		// Make scrollpane not be funny with arrow keys
		// Basically re-mapping normal arrows with shift+arrow to traverse inside of the scrollpane instead of scrolling
		List<KeyEvent> mappedEvents = new ArrayList<>();
		this.scroll.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				if (mappedEvents.remove(event)) {
					return;
				}

				switch (event.getCode()) {
				case UP:
				case DOWN:
				case LEFT:
				case RIGHT:
					KeyEvent newEvent = this.remap(event);
					mappedEvents.add(newEvent);
					event.consume();
					Event.fireEvent(event.getTarget(), newEvent);
				default:
					break;
				}
			}

			private KeyEvent remap(KeyEvent event) {
				KeyEvent newEvent = new KeyEvent(event.getEventType(), event.getCharacter(), event.getText(), event
				        .getCode(), !event.isShiftDown(), event.isControlDown(), event.isAltDown(), event.isMetaDown());

				return newEvent.copyFor(event.getSource(), event.getTarget());
			}
		});
	}

	@Override
	protected void layoutChildren() {

		// Bind the tab spacing property only once
		if (this.tabSpacing != null && !this.filterBox.spacingProperty().isBound()) {
			this.filterBox.spacingProperty().bind(this.tabSpacing);
		}
		super.layoutChildren();
	}

	/**
	 * Get the wrap length property of the internal flow pane<br/>
	 * Useful for laying out the FilterFlowPane
	 *
	 * @return The FilterFlowPanes internal FlowPanes prefWrapLengthProperty
	 */
	public DoubleProperty prefWrapLengthProperty() {
		return this.flowPane.prefWrapLengthProperty();
	}

	/**
	 * Add a filter to the flow pane. This will filter the children using the predicate supplied <br/>
	 * Does not actually remove children from the list, just uses an internal filtered list to filter the children<br/>
	 * The original list of children can still be obtained by calling getFilteredChildren
	 *
	 * @param name
	 *            The name of the filter to be displayed on the tab
	 * @param predicate
	 *            The predicate to decide if the child will be visible
	 */
	public void addFilter(String name, Predicate<T> predicate) {
		RadioToggle newFilter = new RadioToggle(name);
		newFilter.setOnAction(event -> this.filteredChildren.setPredicate(predicate));
		// newFilter.setPrefWidth(64);
		newFilter.getStyleClass().add("tab-button");
		newFilter.setToggleGroup(this.group);
		this.buttons.add(newFilter);
		this.update();
	}

	/**
	 * Remove a specific filter using it name that was provided when made
	 *
	 * @param name
	 *            The given name for the filter
	 */
	public void removeFilter(String name) {
		this.buttons.removeIf(button -> name.equals(button.getText()));
		this.update();
	}

	/**
	 * The list of child nodes in the internal flow pane. May not include all visible children due to the current filter
	 * being selected.
	 *
	 * @return The backing list to the filtered list used by the internal flow pane
	 */
	public ObservableList<T> getFilteredChildren() {
		return this.children;
	}

	/**
	 * Update the buttons in the tab menu.
	 */
	private void update() {
		if (Platform.isFxApplicationThread()) {
			this.filterBox.getChildren().setAll(this.buttons);
		} else {
			Platform.runLater(() -> this.update());
		}

	}

	/**
	 * Small class wrapper for the toggle buttons to make them more like radio buttons
	 */
	public static class RadioToggle extends ToggleButton {

		/**
		 * Creates a toggle button with an empty string for its label.
		 */
		public RadioToggle() {
			super();
		}

		/**
		 * Creates a toggle button with the specified text as its label.
		 *
		 * @param text
		 *            A text string for its label.
		 */
		public RadioToggle(String text) {
			super(text);
		}

		/**
		 * Creates a toggle button with the specified text and icon for its label.
		 *
		 * @param text
		 *            A text string for its label.
		 * @param graphic
		 *            the icon for its label.
		 */
		public RadioToggle(String text, Node graphic) {
			super(text, graphic);
		}

		/**
		 * Same as the radio button but with toggle button styling
		 */
		@Override
		public void fire() {
			if (this.getToggleGroup() == null || !this.isSelected()) {
				super.fire();
			}
		}

	}

	/**
	 * Things to get spacing working for the tab buttons. Copied most from VBox/HBox source <br/>
	 * Allows styling from css. Style is "-tab-spacing" and takes numbers as the arguments
	 */

	private static final CssMetaData<FilterFlowPane<Node>, Number> TAB_SPACING = new CssMetaData<FilterFlowPane<Node>, Number>(
	                                                                                   "-tab-spacing",
	                                                                                   SizeConverter.getInstance(), 0d) {

		                                                                           @Override
		                                                                           public boolean isSettable(FilterFlowPane<Node> node) {
			                                                                           return node.tabSpacing == null
			                                                                                   || !node.tabSpacing
			                                                                                           .isBound();

		                                                                           }

		                                                                           @SuppressWarnings("unchecked")
		                                                                           @Override
		                                                                           public StyleableProperty<Number> getStyleableProperty(FilterFlowPane<Node> node) {
			                                                                           return (StyleableProperty<Number>) node
			                                                                                   .tabSpacingProperty();
		                                                                           }

	                                                                           };

	/**
	 * The amount of space between each filter button in the hbox.
	 */
	public final DoubleProperty tabSpacingProperty() {
		if (this.tabSpacing == null) {
			this.tabSpacing = new StyleableDoubleProperty() {
				@Override
				public void invalidated() {
					FilterFlowPane.this.requestLayout();
				}

				@Override
				public Object getBean() {
					return FilterFlowPane.this;
				}

				@Override
				public String getName() {
					return "tab spacing";
				}

				@Override
				public CssMetaData<FilterFlowPane<Node>, Number> getCssMetaData() {
					return TAB_SPACING;
				}
			};
		}
		return this.tabSpacing;
	}

	private DoubleProperty tabSpacing;

	/**
	 * Sets the amount of space between the tab bar and flowpane content
	 *
	 * @param value
	 *            The amount of space
	 */
	public final void setTabSpacing(double value) {
		this.spacingProperty().set(value);
	}

	/**
	 * @return The amount of space between the tab bar and flowpane content
	 */
	public final double getTabSpacing() {
		return this.tabSpacing == null ? 0 : this.tabSpacing.get();
	}

	/**
	 * The current item that is selected, or null if none is selected
	 */
	public final ObjectProperty<T> selectedItemProperty() {
		if (this.selectedItem == null) {
			this.selectedItem = new SimpleObjectProperty<T>() {

				@Override
				public Object getBean() {
					return FilterFlowPane.this;
				}

				@Override
				public String getName() {
					return "selected item";
				}

			};
		}
		return this.selectedItem;
	}

	private ObjectProperty<T> selectedItem;

	/**
	 * Sets the selected item in the FlowPane
	 *
	 * @param value
	 *            The item to select
	 */
	public final void setSelectedItem(T value) {
		this.selectedItemProperty().set(value);
	}

	/**
	 * @return The item currently selected in this FilterFlowPane
	 */
	public final T getSelectedItem() {
		return this.selectedItem == null ? null : this.selectedItem.get();
	}

	/**
	 * @return The CssMetaData associated with this class, which may include the CssMetaData of its super classes.
	 */
	public static List<CssMetaData<? extends Styleable, ?>> getClassCssMetaData() {
		return STYLEABLES;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @since JavaFX 8.0
	 */
	@Override
	public List<CssMetaData<? extends Styleable, ?>> getCssMetaData() {
		return FilterFlowPane.getClassCssMetaData();
	}

	private static final List<CssMetaData<? extends Styleable, ?>> STYLEABLES;
	static {
		final List<CssMetaData<? extends Styleable, ?>> styleables = new ArrayList<CssMetaData<? extends Styleable, ?>>(
		        Region.getClassCssMetaData());
		styleables.add(TAB_SPACING);
		STYLEABLES = Collections.unmodifiableList(styleables);
	}
}
