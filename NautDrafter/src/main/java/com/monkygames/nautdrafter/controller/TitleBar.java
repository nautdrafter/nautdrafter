/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.IOException;
import java.util.Arrays;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.manager.NautStats;
import com.monkygames.nautdrafter.manager.SteamStats;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.network.DraftingClient;
import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.resource.ImageCacher;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.scene.control.Separator;
import javafx.scene.input.MouseEvent;

/**
 * Pane to show at the top of the Base class for player details and things
 *
 * @author Adam
 */
public class TitleBar extends HBox {

	@FXML
	private MenuButton        menu;

	@FXML
	private Label             playerName;
	@FXML
	private ImageView         playerIcon;

	private Player            player;

	@FXML
	private HBox              logoContainer;

        @FXML
        private Label             playerCountL;

        @FXML
        private Label             rankL;

        @FXML
        private Separator         separator;

        @FXML
        private ImageView         leagueIcon;

	private static ServerMenu runningServers;

	private static MenuItem[] globalMenus;

        private SteamStats steamStats;

        private NautStats nautStats;

	/**
	 * Construct a new instance of TitleBar by loading FXML from file
	 */
	public TitleBar() {
		super();

		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
		        "/com/monkygames/nautdrafter/view/TitleBar.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		this.menu.getItems().addAll(TitleBar.getGlobalMenuItems());

		this.menu.showingProperty().addListener((ov, o, iShowing) -> {
			if (iShowing) {
				runningServers.buildItems();
			}
		});
            initSteamStats();
	}

	/**
	 * Get the array of menus that are global for the whole application
	 * 
	 * @return the existing menus or a new copy if they did not already exist
	 */
	static MenuItem[] getGlobalMenuItems() {
		if (globalMenus == null) {
			MenuItem returnToBrowser = new MenuItem(LanguageResource.getString("global.menu.home"));
			returnToBrowser.setOnAction(event -> {
				NautDrafter.getBase().popMain(LanguageResource.getString("server_browser.screen_name"));
				DraftingClient.disconnect();
			});
			
			MenuItem directConnect = new MenuItem(LanguageResource.getString("global.menu.direct_connect"));
			directConnect.setOnAction(event -> {
				new DirectConnectWindow().showWindow();
			});

			MenuItem helpItem = new MenuItem(LanguageResource.getString("global.menu.help"));
			helpItem.setOnAction(event -> {
				String docId = NautDrafter.getBase().getCurrentScreen().getDocIdentifier();

				if (docId == null) {
					docId = "User Documentation and Guides";
				}

				NautDrafter.getInstance().showDocumentation(docId);
			});

			globalMenus = new MenuItem[] { returnToBrowser, runningServers = new ServerMenu(), directConnect,
			        new SeparatorMenuItem(), TitleBar.prefs(), helpItem };

		}
		return globalMenus;
	}

	/**
	 * @return The menu items required for the login screen.
	 */
	static MenuItem[] getLoginMenus() {
		// We need to rebuild the prefs menu as JavaFX wants to dispose of the menu when the login screen is closed if
		// we re-use the same menu
		return new MenuItem[] { TitleBar.prefs(), TitleBar.getGlobalMenuItems()[globalMenus.length - 1] };
	}

	/**
	 * Builds the preferences menu
	 */
	private static MenuItem prefs() {
		Menu prefs = new Menu(LanguageResource.getString("global.menu.prefs"));

		CheckMenuItem allowFocusStealingToggle = new CheckMenuItem(LanguageResource.getString(
		        "global.settings.steal_focus"));
		CheckMenuItem allowAudioPlayerToggle = new CheckMenuItem(LanguageResource.getString(
		        "global.settings.audio_player"));
		CheckMenuItem useExternalBrowser = new CheckMenuItem(LanguageResource.getString(
		        "global.settings.external_browser"));

		// Checking the Toggle Button for Allowing Focus Stealing
		allowFocusStealingToggle.setSelected(NautDrafter.getPreferences().getBoolean("allowFocusStealing", false));

		allowFocusStealingToggle.selectedProperty().addListener(
		        (ChangeListener<Boolean>) (observable, oldValue, newValue) -> {
			        NautDrafter.getPreferences().putBoolean("allowFocusStealing", newValue);
		        });

		// Checking the Toggle Button for Allowing Audio Player
		allowAudioPlayerToggle.setSelected(NautDrafter.getPreferences().getBoolean("allowAudioPlayer", true));

		allowAudioPlayerToggle.selectedProperty().addListener(
		        (ChangeListener<Boolean>) (observable, oldValue, newValue) -> {
			        NautDrafter.getPreferences().putBoolean("allowAudioPlayer", newValue);
		        });

		useExternalBrowser.setSelected(NautDrafter.getPreferences().getBoolean("useExternalBrowser", false));

		useExternalBrowser.selectedProperty().addListener(
		        (ChangeListener<Boolean>) (observable, oldValue, newValue) -> {
			        NautDrafter.getPreferences().putBoolean("useExternalBrowser", newValue);
		        });

		prefs.getItems().addAll(allowAudioPlayerToggle, allowFocusStealingToggle, useExternalBrowser);
		return prefs;
	}

	/**
	 * Set screens name. Only Base should call this
	 *
	 * @param title
	 */
	void setScreenName(String title) {
		this.menu.setText(title);
	}

	/**
	 * Set players name
	 *
	 * @param player
	 */
	public void setPlayer(Player player) {
		this.player = player;
		if (player == null) {
			this.playerName.setText("Spectator");
			this.playerIcon.setImage(ImageCacher.get("assets/ronimo/images/icons/logo.png"));
		} else {
			this.playerName.setText(player.name);
			ImageCacher.putPlayerIcon(player, player.iconUrl);
			this.playerIcon.setImage(ImageCacher.get(player.iconUrl));
			
			// Save for fake login later use
			NautDrafter.getPreferences().put("fake-player-image-url", player.iconUrl);			
			NautDrafter.getPreferences().put("fake-player-name", player.name);
                        initNautStats(player.name);
		}
	}

	/**
	 * Get the player
	 *
	 * @return The player
	 */
	public Player getPlayer() {
		return this.player;
	}

	/**
	 * Get the image of the icon
	 *
	 * @return
	 */
	public Image getPlayerIcon() {
		return this.playerIcon.getImage();
	}

	/**
	 * Set game logo
	 *
	 * @param image
	 */
	public void setApplicationLogo(ImageView image) {
		this.logoContainer.getChildren().clear();
		this.logoContainer.getChildren().add(image);
	}

	/**
	 * Adds extra menu options to the menu in the top left of the screen
	 * 
	 * @param options
	 *            any number of menu items to add
	 */
	public void addExtraMenuOption(MenuItem... options) {
		if (options == null) {
			return;
		}
		if (Platform.isFxApplicationThread()) {
			this.menu.getItems().addAll(0, Arrays.asList(options));
		} else {
			Platform.runLater(() -> this.addExtraMenuOption(options));
		}
	}

	/**
	 * Remove any extra menus that may have been added
	 */
	public void removeExtraMenuOption(MenuItem... options) {
		if (options == null) {
			return;
		}
		if (Platform.isFxApplicationThread()) {
			this.menu.getItems().removeAll(options);
		} else {
			Platform.runLater(() -> this.removeExtraMenuOption(options));
		}

	}
    /**
     * Initializes the steam stats.
     */
    private void initSteamStats(){
        steamStats = new SteamStats(playerCountL);
    }

    /**
     * Set the Naut stats like League rating and rank.
     * @param playerName the name of the player.
     */
    private void initNautStats(String playerName){
        nautStats = new NautStats(playerName,rankL,separator,leagueIcon);
    }

    public void handleSteamStats(MouseEvent event) {
        NautDrafter.getInstance().openBrowser(steamStats.AWESOMENAUTS_URL, steamStats.DOMAIN);
    }
    public void handlePlayerStats(MouseEvent event) {
        URI uri;
            try {
                uri = new URI(NautStats.URL);
                System.out.println("URI.getHost() = "+uri.getHost());
            } catch (URISyntaxException ex) {
                Logger.getLogger(TitleBar.class.getName()).log(Level.SEVERE, null, ex);
            }
        if(player != null && player.name != null){
            System.out.println("Handle Player Stats");
            // note, creates an infanite number of browser tabs
            // this is a bug but don't know how to fix it.
            //NautDrafter.getInstance().openBrowser(NautStats.URL+player.name, NautStats.DOMAIN);
        }
    }
    public void stopSteamStats(){
        steamStats.stopThread();
    }
    public void stopNautStats(){
        if(nautStats != null){
            nautStats.stopThread();
        }
    }
}
