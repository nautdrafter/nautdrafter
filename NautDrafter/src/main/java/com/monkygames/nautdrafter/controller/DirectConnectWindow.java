/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.MissingResourceException;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.network.DraftingClient;
import com.monkygames.nautdrafter.network.DraftingServer;
import com.monkygames.nautdrafter.network.callbacks.ConnectionCallback;
import com.monkygames.nautdrafter.view.NotificationPopUp;

/**
 * Window that allows the user to directly connect to a server using the hosts ip address and socket
 */
public class DirectConnectWindow extends Stage implements ConnectionCallback {

	private BorderPane    root;

	@FXML
	private TextField     ip;
	@FXML
	private TextField     port;
	@FXML
	private PasswordField password;
	@FXML
	private Button        goPlayer;
	@FXML
	private Button        goSpectator;
	@FXML
	private HBox          bottomBox;

	public DirectConnectWindow() {

		// Load xml
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
		        "/com/monkygames/nautdrafter/view/DirectConnectWindow.fxml"), LanguageResource.getLanguageResource());

		fxmlLoader.setController(this);
		try {
			this.root = fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		this.initOwner(NautDrafter.stage);
		this.setScene(new Scene(this.root));
		this.sizeToScene();
		this.centerOnScreen();
		this.root.getStylesheets().add("style/Base.css");

		this.port.textProperty().addListener((ov, o, n) -> {
			if (n != null && !n.matches("[0-9]*")) {
				this.port.setText(o);
				return;
			}
			if (!n.equals("")&&Integer.parseInt(n) > 0xFFFF) {
				this.port.setText("65535");
			}
		});
		this.goPlayer.setOnAction(event -> {
			try {
				DraftingClient.connectTo(InetAddress.getByName(this.ip.getText()),
				        Integer.parseInt(this.port.getText()), NautDrafter.getBase().getPlayer(), this, this.password
				                .getText().isEmpty() ? null : this.password.getText());
				NautDrafter.setIsPlayer(true);
			} catch (UnknownHostException | NumberFormatException e) {
				new NotificationPopUp(LanguageResource.getString("join_errors.error0"),
				        NotificationPopUp.LENGTH_MEDIUM).showNotification(NautDrafter.getBase().getScene());
			}

			this.close();
		});
		this.goSpectator.setOnAction(event -> {
			try {
				DraftingClient.connectTo(InetAddress.getByName(this.ip.getText()),
				        Integer.parseInt(this.port.getText()), null, this, this.password.getText().isEmpty() ? null
				                : this.password.getText());
				NautDrafter.setIsPlayer(false);

			} catch (UnknownHostException | NumberFormatException e) {
				new NotificationPopUp(LanguageResource.getString("join_errors.error0"),
				        NotificationPopUp.LENGTH_MEDIUM).showNotification(NautDrafter.getBase().getScene());
			}
			this.close();
		});
		
		if (!NautDrafter.isMe(NautDrafter.getBase().getPlayer())) {
			this.bottomBox.getChildren().remove(this.goPlayer);
		}
	}

	/**
	 * Show the window
	 */
	public void showWindow() {
		super.show();
		Platform.runLater(() -> this.requestFocus());
	}

	@Override
	public void onConnectionSucceeded(int stage) {
		if (Platform.isFxApplicationThread()) {
			NautDrafter.getBase().stopLoadingOverlay();
			
			// Go back to server browser screen if applicable
			NautDrafter.getBase().popMain(LanguageResource.getString("server_browser.screen_name"));
			
			// Go to apropriate stage
			switch (stage) {
			case DraftingServer.STAGE_TEAM_SELECTION:
				NautDrafter.setScreen(new TeamSelectionScreen(), true);
				break;
			case DraftingServer.STAGE_DRAFTING:
				NautDrafter.setScreen((NautDrafter.isPlayer() ? new DraftingScreenPlayer()
				        : new DraftingScreenSpectator()), true);
				break;
			case DraftingServer.STAGE_NAUT_SELECTION:
				// player goes to naut selection, spectator drops down to versus
				if (NautDrafter.isPlayer()) {
					DraftingScreenPlayer screen = new DraftingScreenPlayer();
					screen.onStageChanged(stage);
					NautDrafter.setScreen(screen, true);
				} else {
					NautDrafter.setScreen(new VersusScreen(), true);
				}
				break;
			case DraftingServer.STAGE_VERSUS:
				NautDrafter.setScreen(new VersusScreen(), true);
				break;
			default:
				this.onConnectionFailed((byte) 0);
				return;
			}
			System.out.println("Connected!!!");
		} else {
			Platform.runLater(() -> this.onConnectionSucceeded(stage));
		}
	}

	@Override
	public void onConnectionFailed(byte id) {
		if (Platform.isFxApplicationThread()) {
			NautDrafter.getBase().stopLoadingOverlay();
			System.out.println("Error " + id);
			String message;
			try {
				message = LanguageResource.getString("join_errors.error" + id);
			} catch (MissingResourceException e) {
				message = LanguageResource.getString("error.unknown");
			}
			new NotificationPopUp(message, NotificationPopUp.LENGTH_MEDIUM).showNotification(NautDrafter.getBase()
			        .getScene());
		} else {
			Platform.runLater(() -> this.onConnectionFailed(id));
		}
	}
}
