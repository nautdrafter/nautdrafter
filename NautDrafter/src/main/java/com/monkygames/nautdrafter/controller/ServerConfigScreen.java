/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.Glow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

import com.monkygames.nautdrafter.Constants;
import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.manager.PlanetPoolManager;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.model.Planet;
import com.monkygames.nautdrafter.network.CreateServerInfo;
import com.monkygames.nautdrafter.network.DraftingServer;
import com.monkygames.nautdrafter.network.LobbyClient;
import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.callbacks.DedicatedServerRequestCallback;
import com.monkygames.nautdrafter.network.callbacks.DedicatedServerResponseCallback;
import com.monkygames.nautdrafter.view.PopUp;
import com.monkygames.nautdrafter.view.SubScreen;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Toggle;

/**
 * Screen implementation for setting up a new server. All relevant fields and configuration settings for a server will
 * be on this screen
 */
public class ServerConfigScreen extends VBox implements SubScreen,
DedicatedServerResponseCallback{

	@FXML
	private PasswordField                  passwordField;
	@FXML
	private TextField                      nameField;
	@FXML
	private TextField                      detailsField;
	@FXML
	private TextField                      portField;
	@FXML
	private ComboBox<DraftingScheduleItem> draftingOrder;
	@FXML
	private TimelineConfig                 config;
	@FXML
	private ComboBox<Planet>               mapMenu;
	@FXML
	private Button                         cancelButton;
	@FXML
	private Button                         createButton;
	@FXML
	private RadioButton                    timeoutRandom;
	@FXML
	private RadioButton                    timeoutForfeit;
	@FXML
	private RadioButton                    pickTypeOneWay;
	@FXML
	private RadioButton                    pickTypeBan;
	@FXML
	private RadioButton                    bantypeBi;
	@FXML
	private RadioButton                    bantypeUni;
        @FXML
        private RadioButton                    serverHostRemote;
        @FXML
        private RadioButton                    serverHostLocal;
        @FXML
        private RadioButton                    draftModeNormal;
        @FXML
        private RadioButton                    draftModeConcurrent;
	@FXML
	private RadioButton		      randomMapShown;
	@FXML
	private RadioButton		      randomMapHidden;
	@FXML
	private RadioButton		      forceRandomMapEnabled;
	@FXML
	private RadioButton		      forceRandomMapDisabled;
        @FXML
        private RadioButton                   isCaptainsModeEnabled;
        @FXML
        private RadioButton                   isCaptainsModeDisabled;
        @FXML
        private ToggleGroup                    pickType;
	@FXML
	private ToggleGroup                    banType;
        @FXML
        private ToggleGroup                    draftMode;
	@FXML
	private ToggleGroup                    outOfTime;
	@FXML
	private ToggleGroup		       randomMap;
	@FXML
	private ToggleGroup		       forceRandomMap;
	@FXML
	private ToggleGroup		       isCaptainsMode;
        @FXML
        private ToggleGroup                    serverHost;
	@FXML
	private CheckBox                       directConnect;

	private final String                   title;
	private StringProperty                 finalDetails  = new SimpleStringProperty("");
	private StringProperty                 finalName     = new SimpleStringProperty();
	private StringProperty                 finalPassword = new SimpleStringProperty();
	private IntegerProperty                port          = new SimpleIntegerProperty(0);
	private boolean                        isPasswordProtected;
        private boolean                        isAutoRandomMap;
	private static byte                    map;

	private ObservableList<Planet>         maps;
	/**
	 * Used for when force maps is enabled, need to store the map pool.
	 */
	private ArrayList<Planet>	       mapPool;
	private PlanetPullDownListener 	       planetPullDownListener;

	/**
	 * Used for selecting available planets.
	 */
	private PlanetPoolManager		planetPoolManager;

	/**
	 * Used for randomly selecting a map
	 */
	private Random randomMapGenerator;

	/**
	 * Checks if the server created should export the game history.
	 */
	private boolean exportGameHistory;
        private ResourceBundle resource;

	/**
	 * Create the new screen <br/>
	 * Load the xml and create the button handlers/menu handlers for the screen
	 */
	public ServerConfigScreen() {
		// Load xml
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
		        "/com/monkygames/nautdrafter/view/ServerConfig.fxml"), resource = LanguageResource.getLanguageResource());
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		this.title = resource.getString("server_config.screen_name");

		planetPullDownListener = new PlanetPullDownListener();
		this.mapPool = new ArrayList<Planet>();

		// setup random generator
		// note, the javadoc says that using the default constructor
		// should use a distant seed
		randomMapGenerator = new Random();

		// Setup planet pools
		planetPoolManager = new PlanetPoolManager();
		// populate the pull down for the planet's menu
		setupMapPullDown(PlanetPoolManager.DEFAULT,false);

		// settings tooltips
		Tooltip.install(this.draftingOrder, new Tooltip(resource.getString("drafting_config.drafting_menu.tooltip")));
		this.draftingOrder.setItems(FXCollections.observableArrayList(DraftingScheduleItem.getPresets()));
		this.draftingOrder.setButtonCell(new DraftingListCell());
		this.draftingOrder.setCellFactory(p -> new DraftingListCell());

		this.draftingOrder.getSelectionModel().selectedItemProperty().addListener((ov, oldVal, newVal) -> {
			this.config.setToPreset(newVal.data);
                        // check if the server is local or remote
                        if(newVal.isRemote){
                            serverHostRemote.setSelected(true);
                            serverHostLocal.setSelected(false);
                            // disable port
                            this.portField.disableProperty().set(false);
                        }else{
                            serverHostRemote.setSelected(false);
                            serverHostLocal.setSelected(true);
                            this.portField.disableProperty().set(true);
                        }
                        if(newVal.isPickBan){
                           pickTypeBan.setSelected(true);
                           pickTypeOneWay.setSelected(false);
                        }else{
                           pickTypeBan.setSelected(false);
                           pickTypeOneWay.setSelected(true);
                        }
			// set the ban type
			if(newVal.isBanBi){
				bantypeUni.setSelected(false);
				bantypeBi.setSelected(true);
			}else{
				bantypeBi.setSelected(false);
				bantypeUni.setSelected(true);

			}
                        if(newVal.isSeqDraft){
			    draftModeNormal.setSelected(true);
			    draftModeConcurrent.setSelected(false);
                        }else{
			    draftModeNormal.setSelected(false);
			    draftModeConcurrent.setSelected(true);
                        }
			if(newVal.isRandomShown){
				this.randomMapShown.setSelected(true);
				this.randomMapHidden.setSelected(false);
			}else {
				this.randomMapShown.setSelected(false);
				this.randomMapHidden.setSelected(true);
			}
			if(newVal.forceRandomMap){
				this.forceRandomMapEnabled.setSelected(true);
				this.forceRandomMapDisabled.setSelected(false);
			}else{
				this.forceRandomMapEnabled.setSelected(false);
				this.forceRandomMapDisabled.setSelected(true);
			}
                        if(newVal.isCaptainsMode){
                            this.isCaptainsModeEnabled.setSelected(true);
                            this.isCaptainsModeDisabled.setSelected(false);
                        }else{
                            this.isCaptainsModeEnabled.setSelected(false);
                            this.isCaptainsModeDisabled.setSelected(true);
                        }
                        // add check
			this.exportGameHistory = newVal.exportGameResults;
			this.setupMapPullDown(newVal.planetPool,newVal.forceRandomMap);
		});
		this.draftingOrder.getSelectionModel().select(0);

		this.cancelButton.setOnAction((ActionEvent e) -> {
			NautDrafter.getBase().popMain();
		});

		this.finalDetails.bind(this.detailsField.textProperty());
		this.finalName.bind(this.nameField.textProperty());
		this.finalPassword.bind(this.passwordField.textProperty());
		this.portField.textProperty().addListener((ov, o, n) -> {
			if (n != null && !n.matches("[0-9]*")) {
				this.portField.setText(o);
				return;
			}
			if (!n.equals("") && Integer.parseInt(n) > 0xFFFF) {
				this.portField.setText("65535");
			}
		});

		// port number
		this.port.bind(Bindings.createIntegerBinding(() -> {
			int i = 0;
			try {
				i = Integer.parseInt(ServerConfigScreen.this.portField.getText());
			} catch (NumberFormatException e) {
			}
			return i;
		}, this.portField.textProperty()));

		/*
		 * TODO: actually implement forfeit game stuff. For now just disable the check box
		 */
		this.timeoutForfeit.setDisable(true);

		this.createButton.setOnAction((ActionEvent e) -> {

			if (!this.config.isValid(false)) {
				return;
			} else {
				NautDrafter.getPreferences().putInt("last-draft-config", this.config.getIntRepresentation());
				try {
					NautDrafter.getPreferences().flush();
				} catch (BackingStoreException e1) {
					e1.printStackTrace();
				}
			}

			NautDrafter.getBase().startLoadingOverlay("Starting server");

			this.isPasswordProtected = this.finalPassword.get().length() > 0 ? true : false;


                        // Check for random maps
                        if(this.forceRandomMapEnabled.isSelected() || (this.randomMapShown.isSelected() && this.map == Planet.UNKNOWN.getId())){
                            // used for selecting the random map
                            List<Planet> mapList;
                            if(this.forceRandomMapEnabled.isSelected()){
                                mapList = this.mapPool;
                            }else{
                                mapList = maps;
                            }
                            // do a dice roll
                            int selection = randomMapGenerator.nextInt(mapList.size()-1) + 1;
                            this.map = mapList.get(selection).getId();
                            this.isAutoRandomMap = true;
                        }

                        // Create new server
			    System.out.println("Create new server:\nName: " + this.finalName.get() + "\nDetails: "
			            + this.finalDetails.get());
			    System.out.println("is password protected? " + this.isPasswordProtected + " "
			            + (this.isPasswordProtected ? ", password = " + this.finalPassword.get() : ""));
			    System.out.println("map id: " + this.map);
                            System.out.println("auto random map: "+this.isAutoRandomMap);
			    System.out.println("port: " + (this.port.get() == 0 ? "USE UPNP" : this.port.get()));
			    System.out.println("ban type: " + ((RadioButton) this.banType.getSelectedToggle()).getText());
			    System.out.println("draft mode: " + ((RadioButton) this.draftMode.getSelectedToggle()).getText());
			    System.out.println("timeout: " + ((RadioButton) this.outOfTime.getSelectedToggle()).getText());
			    System.out.println("random map: " + ((RadioButton) this.randomMap.getSelectedToggle()).getText());
			    System.out.println("force random: " + ((RadioButton) this.forceRandomMap.getSelectedToggle()).getText());
                            System.out.println("is captains mode: " + ((RadioButton)this.isCaptainsMode.getSelectedToggle()).getText());
			    System.out.println("export game history: "+this.exportGameHistory);
			    NautDrafter.getBase().startLoadingOverlay("Creating server");

                            if(serverHostRemote.isSelected()){  
			        Player player = NautDrafter.getBase().getPlayer();
                                if(player == null || !player.isSteam()){
                                    // do not create a server as they are not authenticated with steam!
                                    Platform.runLater(() -> new PopUp(
                                        this.resource.getString("server_config.create_server.no_steam"), 
                                        this.resource.getString("server_config.create_server.no_steam.title")
                                    ).showPopup());
                                    NautDrafter.getBase().stopLoadingOverlay();
                                }

                                CreateServerInfo info = new CreateServerInfo(
                                    this.finalName.get(),
                                    this.finalDetails.get(),
                                    this.map,
                                    this.isAutoRandomMap,
                                    this.config.getSchedule(),
                                    this.bantypeBi.isSelected(),
                                    this.draftModeNormal.isSelected(),
                                    this.isCaptainsModeEnabled.isSelected(),
                                    this.pickTypeBan.isSelected()
                                );
                                // send a REQ to the server!
                                //NautDrafter.getInstance().requestDedicatedServer();
                                LobbyClient.createDedicatedServer(this,info,player);
                                
                            }else{

                                new Thread(() -> {
                                    DraftingServer newServer;
                                    try {
                                        // TODO: add option for enabling/disabling LAN discovery in UI
                                        newServer = new DraftingServer(
                                            this.finalName.get(), 
                                            this.finalDetails.get(),
                                            this.isPasswordProtected ? this.finalPassword.get() : null, 
                                            this.map, 
                                            this.isAutoRandomMap, 
                                            "",
                                            this.port.get(),
                                            this.config.getSchedule(), this.directConnect.isSelected() ? null
                                                    : new InetSocketAddress(Constants.LOBBY_ADDRESS, Constants.LOBBY_PORT),
                                            this.bantypeBi.isSelected(), 
                                            this.draftModeNormal.isSelected(),
                                            true, 
                                            this.exportGameHistory,
                                            this.isCaptainsModeEnabled.isSelected(),
                                            false,
                                            this.pickTypeBan.isSelected()
                                        );
                                        new Thread(() -> newServer.run()).start();
                                        NautDrafter.getInstance().addNewDraftingServer(newServer);
                                    } catch (IOException ex) {
                                        Logger.getLogger(ServerConfigScreen.class.getName()).log(Level.SEVERE, null, ex);
                                        NautDrafter.getBase().stopLoadingOverlay();
                                        Platform.runLater(() -> new PopUp("Failed to create server", "ERROR").showPopup());
                                        return;
                                    }
                                    this.onServerCreated();
                                }).start();
                            }

		    });

		this.createButton.setDisable(true);

		this.nameField.textProperty().addListener((ov, oldVal, newVal) -> {
			if (newVal == null || newVal.isEmpty()) {
				this.createButton.setDisable(true);
			} else {
				this.createButton.setDisable(false);
			}
		});
                this.serverHost.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
                    @Override
                    public void changed(ObservableValue<? extends Toggle> ov,
                        Toggle old_toggle, Toggle new_toggle) {
                        if (serverHost.getSelectedToggle() != null) {
                            if(serverHost.getSelectedToggle() == serverHostRemote){
                                setRemoteServerControls();
                            }else{
                                setLocalServerControls();
                            }
                        }

                    }
                });


		Platform.runLater(() -> this.nameField.requestFocus());

                Player player = NautDrafter.getBase().getPlayer();
                if(player.isSteam()){
                    setRemoteServerControls();
                }else{
                    // disable the controls
                    setLocalServerControls();
                    this.serverHostRemote.disableProperty().set(true);
                    this.serverHostLocal.disableProperty().set(true);
                    this.serverHostLocal.selectedProperty().set(true);
                }
	}

	/**
	 * Setup the map pull down with the specified planetPool.
	 */
	private void setupMapPullDown(String planetPool, boolean forceRandom){
		// remove listener
	    	this.mapMenu.getSelectionModel().selectedItemProperty().removeListener(planetPullDownListener);
		//this.maps = FXCollections.observableArrayList(planetPoolManager.getValues(PlanetPoolManager.DEFAULT));
		this.maps = FXCollections.observableArrayList(planetPoolManager.getValues(planetPool));

		if(forceRandom){
			this.mapMenu.setDisable(true);
			this.mapPool.clear();
			this.mapPool.addAll(maps);
			this.maps = FXCollections.observableArrayList(planetPoolManager.getOnlyRandomMap());
		}else{
			this.mapMenu.setDisable(false);
		}
		this.mapMenu.setItems(this.maps);
		this.mapMenu.setButtonCell(new MapListCell());
		this.mapMenu.setCellFactory(p -> new MapListCell());
		this.mapMenu.getSelectionModel().selectedItemProperty().addListener(planetPullDownListener);
		this.mapMenu.getSelectionModel().select(0);
	}

    @Override
    public void onDedicatedServerResponseReceived(int created) {
        this.onServerCreated();

        switch(created){

            case DedicatedServerRequestCallback.RESPONSE_SUCCESS:
                Platform.runLater(() -> new PopUp(
                    this.resource.getString("server_config.create_server.success"), 
                    this.resource.getString("server_config.create_server.success.title")
                ).showPopup());
                break;

            case DedicatedServerRequestCallback.RESPONSE_FAILED_EXISTING_SERVER:
                Platform.runLater(() -> new PopUp(
                    this.resource.getString("server_config.create_server.failed.existing"), 
                    this.resource.getString("server_config.create_server.failed.title")
                ).showPopup());
                break;

            case DedicatedServerRequestCallback.RESPONSE_FAILED_NO_SERVERS:
                Platform.runLater(() -> new PopUp(
                    this.resource.getString("server_config.create_server.failed.no_servers"), 
                    this.resource.getString("server_config.create_server.failed.title")
                ).showPopup());
                break;
            case DedicatedServerRequestCallback.RESPONSE_FAILED_NO_STEAM:
                Platform.runLater(() -> new PopUp(
                    this.resource.getString("server_config.create_server.failed.no_steam"), 
                    this.resource.getString("server_config.create_server.failed.title")
                ).showPopup());
                break;
            default:
        }
    }
	private class PlanetPullDownListener implements ChangeListener<Planet> {
	    @Override
	    public void changed(ObservableValue observable, Planet oldValue, Planet newValue) {
		ServerConfigScreen.map = newValue.getId();
	    }
	}


	@Override
	public Node getRoot() {
		return this;
	}

	@Override
	public String getTitle() {
		return this.title;
	}

	@Override
	public MenuItem[] onShow() {
		return null;
	}

	@Override
	public MenuItem[] onHide() {
		// Not used
		return null;
	}

	/**
	 * Called when server is created
	 */
	public void onServerCreated() {
		NautDrafter.getBase().stopLoadingOverlay();
		NautDrafter.getBase().popMain();
	}

        /**
         * Set the controls for proper dedicated server requests.
         */
        private void setRemoteServerControls(){
            portField.disableProperty().set(true);
            passwordField.disableProperty().set(true);
            passwordField.clear();
            portField.disableProperty().set(true);
            portField.clear();
        }

        /**
         * Set the controls for local server requests.
         */
        private void setLocalServerControls(){
            portField.disableProperty().set(false);
            passwordField.disableProperty().set(false);
            passwordField.clear();
            portField.disableProperty().set(false);
            portField.clear();
        }

	private static class MapListCell extends ListCell<Planet> {
		private ImageListCell content;

		MapListCell() {
			this.content = new ImageListCell();

			// Removing the standard JFX list-cell class in to remove default padding and highlighting
			this.getStyleClass().clear();
		}

		@Override
		protected void updateItem(Planet item, boolean empty) {
			super.updateItem(item, empty);

			if (item == null || empty) {
				this.clearContent();
			} else {
				this.content.setImage(item.getImage(Planet.ImageType.DETAILED));
				this.content.setText(item.getName());

				this.setGraphic(this.content);
			}
		}

		private void clearContent() {
			this.setGraphic(null);
		}
	}

	private static class DraftingListCell extends ListCell<DraftingScheduleItem> {
		private HBox     content;
		private Label    title = new Label(); ;
		private Glow     glowEffect;
		private Timeline pulse;

		DraftingListCell() {
			this.content = new HBox();

			this.getStyleClass().clear();
			this.content.getStyleClass().addAll("timeline-conf", "menu");

			this.glowEffect = new Glow(0.5);
			this.pulse = new Timeline();

			this.pulse.getKeyFrames().addAll(
			        new KeyFrame(Duration.ZERO, new KeyValue(this.glowEffect.levelProperty(), 0.4)),
			        new KeyFrame(new Duration(1500), new KeyValue(this.glowEffect.levelProperty(), 0.8)));

			this.pulse.setCycleCount(Animation.INDEFINITE);
			this.pulse.setAutoReverse(true);

			this.addEventHandler(MouseEvent.MOUSE_ENTERED, event -> {
				this.content.setEffect(this.glowEffect);
				this.pulse.play();
			});

			this.addEventHandler(MouseEvent.MOUSE_EXITED, event -> {
				this.content.setEffect(null);
				this.pulse.stop();
			});
		}

		@Override
		protected void updateItem(DraftingScheduleItem item, boolean empty) {
			super.updateItem(item, empty);

			if (item == null || empty) {
				this.setGraphic(null);
			} else {
				this.content.getChildren().clear();
				this.title.setText(item.name + ": ");
				this.content.getChildren().add(this.title);
				this.content.getChildren().addAll(item.getLabelList());
				this.setGraphic(this.content);

			}
		}
	}

	private static class DraftingScheduleItem {
                //int schedule, 
                //String name, 
                //boolean isBanBi,
                //boolean isSeqDraft,
                //String planetPool,
                //boolean isRandomShown,
                //boolean forceRandomMap,
                //boolean exportGameResults,
                //boolean isCaptainsMode

		// schedule item int structure
		// 0b<last pick .... first pick><action count>
		// 0bxxxx = action count
		// 0b00   = Red Pick
		// 0b01   = Red Ban
		// 0b10   = Blue Pick
		// 0b11   = Blue Ban
		static DraftingScheduleItem[] getPresets() {
			return new DraftingScheduleItem[] {
			        new DraftingScheduleItem(0b10000010100011011000,
				    "Default", true, true, true, true, PlanetPoolManager.DEFAULT, 
				    true, false, false, false),
                                new DraftingScheduleItem(0b1000110110001101100011011100,
                                    "ALWB Season - Game 1",true, false, true,false,PlanetPoolManager.ALWB_GAME_1,
                                     true,false,false,true),
                                new DraftingScheduleItem(0b1000110110001101100011011100,
                                    "ALWB Season - Game 2",true, false, true,false,PlanetPoolManager.ALWB_GAME_2,
                                     true,false,false,true),
                                new DraftingScheduleItem(0b1000110110001101100011011100,
                                    "ALWB Season - Game 3",true, false, true,false,PlanetPoolManager.ALWB_GAME_3,
                                     true,false,false,true),
                                new DraftingScheduleItem(0b1000110110001101100011011100,
                                    "ALWB Playoffs",true, true, false, false,PlanetPoolManager.ALWB_PLAYOFFS,
                                     true,false,false,true),
			        new DraftingScheduleItem(0b001011010010100011011010, 
				    "Awesome Cup - Game 1",true, true, true, true, PlanetPoolManager.AWESOME_CUP, 
				    true, true, true, false),
			        new DraftingScheduleItem(0b001011010010100011011010, 
				    "Awesome Cup - Game X",true, true, true, true, PlanetPoolManager.AWESOME_CUP, 
				    true, false, true, false),
			        new DraftingScheduleItem(0b001011010010100011011010, 
				    "Awesome Cup - Best of 1",true, true, true, true, PlanetPoolManager.AWESOME_CUP_Bo1, 
				    true, false, true, false),
			        new DraftingScheduleItem(0b1000001010000110, 
				    "AAL - Game 1",true, false, true, true, PlanetPoolManager.ANAL, 
				    true, true, false, false),
			        new DraftingScheduleItem(0b10000010100001111000, 
				    "AAL - Game X",true, false, true, true, PlanetPoolManager.ANAL, 
				    true, false, false, false),
			        new DraftingScheduleItem(NautDrafter.getPreferences().getInt("last-draft-config",
			                0b1000110110001000110111011100), "Last used",true, false, true,true, PlanetPoolManager.DEFAULT, 
				    true, false, false, false) };
		}

		private int    data;
		private String name;
                private boolean isRemote;
                private boolean isPickBan;
		private boolean isBanBi;
                private boolean isSeqDraft;
		private String planetPool;
		private boolean isRandomShown;
		private boolean forceRandomMap;
		private boolean exportGameResults;
                private boolean isCaptainsMode;

		/**
		 * Construct a new item with a preset schedule
		 *
		 * @param schedule
		 *            data for timeline
		 * @param name
		 *            title of the preset
                 * @param isRemote
                 *            true if its a remote server and false otherwise.
                 * @param isPickBan
                 *            true if the pick means a ban on the opponents team.
		 * @param isBanBi
		 * 		true if bidirectional ban is enabled and false otherwise.
                 * @param isSeqDraft
                 *              true if a normal draft and false for concurrent.
		 * @param planetPool
		 * 		the name of the planet pool to use 
		 * @param isRandomShown
		 * 		true if the random map is shown
		 */
		public DraftingScheduleItem(
                    int schedule, 
                    String name, 
                    boolean isRemote,
                    boolean isPickBan,
		    boolean isBanBi,
                    boolean isSeqDraft,
                    String planetPool,
                    boolean isRandomShown,
		    boolean forceRandomMap,
                    boolean exportGameResults,
                    boolean isCaptainsMode) {
			this.data = schedule;
			this.name = name;
                        this.isRemote = isRemote;
                        this.isPickBan = isPickBan;
			this.isBanBi = isBanBi;
                        this.isSeqDraft = isSeqDraft;
			this.planetPool = planetPool;
			this.isRandomShown = isRandomShown;
			this.forceRandomMap = forceRandomMap;
			this.exportGameResults = exportGameResults;
                        this.isCaptainsMode = isCaptainsMode;
		}

		/**
		 * Decodes the data into actual labels
		 *
		 * @return what to display in the node
		 */
		public Label[] getLabelList() {
			int len = this.data & 0xf;
			int rest = this.data >> 4;

			Label[] result = new Label[len];

			for (int i = 0; i < len; i++) {
				int teamAndType = (rest >> (i * 2)) & 0b11;
				switch (teamAndType) {
				case 0b00:
					result[i] = new Label("Pick");
					result[i].getStyleClass().add("red-team");
					break;
				case 0b01:
					result[i] = new Label("Ban");
					result[i].getStyleClass().add("red-team");
					break;
				case 0b10:
					result[i] = new Label("Pick");
					result[i].getStyleClass().add("blue-team");
					break;
				case 0b11:
					result[i] = new Label("Ban");
					result[i].getStyleClass().add("blue-team");
					break;
				}
			}
			return result;
		}
	}

	@Override
	public String getDocIdentifier() {
		return "How to use the Server Configuration screen";
	}
}
