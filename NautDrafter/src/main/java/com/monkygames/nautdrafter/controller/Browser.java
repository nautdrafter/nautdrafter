/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.view.NotificationPopUp;

/**
 * A basic web browser. <br>
 * Note that the user may have chosen (in their preferences) to use the system web browser for displaying web-pages.
 * This class does not check whether or not this preference is set, so in most situations the calling function should
 * check if "useExternalBrowser" is false before continuing.
 */
public class Browser extends BorderPane {

	@FXML
	protected WebView   webview;

	@FXML
	private TextField   titleField;

	@FXML
	private Button      zoomOutButton;

	@FXML
	private Button      forwardButton;

	@FXML
	private Button      backButton;

	@FXML
	private Button      zoomInButton;

	@FXML
	private Button      externalBrowserButton;

	@FXML
	private HBox        rightButtons;

	@FXML
	private ProgressBar loadingBar;

	private String[]    allowedDomains;

	/**
	 * Creates a new browser instance, with a standard control bar. Should only be called when on the FX thread.
	 * 
	 * @param allowedDomains
	 *            Domains that this browser instance should be allowed to load, or empty to allow unrestricted access.
	 *            Attempting to access a page that does not fall within these domains will open the requested page in
	 *            the system's default browser. Matches against full domains, so allowing just "example.com" will still
	 *            prevent access to "sub.example.com". Provided domains are checked for an exact match against the host
	 *            portion of the requested URI - see {@link URI#getHost()}.
	 *
	 */
	public Browser(String... allowedDomains) {
		// Load xml
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
		        "/com/monkygames/nautdrafter/view/Browser.fxml"), LanguageResource.getLanguageResource());

		fxmlLoader.setController(this);
		fxmlLoader.setRoot(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		this.allowedDomains = allowedDomains;

		// Binding progress bar to the loading progress. A little extra progress is added so that the bar is visible
		// straight away
		this.loadingBar.progressProperty().bind(this.webview.getEngine().getLoadWorker().progressProperty().add(0.05));
		this.loadingBar.visibleProperty().bind(this.webview.getEngine().getLoadWorker().runningProperty());

		this.webview.getEngine().getHistory().currentIndexProperty().addListener((observable, oldValue, newValue) -> {
			if (!this.webview.getEngine().getHistory().getEntries().isEmpty()) {

				int index = this.webview.getEngine().getHistory().getCurrentIndex();
				// TODO this.setTitle(this.webview.getEngine().getHistory().getEntries().get(index).getTitle());

				// Disabling the back & forward buttons if there is nothing to go back or forward to
				this.backButton.setDisable(index == 0);
				this.forwardButton.setDisable(index + 1 >= this.webview.getEngine().getHistory().getEntries().size());
			}

		});

		this.webview.getEngine().locationProperty().addListener((observable, oldValue, newValue) -> {
			if (this.allowedDomains.length > 0) {
				try {
					URI uri = new URI(newValue);

					// Check the requested page is allowed
				for (String domain : this.allowedDomains) {
					if (uri.getHost().equals(domain)) {
						return;
					}
				}
			} catch (NullPointerException | URISyntaxException e) {
				e.printStackTrace();
			}

			// Requested domain not in the whitelist (or was not a valid URI), so it is opened in an external browser.
			// The webview's location property will already be the new value, so we have to start loading the previous
			// page then stop straight away in order for the location property to be correct. Also must be in a
			// runLater(), or the JVM will crash.
			Platform.runLater(() -> {
				this.webview.getEngine().load(oldValue);
				this.webview.getEngine().getLoadWorker().cancel();
			});
			this.openExternally(newValue);
		}
	}   );
	}

	/**
	 * Creates a new browser instance, intended to be used in an overlay. A "Close" button will be included (in addition
	 * to the default browser's toolbar), with the provided Runnable being called when the button is clicked. <br>
	 * Should only be called when on the FX thread.
	 * 
	 * @param closeAction
	 *            Runnable to run when the close button is fired.
	 * @param allowedDomains
	 *            Domains that this browser instance should be allowed to load, or empty to allow unrestricted access.
	 *            See {@link #Browser(String...)}
	 */
	public Browser(Runnable closeAction, String... allowedDomains) {
		this(allowedDomains);

		this.zoomInButton.setVisible(false);
		this.zoomOutButton.setVisible(false);

		ImageView closeIcon = new ImageView("/assets/images/icons/close-16px.png");
		Button closeButton = new Button(null, closeIcon);
		closeButton.setPrefSize(this.externalBrowserButton.getPrefWidth(), this.externalBrowserButton.getPrefHeight());

		closeButton.setOnAction(event -> closeAction.run());

		this.rightButtons.getChildren().add(closeButton);
	}

	/**
	 * Sets the user agent string reported by this browser instance. <br>
	 * See {@link WebEngine#setUserAgent(String)}
	 * 
	 * @see WebEngine#setUserAgent(String)
	 * @param uas
	 *            User Agent string to set for the browser
	 */
	public void setUserAgent(String uas) {
		this.webview.getEngine().setUserAgent(uas);
	}

	/**
	 * Load the given URL in this browser
	 * 
	 * @param url
	 *            Location to load
	 * @see WebEngine#load(String)
	 */
	public void load(String url) {
		this.webview.getEngine().load(url);
	}

	@FXML
	protected void goBack() {
		this.webview.getEngine().getHistory().go(-1);
	}

	@FXML
	protected void goForward() {
		this.webview.getEngine().getHistory().go(1);
	}

	@FXML
	protected void zoomIn() {
		if (this.webview.getZoom() < 4) {
			this.webview.setZoom(this.webview.getZoom() + 0.2);
		}
	}

	@FXML
	protected void zoomOut() {
		if (this.webview.getZoom() > 0.4) {
			this.webview.setZoom(this.webview.getZoom() - 0.2);
		}
	}

	@FXML
	protected void onWebviewScroll(ScrollEvent e) {
		// Zoom the page in and out on ctrl-scroll
		if (e.isControlDown()) {
			if (e.getDeltaY() < 0) {
				this.zoomOut();
			} else {
				this.zoomIn();
			}

			e.consume();
		}
	}

	/**
	 * Opens the current page in the system's default browser
	 */
	@FXML
	protected void openExternally() {
		this.openExternally(this.webview.getEngine().getLocation());
	}

	/**
	 * Opens the given URL in the system's default browser
	 * 
	 * @param url
	 *            Location to open
	 */
	private void openExternally(String url) {
		NautDrafter.getInstance().getHostServices().showDocument(url);
		new NotificationPopUp(LanguageResource.getLanguageResource().getString("browser.opened_external"),
		        NotificationPopUp.LENGTH_MEDIUM).show(this.getScene().getWindow());
	}
}
