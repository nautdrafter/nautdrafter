/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import java.io.IOException;
import java.util.ArrayList;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PopupControl;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.model.Character;
import com.monkygames.nautdrafter.model.Character.AttackType;
import com.monkygames.nautdrafter.model.Character.IconType;
import com.monkygames.nautdrafter.model.Character.Mobility;
import com.monkygames.nautdrafter.resource.LanguageResource;
import com.monkygames.nautdrafter.model.Planet;
import com.monkygames.nautdrafter.model.Planet.ImageType;
import com.monkygames.nautdrafter.network.DraftingClient;
import com.monkygames.nautdrafter.network.DraftingServer;
import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.Team;
import com.monkygames.nautdrafter.network.Timeline;
import com.monkygames.nautdrafter.network.Timeline.TimelineItem;
import com.monkygames.nautdrafter.network.callbacks.ChatCallback;
import com.monkygames.nautdrafter.network.callbacks.ClientDraftingCallback;
import com.monkygames.nautdrafter.network.callbacks.StageChangeCallback;
import com.monkygames.nautdrafter.network.callbacks.TeamCallback;
import com.monkygames.nautdrafter.resource.ImageCacher;
import com.monkygames.nautdrafter.view.SubScreen;

/**
 * This represents the drafting screen for the players, enabling the captains to pick and ban Naut while allowing the
 * other players to browse the Nauts and chat with the team
 */
public class DraftingScreenPlayer implements SubScreen, ChatCallback, ClientDraftingCallback, StageChangeCallback,
        TeamCallback {

	private VBox                               root;

	@FXML
	private Label                              redName;
	@FXML
	private Label                              blueName;
	@FXML
	private ImageView                          redCaptain;
	@FXML
	private ImageView                          redCaptainBadge;
	@FXML
	private ImageView                          redCrew1;
	@FXML
	private ImageView                          redCrew2;
	@FXML
	private ImageView                          blueCaptain;
	@FXML
	private ImageView                          blueCaptainBadge;
	@FXML
	private ImageView                          blueCrew1;
	@FXML
	private ImageView                          blueCrew2;
	@FXML
	private ImageView                          mapIcon;
	@FXML
	private FilterFlowPane<CharacterDisplayer> characterArray;
	@FXML
	private PickBanArea                        infoArea;
	@FXML
	private ChatBox                            chatBox;
	@FXML
	private CharacterDisplay                   characterDisplay;
	@FXML
	private HBox                               upComingThings;
	@FXML
	private VBox                               lefty;

	private ImageView[]                        blueIcons;
	private ImageView[]                        redIcons;

	private ToggleGroup                        characterButtons;
	private ArrayList<TimelineItem>            schedule;
	private int                                positionInSchedule;
	private int                                team;
	private int                                captainsPick = -1;
	NautPicksDecider                           picks;

	private boolean                            inDrafting   = true;
        /**
         * True if the pick/ban mode is sequential and false for concurrent.
         */
        private boolean                            isSequentialDraft = true;

	/**
	 * Create the drafting screen for the players
	 */
	public DraftingScreenPlayer() {
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(
		        "/com/monkygames/nautdrafter/view/DraftingScreenPlayer.fxml"), LanguageResource.getLanguageResource());

		fxmlLoader.setController(this);
		try {
			this.root = fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		this.blueIcons = new ImageView[] { this.blueCaptain, this.blueCrew1, this.blueCrew2 };
		this.redIcons = new ImageView[] { this.redCaptain, this.redCrew1, this.redCrew2 };

		// Adding tooltips for the captain badges
		Tooltip.install(this.blueCaptainBadge, new Tooltip(LanguageResource.getString("drafting.blue_captain")));
		Tooltip.install(this.redCaptainBadge, new Tooltip(LanguageResource.getString("drafting.red_captain")));

		// Linking the flow pane with the display area
		this.characterArray.prefWrapLengthProperty().set(540);
		this.characterButtons = new ToggleGroup();
		this.characterButtons.selectedToggleProperty().addListener((ov, o, n) -> {
			this.characterDisplay.setCharacter(n == null ? Character.UNKNOWN : ((CharacterDisplayer) n).character);
		});
		this.characterArray.getFilteredChildren().addAll(this.getCharacterDisplays());

		// Setting up listeners for messages and selections
		this.chatBox.setOnNewMessageListener(message -> DraftingClient.sendChatMessage(message));
		this.characterDisplay.setPickBanListener(selectedCharacter -> DraftingClient
		        .captainChooseNaut(selectedCharacter.id));

		// Filters
		this.characterArray.addFilter("Melee", item -> item.character.attack == AttackType.MELEE
		        || item.character.attack == AttackType.COMBINED);
		this.characterArray.addFilter("Ranged", item -> item.character.attack == AttackType.COMBINED
		        || item.character.attack == AttackType.SHORT_RANGE || item.character.attack == AttackType.LONG_RANGE
		        || item.character.attack == AttackType.MEDIUM_RANGE);
		this.characterArray.addFilter("Fast", item -> {
			return item.character.mobility == Mobility.SWIFT;
		});

		this.setUp(DraftingClient.getRedTeam(), 
                    DraftingClient.getBlueTeam(), 
                    Planet.getById(DraftingClient.getMapId()),
                    DraftingClient.isSequentialDraft());
		DraftingClient.setCallbacks(this);
		DraftingClient.requestInitCallbacks();
	}

	/**
	 * Sets up the drafting screen, including populating the appropriate UI elements with player and map info.<br/>
	 * Sets the appropriate action listeners for the things that this screen controls.<br/>
	 * Also sets whether the screen is in Captain Mode - i.e. whether the UI element for picking/banning characters is
	 * enabled for the local player.
	 *
	 * @param red
	 *            The red team object
	 * @param blue
	 *            The blue team object
	 * @param map
	 *            Map to display at the top of the screen
	 */
	public void setUp(Team red, Team blue, Planet map, boolean isSequentialDraft) {

		this.setTeams(red, blue);
		this.setMap(map);
                this.isSequentialDraft = isSequentialDraft;

		if (NautDrafter.isMe(red.players[0])) {
			this.characterDisplay.setCaptainMode(true, this.team = Team.RED);
                        this.characterDisplay.setDraftMode(isSequentialDraft);
			this.characterDisplay.characterProperty().addListener((ov, oldChar, newChar) -> {
				DraftingClient.captainSelectNaut(newChar.id);
			});
		} else if (NautDrafter.isMe(blue.players[0])) {
			this.characterDisplay.setCaptainMode(true, this.team = Team.BLUE);
                        this.characterDisplay.setDraftMode(isSequentialDraft);
			this.characterDisplay.characterProperty().addListener((ov, oldChar, newChar) -> {
				DraftingClient.captainSelectNaut(newChar.id);
			});
		} else {
			this.team = NautDrafter.isMe(red.players[1]) || NautDrafter.isMe(red.players[2]) ? Team.RED : Team.BLUE;
			this.characterDisplay.setCaptainMode(false, this.team);
                        this.characterDisplay.setDraftMode(isSequentialDraft);
		}
	}

        //TODO modify this method for handling sequential vs conncurrent picking
	@Override
	public void onCaptainChooseNaut(byte nautId, byte bonusTimeRemaining) {
		if (Platform.isFxApplicationThread()) {
			if (this.positionInSchedule >= this.schedule.size()) {
				return;
			}
			Character uiNautRepresentation = Character.getById(nautId);
			this.infoArea.notifyOfSelection(uiNautRepresentation, bonusTimeRemaining);
			this.characterDisplay.updateSchedule();
			if (uiNautRepresentation.id >= 0) {
				CharacterDisplayer display = this.characterArray.getFilteredChildren().get(uiNautRepresentation.id);
				TimelineItem current;
				if ((current = this.schedule.get(this.positionInSchedule++)).type == Timeline.BAN) {
					if (current.team != this.team || DraftingClient.isBanBidirectional()) {
						display.setSelected(false);
						display.setBanned(true);
					}
                                        // only play the sound for home team
                                        // otherwise two sounds will be played at once.
                                        if(!this.isSequentialDraft) {
                                            if(current.team == this.team){
					        NautDrafter.getAudioPlayer().playSound(uiNautRepresentation, false);
                                            }
                                        }else{
					    NautDrafter.getAudioPlayer().playSound(uiNautRepresentation, false);
                                        }
				} else {
					if (current.team == this.team) {
						display.setSelected(false);
						display.setPicked(true);
					}
                                        if(DraftingClient.isPickBan() && current.team != this.team){
                                            //Set the opponent's team naut as banned!
                                            display.setBanned(true);
                                        }
                                        if(!this.isSequentialDraft) {
                                            if(current.team == this.team){
					        NautDrafter.getAudioPlayer().playSound(uiNautRepresentation, true);
                                            }
                                        }else{
					    NautDrafter.getAudioPlayer().playSound(uiNautRepresentation, true);
                                        }
				}
			} else {
				this.positionInSchedule++;
			}
			this.updateUpComing();
		} else {
			Platform.runLater(() -> this.onCaptainChooseNaut(nautId, bonusTimeRemaining));
		}
	}

	/**
	 * Set the two teams information. This will populate the team name and player icons at the top of the screen
	 *
	 * @param red
	 *            The red team
	 * @param blue
	 *            The blue team
	 */
	public void setTeams(Team red, Team blue) {
		this.redName.setText(red.name);
		this.blueName.setText(blue.name);

		for (int i = 0; i < 3; i++) {
			this.redIcons[i].setImage(ImageCacher.get(red.players[i].iconUrl));
			this.blueIcons[i].setImage(ImageCacher.get(blue.players[i].iconUrl));

			Tooltip.install(this.redIcons[i], new Tooltip(red.players[i].name));
			Tooltip.install(this.blueIcons[i], new Tooltip(blue.players[i].name));

			if (NautDrafter.isMe(red.players[i])) {
				this.redIcons[i].getStyleClass().add("current-player");
			} else if (NautDrafter.isMe(blue.players[i])) {
				this.blueIcons[i].getStyleClass().add("current-player");
			}
		}
	}

	/**
	 * Set the map for use in displaying the icon at the top of the screen
	 *
	 * @param map
	 *            constant from {@link Planet}
	 */
	public void setMap(Planet map) {
		this.mapIcon.setImage(map.getImage(ImageType.MAP));

		Tooltip.install(
		        this.mapIcon,
		        new Tooltip(LanguageResource.getString("drafting.battle_info.tooltip.map")
		                + " "
		                + (map.getId() != Planet.UNKNOWN.getId() ? map.getName() : LanguageResource.getString(
		                        "drafting.battle_info.tooltip.map.random"))));
	}

	@Override
	public void onTimelineReceived(Timeline timeline, byte currentPhaseTime) {
		if (!Platform.isFxApplicationThread()) {
			Platform.runLater(() -> this.onTimelineReceived(timeline, currentPhaseTime));
			return;
		}
		this.positionInSchedule = timeline.pos;
		boolean isRed = timeline.items.get(this.positionInSchedule).team == Team.RED;
		Team red = DraftingClient.getRedTeam();
		Team blue = DraftingClient.getBlueTeam();
		this.schedule = timeline.items;

		int[] pick = new int[2], ban = new int[2];

		int index = 0;
		// Fill out nautId and pos fields
		for (TimelineItem i : this.schedule) {
			Team t = i.team == Team.RED ? red : blue;
			if (i.type == Timeline.PICK) {
				i.pos = pick[i.team - 1]++;
				i.charId = t.pickedNauts[i.pos];
			} else {
				i.pos = ban[i.team - 1]++;
				i.charId = t.bannedNauts[i.pos];
			}

			if (index++ < this.positionInSchedule) {
				CharacterDisplayer display = this.characterArray.getFilteredChildren().get(i.charId);

				if (i.type == Timeline.BAN) {
					display.setSelected(false);
					display.setBanned(true);
				} else {
					if (i.team == this.team) {
						display.setSelected(false);
						display.setPicked(true);
					}
				}
			}
		}

		this.infoArea.init(this.schedule, 
                    this.positionInSchedule, 
                    currentPhaseTime, 
                    isRed ? timeline.redBonus : timeline.blueBonus, 
                    isRed ? timeline.blueBonus : timeline.redBonus, ban[0]);
		this.characterDisplay.schedule(this.schedule, this.positionInSchedule);

		// quick timeline
		Label t;
		this.upComingThings.getChildren().add(t = new Label("Up coming: "));
		t.setMinWidth(Region.USE_PREF_SIZE);

		for (int ind = this.positionInSchedule; ind < this.schedule.size(); ind++) {
			TimelineItem i = this.schedule.get(ind);
			t = new Label(i.type == Timeline.PICK ? "Pick " : "Ban ");
			t.setMinWidth(Region.USE_PREF_SIZE);
			t.getStyleClass().add(i.team == Team.RED ? "red" : "blue");
			if (ind == this.positionInSchedule) {
				t.getStyleClass().add("current");
			}
			t.setOpacity(1 - (ind / 10d));
			this.upComingThings.getChildren().add(t);

		}
	}
	@Override
	public void onCaptainSelectNaut(byte nautId) {
		if (Platform.isFxApplicationThread()) {
			// Check for previous pick
			if (this.captainsPick != -1) {
				// Remove style class for captains highlight
				this.characterArray.getFilteredChildren().get(this.captainsPick).setSelectedByCaptain(false);
			}

			// No pick
			if (nautId == -1) {
				return;
			}

			this.captainsPick = nautId;

			// Add style class for captains pick
			this.characterArray.getFilteredChildren().get(nautId).setSelectedByCaptain(true);
		} else {
			Platform.runLater(() -> this.onCaptainSelectNaut(nautId));
		}
	}

	@Override
	public void onChatReceived(Player sender, String message) {
		this.chatBox.addPlayerChatMessage(sender, message);

	}

	@Override
	public Node getRoot() {
		return this.root;
	}

	@Override
	public String getTitle() {
		return LanguageResource.getString("drafting.title");
	}

	@Override
	public MenuItem[] onShow() {
		return null;
	}

	private void updateUpComing() {
		this.upComingThings.getChildren().remove(1);
		if (this.positionInSchedule >= this.schedule.size()) {
			return;
		}
		this.upComingThings.getChildren().get(1).getStyleClass().add("current");
		int ind = 0;
		for (Node n : this.upComingThings.getChildren()) {
			n.setOpacity(1 - (ind++ / 10d));
		}
	}

	/**
	 * @return the full list of all characters in the game put into a diplayer
	 */
	private CharacterDisplayer[] getCharacterDisplays() {
		Character[] chars = Character.values();
		CharacterDisplayer[] array = new CharacterDisplayer[chars.length - 2];
		for (int i = 0; i < chars.length - 2; i++) {
			array[i] = new CharacterDisplayer(chars[i]);
			array[i].setToggleGroup(this.characterButtons);
			final CharacterDisplayer display = array[i];

			CharacterDetailsPopup p = new CharacterDetailsPopup();
			p.setGraphic(array[i].popup);

			p.setAutoHide(false);

			array[i].hoverProperty().addListener(
			        (obv, oldV, newV) -> {
				        if (newV) {
					        Point2D p2 = display.localToScene(0.0, 0.0);
					        p.show(display, p2.getX() + display.getScene().getX()
					                + display.getScene().getWindow().getX() + display.getWidth() + 5, p2.getY()
					                + display.getScene().getY() + display.getScene().getWindow().getY());

				        } else {
					        p.hide();
				        }
			        });
		}
		return array;
	}

	/**
	 * Class to display the character icons and to be used as a toggle button <br/>
	 * Has a field called popup to be used for the hover display (which does not inherit styles from the main stage's
	 * CSS for some reason)
	 */
	public class CharacterDisplayer extends ToggleButton {
		public VBox popup;
		Character   character;
		Label[]     labels;
		Region      overlay;

		/**
		 * Create a new toggle that will display the given character
		 *
		 * @param character
		 *            {@link Character} to be displayed on this toggle
		 */
		public CharacterDisplayer(Character character) {
			this.getStyleClass().add("character");
			ImageView icon = new ImageView(character.getImage(IconType.ICON));
			icon.setFitHeight(64);
			icon.setFitWidth(64);
			StackPane container = new StackPane(icon, this.overlay = new Region());
			this.overlay.getStyleClass().setAll("overlay");
			StackPane.setMargin(icon, new Insets(5));
			this.setPadding(new Insets(0));
			this.setGraphic(container);

			this.character = character;

			// Set the popup
			String roles = "Roles: ";
			int i = 0;
			for (; i < character.roles.length - 1; i++) {
				roles += character.roles[i] + ", ";
			}
			roles += character.roles[i];
			this.labels = new Label[] { new Label("Name: " + character.name),
			        new Label("Health: " + character.getDisplayHealth()),
			        new Label("Speed: " + character.movementSpeed), new Label("Attack type: " + character.attack),
			        new Label("Mobility type: " + character.mobility), new Label(roles) };
			this.popup = new VBox(this.labels);
			for (Label l : this.labels) {
				l.setStyle(" -fx-text-fill: white;-fx-font: 17px Play, sans-serif;");
			}

		}

		@Override
		public void fire() {
			if (!this.isDisabled()
			        && !this.isBanned()
			        && (!this.isPicked() || (DraftingScreenPlayer.this.positionInSchedule < DraftingScreenPlayer.this.schedule
			                .size() && DraftingScreenPlayer.this.schedule
			                .get(DraftingScreenPlayer.this.positionInSchedule).type == Timeline.BAN))) {
				this.setSelected(!this.isSelected());
				this.fireEvent(new ActionEvent());
			}
		}

		private BooleanProperty banned;

		/**
		 * Property that determines whether or not this toggle's character has been banned by any team captain <br/>
		 * If this is set to true the BANNED psuedo class will be applied to the button
		 */
		public BooleanProperty bannedProperty() {
			if (this.banned == null) {
				this.banned = new SimpleBooleanProperty(false) {
					@Override
					public void invalidated() {
						CharacterDisplayer.this.pseudoClassStateChanged(BANNED, this.get());

						CharacterDisplayer.this.setFocusTraversable(!this.get());
					}

					@Override
					public Object getBean() {
						return CharacterDisplayer.this;
					}

					@Override
					public String getName() {
						return "banned";
					}
				};
			}
			return this.banned;
		}

		/**
		 * @param value
		 *            Whether or not this toggle's character has been banned by any team captain
		 */
		public final void setBanned(boolean value) {
			this.bannedProperty().set(value);
		}

		/**
		 * @return Whether or not this toggle's character has been banned by any team captain
		 */
		public final boolean isBanned() {
			return this.banned == null ? false : this.banned.get();
		}

		private BooleanProperty picked;

		/**
		 * Property that determines whether or not this toggle's character has been picked by this teams captain <br/>
		 * If this is set to true the PICKED psuedo class will be applied to the button
		 */
		public BooleanProperty pickedProperty() {
			if (this.picked == null) {
				this.picked = new SimpleBooleanProperty(false) {
					@Override
					public void invalidated() {
						CharacterDisplayer.this.pseudoClassStateChanged(PICKED, this.get());

						CharacterDisplayer.this.setFocusTraversable(!this.get());
					}

					@Override
					public Object getBean() {
						return CharacterDisplayer.this;
					}

					@Override
					public String getName() {
						return "picked";
					}
				};
			}
			return this.picked;
		}

		/**
		 * @param value
		 *            Whether or not this toggle's character has been picked by any team captain
		 */
		public final void setPicked(boolean value) {
			this.pickedProperty().set(value);
		}

		/**
		 * @return A boolean to represent whether or not this toggle's character has been picked by any team captain
		 */
		public final boolean isPicked() {
			return this.picked == null ? false : this.picked.get();
		}

		/**
		 * The property that determines whether this toggle is currently selected by the captain<br/>
		 *
		 * If this is set to true the CAPTAIN psuedo class will be applied to the button
		 */
		public final BooleanProperty selectedByCaptainProperty() {
			if (this.selectedItem == null) {
				this.selectedItem = new SimpleBooleanProperty(false) {

					@Override
					public void invalidated() {
						CharacterDisplayer.this.pseudoClassStateChanged(CAPTAIN, this.get());
					}

					@Override
					public Object getBean() {
						return CharacterDisplayer.this;
					}

					@Override
					public String getName() {
						return "selected character";
					}

				};
			}
			return this.selectedItem;
		}

		private BooleanProperty selectedItem;

		/**
		 * @param value
		 *            Whether or not this toggle's character has been selected by any team captain
		 */
		public final void setSelectedByCaptain(boolean value) {
			this.selectedByCaptainProperty().set(value);
		}

		/**
		 * @return A boolean to represent whether or not this toggle's character has been selected by any team captain
		 */
		public boolean isSelectedByCaptain() {
			return this.selectedItem == null ? false : this.selectedItem.get();

		}
	}

	/**
	 * {@link PseudoClass} for being banned
	 */
	private static final PseudoClass BANNED  = PseudoClass.getPseudoClass("banned");
	/**
	 * {@link PseudoClass} for being picked
	 */
	private static final PseudoClass PICKED  = PseudoClass.getPseudoClass("picked");
	/**
	 * {@link PseudoClass} for being currently selected by the captain
	 */
	private static final PseudoClass CAPTAIN = PseudoClass.getPseudoClass("captain");

	/**
	 * Implementation of the popup that is displayed when mousing over the toggle
	 */
	public class CharacterDetailsPopup extends PopupControl {

		/**
		 * Set the node to be displayed
		 *
		 * @param n
		 *            node to show
		 */
		public void setGraphic(VBox node) {

			this.getScene().setRoot(node);
			node.setStyle("-fx-background-color: #000000cc;-fx-background-radius: 5px; -fx-padding: 10px;");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.monkygames.nautdrafter.drafting.network.callbacks.StageChangeCallback#onStageChanged(int)
	 */
	@Override
	public void onStageChanged(int stage) {
		if (!Platform.isFxApplicationThread()) {
			Platform.runLater(() -> this.onStageChanged(stage));
			return;
		}
		if (stage == DraftingServer.STAGE_NAUT_SELECTION) {
			this.inDrafting = false;

			// Switch out UI for the naut picker thingy
			this.lefty.getChildren().clear();
			this.lefty.getChildren().add(this.picks = new NautPicksDecider(this.characterDisplay.isCaptain()));
			VBox.setVgrow(this.picks, Priority.ALWAYS);
			this.picks.setTeam(this.team);
			this.picks.init(this.team == Team.RED ? DraftingClient.getRedTeam() : DraftingClient.getBlueTeam(),
			        NautDrafter.getBase().getPlayer());
		} else if (stage == DraftingServer.STAGE_VERSUS) {
			NautDrafter.setScreen(new VersusScreen(), false);
		}
	}

	/*
	 * Implementing TeamCallback to listen for onLockTeam, all other methods are not used
	 */

	@Override
	public void onChangedTeam(Player player, int teamId, int position) {
	}

	@Override
	public void onCaptainSwap(int teamId, Player newCaptain, Player oldCaptain, int oldCaptainNewPosition) {
	}

	@Override
	public void onChangedTeamName(int teamId, String name) {
	}

	/*
	 * If our team is being locked, progress to the vs screen
	 */
	@Override
	public void onLockTeam(int teamId) {

		// If the check for in drafting was not here, this will be run when initCallbacks is called and mess things up
		if (!this.inDrafting && teamId == this.team) {
			NautDrafter.setScreen(new VersusScreen(), false);
		}
	}

	@Override
	public MenuItem[] onHide() {
		// Not used
		return null;
	}

	@Override
	public String getDocIdentifier() {
		return "How to use the Drafting screen (Captain & Player)";
	}
}
