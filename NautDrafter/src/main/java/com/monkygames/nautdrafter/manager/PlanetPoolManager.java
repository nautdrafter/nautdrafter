/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.manager;

import com.monkygames.nautdrafter.model.Planet;
import com.monkygames.nautdrafter.model.PlanetPool;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Manages planet pools that are used in the configuration.
 */
public class PlanetPoolManager {

    public static String DEFAULT = "Default";
    public static String ANAL = "ANAL";
    public static String AWESOME_CUP = "Awesome Cup";
    public static String AWESOME_CUP_Bo1 = "Awesome Cup - Best of 1";
    public static String ALWB_PLAYOFFS = "ALWB";
    public static String ALWB_GAME_1 = "ALWB Game 1";
    public static String ALWB_GAME_2 = "ALWB Game 2";
    public static String ALWB_GAME_3 = "ALWB Game 3";

    private HashMap<String,PlanetPool> planetPools;

    public PlanetPoolManager(){
	planetPools = new HashMap<String,PlanetPool>();

	// initialize pools
	planetPools.put(DEFAULT,new PlanetPool(DEFAULT,
	    Planet.UNKNOWN.getId(),
	    Planet.RIBBIT_IV.getId(),
	    Planet.AI_STATION_404.getId(),
	    Planet.SORONA.getId(),
	    Planet.AIGUILLON.getId(),
	    Planet.AI_STATION_205.getId()
	));

	planetPools.put(ANAL,new PlanetPool(ANAL,
	    Planet.RIBBIT_IV.getId(),
	    Planet.SORONA.getId(),
	    Planet.AIGUILLON.getId()
	));

	planetPools.put(AWESOME_CUP,new PlanetPool(AWESOME_CUP,
	    Planet.RIBBIT_IV.getId(),
	    Planet.SORONA.getId(),
	    Planet.AIGUILLON.getId()
	));
	planetPools.put(AWESOME_CUP_Bo1,new PlanetPool(AWESOME_CUP_Bo1,
	    Planet.AIGUILLON.getId()
	));
        planetPools.put(ALWB_PLAYOFFS,new PlanetPool(ALWB_PLAYOFFS,
            Planet.RIBBIT_IV.getId(), Planet.AIGUILLON.getId()
        ));
        planetPools.put(ALWB_GAME_1,new PlanetPool(ALWB_GAME_1,
	    Planet.AIGUILLON.getId()
        ));
        planetPools.put(ALWB_GAME_2,new PlanetPool(ALWB_GAME_2,
            Planet.RIBBIT_IV.getId()
        ));
        planetPools.put(ALWB_GAME_3,new PlanetPool(ALWB_GAME_3,
	    Planet.AIGUILLON.getId()
        ));
    }

    /**
     * Returns the name of the planet pool.
     * Note, use the static defined strings for the name of the pool.
     * @return the planet pool that matches the name or null if not found.
     */
    public PlanetPool getPlanetPool(String name){
	return planetPools.get(name);
    }

    /**
     * Returns an array of enums that represent the map pool.
     */
    public Planet[] getValues(String name){
	PlanetPool pool = getPlanetPool(name);
	if(pool == null){
	    return null;
	}
	Planet planets[] = new Planet[pool.size()];

	for(int i = 0; i < pool.size(); i++){
	    planets[i] = Planet.getById(pool.getPlanets().get(i));
	}
	return planets;
    }
    /**
     * Returns only the random planet.
     * @return an array of 1 element.
     */
    public Planet[] getOnlyRandomMap(){
	Planet planets[] = new Planet[1];
	planets[0] = Planet.UNKNOWN;
	return planets;
    }
}