package com.monkygames.nautdrafter.manager;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Label;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;


/**
 * Communicates with steamstats.com to get information on the player count.
 * Also updates the title bar.
 */
public class SteamStats implements Runnable{


    /**
     * The steam stats URL.
     */
    public static final String AWESOMENAUTS_URL = "http://steamcharts.com/app/204300#7d";
    public static final String DOMAIN = "steamcharts.com";

    /**
     * A connection to the url of the steamcharts.
     */
    private Connection connection;

    /**
     * The label that contains the player count.
     */
    private Label playerCountL;

    /**
     * The amount of time to wait to update.
     * 5 minutes of wait time between refreshes
     */
    public static long WAIT=1000*60*5;
    public boolean doRun = false;
    private Thread thread;
    boolean isWaiting = false;

    public SteamStats(Label playerCountL){
        this.playerCountL = playerCountL;
        connection = Jsoup.connect(AWESOMENAUTS_URL);
        updateLabel();
        doRun = true;
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        isWaiting = true;
        while(doRun){
            updateLabel();
            try {
                Thread.sleep(WAIT);
            } catch (InterruptedException ex) { }
        }
        isWaiting = false;
    }

    /**
     * Stops the thread and interrupts it.
     */
    public void stopThread(){
        doRun = false;
        thread.interrupt();
        while(isWaiting){
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) { }
        }
    }

    private String getPlayerCount(){
        String playerCount = "";
        try {
            Document doc;
            doc = connection.get();
            Element appStat = doc.select("div.app-stat").first();
            Element num = appStat.select("span.num").first();
            playerCount = num.text();

        } catch (IOException ex) {
            Logger.getLogger(SteamStats.class.getName()).log(Level.SEVERE, null, ex);
        }

        return playerCount;
    }

    /**
     * Updates the label so that its on the GFX thread.
     */

    private void updateLabel(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                playerCountL.setText("Players: "+getPlayerCount());
            }
        });

    }
}
