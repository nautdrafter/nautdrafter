package com.monkygames.nautdrafter.manager;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;


/**
 * Communicates with nautrankings.com to get information about the player.
 * Also updates the title bar.
 */
public class NautStats implements Runnable{


    public static final String URL = "http://www.nautsrankings.com/index.php?search=";
    public static final String DOMAIN = "www.nautsrankings.com";

    /**
     * A connection to the url of the nautsrankings.
     */
    private Connection connection;

    /**
     * The label that contains the player count.
     */
    //private Label playerCountL;

    /**
     * The amount of time to wait to update.
     * 5 minutes of wait time between refreshes
     */
    public static long WAIT=1000*60*5;
    public boolean doRun = false;
    private Thread thread;
    private String username;
    private Label rankL;
    private Separator separator;
    private ImageView leagueIcon;
    private boolean isWaiting;

    public NautStats(String username, Label rankL, Separator separator, ImageView leagueIcon){
        this.username   = username;
        this.rankL      = rankL;
        this.separator  = separator;
        this.leagueIcon = leagueIcon;
        connection      = Jsoup.connect(URL+username);
        doRun = true;
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        isWaiting = true;
        while(doRun){
            updateLabels();
            try {
                Thread.sleep(WAIT);
            } catch (InterruptedException ex) { }
        }
        isWaiting = false;
    }

    /**
     * Stops the thread and interrupts it.
     */
    public void stopThread(){
        doRun = false;
        thread.interrupt();
        while(isWaiting){
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) { }
        }
    }

    private String getPlayerRank(){
        String rankS = "";
        try {
            Document doc;
            doc = connection.get();
            Element rank = doc.select("div.rank").get(1);
            Element aRank = rank.getElementsByAttribute("name").first();
            rankS = aRank.text();

        } catch (IOException ex) {
            Logger.getLogger(NautStats.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rankS;
    }

    private String getPlayerLeague(){
        String leagueS = "";
        try {
            Document doc;
            doc = connection.get();
            Element rankings = doc.select("#rankings").first();
            Element img = rankings.getElementsByClass("league-img").first();
            leagueS = "http://www.nautsrankings.com/"+img.attr("src");
        } catch (IOException ex) {
            Logger.getLogger(NautStats.class.getName()).log(Level.SEVERE, null, ex);
        }
        return leagueS;
    }

    /**
     * Updates the label so that its on the GFX thread.
     */

    private void updateLabels(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                String rank = getPlayerRank();
                String iconS = getPlayerLeague();
                rankL.setText(rank);
                rankL.setVisible(true);
                separator.setVisible(true);
                Image image = new Image(iconS);
                leagueIcon.setImage(image);
                leagueIcon.setVisible(true);
            }
        });

    }
}
