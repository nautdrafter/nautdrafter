/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import com.monkygames.nautdrafter.NautDrafter;

/**
 * A dialog/popup window that can be used to prompt the user or provide them with information
 */
public class PopUp extends Stage {

	BorderPane                pane;
	Label                     message;
	Button                    close;
	HBox                      buttonBox;
	EventHandler<ActionEvent> handler       = (ActionEvent e) -> {
		                                        this.close();
	                                        };

	/**
	 * Creates a new popup window (but does not display it). By default the popup's owner will be the main NautDrafter
	 * window ({@link NautDrafter#stage}). {@link #initOwner(javafx.stage.Window)} should be called on the popup if this
	 * is not the desired setting.
	 * 
	 * @param message
	 *            Main message to display inside the popup window
	 * @param title
	 *            Short title for the popup window. May or may not be visible to the user, depending on the window
	 *            manager.
	 */
	public PopUp(String message, String title) {

		this.message = new Label(message);
		this.message.setWrapText(true);
		this.initModality(Modality.APPLICATION_MODAL);
		this.initOwner(NautDrafter.stage);
		this.setTitle(title);
		this.pane = new BorderPane();
		this.buttonBox = new HBox(this.close = new Button("OK"));
		this.buttonBox.setAlignment(Pos.CENTER);
		this.close.setOnAction(this.handler);
		BorderPane.setAlignment(this.buttonBox, Pos.CENTER);
		BorderPane.setAlignment(this.message, Pos.CENTER);
		BorderPane.setMargin(this.message, new Insets(5, 5, 5, 5));
		this.pane.setBottom(this.buttonBox);
		this.pane.setCenter(this.message);
		this.setScene(new Scene(this.pane));
		this.setResizable(false);
		this.sizeToScene();
		this.centerOnScreen();
		this.pane.getStylesheets().add("style/Base.css");
		this.getScene().getRoot().getStyleClass().add("dialog");
	}

	/**
	 * @param message
	 *            Main message to display inside the popup window
	 * @param title
	 *            Short title for the popup window. May or may not be visible to the user, depending on the window
	 *            manager.
	 * @param button1
	 *            Text to display on the first button (generally the affirmative or default action)
	 * @param button2
	 *            Text to display on the second button (generally the cancellation or secondary action)
	 * @param event1
	 *            Runnable to run if the first button is clicked
	 * @param event2
	 *            Runnable to run if the second button is clicked
	 */
	public PopUp(String message, String title, String button1, String button2, Runnable event1, Runnable event2) {
		this(message, title);
		this.buttonBox.getChildren().clear();

		Button b = new Button(button1);
		b.setOnAction((ActionEvent e) -> {
			event1.run();
			this.close();
		});

		Button b2 = new Button(button2);
		b2.setOnAction((ActionEvent e) -> {
			event2.run();
			this.close();
		});

		this.buttonBox.getChildren().addAll(b, b2);

	}

	/**
	 * @param message
	 *            Main message to display inside the popup window
	 * @param title
	 *            Short title for the popup window. May or may not be visible to the user, depending on the window
	 *            manager.
	 * @param button1
	 *            Text to display on the popup's button
	 * @param event1
	 *            Runnable to run if the popup's button is clicked
	 */
	public PopUp(String message, String title, String button1, Runnable event1) {
		this(message, title);
		this.buttonBox.getChildren().clear();

		Button b = new Button(button1);
		b.setOnAction((ActionEvent e) -> {
			event1.run();
			this.close();
		});

		this.buttonBox.getChildren().addAll(b);

	}

	/**
	 * Set the buttons after creating the popup. Will not auto close when the buttons are pushed and
	 * {@link #setReadyToClose()} must be called to close the window
	 *
	 * @param buttons
	 *            list of title,action pairs
	 */
	public void setButtons(ButtonCreator... buttons) {
		this.buttonBox.getChildren().clear();

		for (ButtonCreator b : buttons) {
			Button button = new Button(b.s);
			button.setOnAction(event -> {
				b.r.run();
			});
			this.buttonBox.getChildren().addAll(button);
		}
	}

	/**
	 * Displays the popup window. Should be called on the JavaFX Thread.
	 *
	 * @see javafx.stage.Stage#showAndWait
	 */
	public void showPopup() {
		this.showAndWait();
	}

	/**
	 * Change the initial message to a new string
	 *
	 * @param newMessage
	 *            the string to show as the message
	 */
	public void setMessage(String newMessage) {
		this.message.setText(newMessage);
	}

	/**
	 * Remove a button from the popup (if there exist one at the index)
	 *
	 * @param index
	 *            the index of the button
	 */
	public void removeButton(int index) {
		if (index >= 0 && index < this.buttonBox.getChildren().size()) {
			this.buttonBox.getChildren().remove(index);
		}
	}

	/**
	 * Simple class to link a message with a button
	 */
	public static class ButtonCreator {
		String   s;
		Runnable r;

		public ButtonCreator(String message, Runnable action) {
			this.s = message;
			this.r = action;
		}
	}
}
