/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.view;

import javafx.scene.Node;
import javafx.scene.control.MenuItem;

/**
 * Super class for all screens to implement so the base node can get correct info from it
 *
 * @author Adam
 */
public interface SubScreen {
	
	/**
	 * Gets the root node associated with this screen
	 *
	 * @return
	 */
	public Node getRoot();

	/**
	 * Gets the title associated with this screen
	 *
	 * @return
	 */
	public String getTitle();

	/**
	 * Called when screen is shown
	 *
	 * @return The menus to show that are specific to this screen. If none are to be added then return null
	 */
	public MenuItem[] onShow();
	
	/**
	 * Called when hidden
	 * 
	 * @return The menus to remove that are specific to this screen (should be the same instances as given for onShow()). If none were added then return null
	 */
	public MenuItem[] onHide();

	/**
	 * @return The page name used on the NautDrafter wiki for this screen's help page, or null of no such page exists
	 */
    public String getDocIdentifier();
}
