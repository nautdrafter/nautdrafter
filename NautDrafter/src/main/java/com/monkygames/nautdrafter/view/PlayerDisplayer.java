/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.view;

import com.monkygames.nautdrafter.resource.ImageCacher;
import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.NodeOrientation;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.Team;

/**
 * Small item that displays the players name and icon
 */
public class PlayerDisplayer extends HBox {

	@FXML
	private ImageView icon;
	@FXML
	private Label     name;
	private Player    player;

	/**
	 * Create an empty item
	 */
	public PlayerDisplayer() {

		FXMLLoader loader = new FXMLLoader(this.getClass().getResource(
		        "/com/monkygames/nautdrafter/view/PlayerDisplayer.fxml"));
		loader.setRoot(this);
		loader.setController(this);

		try {
			loader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	/**
	 * Create the component and fills it with the given player's details.
	 *
	 * @param player
	 *            the player whose details are to be displayed
	 */
	public PlayerDisplayer(Player player) {
		this(player, Team.NONE);
	}

	/**
	 * Create the component and fills it with the given player's details, as well as assigning it a CSS class based on
	 * the player's team.
	 *
	 * @param player
	 *            the player whose details are to be displayed
	 * @param team
	 *            The team that player belongs to. Used to style the element. Use constants from {@link Team}
	 */
	public PlayerDisplayer(Player player, int team) {
		this();
		this.setPlayer(player, team, false);
	}

	/**
	 * Set the displayer to show the details of a specific player
	 *
	 * @param player
	 *            The player whose details to show
	 * @param team
	 *            The team that player belongs to. Used to style the element. Use constants from {@link Team}
	 * @param reversed
	 *            set to reverse node orientation
	 */
	public void setPlayer(Player player, int team, boolean reversed) {
		if (reversed) {
			this.nodeOrientationProperty().set(NodeOrientation.RIGHT_TO_LEFT);
		} else {
			this.nodeOrientationProperty().set(NodeOrientation.LEFT_TO_RIGHT);
		}
		this.icon.setImage(ImageCacher.get(player.iconUrl));
		this.name.setText(player.name);
		this.setTeam(team);
		this.player = player;

		if (NautDrafter.isMe(player)) {
			this.icon.getStyleClass().add("current-player");
		}
	}

	/**
	 * Clear the player details
	 */
	public void clearPlayer() {
		if (this.player != null && NautDrafter.isMe(this.player)) {
			this.getStyleClass().remove("current-player");
		}
		this.player = null;
		this.icon.setImage(null);
		this.name.setText(null);
	}

	/**
	 * Get the associated player with this displayer
	 *
	 * @return Player object for the displaying player
	 */
	public Player getPlayer() {
		return this.player;
	}

	/**
	 * Sets the team that this player is associated with. This is used purely for changing this item's appearance - does
	 * not actually move the player between teams.
	 *
	 * @param team
	 *            The constant value associated with the team - use constants from {@link Team}
	 */
	public void setTeam(int team) {
		// Remove any existing team-related style classes
		this.getStyleClass().removeAll("blue-team", "red-team", "neutral-team");

		switch (team) {
		case Team.RED:
			this.getStyleClass().add("red-team");
			break;
		case Team.BLUE:
			this.getStyleClass().add("blue-team");
		default:
			this.getStyleClass().add("neutral-team");
			break;
		}
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (o instanceof PlayerDisplayer) {
			return ((PlayerDisplayer) o).player.equals(this.player);
		}
		return false;
	}
}
