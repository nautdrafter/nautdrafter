/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.view;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.resource.LanguageResource;

/**
 * Pane for using when a locking overlay is to be used
 *
 * @author agkf1
 */
public class LockerPane extends StackPane {

	private final BooleanProperty isOverlayShowing;
	private final StringProperty  overlayDescription;
	private BooleanProperty       isCancelable;
	/**
	 * Node that holds the "children"
	 */
	protected AnchorPane          main;

	private Label                 overlayWords;
	private Button                cancelButton;

	private final StackPane       overlay;
	private Runnable              onUnlock;

	/**
	 * Create an empty locker pane, default is set to locked to work around a bug in javaFX with the ProgressIndicator
	 */
	public LockerPane() {
		this.main = new AnchorPane();
		BorderPane.setAlignment(this.main, Pos.CENTER);
		this.main.setId("main");
		this.cancelButton = new Button(LanguageResource.getString("global.cancel"));
		this.cancelButton.setCancelButton(true);
		StackPane.setAlignment(this.cancelButton, Pos.BOTTOM_CENTER);

		this.overlayWords = new Label();

		this.overlay = new StackPane();
		this.overlay.getStyleClass().add("locked-overlay");
		this.getChildren().addAll(this.main, this.overlay);
		this.isOverlayShowing = new SimpleBooleanProperty(true);
		this.overlayDescription = new SimpleStringProperty("Loading");
		this.overlay.visibleProperty().bind(this.isOverlayShowing);
		this.main.disableProperty().bind(this.isOverlayShowing);
		this.overlayWords.textProperty().bind(this.overlayDescription);
		this.isCancelable = new SimpleBooleanProperty(false);

		// Only need to show cancel button when onUnlock is set
		this.cancelButton.visibleProperty().bind(this.isCancelable);
		this.cancelButton.setOnAction(action -> {
			this.unlock(true);
		});
	}

	/**
	 * Set the "child" node of the locker pane<br/>
	 * This node should be where the main content of the lockerpane goes, that is the content that will be locked when
	 * this pane is set to locked
	 *
	 * @param node
	 *            The node containing the main content for this pane
	 */
	public void setRootContent(Node node) {
		this.main.getChildren().clear();
		this.main.getChildren().add(node);
	}

	/**
	 * Show the locking overlay - disables all UI in main node
	 *
	 * @param content
	 *            Message to be displayed on the lock overlay when the screen is locked
	 */
	public void lock(String content) {
		this.overlayDescription.set(content);
		ProgressIndicator spinner = new ProgressIndicator(ProgressIndicator.INDETERMINATE_PROGRESS);
		HBox.setHgrow(spinner, Priority.NEVER);
		HBox toCenter = new HBox(spinner);
		toCenter.setAlignment(Pos.CENTER);

		this.lock(toCenter, this.overlayWords);
		this.isOverlayShowing.set(true);
		this.overlay.requestFocus();
	}

	/**
	 * Show the locking overlay - disables all UI in main node
	 *
	 * @param content
	 *            Message to be displayed on the lock overlay when the screen is locked
	 * @param onCancel
	 *            Runnable to be called if the user cancels the locking
	 */
	public void lock(String content, Runnable onCancel) {
		this.lock(content);
		this.onUnlock = onCancel;
		if (onCancel != null) {
			this.overlay.getChildren().add(this.cancelButton);
			this.isCancelable.set(true);
		}
	}

	/**
	 * Show the locking overlay - disables all UI in main node. The provided Node(s) will be displayed on the overlay,
	 * added as children of the overlay {@link StackPane}.
	 * 
	 * @param graphics
	 *            Node(s) to display on the overlay.
	 */
	public void lock(Node... graphics) {
		this.overlay.getChildren().clear();
		this.overlay.getChildren().addAll(graphics);
		this.isOverlayShowing.set(true);
		this.overlay.requestFocus();

	}

	/**
	 * Stop the overlay and unlock the UI. Removes all child nodes from the overlay.
	 */
	public void unlock() {
		this.isOverlayShowing.set(false);
		this.overlay.getChildren().clear();
		this.onUnlock = null;
		this.isCancelable.set(false);
	}

	/**
	 * Stop the overlay and unlock the UI. Removes all child nodes from the overlay.<br/>
	 * If onUnlock was set when locked, onUnlock will be called if isCanceled is true
	 * @see #unlock()
	 */
	public void unlock(boolean isCanceled) {
		if (this.onUnlock != null && isCanceled) {
			this.onUnlock.run();
		}
		this.unlock();
	}
}
