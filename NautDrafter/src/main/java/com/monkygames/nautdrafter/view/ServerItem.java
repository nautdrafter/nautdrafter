/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.view;

import java.net.InetAddress;
import java.util.Arrays;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.image.Image;

import com.monkygames.nautdrafter.network.ServerInfo;
import java.net.InetSocketAddress;

/**
 *
 * @author Adam
 */
public class ServerItem {

	private StringProperty                   name;
	private StringProperty                   playerCount;
	private int                              numOfPlayers;
	private BooleanProperty                  isLocked;
	private int                              map;
	private String                           details;
	private ObjectProperty<Image>            icon;
	private InetAddress                      address;
        private String                           dns;
	private int                              port;

	static final Image                       LOCK, UNLOCK;
	static {
		LOCK = new Image("/assets/images/icons/locked-16px.png");
		UNLOCK = new Image("/assets/images/icons/unlocked-16px.png");
	}

	/**
	 * Reference to itself for displaying in tableview
	 */
	public final ObservableValue<ServerItem> observable = new SimpleObjectProperty<>(this);

	/**
	 * Create a new ServerItem
	 *
	 * @param serverName
	 *            Name of the server
	 * @param details
	 *            Extra details of the server
	 * @param map
	 *            Id of the map
	 * @param lockStatus
	 *            Whether the server is password protected
	 */
	public ServerItem(String serverName, String details, int map, boolean lockStatus, int players) {
		this.details = details;
		this.map = map;
		this.name = new SimpleStringProperty(serverName);
		this.playerCount = new SimpleStringProperty(players + "/6");
		this.isLocked = new SimpleBooleanProperty(lockStatus);
		this.icon = new SimpleObjectProperty<>(lockStatus ? ServerItem.LOCK : ServerItem.UNLOCK);
	}

	public ServerItem(ServerInfo info) {
		this(info.name, info.description, info.mapId, info.hasPassword, info.playerCount);
		this.address = info.address;
                this.dns     = info.dns;
		this.port    = info.port;
                // set the address to the dns name instead of ip address.
                if(!dns.equals("")){
                    this.address = new InetSocketAddress(dns,port).getAddress();
                }
	}

	/**
	 * Set the current player count
	 *
	 * @param count
	 */
	public void setPlayerCount(int count) {
		this.numOfPlayers = count;
		this.playerCount.setValue(count + "/6");

	}

	/**
	 * Get the current player count
	 *
	 * @return
	 */
	public int getPlayerCount() {
		return this.numOfPlayers;
	}

	/**
	 * get the player count property
	 *
	 * @return
	 */
	public StringProperty playerCount() {
		return this.playerCount;
	}

	/**
	 * Set the server name
	 *
	 * @param newName
	 */
	public void setServerName(String newName) {
		this.name.setValue(newName);
	}

	/**
	 * Get the server name
	 *
	 * @return
	 */
	public String getServerName() {
		return this.name.get();
	}

	/**
	 * Get the server name property
	 *
	 * @return
	 */
	public StringProperty serverName() {
		return this.name;
	}

	/**
	 * Set the lock status
	 *
	 * @param lock
	 */
	public void setLockStatus(boolean lock) {
		this.isLocked.setValue(lock);
		this.icon.set(lock ? ServerItem.LOCK : ServerItem.UNLOCK);
	}

	/**
	 * Get the lock status
	 *
	 * @return
	 */
	public boolean getLocksStatus() {
		return this.isLocked.get();
	}

	/**
	 * Get the image associated with the lock status
	 *
	 * @return
	 */
	public ObjectProperty<Image> lockStatus() {
		return this.icon;
	}

	/**
	 * Get the details of the server
	 *
	 * @return
	 */
	public String getServerDetails() {
		return this.details;
	}

	/**
	 * Get the map id of the server
	 *
	 * @return
	 */
	public int getMapId() {
		return this.map;
	}

	/**
	 * Get the port number of the server
	 *
	 * @return
	 */
	public int getPort() {
		return this.port;
	}

        public String getDNS(){
            return this.getDNS();
        }

	/**
	 * Get the IP address of the server
	 *
	 * @return
	 */
	public InetAddress getAddress() {
            return this.address;
	}

	/**
	 * Update server item (currently only player count)
	 *
	 * @param info
	 *            Update using this information
	 */
	public void update(ServerInfo info) {
		this.setPlayerCount(info.playerCount);
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}

		// Test all fields in ServerInfo/Item except lock status and player count
		if (o instanceof ServerItem) {
			ServerItem item = (ServerItem) o;

			return item.getMapId() == this.map
			        && item.getServerName().equals(this.name.get())
			        && item.getServerDetails().equals(this.details)
			        && item.getPort() == this.port
			        && ((this.address == null || item.getAddress() == null) ? true : Arrays.equals(
			                this.address.getAddress(), item.getAddress().getAddress()));

		}
		return false;
	}

	@Override
	public String toString() {
		return this.getServerName();
	}
}
