/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.view;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.VBox;

/**
 * Popup with a password field that returns the input if {@link #showPopupForResult()} is called
 */
public class FieldPopUp extends PopUp {

	private String password;

	/**
	 * Creates a pop up that has a text field that is returned if {@link #showPopupForResult()} is called to show it<br/>
	 * The method returns when the user clicks the button or hits "Enter" or closes the window
	 *
	 * @param message
	 * @param buttonText
	 * @param title
	 */
	public FieldPopUp(String message, String buttonText, String title) {
		super(message, title);
		PasswordField field = new PasswordField();

		VBox box = new VBox(this.message, field);
		box.setAlignment(Pos.CENTER);
		this.pane.setCenter(box);
		this.buttonBox.setPadding(new Insets(5));
		this.close.removeEventHandler(ActionEvent.ACTION, this.handler);
		this.close.setOnAction(event -> {
			this.password = field.getText();
			this.close();
		});
		this.close.setText(buttonText);
		field.setOnAction(action -> this.close.fire());
	}

	/**
	 * Show the pop up and block until the user returns from this window<br/>
	 * Will return when the button is clicked or "Enter" is pushed or if the user uses the close button in the window
	 * (force closing) decoration
	 *
	 * @return The String that is in the text field or null if the user forced closed
	 */
	public String showPopupForResult() {
		this.showAndWait();
		return this.password;
	}

}
