/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.view;

import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PopupControl;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;

/**
 * Popup for notifying the user of something. Does not interact with any UI action <br/>
 */
public class NotificationPopUp extends PopupControl {

	/**
	 * Value that can be used to show the popup for a long period of time
	 */
	public static final long LENGTH_LONG   = 4800;
	/**
	 * Value that can be used to show the popup for a medium period of time
	 */
	public static final long LENGTH_MEDIUM = 2400;
	/**
	 * Value that can be used to show the popup for a short period of time
	 */
	public static final long LENGTH_SHORT  = 1000;

	private Behavior         timeline;
	HBox                     display;

	/**
	 * Create a new notification popup that is ready to be displayed.
	 *
	 * @param message
	 *            Message to display on the notification
	 * @param displayTimeMillis
	 *            delay before the popup will disappear (does not include time for any entrance and exit animations).
	 *            For consistency, it is recommended that {@link #LENGTH_LONG}, {@link #LENGTH_MEDIUM} or
	 *            {@link #LENGTH_SHORT} be used.
	 */
	public NotificationPopUp(String message, long displayTimeMillis) {
		Label l;
		this.display = new HBox(l = new Label(message));
		l.setStyle(" -fx-text-fill: #ffffffdd;-fx-font: 19px Play, sans-serif;");
		this.getScene().setRoot(this.display);
		this.display.setStyle("-fx-background-color: #000000cc;-fx-background-radius: 5px; -fx-padding: 10px;");
		this.display.setEffect(new DropShadow(15.0, new Color(0, 0, 0, 0.5)));
		this.timeline = new Behavior(new Duration(500), new Duration(displayTimeMillis), new Duration(500));

		this.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
			this.timeline.hideTransition.setRate(2);
			this.timeline.hideTransition.playFromStart();
		});
	}

	/**
	 * Starts the popup display by showing the message and positioning it at the center of the NautDrafter window. The
	 * popup will automatically be hidden after the time specified in the constructor.<br/>
	 * Should only be run from the FX thread
	 */
	public void showNotification(Scene parent) {

		// Show sort of centered
		this.show(parent.getRoot(), parent.getX() + parent.getWindow().getX() + parent.getWidth() / 2, parent.getY()
		        + parent.getWindow().getY() + parent.getHeight() / 2);

		// Adjust position to be centered
		this.setAnchorX(this.getAnchorX() - this.display.getWidth() / 2);
		this.setAnchorY(this.getAnchorY() - this.display.getHeight() / 2);

		// Stop any previous starting to the timers and start from beginning
		this.timeline.showTransition.stop();
		this.timeline.showTransition.playFromStart();

	}

	/**
	 * Implementation of the timings of the notification
	 */
	private class Behavior {

		private Timeline       durationTimer = new Timeline();
		private FadeTransition hideTransition;
		private FadeTransition showTransition;

		/**
		 * Create the timing behavior
		 *
		 * @param openDuration
		 *            duration for the entrance animation
		 * @param visibleDuration
		 *            duration for displaying
		 * @param closeDuration
		 *            duration for exit
		 */
		Behavior(Duration openDuration, Duration visibleDuration, Duration closeDuration) {

			// Fade in
			this.showTransition = new FadeTransition(openDuration, NotificationPopUp.this.display);
			this.showTransition.setFromValue(0);
			this.showTransition.setToValue(1);
			this.showTransition.setOnFinished(event -> this.durationTimer.playFromStart());
			this.showTransition.play();

			// Show
			this.durationTimer.getKeyFrames().add(new KeyFrame(visibleDuration));
			this.durationTimer.setOnFinished(event -> Behavior.this.hideTransition.playFromStart());

			// Fade out
			this.hideTransition = new FadeTransition(closeDuration, NotificationPopUp.this.display);
			this.hideTransition.setFromValue(1);
			this.hideTransition.setToValue(0);
			this.hideTransition.setOnFinished(event -> NotificationPopUp.this.hide());
		}
	}
}
