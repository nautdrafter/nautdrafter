/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.audio;

import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Random;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.model.Character;

/**
 * Class for playing sounds. It should be called to play anything, rather than directly using an OggPlayer.
 */
public class AudioPlayer {

	private static final String              SOUNDS_DIR = AudioPlayer.class.getResource("/assets/ronimo/audio/")
	                                                            .getPath();

	private static final Random              random     = new Random(System.currentTimeMillis());
	private static final ArrayList<String[]> pickSounds = new ArrayList<String[]>();
	private static final ArrayList<String[]> banSounds  = new ArrayList<String[]>();
	// Populate the sound arrays
	static {
		File sounds;
		for (Character ch : Character.values()) {
			if (ch != Character.UNKNOWN) {
				sounds = new File(SOUNDS_DIR + "picked/" + ch.shortName + "/");
				pickSounds.add(sounds.list());
				sounds = new File(SOUNDS_DIR + "banned/" + ch.shortName + "/");
				banSounds.add(sounds.list());
			}
		}
	}

	/**
	 * Plays a random sound associated with the specified character. If isPick is true one of the specified character's
	 * positive/happy catch-phrase will be played, otherwise one of the specified character's negative/unhappy
	 * catch-phrase will be played.
	 *
	 * @param character
	 *            The character whose catch-phrase should be played.
	 * @param isPick
	 *            True if you want a sound from the "Chosen" library (ie. a positive/happy catch-phrase) and false if
	 *            you want a sound from the "Killed" library (i.e. a negative/unhappy catch-phrase).
	 */
	public void playSound(Character character, boolean isPick) {
		// Check if the preferences allow us to play music
		if (NautDrafter.getPreferences().getBoolean("allowAudioPlayer", true)) {
			String soundDir = SOUNDS_DIR;

			if (isPick && pickSounds.get(character.id).length > 0) {
				soundDir += "picked/" + character.shortName + "/"
				        + pickSounds.get(character.id)[random.nextInt(pickSounds.get(character.id).length)];
			} else if (!isPick && banSounds.get(character.id).length > 0) {
				soundDir += "banned/" + character.shortName + "/"
				        + banSounds.get(character.id)[random.nextInt(banSounds.get(character.id).length)];
			} else {
				return;
			}

			try {
				soundDir = new File(soundDir).toURI().toURL().toString();
				OggPlayer.play(soundDir);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
	}
}
