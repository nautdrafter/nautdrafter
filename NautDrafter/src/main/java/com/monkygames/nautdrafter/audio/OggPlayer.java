/*
 * Most of the code in this file is from a tutorial about JOrbis by Jon Kristensen
 * http://www.jcraft.com/jorbis/tutorial/Tutorial.html
 * The tutorial is licensed under Creative Commons Attribution 3.0 https://creativecommons.org/licenses/by/3.0/
 */
package com.monkygames.nautdrafter.audio;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import com.jcraft.jogg.Packet;
import com.jcraft.jogg.Page;
import com.jcraft.jogg.StreamState;
import com.jcraft.jogg.SyncState;
import com.jcraft.jorbis.Block;
import com.jcraft.jorbis.Comment;
import com.jcraft.jorbis.DspState;
import com.jcraft.jorbis.Info;

/**
 * Plays OGG files
 */
public class OggPlayer {

	private Thread           thread;

	private InputStream      dataStream;

	/*
	 * We need a buffer, it's size, a count to know how many bytes we have read and an index to keep track of where we
	 * are. This is standard networking stuff used with read().
	 */
	byte[]                   buffer          = null;
	private static final int bufferSize      = 2048;
	int                      count           = 0;
	int                      index           = 0;

	/*
	 * JOgg and JOrbis require fields for the converted buffer. This is a buffer that is modified in regards to the
	 * number of audio channels. Naturally, it will also need a size.
	 */
	byte[]                   convertedBuffer;
	int                      convertedBufferSize;

	// The source data line onto which data can be written.
	private SourceDataLine   outputLine      = null;

	// PCM information
	private float[][][]      pcmInfo;
	private int[]            pcmIndex;

	private Packet           joggPacket      = new Packet();
	private Page             joggPage        = new Page();
	private StreamState      joggStreamState = new StreamState();
	private SyncState        joggSyncState   = new SyncState();

	private DspState         jorbisDspState  = new DspState();
	private Block            jorbisBlock     = new Block(this.jorbisDspState);
	private Comment          jorbisComment   = new Comment();
	private Info             jorbisInfo      = new Info();

	private OggPlayer(String oggFile) throws IOException {
		this.dataStream = new URL(oggFile).openStream();
	}

	/**
	 * This method reads the header of the stream, which consists of three packets.
	 * 
	 * @return true if the header was successfully read, false otherwise
	 */
	private boolean readHeader() {
		boolean needMoreData = true;

		/*
		 * We will read the first three packets of the header. We start off by defining packet = 1 and increment that
		 * value whenever we have successfully read another packet.
		 */
		int packet = 1;

		/*
		 * While we need more data (which we do until we have read the three header packets), this loop reads from the
		 * stream and has a big <code>switch</code> statement which does what it's supposed to do in regards to the
		 * current packet.
		 */
		while (needMoreData) {
			// Read from the InputStream.
			try {
				this.count = this.dataStream.read(this.buffer, this.index, bufferSize);
			} catch (IOException exception) {
				System.err.println("Could not read from the input stream.");
				System.err.println(exception);
			}

			// We let SyncState know how many bytes we read.
			this.joggSyncState.wrote(this.count);

			/*
			 * We want to read the first three packets. For the first packet, we need to initialize the StreamState
			 * object and a couple of other things. For packet two and three, the procedure is the same: we take out a
			 * page, and then we take out the packet.
			 */
			switch (packet) {
			// The first packet.
			case 1:
				// We take out a page.
				switch (this.joggSyncState.pageout(this.joggPage)) {
				// If there is a hole in the data, we must exit.
				case -1:
					System.err.println("There is a hole in the first " + "packet data.");
					return false;

					// If we need more data, we break to get it.
				case 0:
					break;

				/*
				 * We got where we wanted. We have successfully read the first packet, and we will now initialize and
				 * reset StreamState, and initialize the Info and Comment objects. Afterwards we will check that the
				 * page doesn't contain any errors, that the packet doesn't contain any errors and that it's Vorbis
				 * data.
				 */
				case 1:
					// Initializes and resets StreamState.
					this.joggStreamState.init(this.joggPage.serialno());
					this.joggStreamState.reset();

					// Initializes the Info and Comment objects.
					this.jorbisInfo.init();
					this.jorbisComment.init();

					// Check the page (serial number and stuff).
					if (this.joggStreamState.pagein(this.joggPage) == -1) {
						System.err.println("We got an error while " + "reading the first header page.");
						return false;
					}

					/*
					 * Try to extract a packet. All other return values than "1" indicates there's something wrong.
					 */
					if (this.joggStreamState.packetout(this.joggPacket) != 1) {
						System.err.println("We got an error while " + "reading the first header packet.");
						return false;
					}

					/*
					 * We give the packet to the Info object, so that it can extract the Comment-related information,
					 * among other things. If this fails, it's not Vorbis data.
					 */
					if (this.jorbisInfo.synthesis_headerin(this.jorbisComment, this.joggPacket) < 0) {
						System.err.println("We got an error while " + "interpreting the first packet. "
						        + "Apparantly, it's not Vorbis data.");
						return false;
					}

					// We're done here, let's increment "packet".
					packet++;
					break;
				}

				/*
				 * Note how we are NOT breaking here if we have proceeded to the second packet. We don't want to read
				 * from the input stream again if it's not necessary.
				 */
				if (packet == 1) {
					break;
				}

				// The code for the second and third packets follow.
			case 2:
			case 3:
				// Try to get a new page again.
				switch (this.joggSyncState.pageout(this.joggPage)) {
				// If there is a hole in the data, we must exit.
				case -1:
					System.err.println("There is a hole in the second " + "or third packet data.");
					return false;

					// If we need more data, we break to get it.
				case 0:
					break;

				/*
				 * Here is where we take the page, extract a packet and and (if everything goes well) give the
				 * information to the Info and Comment objects like we did above.
				 */
				case 1:
					// Share the page with the StreamState object.
					this.joggStreamState.pagein(this.joggPage);

					/*
					 * Just like the switch(...packetout...) lines above.
					 */
					switch (this.joggStreamState.packetout(this.joggPacket)) {
					// If there is a hole in the data, we must exit.
					case -1:
						System.err.println("There is a hole in the first" + "packet data.");
						return false;

						// If we need more data, we break to get it.
					case 0:
						break;

					// We got a packet, let's process it.
					case 1:
						/*
						 * Like above, we give the packet to the Info and Comment objects.
						 */
						this.jorbisInfo.synthesis_headerin(this.jorbisComment, this.joggPacket);

						// Increment packet.
						if (++packet >= 4) {
							// There is no fourth packet, so we will just end the loop here.
							needMoreData = false;
						}
						break;
					}
					break;
				}
				break;
			}

			// We get the new index and an updated buffer.
			this.index = this.joggSyncState.buffer(bufferSize);
			this.buffer = this.joggSyncState.data;

			/*
			 * If we need more data but can't get it, the stream doesn't contain enough information.
			 */
			if (this.count == 0 && needMoreData) {
				System.err.println("Not enough header data was supplied.");
				return false;
			}
		}

		return true;
	}

	/**
	 * This method starts the sound system. It starts with initializing the <code>DspState</code> object, after which it
	 * sets up the <code>Block</code> object. Last but not least, it opens a line to the source data line.
	 * 
	 * @return true if the sound system was successfully started, false otherwise
	 */
	private boolean initSound() {
		// This buffer is used by the decoding method.
		this.convertedBufferSize = bufferSize * 2;
		this.convertedBuffer = new byte[this.convertedBufferSize];

		// Initializes the DSP synthesis.
		this.jorbisDspState.synthesis_init(this.jorbisInfo);

		// Make the Block object aware of the DSP.
		this.jorbisBlock.init(this.jorbisDspState);

		// Wee need to know the channels and rate.
		int channels = this.jorbisInfo.channels;
		int rate = this.jorbisInfo.rate;

		// Creates an AudioFormat object and a DataLine.Info object.
		AudioFormat audioFormat = new AudioFormat(rate, 16, channels, true, false);
		DataLine.Info datalineInfo = new DataLine.Info(SourceDataLine.class, audioFormat, AudioSystem.NOT_SPECIFIED);

		// Check if the line is supported.
		if (!AudioSystem.isLineSupported(datalineInfo)) {
			System.err.println("Audio output line is not supported.");
			return false;
		}

		/*
		 * Everything seems to be alright. Let's try to open a line with the specified format and start the source data
		 * line.
		 */
		try {
			this.outputLine = (SourceDataLine) AudioSystem.getLine(datalineInfo);
			this.outputLine.open(audioFormat);
		} catch (LineUnavailableException exception) {
			System.out.println("The audio output line could not be opened due " + "to resource restrictions.");
			System.err.println(exception);
			return false;
		} catch (IllegalStateException exception) {
			System.out.println("The audio output line is already open.");
			System.err.println(exception);
			return false;
		} catch (SecurityException exception) {
			System.out.println("The audio output line could not be opened due " + "to security restrictions.");
			System.err.println(exception);
			return false;
		}

		// Start it.
		this.outputLine.start();

		/*
		 * We create the PCM variables. The index is an array with the same length as the number of audio channels.
		 */
		this.pcmInfo = new float[1][][];
		this.pcmIndex = new int[this.jorbisInfo.channels];

		return true;
	}

	/**
	 * Decodes the current packet and sends it to the audio output line.
	 */
	private void decodeCurrentPacket() {
		int samples;

		// Check that the packet is a audio data packet etc.
		if (this.jorbisBlock.synthesis(this.joggPacket) == 0) {
			// Give the block to the DspState object.
			this.jorbisDspState.synthesis_blockin(this.jorbisBlock);
		}

		// We need to know how many samples to process.
		int range;

		/*
		 * Get the PCM information and count the samples. And while these samples are more than zero...
		 */
		while ((samples = this.jorbisDspState.synthesis_pcmout(this.pcmInfo, this.pcmIndex)) > 0) {
			// We need to know for how many samples we are going to process.
			if (samples < this.convertedBufferSize) {
				range = samples;
			} else {
				range = this.convertedBufferSize;
			}

			// For each channel...
			for (int i = 0; i < this.jorbisInfo.channels; i++) {
				int sampleIndex = i * 2;

				// For every sample in our range...
				for (int j = 0; j < range; j++) {
					/*
					 * Get the PCM value for the channel at the correct position.
					 */
					int value = (int) (this.pcmInfo[0][i][this.pcmIndex[i] + j] * 32767);

					/*
					 * We make sure our value doesn't exceed or falls below +-32767.
					 */
					if (value > 32767) {
						value = 32767;
					}
					if (value < -32768) {
						value = -32768;
					}

					/*
					 * It the value is less than zero, we bitwise-or it with 32768 (which is 1000000000000000 = 10^15).
					 */
					if (value < 0) {
						value = value | 32768;
					}

					/*
					 * Take our value and split it into two, one with the last byte and one with the first byte.
					 */
					this.convertedBuffer[sampleIndex] = (byte) (value);
					this.convertedBuffer[sampleIndex + 1] = (byte) (value >>> 8);

					/*
					 * Move the sample index forward by two (since that's how many values we get at once) times the
					 * number of channels.
					 */
					sampleIndex += 2 * (this.jorbisInfo.channels);
				}
			}

			// Write the buffer to the audio output line.
			this.outputLine.write(this.convertedBuffer, 0, 2 * this.jorbisInfo.channels * range);

			// Update the DspState object.
			this.jorbisDspState.synthesis_read(range);
		}
	}

	/**
	 * Reads the audio file data until the end of the file
	 */
	private void read() {
		/*
		 * Variable used in loops below, like in readHeader(). While we need more data, we will continue to read from
		 * the InputStream.
		 */
		boolean needMoreData = true;

		while (needMoreData) {
			switch (this.joggSyncState.pageout(this.joggPage)) {
			// If there is a hole in the data, we just proceed.
			// If we need more data, we break to get it.
			case -1:
			case 0:
				break;

			// If we have successfully checked out a page, we continue.
			case 1:
				// Give the page to the StreamState object.
				this.joggStreamState.pagein(this.joggPage);

				// If granulepos() returns "0", we don't need more data.
				if (this.joggPage.granulepos() == 0) {
					needMoreData = false;
					break;
				}

				// Here is where we process the packets.
				processPackets: while (true) {
					switch (this.joggStreamState.packetout(this.joggPacket)) {
					// Is it a hole in the data?
					// If we need more data, we break to get it.
					case -1:
					case 0:
						break processPackets;

					/*
					 * If we have the data we need, we decode the packet.
					 */
					case 1:
						this.decodeCurrentPacket();
					}
				}

				/*
				 * If the page is the end-of-stream, we don't need more data.
				 */
				if (this.joggPage.eos() != 0) {
					needMoreData = false;
				}
			}

			// If we need more data...
			if (needMoreData) {
				// We get the new index and an updated buffer.
				this.index = this.joggSyncState.buffer(bufferSize);
				if (this.index < 0 || this.dataStream == null) {
					return;
				}
				this.buffer = this.joggSyncState.data;

				// Read from the InputStream.
				try {
					this.count = this.dataStream.read(this.buffer, this.index, bufferSize);
				} catch (Exception e) {
					e.printStackTrace();
					return;
				}

				// We let SyncState know how many bytes we read.
				this.joggSyncState.wrote(this.count);

				// There's no more data in the stream.
				if (this.count == 0) {
					needMoreData = false;
				}
			}
		}
	}

	/**
	 * Clears the JOgg/JOrbis objects and closes the InputStream
	 */
	private void cleanUp() {

		// Clear the necessary JOgg/JOrbis objects.
		this.joggStreamState.clear();
		this.jorbisBlock.clear();
		this.jorbisDspState.clear();
		this.jorbisInfo.clear();
		this.joggSyncState.clear();

		// play the rest of the audio, then close the audio connection
		this.outputLine.drain();
		this.outputLine.close();

		// Closes the stream.
		if (this.dataStream != null) {
			try {
				this.dataStream.close();
			} catch (Exception e) {
			}
		}
	}

	/**
	 * Attempts to play the file
	 * 
	 * @param oggFile
	 *            The OGG file to play
	 * @return Reference to the player so it can be stopped if necessary
	 */
	public static OggPlayer play(String oggFile) {
		try {
			OggPlayer player = new OggPlayer(oggFile);
			(player.thread = new Thread(() -> {
				player.joggSyncState.init();
				player.joggSyncState.buffer(bufferSize);
				player.buffer = player.joggSyncState.data;
				if (player.readHeader() && player.initSound()) {
					player.read();
				}
				player.cleanUp();
			})).start();
			return player;
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 * Attempts to cancel playing the audio<br>
	 * Currently does this by closing the input stream, which means any buffered data will still be played.
	 */
	public void stop() {
		if (this.dataStream != null) {
			try {
				InputStream tmp = this.dataStream;
				synchronized (this.dataStream) {
					this.outputLine.stop();
					this.dataStream = null;
				}
				tmp.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			if (this.thread != null) {
				this.thread.join();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
