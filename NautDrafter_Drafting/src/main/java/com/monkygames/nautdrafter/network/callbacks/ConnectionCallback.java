/*
 * Copyright (C) 2014 NautDrafter Drafting
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.callbacks;

/**
 * Callback for a connection being established, or failing
 */
public interface ConnectionCallback {

	/**
	 * Connection was successfully established
	 *
	 * @param stage
	 *            The current stage of the server
	 */
	public void onConnectionSucceeded(int stage);

	/**
	 * Connection failed
	 *
	 * @param id
	 *            The id of the error
	 */
	public void onConnectionFailed(byte id);

}
