/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.callbacks.TeamCallback;

/**
 * A player is asking the Drafting Server if they can move to a new position
 */
public class TeamChangeRequestPacket extends Packet {
	protected Integer      team, position;

	protected TeamCallback receiver;
	protected Player       player;

	/**
	 * Creates a new TeamChangeRequestPacket for decoding
	 *
	 * @param data
	 *            The packet data
	 * @param receiver
	 *            The callback to call when the packet is decoded
	 * @param player
	 *            The player who made this request
	 * @throws PacketDecoderException
	 */
	public TeamChangeRequestPacket(byte[] data, TeamCallback receiver, Player player) throws PacketDecoderException {
		super(data);
		this.receiver = receiver;
		this.player = player;
	}

	/**
	 * Constructs a new TeamChangeRequestPacket for sending
	 *
	 * @param team
	 *            The team we want to move to
	 * @param position
	 *            The position we want to move to
	 * @throws PacketEncoderException
	 */
	public TeamChangeRequestPacket(int team, int position) throws PacketEncoderException {
		this.team = team;
		this.position = position;

		this.data = new byte[2];
		this.data[0] = Packet.TEAM_CHANGE_PACKET_ID;

		this.data[1] = (byte) ((this.team << 4) | this.position);
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		// teamId & position
		if (this.team == null && this.position == null) {
			if (this.available() < 1) {
				return false;
			}
			int encoded = this.data[this.pos++];

			this.team = ((encoded & 0xF0) >> 4);
			this.position = (encoded & 0x0F);
		}
		return true;
	}

	@Override
	protected void handleImpl() {
		this.receiver.onChangedTeam(this.player, this.team, this.position);
	}
}
