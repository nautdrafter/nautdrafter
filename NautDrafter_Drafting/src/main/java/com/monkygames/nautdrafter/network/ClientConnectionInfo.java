/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network;

/**
 * Stores information about a client and check whether they have authenticated yet or not
 */
public class ClientConnectionInfo extends ConnectionInfo {
	private boolean authenticated;
	private int     type = 0;

	/**
	 * Creates a authenticated ConnectionInfo
	 *
	 * @param upgrade
	 *            A ConnectionInfo that contains the authenticated client
	 * @param authenticated
	 *            Boolean for whether the client is authenticated or not
	 */
	public ClientConnectionInfo(ConnectionInfo upgrade, boolean authenticated) {
		super(upgrade);
		this.authenticated = authenticated;
	}

	/**
	 * @return Whether this has been authenticated or not
	 */
	public boolean isAuthenticated() {
		return this.authenticated;
	}

	/**
	 * Set that this client has been authenticated
	 */
	public void setAuthenticated() {
		this.authenticated = true;
	}

	/**
	 * Sets the type of this connection to spectator<br>
	 * Only works once
	 */
	public void setSpectator() {
		if (this.type == 0) {
			this.type = 1;
		}
	}

	/**
	 * Sets the type of this connection to player<br>
	 * Only works once
	 */
	public void setPlayer() {
		if (this.type == 0) {
			this.type = 2;
		}
	}

	/**
	 * @return true if this connection has declared whether it is a spectator or player
	 */
	public boolean isDeclared() {
		return this.type != 0;
	}

	/**
	 * @return true if this connection is a spectator
	 */
	public boolean isSpectator() {
		return this.type == 1;
	}

	/**
	 * @return true if this connection is a player
	 */
	public boolean isPlayer() {
		return this.type == 2;
	}
}
