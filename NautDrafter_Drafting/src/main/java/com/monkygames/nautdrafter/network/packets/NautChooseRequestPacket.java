/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.callbacks.NautChooseCallback;

/**
 * Used for a player to request a naut selection to the server
 */
public class NautChooseRequestPacket extends Packet {

	protected NautChooseCallback receiver;
	protected Byte               position;

	/**
	 * Encodes a new NautSelectionRequestPacket
	 *
	 * @param position
	 *            The position (naut) that was selected
	 * @throws PacketEncoderException
	 */
	public NautChooseRequestPacket(byte position) {
		this.data = new byte[] { Packet.NAUT_CHOOSE_PACKET_ID, position };
	}

	/**
	 * Creates a new NautSelectionRequestCallback for decoding
	 *
	 * @param data
	 *            The data to decode
	 * @param receiver
	 *            The callback to call once decoded
	 */
	public NautChooseRequestPacket(byte[] data, NautChooseCallback receiver) {
		super(data);
		this.receiver = receiver;
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		if (this.position == null) {
			if (this.available() < 1) {
				return false;
			}
			this.position = this.data[this.pos++];
		}
		return true;
	}

	@Override
	protected void handleImpl() {
		this.receiver.onPlayerChoose(null, (byte) 0, this.position);
	}
}
