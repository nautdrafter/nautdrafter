/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network;

/**
 * Counts down the remaining bonus time
 */
public class BonusTimer extends Thread {

	private byte           remainingBonus;
	private long           phaseStartTime = 0;
	private long           bonusStartTime = 0;
	private final Runnable onTimeout;

	/**
	 * Creates a new BonusTimer and sets the remaining_bonus to a value
	 *
	 * @param bonus
	 *            The remaining bonus time
	 * @param selector
	 *            The selector of the server which owns us
	 */
	public BonusTimer(byte bonus, Runnable onTimeout) {
		this.remainingBonus = bonus;
		this.onTimeout = onTimeout;
	}

	private static byte getElapsedSeconds(long start) {
		return (byte) ((System.currentTimeMillis() - start) / 1000);
	}

	@Override
	public void run() {
		this.phaseStartTime = System.currentTimeMillis();
		try {
			Thread.sleep(Timeline.PHASE_LENGTH * 1000);
		} catch (Exception e) {
			return;
		}
		this.bonusStartTime = System.currentTimeMillis();
		try {
			Thread.sleep(this.remainingBonus * 1000);
		} catch (Exception e) {
			this.remainingBonus -= BonusTimer.getElapsedSeconds(this.bonusStartTime);
			return;
		}
		this.remainingBonus = 0;
		this.onTimeout.run();
	}

	/**
	 * Stops the timer
	 *
	 * @return the remaining bonus time
	 */
	public byte time() {
		this.interrupt();
		try {
			this.join();
		} catch (InterruptedException e) {
		}
		return this.remainingBonus;
	}

	/**
	 * Gets the remaining phase time without affecting the timer
	 *
	 * @return the remaining phase time
	 */
	public byte peekPhaseTime() {
		if (this.phaseStartTime == 0) {
			return Timeline.PHASE_LENGTH;
		}
		if (!this.isAlive()) {
			return 0;
		}
		return (byte) (Timeline.PHASE_LENGTH - BonusTimer.getElapsedSeconds(this.phaseStartTime));
	}

	/**
	 * Gets the remaining bonus time without affecting the timer
	 *
	 * @return the remaining bonus time
	 */
	public byte peekBonusTime() {
		if (this.bonusStartTime == 0 || !this.isAlive()) {
			return this.remainingBonus;
		}
		return (byte) (this.remainingBonus - BonusTimer.getElapsedSeconds(this.bonusStartTime));
	}
}
