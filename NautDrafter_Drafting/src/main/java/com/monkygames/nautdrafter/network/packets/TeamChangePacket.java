/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.callbacks.PlayerIdentifyCallback;
import com.monkygames.nautdrafter.network.callbacks.TeamCallback;
import com.monkygames.nautdrafter.network.packets.segments.PlayerIdentifierSegment;
import com.monkygames.nautdrafter.network.packets.segments.Segment;

/**
 * Drafting Server is notifying players of a player changing team
 */
public class TeamChangePacket extends TeamChangeRequestPacket {

	private PlayerIdentifyCallback playerIdent;

	/**
	 * Creates a new TeamChangePacket for decoding
	 *
	 * @param data
	 *            The packet data
	 * @param receiver
	 *            The callback to call once the packet is decoded
	 * @param playerIdent
	 *            The callback to call to get a player by their id
	 * @throws PacketDecoderException
	 */
	public TeamChangePacket(byte[] data, TeamCallback receiver, PlayerIdentifyCallback playerIdent)
	        throws PacketDecoderException {
		super(data, receiver, null);
		this.playerIdent = playerIdent;
	}

	/**
	 * Constructs a new TeamChangePacket for sending
	 *
	 * @param player
	 *            The player who changed team
	 * @param team
	 *            The team the player moved to
	 * @param position
	 *            The position the player moved to
	 * @throws PacketEncoderException
	 */
	public TeamChangePacket(Player player, int team, int position) throws PacketEncoderException {

		super(team, position);

		this.player = player;

		this.data = Segment.concat(this.data, PlayerIdentifierSegment.encode(this.player));
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		if (!super.decodeImpl()) {
			return false;
		}

		if (this.player == null) {
			if (!PlayerIdentifierSegment.canDecode(this)) {
				return false;
			}
			this.player = this.playerIdent.getPlayer(PlayerIdentifierSegment.decode(this));
		}

		return true;
	}

	@Override
	protected void handleImpl() {
		this.receiver.onChangedTeam(this.player, this.team, this.position);
	}
}
