/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets.segments;

import java.util.ArrayList;
import java.util.List;

import com.monkygames.nautdrafter.network.Timeline;
import com.monkygames.nautdrafter.network.Timeline.TimelineItem;
import com.monkygames.nautdrafter.network.packets.Packet;
import com.monkygames.nautdrafter.network.packets.PacketEncoderException;

/**
 * Represents a Timeline as an array of bytes
 */
public class TimelineSegment {
	/**
	 * Encodes a Timeline
	 *
	 * @param timeline
	 * @return encoded data
	 * @throws PacketEncoderException
	 */
	public static byte[] encode(Timeline timeline) throws PacketEncoderException {
		byte[] stages = new byte[timeline.items.size()];
		int p = 0;
		for (TimelineItem i : timeline.items) {
			stages[p++] = (byte) (((i.team << 4) & 0xF0) | (i.type & 0x0F));
		}

		return Segment.concat(timeline.pos, timeline.redBonus, timeline.blueBonus, (byte) stages.length, stages);
	}

	/**
	 * Decodes a Timeline from the packet
	 * 
	 * @param packet
	 * @return The decoded Timeline
	 */
	public static Timeline decode(Packet packet) {
		byte pos = packet.data[packet.pos++];
		byte redBonus = packet.data[packet.pos++];
		byte blueBonus = packet.data[packet.pos++];
		byte numStages = packet.data[packet.pos++];
		List<TimelineItem> stages = new ArrayList<>(numStages);
		while (numStages-- > 0) {
			byte item = packet.data[packet.pos++];
			stages.add(new TimelineItem((byte) ((item & 0xF0) >> 4), (byte) (item & 0x0F)));
		}
		return new Timeline(pos, redBonus, blueBonus, stages);
	}

	/**
	 * Check if we have enough data to decode a Timeline
	 *
	 * @param packet
	 *            The Packet to decode from (data and offset)
	 * @param advancePos
	 *            Whether to advance the position of the Packet
	 * @return true if we can decode a Timeline
	 */
	public static boolean canDecode(Packet packet, boolean advancePos) {
		int pos = packet.pos;
		boolean can = false;
		do {
			if (packet.available() < 4) {
				break;
			}
			packet.pos += 3;
			byte numStages = packet.data[packet.pos++];
			if (packet.available() < numStages) {
				break;
			}
			can = true;
		} while (false);
		if (!advancePos) {
			// if we're not advancing the position, restore to original position
			packet.pos = pos;
		}
		return can;
	}

	/**
	 * Check if we have enough data to decode a Timeline
	 *
	 * @param packet
	 *            The Packet to decode from (data and offset)
	 * @return true if we can decode a Timeline
	 */
	public static boolean canDecode(Packet packet) {
		return TimelineSegment.canDecode(packet, false);
	}
}
