/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.callbacks;

import com.monkygames.nautdrafter.network.Timeline;

/**
 * Contains callbacks used by the Drafting Client, and also all the callbacks used by the Drafting Server
 */
public interface ClientDraftingCallback extends DraftingCallback {
	/**
	 * Callback called when a Timeline is received from the server
	 *
	 * @param timeline
	 *            The Timeline the server is using
	 * @param currentPhaseTime
	 *            The remaining time of the current phase (given because we might have connected part-way through a
	 *            phase)
	 */
	public void onTimelineReceived(Timeline timeline, byte currentPhaseTime);
}
