/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.callbacks;

import com.monkygames.nautdrafter.network.Player;

/**
 * Callbacks for the NautSelection stage
 */
public interface NautChooseCallback {
	/**
	 * A player has chosen a naut
	 *
	 * @param player
	 *            The player who chose the naut
	 * @param teamId
	 *            The team the player is on
	 * @param position
	 *            The position (naut) the player selected
	 */
	public void onPlayerChoose(Player player, byte teamId, byte position);
}
