/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network;

import static com.monkygames.nautdrafter.network.Timeline.TimelineInit.BLUE_BAN;
import static com.monkygames.nautdrafter.network.Timeline.TimelineInit.BLUE_PICK;
import static com.monkygames.nautdrafter.network.Timeline.TimelineInit.RED_BAN;
import static com.monkygames.nautdrafter.network.Timeline.TimelineInit.RED_PICK;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.channels.Channel;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Application;

import com.monkygames.nautdrafter.Constants;
import com.monkygames.nautdrafter.model.Formatter;
import com.monkygames.nautdrafter.model.Planet;
import com.monkygames.nautdrafter.model.Summary;
import com.monkygames.nautdrafter.network.Timeline.TimelineInit;
import com.monkygames.nautdrafter.network.Timeline.TimelineItem;
import com.monkygames.nautdrafter.network.callbacks.AuthenticationCallback;
import com.monkygames.nautdrafter.network.callbacks.ChatCallback;
import com.monkygames.nautdrafter.network.callbacks.DraftingCallback;
import com.monkygames.nautdrafter.network.callbacks.NautChooseCallback;
import com.monkygames.nautdrafter.network.callbacks.ServerPlayerConnectionCallback;
import com.monkygames.nautdrafter.network.callbacks.SpectatorConnectionCallback;
import com.monkygames.nautdrafter.network.callbacks.TeamCallback;
import com.monkygames.nautdrafter.network.callbacks.VersionCallback;
import com.monkygames.nautdrafter.network.packets.CaptainChooseNautPacket;
import com.monkygames.nautdrafter.network.packets.CaptainSelectNautPacket;
import com.monkygames.nautdrafter.network.packets.ChatPacket;
import com.monkygames.nautdrafter.network.packets.ChatRequestPacket;
import com.monkygames.nautdrafter.network.packets.ConnectionErrorPacket;
import com.monkygames.nautdrafter.network.packets.DraftingKeepalivePacket;
import com.monkygames.nautdrafter.network.packets.LockTeamPacket;
import com.monkygames.nautdrafter.network.packets.NautChoosePacket;
import com.monkygames.nautdrafter.network.packets.NautChooseRequestPacket;
import com.monkygames.nautdrafter.network.packets.Packet;
import com.monkygames.nautdrafter.network.packets.PacketEncoderException;
import com.monkygames.nautdrafter.network.packets.PasswordPacket;
import com.monkygames.nautdrafter.network.packets.PlayerJoinedPacket;
import com.monkygames.nautdrafter.network.packets.PlayerLeftPacket;
import com.monkygames.nautdrafter.network.packets.RoomInfoPacket;
import com.monkygames.nautdrafter.network.packets.ServerAnnouncePacket;
import com.monkygames.nautdrafter.network.packets.ServerUpdatePacketEncoder;
import com.monkygames.nautdrafter.network.packets.SpectatorJoinedPacket;
import com.monkygames.nautdrafter.network.packets.StageChangePacket;
import com.monkygames.nautdrafter.network.packets.SummaryPacket;
import com.monkygames.nautdrafter.network.packets.TeamCaptainSwapPacket;
import com.monkygames.nautdrafter.network.packets.TeamChangePacket;
import com.monkygames.nautdrafter.network.packets.TeamChangeRequestPacket;
import com.monkygames.nautdrafter.network.packets.TeamNameChangePacket;
import com.monkygames.nautdrafter.network.packets.VersionPacket;
import com.monkygames.nautdrafter.network.packets.segments.Segment;
import com.sun.javafx.application.ParametersImpl;

/**
 * Drafting Server
 * <ul>
 * <li>Adds itself to the Lobby Server's list of Drafting Servers
 * <li>Clients connect using the listing from the Lobby Server
 * <li>Handles all logic, data and requests for a Drafting Room
 * </ul>
 */
public class DraftingServer extends Server implements ServerPlayerConnectionCallback, ChatCallback, TeamCallback,
        DraftingCallback, AuthenticationCallback, NautChooseCallback, SpectatorConnectionCallback, VersionCallback {

	// stage ids
	public static final byte                STAGE_TEAM_SELECTION     = 0;
	public static final byte                STAGE_DRAFTING           = 1;
	public static final byte                STAGE_NAUT_SELECTION     = 2;
	public static final byte                STAGE_VERSUS             = 3;
	// error ids
	public static final byte                ERROR_UNKNOWN            = 0;
	public static final byte                ERROR_INCORRECT_PASSWORD = 1;
	public static final byte                ERROR_SERVER_FULL        = 2;
	public static final byte                ERROR_DUPLICATE_PLAYER   = 3;
	public static final byte                ERROR_SERVER_OUTDATED    = 4;
	public static final byte                ERROR_CLIENT_OUTDATED    = 5;

	private static final Random             rng                      = new Random();

	private final boolean                   hasLobby;
	private final SelectionKey              lobbyKey;
	private final SocketChannel             lobbyChannel;
	public final String                     name, description;
	public final String                     password;
        public final String                     dns;
	public final byte                       mapId;
	public final boolean                    banBidirectional;
	public final boolean                    isSequentialDraft;
	public final boolean                    isAutoRandomMap;
        public final boolean                    isPickBan;

	private final LanServerInfo             lanServerInfo;

	public final Map<SelectionKey, Player>  players                  = new HashMap<>();
	private final Map<Player, SelectionKey> playerConnections        = new HashMap<>();
	public final Set<SelectionKey>          clients                  = new HashSet<>();
	private final Set<SelectionKey>         pending                  = new HashSet<>();
	public final Team                       teamRed                  = new Team("Red Team");
	public final Team                       teamBlue                 = new Team("Blue Team");
	public final Set<Player>                unassigned               = new HashSet<>();

	public final Timeline                   timeline;
	private BonusTimer                      bonusTimer;
	private byte                            stage                    = STAGE_TEAM_SELECTION;
	/**
	 * Determines if once completed, that this server should export to the lobby server.
	 */
	private boolean                         exportGameToLobbyServer;

	/**
	 * Determines if this is captains only mode.
	 */
	private boolean                         isCaptainsOnlyMode;
	private ArrayList<Player>               captainsOnlyPlayers;
        private DraftTimeLine   redDraftTimeLine  = null;
        private DraftTimeLine   blueDraftTimeLine = null;
        private boolean isDedicatedServer = false;
        /**
         * Used for dedicated servers in order to determine when the server should be killed.
         */
        private boolean isCompletedMatch = false;

	public byte getStage() {
		return this.stage;
	}

	public final long initTime;



	/**
	 * Creates a new DraftingServer<br>
	 * Automatically binds to a free port<br>
	 * If UPNP is enabled, attempts to have the router map that port for external access<br>
	 * Automatically identifies itself with the Lobby Server
	 *
	 * @param name
	 *            The name of the Drafting Server
	 * @param description
	 *            The description of the Drafting Server
	 * @param password
	 *            The password of the Drafting Server, or null if there should be no password
	 * @param mapId
	 *            The map the Drafting Server will play on
	 * @param isAutoRandomMap
	 *            True if the client's code automatically selected the map randomly (non-interactive).
         * @param dns
         *            If the addr used to connect should be a dns address, set this to a valid FQDN.
         *            If unused, set to an empty string.
	 * @param port
	 * @param timelineSchedule
	 * @param lobbyAddress
	 * @param banBidirectional
         * @param isSequentialDraft
         *            True if draft is done sequentially and false for concurrent.
	 * @param exportGameToLobbyServer
	 *            Determines at the end of a match if an export of the game should be sent to the lobby server.
	 * @param enableLan
	 * @param isCaptainsOnlyMode
	 *            True if captains only where only the captains play and false otherwise.
         * @param isDedicatedServer
         *   True if this is a dedicated server started by the lobby server or if its a player started server.
     * @param isPickBan
	 * @throws IOException
	 */
	public DraftingServer(
            String name,
            String description,
            String password,
            byte mapId,
            boolean isAutoRandomMap,
            String dns,
	    int port, 
            int[] timelineSchedule,
            InetSocketAddress lobbyAddress,
            boolean banBidirectional,
            boolean isSequentialDraft,
	    boolean enableLan,
            boolean exportGameToLobbyServer,
            boolean isCaptainsOnlyMode,
            boolean isDedicatedServer,
            boolean isPickBan
        ) throws IOException {
	     this(
                 name,
                 description,
                 password,
                 mapId,
                 isAutoRandomMap,
                 new InetSocketAddress(port),
                 dns,
                 port,
                 timelineSchedule,
                 lobbyAddress,
                 banBidirectional,
                 isSequentialDraft,
                 enableLan,
                 exportGameToLobbyServer,
                 isCaptainsOnlyMode,
                 isDedicatedServer,
                 isPickBan
             );
        }
	public DraftingServer(
            String name,
            String description,
            String password,
            byte mapId,
            boolean isAutoRandomMap,
            InetSocketAddress draftingAddress,
            String dns,
            int port,
            int[] timelineSchedule,
            InetSocketAddress lobbyAddress,
            boolean banBidirectional,
            boolean isSequentialDraft,
	    boolean enableLan,
            boolean exportGameToLobbyServer,
            boolean isCaptainsOnlyMode,
            boolean isDedicatedServer,
            boolean isPickBan
        ) throws IOException {
            super(draftingAddress);

		// if the port is not defined, try UPnP
		if (port == 0 && !PortMapper.map(this.ssc.socket().getLocalPort())) {
			throw new IOException("Failed to bind port");
		}

		this.name                    = name;
		this.description             = description;
		this.password                = password;
		this.mapId                   = mapId;
		this.banBidirectional        = banBidirectional;
                this.isSequentialDraft       = isSequentialDraft;
		this.isAutoRandomMap         = isAutoRandomMap;
		this.exportGameToLobbyServer = exportGameToLobbyServer;
		this.isCaptainsOnlyMode      = isCaptainsOnlyMode;
                this.isDedicatedServer       = isDedicatedServer;
                this.isPickBan               = isPickBan;
                this.dns                     = dns;

		TimelineInit[] schedule = new TimelineInit[timelineSchedule.length];
		TimelineInit[] vals = TimelineInit.values();
		int index = 0;
		for (int i : timelineSchedule) {
			schedule[index++] = vals[i];
		}
		this.timeline = new Timeline(schedule);

		byte[] initialServerInfo = new ServerAnnouncePacket(this.getServerInfo()).data;
		if (enableLan) {
			this.lanServerInfo = new LanServerInfo(initialServerInfo);
			this.lanServerInfo.start();
		} else {
			this.lanServerInfo = null;
		}

		if (lobbyAddress != null) {
			this.hasLobby = true;
			this.lobbyChannel = SocketChannel.open();
			this.lobbyChannel.connect(lobbyAddress);
			this.lobbyChannel.configureBlocking(false);
			ConnectionInfo info = new ConnectionInfo();
			this.lobbyChannel.finishConnect();
			Logger.getLogger(DraftingServer.class.getName()).log(
			        Level.INFO,
			        "Connected to Lobby server at {0}:{1}",
			        new Object[] { this.lobbyChannel.socket().getInetAddress().getHostAddress(),
			                this.lobbyChannel.socket().getPort() });
			this.lobbyKey = this.lobbyChannel.register(this.selector, SelectionKey.OP_READ, info);
			// announce ourselves as a server
			info.sendBuffer.put(Segment.concat(Constants.VERSION_PACKET, initialServerInfo));
			this.write(this.lobbyKey);
		} else {
			this.hasLobby = false;
			this.lobbyKey = null;
			this.lobbyChannel = null;
		}

		this.initTime = System.currentTimeMillis();

		// prepare for captains only mode
		if (this.isCaptainsOnlyMode) {
			this.setupCaptainsOnlyMode();
		}
	}

	@Override
	protected SelectionKey accept(SelectionKey key) throws IOException {
		key = super.accept(key);
		Socket s = ((SocketChannel) key.channel()).socket();
                ConnectionInfo connectionInfo = (ConnectionInfo)key.attachment();

                boolean isAuthenticated = false;
                if(this.password == null){
                    isAuthenticated = true;
                }

		ClientConnectionInfo info = new ClientConnectionInfo(
                    connectionInfo,
                    isAuthenticated
                );

		//DraftingConnectionInfo info = new DraftingConnectionInfo((ConnectionInfo) key.attachment(),
		//        this.password == null);
		key.attach(info);
		this.pending.add(key);
		return key;
	}

	@Override
	protected void kill(SelectionKey key) {
		Socket s = ((SocketChannel) key.channel()).socket();
		System.out.println("Killing connection from " + s.getInetAddress() + ":" + s.getPort());
		super.kill(key);
		this.clients.remove(key);
		this.pending.remove(key);
		Player player;
		if ((player = this.players.remove(key)) != null) {
			this.playerConnections.remove(player);
			Team team = this.getTeam(player);
			if (team == null || !team.lockedIn) {
				this.onPlayerLeave(player);
			}
		}
		if (this.hasLobby && (!this.lobbyKey.isValid() || !this.lobbyChannel.isConnected()) && this.players.size() <= 0) {
			this.shutdown();
		}
	}

	/**
	 * Returns the server to its initial starting state and will disconnect all connected clients
	 */
	public void restartServer() {

		// Kill clients
		try {
			for (SelectionKey key : this.clients) {
				Channel channel = key.channel();
				if (channel instanceof SocketChannel) {
					((SocketChannel) channel).socket().close();
				}
				channel.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.players.clear();
		this.playerConnections.clear();
		this.clients.clear();
		this.pending.clear();
		this.teamRed.reset("Red Team");
		this.teamBlue.reset("Blue Team");
		this.unassigned.clear();
		this.timeline.reset();
		this.bonusTimer = null;
		this.stage = STAGE_TEAM_SELECTION;
		this.updateServerInfo();
		if (this.isCaptainsOnlyMode) {
			this.setupCaptainsOnlyMode();
		}
	}

	/**
	 * Kills a connection, and provides an error id
	 *
	 * @param key
	 *            Connection to kill
	 * @param errorId
	 *            Error id to send
	 */
	protected void kill(SelectionKey key, byte errorId) {
		ClientConnectionInfo info = (ClientConnectionInfo) this.readingKey.attachment();
		info.sendBuffer.put(new ConnectionErrorPacket(errorId).data);
		info.kill = true; // kill after sending packet
		this.write(key);
	}

	@Override
	protected Packet createPacket(SelectionKey key, byte[] data) throws IOException {
		if (data[0] == Packet.VERSION_PACKET_ID) {
			return new VersionPacket(data, this);
		}
		if (key == this.lobbyKey && data[0] == Packet.KEEPALIVE_PACKET_ID) {
			return new DraftingKeepalivePacket(data);
		}
		ClientConnectionInfo info = (ClientConnectionInfo) key.attachment();
		if (info.isAuthenticated()) {
			if (info.isPlayer()) {
				switch (data[0]) {
				case Packet.CHAT_PACKET_ID:
					return new ChatRequestPacket(data, this, this.players.get(key));
				case Packet.TEAM_CHANGE_PACKET_ID:
					return new TeamChangeRequestPacket(data, this, this.players.get(key));
				case Packet.TEAM_NAME_CHANGE_PACKET_ID:
					return new TeamNameChangePacket(data, this);
				case Packet.TEAM_READY_PACKET_ID:
					return new LockTeamPacket(data, this);
				case Packet.CAPTAIN_CHOOSE_NAUT_PACKET_ID:
					return new CaptainChooseNautPacket(data, this);
				case Packet.CAPTAIN_SELECT_NAUT_PACKET_ID:
					return new CaptainSelectNautPacket(data, this);
				case Packet.NAUT_CHOOSE_PACKET_ID:
					return new NautChooseRequestPacket(data, this);
				}
			} else if (!info.isDeclared()) {
				switch (data[0]) {
				case Packet.PLAYER_JOINED_PACKET_ID:
					return new PlayerJoinedPacket(data, this);
				case Packet.SPECTATOR_JOINED_PACKET_ID:
					return new SpectatorJoinedPacket(data, this);
				}
			}
		} else if (data[0] == Packet.PASSWORD_PACKET_ID) {
			return new PasswordPacket(data, this);
		}

		return null;
	}

	/**
	 * Check if a packet is valid for our current state
	 *
	 * @param packetId
	 * @return
	 */
	private boolean validPacket(int packetId) {
		if (packetId == Packet.KEEPALIVE_PACKET_ID || packetId == Packet.PLAYER_JOINED_PACKET_ID
		        || packetId == Packet.SPECTATOR_JOINED_PACKET_ID || packetId == Packet.PASSWORD_PACKET_ID
		        || packetId == Packet.VERSION_PACKET_ID) {
			return true;
		}
		switch (this.stage) {
		case STAGE_TEAM_SELECTION:
			switch (packetId) {
			case Packet.CHAT_PACKET_ID:
			case Packet.TEAM_CHANGE_PACKET_ID:
			case Packet.TEAM_NAME_CHANGE_PACKET_ID:
			case Packet.TEAM_READY_PACKET_ID:
				return true;
			}
		case STAGE_DRAFTING:
			switch (packetId) {
			case Packet.CHAT_PACKET_ID:
			case Packet.CAPTAIN_CHOOSE_NAUT_PACKET_ID:
			case Packet.CAPTAIN_SELECT_NAUT_PACKET_ID:
				return true;
			}
		case STAGE_NAUT_SELECTION:
			switch (packetId) {
			case Packet.CHAT_PACKET_ID:
			case Packet.TEAM_READY_PACKET_ID:
			case Packet.NAUT_CHOOSE_PACKET_ID:
				return true;
			}
		}
		return false;
	}

	@Override
	protected void handlePacket(Packet packet) {
		if (this.validPacket(packet.data[0])) {
			packet.handle();
		}
	};

	/**
	 * @return A ServerInfo object describing this server
	 */
	public ServerInfo getServerInfo() {
		return new ServerInfo(this.name, this.description, null, this.mapId, this.getPort(), this.players.size(),
		        this.password != null,dns);
	}

	/**
	 * Sends an update packet to the Lobby Server
	 */
	protected void updateServerInfo() {
		if (this.lanServerInfo != null) {
			try {
				this.lanServerInfo.update(new ServerAnnouncePacket(this.getServerInfo()).data);
			} catch (PacketEncoderException e) {
				e.printStackTrace();
			}
		}
		if (this.hasLobby) {
			try {
				if (this.lobbyKey.isValid() && this.lobbyChannel.isConnected()) {
					ConnectionInfo info = (ConnectionInfo) this.lobbyKey.attachment();
					info.sendBuffer.put(new ServerUpdatePacketEncoder(this.players.size()).data);
					this.write(this.lobbyKey);
				}
			} catch (PacketEncoderException ex) {
				Logger.getLogger(DraftingServer.class.getName()).log(Level.SEVERE, null, ex);
				this.kill(this.lobbyKey);
			}
		}
	}

	/**
	 * Sends to all connected clients
	 *
	 * @param data
	 */
	protected void sendAll(byte[] data) {
		for (SelectionKey client : this.clients) {
			ConnectionInfo info = (ConnectionInfo) client.attachment();
			info.sendBuffer.put(data);
			this.write(client);
		}
	}

	/**
	 * Sends to all players in a list
	 *
	 * @param data
	 * @param players
	 */
	protected void sendPlayers(byte[] data, Collection<Player> players) {
		for (Player player : players) {
			SelectionKey key = this.playerConnections.get(player);
			if (key != null) {
				ConnectionInfo info = (ConnectionInfo) key.attachment();
				info.sendBuffer.put(data);
				this.write(key);
			}
		}
	}

	/**
	 * Varargs wrapper method for sendPlayers
	 *
	 * @param data
	 * @param players
	 */
	protected void sendPlayers(byte[] data, Player... players) {
		this.sendPlayers(data, Arrays.asList(players));
	}

	/**
	 * Sends to all players
	 *
	 * @param data
	 */
	protected void sendPlayers(byte[] data) {
		this.sendPlayers(data, this.playerConnections.keySet());
	}

	/**
	 * Gets the team for a Player, if any
	 *
	 * @param player
	 * @return the player's team
	 */
	protected Team getTeam(Player player) {
		for (int i = 0; i < 3; i++) {
			if (this.teamRed.players[i] != null && this.teamRed.players[i].equals(player)) {
				return this.teamRed;
			}
			if (this.teamBlue.players[i] != null && this.teamBlue.players[i].equals(player)) {
				return this.teamBlue;
			}
		}
		return null;
	}

	/**
	 * Gets the team for a teamId, if valid
	 *
	 * @param teamId
	 * @return
	 */
	protected Team getTeam(int teamId) {
		switch (teamId) {
		case Team.RED:
			return this.teamRed;
		case Team.BLUE:
			return this.teamBlue;
		default:
			return null;
		}
	}
        /**
         * Gets the other team.
         * @param team the team to compare.
         * @return the other team.
         */
        protected Team getOtherTeam(Team team){
            if(team == this.teamBlue){
                return this.teamRed;
            }
            return this.teamBlue;
        }
        protected boolean isRedTeam(int teamId){
            if(teamId == Team.RED){
                return true;
            }
            return false;
        }

	/**
	 * Sends to all players in the same team as a player
	 *
	 * @param player
	 * @param data
	 */
	protected void sendTeam(Player player, byte[] data) {
		Team team = this.getTeam(player);
		if (team != null) {
			this.sendPlayers(data, Arrays.asList(team.players));
		}
	}

	protected void confirmConnection(SelectionKey key) {
		// add to clients list
		if (this.clients.add(key)) {
			this.pending.remove(key);
		}
		// send room info
		ConnectionInfo info = (ConnectionInfo) key.attachment();
		try {
			byte phaseTime = 30;
			if (this.stage == STAGE_DRAFTING) {
				this.timeline.setBonus(this.bonusTimer.peekBonusTime());
				phaseTime = this.bonusTimer.peekPhaseTime();
			}
			info.sendBuffer.put(new RoomInfoPacket(this.stage, 
                            this.mapId, 
                            this.banBidirectional, 
                            this.isSequentialDraft,
                            this.isPickBan,
                            this.teamRed,
			    this.teamBlue, 
                            this.unassigned, 
                            this.timeline, 
                            phaseTime
                        ).data);
		} catch (PacketEncoderException ex) {
			this.kill(key, ERROR_UNKNOWN);
		}
		this.write(key);
	}

	/**
	 * Called when a client identifies themselves as a Player
	 */
	@Override
	public void onPlayerJoin(Player player) {
		// can only have 6 players per room
		if (this.players.size() >= 6) {
			this.kill(this.readingKey, ERROR_SERVER_FULL);
			return;
		}
		// disallow joining multiple times with the same SteamId
		for (Player existing : this.players.values()) {
			if (existing.equals(player)) {
				this.kill(this.readingKey, ERROR_DUPLICATE_PLAYER);
				return;
			}
		}
		// add the player
		ClientConnectionInfo info = (ClientConnectionInfo) this.readingKey.attachment();
		info.setPlayer();
		this.confirmConnection(this.readingKey);
		this.players.put(this.readingKey, player);
		this.playerConnections.put(player, this.readingKey);
		if (this.getTeam(player) == null) {
			// if the server is past team selection, the player must be in a team
			if (this.stage != STAGE_TEAM_SELECTION) {
				this.kill(this.readingKey, ERROR_SERVER_FULL);
			} else {
				this.unassigned.add(player);
				Logger.getLogger(DraftingServer.class.getName()).log(Level.INFO, "{0} joined", player.name);
				// tell the lobby and all clients
				this.updateServerInfo();
				try {
					this.sendAll(new PlayerJoinedPacket(player).data);
				} catch (PacketEncoderException e) {
					e.printStackTrace();
				}
			}
		} else {
			Logger.getLogger(DraftingServer.class.getName()).log(Level.INFO, "{0} reconnected", player.name);
		}
	}

	/**
	 * Called when a player disconnects from the server
	 */
	@Override
	public void onPlayerLeave(Player player) {
		this.unassignPlayer(player, false);
		Logger.getLogger(DraftingServer.class.getName()).log(Level.INFO, "{0} left", player.name);
		this.updateServerInfo();
		try {
			this.sendAll(new PlayerLeftPacket(player).data);
		} catch (PacketEncoderException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onSpectatorJoined() {
		ClientConnectionInfo info = (ClientConnectionInfo) this.readingKey.attachment();
		info.setSpectator();
		this.confirmConnection(this.readingKey);
	}

	@Override
	public void onPasswordReceived(String password) {
		if (this.password != null && !this.password.equals(password)) {
			this.kill(this.readingKey, ERROR_INCORRECT_PASSWORD);
		} else {
			ClientConnectionInfo info = (ClientConnectionInfo) this.readingKey.attachment();
			info.setAuthenticated();
		}
	}

	/**
	 * Called when a player sends a chat message
	 */
	@Override
	public void onChatReceived(Player sender, String message) {
		if (message.trim().length() == 0) {
			return;
		}
		try {
			ChatPacket packet = new ChatPacket(sender, message);
			if (this.stage == STAGE_TEAM_SELECTION) {
				this.sendAll(packet.data);
			} else {
				this.sendTeam(sender, packet.data);
			}
		} catch (PacketEncoderException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Remove a player from whatever team they're in
	 *
	 * @param player
	 * @param add
	 *            If true, put the player in the list of unassigned players.<br>
	 *            Otherwise remove the player from the unassigned list too.
	 * @return true if the player was in a team
	 */
	private boolean unassignPlayer(Player player, boolean add) {
		boolean ret = Team.removePlayer(player, this.teamRed, this.teamBlue);
		if (add) {
			this.unassigned.add(player);
		} else {
			this.unassigned.remove(player);
		}
		return ret;
	}

	/**
	 * Remove a player from whatever team they're in, and put them in the list of unassigned players
	 *
	 * @param player
	 * @return true if the player was in a team
	 */
	private boolean unassignPlayer(Player player) {
		return this.unassignPlayer(player, true);
	}

	private boolean canMove(Player player) {
		Team currentTeam = this.getTeam(player);
		return (currentTeam == null || !currentTeam.lockedIn);
	}

	/**
	 * Handles an attempt by a player to change team<br>
	 * A player can leave their current position, which will result in them being moved back to the unassigned players
	 * list.<br>
	 * A player can switch into any empty position, be it in their own team or in the other team, at will.<br>
	 * A player can take another player's position if and only if that player is the team captain, and the disputing
	 * player has a better score than the current captain, and there is space in the team for the current captain to
	 * move into (such space could be the disputing player's current position if they are in the same team). If a player
	 * disputes a captain's position but has a lower score (and thus fails), they will be moved into the team if they
	 * are not already in that team and there is space for them. If there is not space in the team, the move will be
	 * aborted entirely and nothing will happen.
	 */
	@Override
	public void onChangedTeam(Player player, int teamId, int position) {
		try {
			// check for invalid position
			if (position < 0 || position > 2) {
				return;
			}
			if (!this.canMove(player)) {
				return;
			}
			// find which team they're moving to
			Team team = this.getTeam(teamId);
			if (team == null) {
				this.unassignPlayer(player);
				this.sendAll(new TeamChangePacket(player, 0, 0).data);
				return;
			}
			if (team.lockedIn) {
				return;
			}
			Player existing = team.players[position];
			if (existing != null) {
				this.onCaptainSwap(teamId, player, existing, position);
			} else {
				this.unassignPlayer(player, false);
				team.players[position] = player;
				this.sendAll(new TeamChangePacket(player, teamId, position).data);
			}

		} catch (PacketEncoderException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Handles a request to change the team name<br>
	 * Only the captain of a team can change the team's name
	 */
	@Override
	public void onChangedTeamName(int teamId, String name) {
		Player player = this.players.get(this.readingKey);
		if (player == null) {
			return;
		}
		Team team = this.getTeam(teamId);
		if (team == null || team.lockedIn) {
			return;
		}
		// only the captain can change the name, and the name must not be blank
		if (team.players[Team.CAPTAIN] == null || !team.isCaptain(player) || team.name.equals(name)
		        || name.trim().length() == 0) {
			SelectionKey key = this.playerConnections.get(player);
			if (key != null) {
				try {
					((ConnectionInfo) key.attachment()).sendBuffer
					        .put(new TeamNameChangePacket(teamId, team.name).data);
					this.write(key);
				} catch (PacketEncoderException e) {
					e.printStackTrace();
				}
			}
		} else {
			// valid request, change the name
			team.name = name;
			try {
				this.sendAll(new TeamNameChangePacket(teamId, name).data);
			} catch (PacketEncoderException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Resolves a dispute for captain's position
	 *
	 * @param teamId
	 *            The team of the captain
	 * @param disputingPlayer
	 *            The disputing Player
	 * @param existingCaptain
	 *            The existing Captain
	 * @param captainPosition
	 *            The position the player is trying to dispute
	 */
	@Override
	public void onCaptainSwap(int teamId, Player disputingPlayer, Player existingCaptain, int captainPosition) {
		// can only contest the captain's position
		if (captainPosition != 0) {
			return;
		}
		// get the team
		Team team = this.getTeam(teamId);
		if (team == null || team.lockedIn || !this.canMove(disputingPlayer)) {
			return;
		}
		// can only contest if the team has open slots
		int newPosition = 0;
		for (int i = 1; i < 3; i++) {
			if (team.players[i] == null || team.players[i].equals(disputingPlayer)) {
				newPosition = i;
				break;
			}
		}
		// cannot continue if the team is full
		// and the disputing player is not in the same team so that they could swap with the captain
		if (newPosition == 0) {
			return;
		}
		// if the players aren't swapping positions, remove the disputing player from their position now
		if (team.players[newPosition] == null) {
			this.unassignPlayer(disputingPlayer, false);
		}
		try {
			if (existingCaptain.tieBreak < disputingPlayer.tieBreak) {
				// swap positions
				team.players[0] = disputingPlayer;
				team.players[newPosition] = existingCaptain;
				this.sendAll(new TeamCaptainSwapPacket(teamId, disputingPlayer, existingCaptain, newPosition).data);
			} else if (team.players[newPosition] == null) {
				// even if they failed to take the captain's spot,
				// if the player is not already in the same team,
				// move them into the team if possible
				team.players[newPosition] = disputingPlayer;
				this.sendAll(new TeamChangePacket(disputingPlayer, teamId, newPosition).data);
			}
		} catch (PacketEncoderException e) {
			e.printStackTrace();
		}
	}

	/**
	 * A team captain has readied their team
	 */
	@Override
	public void onLockTeam(int teamId) {
		if (this.stage != STAGE_TEAM_SELECTION && this.stage != STAGE_NAUT_SELECTION) {
			return;
		}
		// get the player
		Player player = this.players.get(this.readingKey);
		if (player == null) {
			return;
		}
		// make sure the player is captain of the team and their team is full
		Team team;
		if (teamId == 0xFF) {
			team = this.getTeam(player);
		} else {
			team = this.getTeam(teamId);
		}
		if (team == null || team.players[0] == null || !team.isCaptain(player) || team.players[1] == null
		        || team.players[2] == null) {
			return;
		}
		Team otherTeam = (team == this.teamRed ? this.teamBlue : this.teamRed);
		System.out.println(team.name + " is locked in... "
		        + (otherTeam.lockedIn ? "Advancing to next stage" : "Waiting on " + otherTeam.name));
		byte[] message = new LockTeamPacket(team == this.teamRed ? Team.RED : Team.BLUE).data;
		switch (this.stage) {
		case STAGE_TEAM_SELECTION:
			team.lockedIn = true;
			if (otherTeam.lockedIn) {
				// go to next stage
				try {
					this.resetBonusTimer();
					message = Segment.concat(message, new StageChangePacket(++this.stage).data);
				} catch (PacketEncoderException e) {
					// TODO: Should probably die here
					System.out.println("FAILED TO CHANGE STAGE\nSHOULD PROBABLY DIE NOW");
					e.printStackTrace();
				}
			}
			break;
		case STAGE_NAUT_SELECTION:
			team.finished = true;
			if (otherTeam.finished) {
				// go to next stage
				try {
					message = Segment.concat(message, new StageChangePacket(++this.stage).data);
				} catch (PacketEncoderException e) {
					// TODO: Should probably die here
					System.out.println("FAILED TO CHANGE STAGE\nSHOULD PROBABLY DIE NOW");
					e.printStackTrace();
				}

				// send summary export to lobby server
				if (this.lobbyKey != null && this.exportGameToLobbyServer) {
                                    sendExportSummaryToLobbyServer();
				}

                                // if a dedicated server, shutdown.
                                if(this.isDedicatedServer){
                                    this.isCompletedMatch = true;
                                    //shutdown();
                                }
			}
			break;
		}
		this.sendAll(message);
	}

	public static final byte       MAX_NAUT  = 19;
	public static final List<Byte> NAUT_LIST = new ArrayList<>(MAX_NAUT);
	static {
		for (int i = 0; i <= MAX_NAUT; i++) {
			NAUT_LIST.add((byte) i);
		}
	}

	private void resetBonusTimer() {
		this.bonusTimer = new BonusTimer(this.timeline.getBonus(), () -> {
			synchronized (this.runLater) {
				this.runLater.add(() -> {
					TimelineItem step = this.timeline.items.get(this.timeline.pos);
					if (step.type == Timeline.PICK) {
						Team team = this.getTeam(step.team);
						List<Byte> possibles = new ArrayList<>(NAUT_LIST);
						// cannot do anything with a banned naut
						if (this.banBidirectional) {
							// bidirectional bans - use both teams' bans
							for (Byte b : this.teamRed.bannedNauts) {
								possibles.remove(b);
							}
							for (Byte b : this.teamBlue.bannedNauts) {
								possibles.remove(b);
							}
						} else {
							// unidirectional bans - use other team's bans only
							Team otherTeam = step.team == Team.RED ? this.teamBlue : this.teamRed;
							for (Byte b : otherTeam.bannedNauts) {
								possibles.remove(b);
							}
						}
						if (step.type == Timeline.PICK) {
							// nauts cannot be picked twice by a team
							for (Byte b : team.pickedNauts) {
								possibles.remove(b);
							}
						}
						// pick a random naut
						this.advanceTimeline(possibles.get(rng.nextInt(possibles.size())), (byte) 0, this.getTeam(step.team));
					} else {
						// empty ban
						this.advanceTimeline((byte) -2, (byte) 0, this.getTeam(step.team));
					}
				});
			}
			this.selector.wakeup();
		});
		this.bonusTimer.start();
	}

	/**
	 * Sets the naut for the current timeline stage, updates the bonus time, and advances the timeline
	 * 
	 * @param nautId
	 *            The naut that was picked or banned
	 * @param remainingBonus
	 *            The remaining bonus time of the team whose turn it was
         * @param team
         *      The team that will be primarily applied.
	 */
	private void advanceTimeline(byte nautId, byte remainingBonus, Team team) {
		TimelineItem step = this.timeline.items.get(this.timeline.pos);
		this.timeline.setBonus(remainingBonus);
		switch (step.type) {
		case Timeline.PICK:
			team.addPick(nautId);
                        if(this.isPickBan){
                            // ban this pick from the other team.
                            //getOtherTeam(team).addBan(nautId);
                        }
			break;
		case Timeline.BAN:
			team.addBan(nautId);
			break;
		}
		this.timeline.pos++;
		// send update packet with naut and new bonus time
		byte[] message = new CaptainChooseNautPacket(nautId, remainingBonus).data;
		if (this.timeline.pos >= this.timeline.items.size()) {
			// finished drafting
			try {
				if (this.isCaptainsOnlyMode) {
					this.stage += 2;
					message = Segment.concat(message, new StageChangePacket(this.stage).data);
                                        // check if export should be sent
                                        // because captains mode skips the normal
                                        // exporting
                                        if(this.exportGameToLobbyServer){
                                            sendExportSummaryToLobbyServer();
                                        }

                                        // if a dedicated server, shutdown.
                                        if(this.isDedicatedServer){
                                            this.isCompletedMatch = true;
                                            //shutdown();
                                        }

				} else {
					message = Segment.concat(message, new StageChangePacket(++this.stage).data);
				}
			} catch (PacketEncoderException e) {
				e.printStackTrace();
			}
		} else {
			this.resetBonusTimer();
		}
		this.sendAll(message);
	}

	@Override
	public void onCaptainChooseNaut(byte nautId, byte bonusTimeRemaining) {
		// get the player
		Player player = this.players.get(this.readingKey);
		if (player == null) {
			return;
		}
		// make sure the player is captain of the team and their team is full
		Team team = this.getTeam(player);
		if (team == null || !team.isCaptain(player)) {
			return;
		}
		int teamId = (team == this.teamRed) ? Team.RED : Team.BLUE;
		TimelineItem step = this.timeline.items.get(this.timeline.pos);

                // bypass this check if its concurrent drafting
		if (isSequentialDraft && step.team != teamId) {
			return;
		}
		// cannot do anything with a banned naut
		if (this.banBidirectional) {
			// bidirectional bans - use both teams' bans
			for (byte banned : this.teamRed.bannedNauts) {
				if (banned == nautId) {
					return;
				}
			}
			for (byte banned : this.teamBlue.bannedNauts) {
				if (banned == nautId) {
					return;
				}
			}

			// Check if the naut was already picked on either team.
			// if it was and this is a ban, its not allowed
			if (step.type == Timeline.BAN) {
				for (byte banned : this.teamRed.pickedNauts) {
					if (banned == nautId) {
						return;
					}
				}
				for (byte banned : this.teamBlue.pickedNauts) {
					if (banned == nautId) {
						return;
					}
				}
			}
		} else {
			// unidirectional bans - use other team's bans only
			Team otherTeam = team == this.teamRed ? this.teamBlue : this.teamRed;
			for (byte banned : otherTeam.bannedNauts) {
				if (banned == nautId) {
					return;
				}
			}
			// Check if the naut was already picked on other team.
			// if it was and this is a ban, its not allowed
			if (step.type == Timeline.BAN) {
				for (byte banned : otherTeam.pickedNauts) {
					if (banned == nautId) {
						return;
					}
				}
			}
		}
		if (step.type == Timeline.PICK) {
			// nauts cannot be picked twice by a team
			for (byte picked : team.pickedNauts) {
				if (picked == nautId) {
					return;
				}
			}
		}
                // check if the timeline should be advanced if its concurrent drafting.
                if(!this.isSequentialDraft){
                    // need to wait for both
                    if(isRedTeam(teamId)){
                        redDraftTimeLine = new DraftTimeLine(nautId,this.bonusTimer.time(),team);
                    }else{
                        blueDraftTimeLine = new DraftTimeLine(nautId,this.bonusTimer.time(),team);
                    }

                    if(redDraftTimeLine != null && blueDraftTimeLine != null){
                        // order is important
		        if ( isRedTeam(step.team)) {
		            this.advanceTimeline(redDraftTimeLine.nautId, redDraftTimeLine.remainingBonus,redDraftTimeLine.team);
		            this.advanceTimeline(blueDraftTimeLine.nautId, blueDraftTimeLine.remainingBonus,blueDraftTimeLine.team);
                        }else{
		            this.advanceTimeline(blueDraftTimeLine.nautId, blueDraftTimeLine.remainingBonus,blueDraftTimeLine.team);
		            this.advanceTimeline(redDraftTimeLine.nautId, redDraftTimeLine.remainingBonus,redDraftTimeLine.team);
                        }
                        redDraftTimeLine = null;
                        blueDraftTimeLine = null;
                    }
                }else{
		    this.advanceTimeline(nautId, this.bonusTimer.time(),team);
                }
	}

	@Override
	public void onCaptainSelectNaut(byte nautId) {
		// get the player
		Player player = this.players.get(this.readingKey);
		if (player == null) {
			return;
		}
		// make sure the player is captain of the team and their team is full
		Team team = this.getTeam(player);
		if (team == null || !team.isCaptain(player)) {
			return;
		}
		// send to the non-captain players in the team
		this.sendPlayers(new CaptainSelectNautPacket(nautId).data, team.players[1], team.players[2]);
	}

	@Override
	public void onPlayerChoose(Player player, byte teamId, byte position) {
		player = this.players.get(this.readingKey);
		if (player == null) {
			return;
		}
		Team team = this.getTeam(player);
		if (team == null || team.finished) {
			return;
		}
		teamId = team == this.teamRed ? Team.RED : Team.BLUE;
		if (team.selectNaut(player, position)) {
			try {
				this.sendAll(new NautChoosePacket(player, teamId, position).data);
			} catch (PacketEncoderException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onVersionReceived(int major, int minor, int patch) {
		if (this.readingKey.equals(this.lobbyKey)) {
			// received version from lobby server
			String version = major + "." + minor + "." + patch;
			if (Constants.VERSION_MAJOR < major) {
				System.out.println("Our version is too old: " + Constants.VERSION + " < " + version);
			} else if (Constants.VERSION_MAJOR > major) {
				System.out.println("Lobby version is too old: " + Constants.VERSION + " > " + version);
			} else {
				System.out.println("Versions compatible: " + Constants.VERSION + " ~ " + version);
			}
		} else {
			// received version from client
			ConnectionInfo info = (ConnectionInfo) this.readingKey.attachment();
			info.sendBuffer.put(Constants.VERSION_PACKET);
			if (Constants.VERSION_MAJOR != major || Constants.VERSION_MINOR != minor) {
				info.kill = true;
			}
			this.write(this.readingKey);
		}
	}

	@Override
	public void shutdown() {
		if (this.lanServerInfo != null) {
			this.lanServerInfo.shutdown();
		}
		super.shutdown();
	}

	/**
	 * Adds fake players to both sides to ensure captains only mode.
	 */
	private void setupCaptainsOnlyMode() {
		for (int i = 1; i < 3; i++) {
			this.teamRed.players[i] = new Player(false, new Random().nextLong(), "Bot",
			        "assets/ronimo/images/icons/logo.png", 0);
			this.teamBlue.players[i] = new Player(false, new Random().nextLong(), "Bot",
			        "assets/ronimo/images/icons/logo.png", 0);
		}
	}

        /**
         * Sends the export of the summary to the lobby server.
         */
        private void sendExportSummaryToLobbyServer(){
            ConnectionInfo info = (ConnectionInfo) this.lobbyKey.attachment();
            // get summary
            try {
                SummaryPacket packet = new SummaryPacket(
                    Summary.exportSummary(false, Formatter.PlainText,
                    this.teamRed, this.teamBlue, this.mapId, this.timeline), 
                    this.teamRed.name, this.teamBlue.name, 
                    Planet.getById(this.mapId)
                    .getName(), this.isAutoRandomMap);
                info.sendBuffer.put(packet.data);
                this.write(this.lobbyKey);
            } catch (Exception e) {
                    e.printStackTrace();
            }
        }

        /**
         * Used for concurrent picks.
         */
        private class DraftTimeLine{
	   byte nautId; 
           byte remainingBonus;
           Team team;
           public DraftTimeLine(byte nautID, byte remainingBonus, Team team){
               this.nautId = nautID;
               this.remainingBonus = remainingBonus;
               this.team = team;
           }
        }

        /**
         * Returns the state of the match.
         * @return true if the match has been completed and false otherwise.
         */
        public boolean isCompletedMatch(){
            return this.isCompletedMatch;
        }

	public static void main(String[] args) throws IOException {

		Application.Parameters parameters = new ParametersImpl(args);

		Map<String, String> named = parameters.getNamed();

		String name, description, password;
		byte mapId;
		boolean isPickBan, banBidirectional, isSequentialDraft, lan, isAutoRandomMap, isCaptainsMode;
		int port;
		boolean isAutoExport;
		InetSocketAddress lobbyAddress;
		int[] schedule;
		try {
			if (parameters.getRaw().contains("-h") || parameters.getRaw().contains("--help")) {
				throw new IOException();
			}
			name = named.getOrDefault("name", "server#" + Integer.toHexString(new Random().nextInt()));
			description = named.getOrDefault("description", "");
			password = named.get("password");
			mapId = Byte.parseByte(named.getOrDefault("map", "0"));
			isAutoRandomMap = Boolean.parseBoolean(named.getOrDefault("autorandommap", "true"));
			port = Integer.parseInt(named.getOrDefault("port", "0"));
			lan = Boolean.parseBoolean(named.getOrDefault("lan", "true"));
			isAutoExport = Boolean.parseBoolean(named.getOrDefault("autoexport", "false"));
			isCaptainsMode = Boolean.parseBoolean(named.getOrDefault("isCaptainsMode", "false"));
			String lobby = named.getOrDefault("lobby", Constants.LOBBY_ADDRESS + ":" + Constants.LOBBY_PORT);
			lobbyAddress = null;
			if (lobby.trim().length() > 0 && !lobby.equals("null")) {
				String[] lobbyParts = lobby.split(":");
				if (lobbyParts.length != 2) {
					throw new IOException();
				}
				lobbyAddress = new InetSocketAddress(lobbyParts[0], Integer.parseInt(lobbyParts[1]));
			}
			String pickBanStr = named.getOrDefault("pick", "no_ban");
                        switch(pickBanStr){
                            case "no_ban":
                                isPickBan = false;
                                break;
                            case "ban":
                                isPickBan = true;
                                break;
                            default:
				throw new IOException();
                        }
			String banStr = named.getOrDefault("ban", "bi");
			switch (banStr) {
			case "uni":
				banBidirectional = false;
				break;
			case "bi":
				banBidirectional = true;
				break;
			default:
				throw new IOException();
			}
			String draftStr = named.getOrDefault("draft", "seq");
			switch (banStr) {
			case "seq":
                            isSequentialDraft = true;
				break;
			case "conc":
                            isSequentialDraft = false;
				break;
			default:
				throw new IOException();
			}

			schedule = new int[] { RED_BAN.index(), BLUE_BAN.index(), BLUE_BAN.index(), RED_BAN.index(),
			        BLUE_PICK.index(), RED_PICK.index(), RED_PICK.index(), BLUE_PICK.index(), RED_BAN.index(),
			        BLUE_BAN.index(), RED_PICK.index(), BLUE_PICK.index() };
		} catch (IOException e) {
			System.err.println("Usage: java -jar DraftingServer.jar <args>");
			System.err.println("  --name=String           sets the name of the server");
			System.err.println("  --description=String    sets the description of the server");
			System.err.println("  --pass=String           sets the password of the server");
			System.err.println("  --map=Integer           sets the map (by id)");
			System.err.println("  --autorandommap=Boolean notifies the server that this map was automatically randomly picked");
			System.err.println("  --port=Integer          sets the port (defaults to an ephemeral port with UPnP)");
			System.err.println("  --lobby=String:Integer  sets the lobby address and port");
			System.err.println("  --pick=no_ban|ban       sets the pick type");
			System.err.println("  --ban=uni|bi            sets the ban type");
			System.err.println("  --lan=Boolean           enables/disables lan discovery");
			System.err.println("  --autoexport=Boolean    enables/disables auto export of completed games to lobby server");
			System.err.println("  --iscaptainsmode=Boolean    enables/disables captains only mode");
			return;
		}
		new DraftingServer(name, description, password, mapId, isAutoRandomMap,"", port, schedule, lobbyAddress,
                    isPickBan,
		    banBidirectional, 
                    isSequentialDraft,
                    lan, 
                    isAutoExport,
                    isCaptainsMode,
                    false).run();
	}
}