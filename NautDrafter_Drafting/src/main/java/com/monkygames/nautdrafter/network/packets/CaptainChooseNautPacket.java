/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.callbacks.DraftingCallback;

/**
 * Packet for when a Captain chooses a 'naut for a pick/ban
 */
public class CaptainChooseNautPacket extends Packet {

	private Byte             nautId;
	private Byte             bonusTime;
	private DraftingCallback receiver;

	/**
	 * Create a new CaptainChooseNautPacket with the following information
	 *
	 * @param nautId
	 *            ID of the 'naut picked/banned by the captain
	 * @param bonusTime
	 *            The amount of time left in the Bonus Time
	 */
	public CaptainChooseNautPacket(byte nautId, byte bonusTime) {
		this.nautId = nautId;
		this.bonusTime = bonusTime;
		this.data = new byte[] { Packet.CAPTAIN_CHOOSE_NAUT_PACKET_ID, nautId, bonusTime };
	}

	/**
	 * Create a new CaptainChooseNautPacket with the following information
	 *
	 * @param nautId
	 *            ID of the 'naut picked/banned by the captain
	 */
	public CaptainChooseNautPacket(byte nautId) {
		this(nautId, (byte) 0xFF);
	}

	/**
	 * Create a new CaptainChooseNautPacket with the following information
	 *
	 * @param data
	 *            The byte[] of data to be split into the 'naut ID and bonus time
	 * @param receiver
	 *            The callback to be called
	 */
	public CaptainChooseNautPacket(byte[] data, DraftingCallback receiver) {
		super(data);
		this.receiver = receiver;
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		if (this.nautId == null) {
			if (this.available() <= 0) {
				return false;
			}
			this.nautId = this.data[this.pos++];
		}
		if (this.bonusTime == null) {
			if (this.available() <= 0) {
				return false;
			}
			this.bonusTime = this.data[this.pos++];
		}
		return true;
	}

	@Override
	protected void handleImpl() {
		this.receiver.onCaptainChooseNaut(this.nautId, this.bonusTime);
	}
}
