/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.PlayerIdentifier;
import com.monkygames.nautdrafter.network.callbacks.NautChooseCallback;
import com.monkygames.nautdrafter.network.callbacks.PlayerIdentifyCallback;
import com.monkygames.nautdrafter.network.packets.segments.PlayerIdentifierSegment;
import com.monkygames.nautdrafter.network.packets.segments.Segment;

/**
 * Used for the server to notify clients of a Naut Selection
 */
public class NautChoosePacket extends NautChooseRequestPacket {

	private PlayerIdentifyCallback playerIdent;

	private PlayerIdentifier       pid;
	private Byte                   teamId;

	/**
	 * Encodes a new NautSelectionPacket
	 *
	 * @param player
	 *            The player who chose the naut
	 * @param teamId
	 *            The team the player is on
	 * @param position
	 *            The position (naut) the player selected
	 */
	public NautChoosePacket(Player player, byte teamId, byte position) throws PacketEncoderException {
		super(position);
		this.data = Segment.concat(this.data, teamId, PlayerIdentifierSegment.encode(player));
	}

	/**
	 * Creates a new NautSelectionCallback for decoding
	 *
	 * @param data
	 *            The data to decode
	 * @param receiver
	 *            The callback to call once decoded
	 */
	public NautChoosePacket(byte[] data, NautChooseCallback receiver, PlayerIdentifyCallback playerIdent) {
		super(data, receiver);
		this.playerIdent = playerIdent;
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		if (!super.decodeImpl()) {
			return false;
		}
		if (this.teamId == null) {
			if (this.available() < 1) {
				return false;
			}
			this.teamId = this.data[this.pos++];
		}
		if (this.pid == null) {
			if (!PlayerIdentifierSegment.canDecode(this)) {
				return false;
			}
			this.pid = PlayerIdentifierSegment.decode(this);
		}
		return true;
	}

	@Override
	protected void handleImpl() {
		this.receiver.onPlayerChoose(this.playerIdent.getPlayer(this.pid), this.teamId, this.position);
	}
}
