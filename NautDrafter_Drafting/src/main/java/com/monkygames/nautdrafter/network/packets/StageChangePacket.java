/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.callbacks.StageChangeCallback;

/**
 * Sent by the Drafting Server to notify clients of a stage change
 */
public class StageChangePacket extends Packet {

	private static final byte   PACKET_ID = Packet.STAGE_CHANGE_PACKET_ID;
	private Byte                stage;
	private StageChangeCallback receiver;

	/**
	 * Constructs a new StageChangePacket for sending
	 *
	 * @param stage
	 *            The stage to change to
	 */
	public StageChangePacket(byte stage) {
		this.stage = stage;
		this.data = new byte[] { PACKET_ID, stage };
	}

	/**
	 * Creates a new StageChangePacket for decoding
	 *
	 * @param data
	 *            The packet data
	 * @param receiver
	 *            The callback to call once the packet is decoded
	 */
	public StageChangePacket(byte[] data, StageChangeCallback receiver) {
		super(data);
		this.receiver = receiver;
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		if (this.stage == null) {
			if (this.max() < 2) {
				return false;
			}
			this.stage = this.data[this.pos++];
		}
		return true;
	}

	@Override
	protected void handleImpl() {
		this.receiver.onStageChanged(this.stage);
	}

}
