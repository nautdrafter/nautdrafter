/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.monkygames.nautdrafter.Constants;
import com.monkygames.nautdrafter.network.callbacks.ServerAnnounceCallback;
import com.monkygames.nautdrafter.network.callbacks.ServerBrowserCallback;
import com.monkygames.nautdrafter.network.packets.Packet;
import com.monkygames.nautdrafter.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.network.packets.ServerAnnouncePacket;

/**
 * Listens for broadcast packets requesting information about servers
 */
public class LanServerInfo extends Thread {

	private static final Logger      logger         = Logger.getLogger(LanServerInfo.class.getName());

	private static final byte[]      MAGIC_PACKET   = "ohai nautdrafter".getBytes(Packet.charset);
	private static final InetAddress BROADCAST_IP;
	private static final int         BROADCAST_PORT = Constants.LOBBY_PORT;
	static {
		InetAddress tmp = null;
		try {
			tmp = InetAddress.getByName("239.6.6.6");
		} catch (UnknownHostException ex) {
			logger.log(Level.SEVERE, null, "Failed to get broadcast IP");
			logger.log(Level.SEVERE, null, ex);
			System.exit(1);
		}
		BROADCAST_IP = tmp;
	}

	private final MulticastSocket    listener;
	private final DatagramSocket     socket;
	private boolean                  running        = true;
	private byte[]                   serverInfo;

	/**
	 * Create a new LanServerInfo Thread
	 * 
	 * @param initialServerInfo
	 *            Initial packet representing information about the server
	 * @throws IOException
	 */
	public LanServerInfo(byte[] initialServerInfo) throws IOException {
		super("LanServerInfo");
		this.listener = new MulticastSocket(BROADCAST_PORT);
		this.socket = new DatagramSocket();
		this.serverInfo = initialServerInfo;
	}

	@Override
	public void run() {
		try {
			this.listener.joinGroup(BROADCAST_IP);
			MULTICAST_LISTEN: while (this.running) {
				try {
					DatagramPacket dp = new DatagramPacket(new byte[MAGIC_PACKET.length], MAGIC_PACKET.length);
					this.listener.receive(dp);
					byte[] packet = dp.getData();
					if (dp.getLength() != MAGIC_PACKET.length) {
						continue MULTICAST_LISTEN;
					}
					for (int i = 0; i < MAGIC_PACKET.length; i++) {
						if (MAGIC_PACKET[i] != packet[i]) {
							continue MULTICAST_LISTEN;
						}
					}
					dp = new DatagramPacket(this.serverInfo, this.serverInfo.length, dp.getAddress(), dp.getPort());
					this.socket.send(dp);
				} catch (IOException ex) {
					if (this.listener.isClosed()) {
						return;
					}
					logger.log(Level.SEVERE, null, ex);
				}
			}
		} catch (IOException ex) {
			logger.log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Updates the server info
	 * 
	 * @param serverInfo
	 *            Packet representing updated information about the server
	 */
	public void update(byte[] serverInfo) {
		synchronized (this.serverInfo) {
			this.serverInfo = serverInfo;
		}
	}

	/**
	 * Shuts down the LAN server info thread
	 */
	public void shutdown() {
		this.interrupt();
		this.listener.close();
		this.socket.close();
		try {
			this.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static ServerDiscovererThread serverListThread = null;

	private static final class ServerDiscovererThread extends Thread implements ServerAnnounceCallback {
		private final Set<ServerInfo>       foundServers = new HashSet<>();
		private final ServerBrowserCallback receiver;
		private MulticastSocket             socket;

		public ServerDiscovererThread(final ServerBrowserCallback receiver) {
			this.receiver = receiver;
		}

		@Override
		public void run() {
			// clear server list
			this.receiver.addServers(null);
			// send broadcast asking for servers on the local network
			try (MulticastSocket s = new MulticastSocket()) {
				this.socket = s;
				s.setSoTimeout(10000);
				DatagramPacket dp = new DatagramPacket(MAGIC_PACKET, MAGIC_PACKET.length, BROADCAST_IP, BROADCAST_PORT);
				this.socket.send(dp);
				byte[] buf = new byte[1024];
				long startTime = System.currentTimeMillis();
				while (System.currentTimeMillis() < startTime + 10000) {
					try {
						dp = new DatagramPacket(buf, buf.length);
						this.socket.receive(dp);
						ServerAnnouncePacket packet = new ServerAnnouncePacket(buf, dp.getAddress(), this);
						packet.setAvailable(dp.getLength());
						if (packet.decode()) {
							packet.handle();
						}
					} catch (SocketException | PacketDecoderException e) {
						if (this.socket == null || this.socket.isClosed()) {
							break;
						}
					}
				}
			} catch (SocketTimeoutException ex) {
				// ignore
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			serverListThread = null;
		}

		@Override
		public void onServerAnnounce(ServerInfo info) {
			this.foundServers.add(info);
			this.receiver.addServers(new ArrayList<ServerInfo>(this.foundServers));
		}

		@Override
		public void onServerUpdate(int playerCount) {
		}

		/**
		 * Shuts down the LAN server discovery thread
		 */
		public void shutdown() {
			try {
				if (this.socket != null) {
					this.socket.close();
				}
				Thread t = serverListThread;
				if (t != null) {
					t.interrupt();
					t.join();
				}
			} catch (InterruptedException ex) {
				// om nom nom
			}
		}
	}

	/**
	 * Aborts retrieval of the LAN server list
	 */
	public static void abortServerList() {
		if (serverListThread != null) {
			serverListThread.shutdown();
		}
	}

	/**
	 * Starts a new thread which retrieves the list of LAN servers
	 * 
	 * @param receiver
	 *            The callback to call with the list of servers
	 */
	public static void getServerList(final ServerBrowserCallback receiver) {
		LanServerInfo.abortServerList();
		serverListThread = new ServerDiscovererThread(receiver);
		serverListThread.start();
	}
}
