/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.callbacks.ServerPlayerConnectionCallback;
import com.monkygames.nautdrafter.network.packets.segments.PlayerSegment;
import com.monkygames.nautdrafter.network.packets.segments.Segment;

/**
 * Notify of a player joining the server
 */
public class PlayerJoinedPacket extends UserDetailsPacket {

	private ServerPlayerConnectionCallback receiver;

	/**
	 * Creates a new PlayerJoinedPacket for decoding
	 *
	 * @param data
	 *            The packet data
	 * @param receiver
	 *            The callback to call once the packet is decoded
	 * @throws PacketDecoderException
	 */
	public PlayerJoinedPacket(byte[] data, ServerPlayerConnectionCallback receiver) throws PacketDecoderException {
		super(data);
		this.receiver = receiver;
	}

	/**
	 * Constructs a new PlayerJoinedPacket for sending
	 *
	 * @param player
	 *            The player who joined
	 * @throws PacketEncoderException
	 */
	public PlayerJoinedPacket(Player player) throws PacketEncoderException {
		this.player = player;
		this.data = Segment.concat(Packet.PLAYER_JOINED_PACKET_ID, PlayerSegment.encode(this.player));
	}

	@Override
	protected void handleImpl() {
		this.receiver.onPlayerJoin(this.player);

	}
}
