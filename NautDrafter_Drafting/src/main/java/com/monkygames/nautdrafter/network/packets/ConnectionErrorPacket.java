/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.callbacks.ConnectionCallback;

/**
 * There was an error between the Drafting Server and the client
 */
public class ConnectionErrorPacket extends Packet {

	private Byte               id;
	private ConnectionCallback receiver;

	/**
	 * Constructs a new ConnectionErrorPacket
	 *
	 * @param id
	 *            The error code to send
	 */
	public ConnectionErrorPacket(byte id) {
		this.id = id;
		this.data = new byte[] { Packet.SERVER_MESSAGE_PACKET_ID, id };
	}

	/**
	 * Reads a ConnectionErrorPacket and calls the callback with the error code
	 *
	 * @param data
	 *            The data
	 * @param receiver
	 *            The callback
	 */
	public ConnectionErrorPacket(byte[] data, ConnectionCallback receiver) {
		super(data);
		this.receiver = receiver;
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		if (this.id == null) {
			if (this.available() < 1) {
				return false;
			}
			this.id = this.data[this.pos++];
		}
		return true;
	}

	@Override
	protected void handleImpl() {
		this.receiver.onConnectionFailed(this.id);
		// this is a connection error, so we should disconnect
		this.disconnect = true;
	}
}
