/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import java.util.HashSet;
import java.util.Set;

import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.Team;
import com.monkygames.nautdrafter.network.Timeline;
import com.monkygames.nautdrafter.network.callbacks.RoomInfoCallback;
import com.monkygames.nautdrafter.network.packets.segments.PlayerSegment;
import com.monkygames.nautdrafter.network.packets.segments.Segment;
import com.monkygames.nautdrafter.network.packets.segments.TeamSegment;
import com.monkygames.nautdrafter.network.packets.segments.TimelineSegment;

/**
 * Sends most of the state of a Drafting Server (stage, map, teams, players)
 */
public class RoomInfoPacket extends Packet {

	public static final byte PACKET_ID = Packet.ROOM_INFO_PACKET_ID;

	private Byte             stage;
	private Byte             mapId;
	private Boolean          banBidirectional;
	private Boolean          isSequentialDraft;
        private Boolean          isPickBan;
	private Team             teamRed, teamBlue;
	private Set<Player>      unassigned;
	private Byte             unassignedCount;
	private Timeline         timeline;
	private Byte             currentPhaseTime;

	private RoomInfoCallback receiver;

	/**
	 * Creates a new RoomInfoPacket for decoding
	 *
	 * @param data
	 *            The data to decode
	 * @param receiver
	 *            The callback to call once we've decoded
	 * @throws PacketDecoderException
	 */
	public RoomInfoPacket(byte[] data, RoomInfoCallback receiver) throws PacketDecoderException {
		super(data);
		this.receiver = receiver;
	}

	/**
	 * Encodes a new RoomInfoPacket
	 *
	 * @param stage
	 *            Stage the Drafting Server is on
	 * @param mapId
	 *            Map the game will be played on
	 * @param teamRed
	 *            The current red team
	 * @param teamBlue
	 *            The current Blue team
	 * @param unassigned
	 *            The current list of unassigned players
	 * @param timeline
	 *            The current Timeline
	 * @param currentPhaseTime
	 *            The remaining time for the current phase
	 * @throws PacketEncoderException
	 */
	public RoomInfoPacket(byte stage, 
            byte mapId, 
            boolean banBidirectional, 
            boolean isSequentialDraft,
            boolean isPickBan,
            Team teamRed, 
            Team teamBlue,
            Set<Player> unassigned,
            Timeline timeline,
            byte currentPhaseTime) throws PacketEncoderException {
		byte bitMap = (byte) (banBidirectional ? 0xF0 : 0);
		byte bitMap2 = (byte) (isSequentialDraft ? 0xF0 : 0);
		byte bitMap3 = (byte) (isPickBan ? 0xF0 : 0);
		byte unassignedCount = (byte) (unassigned.size() & 0xFF);
		byte[][] playerBuffer = new byte[unassignedCount][];
		int i = 0;
		for (Player player : unassigned) {
			playerBuffer[i++] = PlayerSegment.encode(player);
		}
		this.data = Segment.concat(PACKET_ID, stage, mapId, bitMap, bitMap2, bitMap3, TeamSegment.encode(teamRed),
		        TeamSegment.encode(teamBlue), unassignedCount, playerBuffer, TimelineSegment.encode(timeline),
		        currentPhaseTime);
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		// stage
		if (this.stage == null) {
			if (this.available() < 1) {
				return false;
			}
			this.stage = this.data[this.pos++];
		}
		// mapId
		if (this.mapId == null) {
			if (this.available() < 1) {
				return false;
			}
			this.mapId = this.data[this.pos++];
		}
		// ban direction
		if (this.banBidirectional == null) {
			if (this.available() < 1) {
				return false;
			}
			byte bitMap = this.data[this.pos++];
			this.banBidirectional = ((bitMap & 0xF0) != 0);
		}
		if (this.isSequentialDraft == null) {
			if (this.available() < 1) {
				return false;
			}
			byte bitMap2 = this.data[this.pos++];
			this.isSequentialDraft = ((bitMap2 & 0xF0) != 0);
		}
		if (this.isPickBan == null) {
			if (this.available() < 1) {
				return false;
			}
			byte bitMap3 = this.data[this.pos++];
			this.isPickBan = ((bitMap3 & 0xF0) != 0);
		}
		// teamRed
		if (this.teamRed == null) {
			if (!TeamSegment.canDecode(this)) {
				return false;
			}
			this.teamRed = TeamSegment.decode(this);
		}
		// teamBlue
		if (this.teamBlue == null) {
			if (!TeamSegment.canDecode(this)) {
				return false;
			}
			this.teamBlue = TeamSegment.decode(this);
		}
		// player count
		if (this.unassignedCount == null) {
			if (this.available() < 1) {
				return false;
			}
			this.unassignedCount = (byte) (this.data[this.pos++] & 0xFF);
		}
		// players
		if (this.unassigned == null) {
			this.unassigned = new HashSet<>();
		}
		for (; this.unassignedCount > 0; this.unassignedCount--) {
			if (!PlayerSegment.canDecode(this)) {
				return false;
			}
			this.unassigned.add(PlayerSegment.decode(this));
		}
		// timeline
		if (this.timeline == null) {
			if (!TimelineSegment.canDecode(this)) {
				return false;
			}
			this.timeline = TimelineSegment.decode(this);
		}
		// currentPhaseTime
		if (this.currentPhaseTime == null) {
			if (this.available() < 1) {
				return false;
			}
			this.currentPhaseTime = this.data[this.pos++];
		}

		return true;
	}

	@Override
	protected void handleImpl() {
		this.receiver.onConnected(this.stage, this.mapId, this.banBidirectional, 
                    this.isSequentialDraft, this.isPickBan, this.teamRed, this.teamBlue,
		    this.unassigned, this.timeline, this.currentPhaseTime);
	}

}
