/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.callbacks;

import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.PlayerIdentifier;

/**
 * Finds a player from their ID
 */
public interface PlayerIdentifyCallback {
	/**
	 * Gets a player instance from their id
	 *
	 * @param pid
	 *            The id of the player
	 * @return The player, or null if there is no player with that id
	 */
	public Player getPlayer(PlayerIdentifier pid);
}
