/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.callbacks.SpectatorConnectionCallback;

/**
 * Notifies the server of a spectator joining
 */
public class SpectatorJoinedPacket extends Packet {

	private SpectatorConnectionCallback receiver;

	/**
	 * Construct a new SpectatorJoinedPacket to send
	 */
	public SpectatorJoinedPacket() {
		this.data = new byte[] { Packet.SPECTATOR_JOINED_PACKET_ID };
	}

	/**
	 * Read a SpectatorJoinedPacket
	 *
	 * @param data
	 *            The packet data
	 * @param receiver
	 *            The callback to call when the packet is read
	 */
	public SpectatorJoinedPacket(byte[] data, SpectatorConnectionCallback receiver) {
		super(data);
		this.receiver = receiver;
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		return true;
	}

	@Override
	protected void handleImpl() {
		this.receiver.onSpectatorJoined();
	}

}
