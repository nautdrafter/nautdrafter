/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.callbacks.ChatCallback;
import com.monkygames.nautdrafter.network.packets.segments.Segment;
import com.monkygames.nautdrafter.network.packets.segments.StringSegment;

/**
 * Chat request packet (send from player to Drafting Server, which will relay it to the relevant players)
 */
public class ChatRequestPacket extends Packet {

	private static final StringSegment messageSegment = new StringSegment(2, true);

	protected Player                   sender         = null;
	private String                     message        = null;
	private ChatCallback               receiver;

	/**
	 * Creates a new ChatRequestPacket for decoding
	 *
	 * @param data
	 *            The packet data
	 * @param receiver
	 *            The callback to call with the decoded chat message
	 * @throws PacketDecoderException
	 */
	protected ChatRequestPacket(byte[] data, ChatCallback receiver) throws PacketDecoderException {
		super(data);
		this.receiver = receiver;
	}

	/**
	 * Creates a new ChatRequestPacket for decoding
	 *
	 * @param data
	 *            The packet data
	 * @param receiver
	 *            The callback to call with the decoded chat message
	 * @param sender
	 *            The player who sent this packet
	 * @throws PacketDecoderException
	 */
	public ChatRequestPacket(byte[] data, ChatCallback receiver, Player sender) throws PacketDecoderException {
		this(data, receiver);
		this.sender = sender;
	}

	/**
	 * Constructs a new ChatRequestPacket for sending
	 *
	 * @param message
	 *            The chat message to send
	 * @throws PacketEncoderException
	 */
	public ChatRequestPacket(String message) throws PacketEncoderException {
		this.message = message;

		this.data = Segment.concat(Packet.CHAT_PACKET_ID, messageSegment.encode(this.message));
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		// message
		if (this.message == null) {
			if (!messageSegment.canDecode(this)) {
				return false;
			}
			this.message = messageSegment.decode(this);
		}

		return true;
	}

	@Override
	protected void handleImpl() {
		if (this.receiver != null) {
			this.receiver.onChatReceived(this.sender, this.message);
		}
	}
}
