/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.callbacks;

import java.util.Set;

import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.Team;
import com.monkygames.nautdrafter.network.Timeline;

/**
 * Used for notifying client of room state upon connection
 */
public interface RoomInfoCallback {

	/**
	 * Callback called when successfully connected to a Drafting Server<br>
	 * Contains all the general information about the room
	 *
	 * @param stage
	 *            The current stage the room is at
	 * @param mapId
	 *            The map the room will play on
	 * @param banBidirectional
	 *            Whether bans are bidirectional in this server
	 * @param isSequentialDraft 
         *           Determines if the drafting sequence is sequential or concurrent.
         * @param isPickBan
         *           Sets if the pick bans the opponets fromm using that pick.
	 * @param teamRed
	 *            The red team
	 * @param teamBlue
	 *            The blue team
	 * @param unassigned
	 *            The unassigned players
	 * @param timeline
	 *            The timeline for Drafting
	 * @param currentPhaseTime
	 *            The time remaining on the current Timeline phase
	 */
	public void onConnected(byte stage,
	        byte mapId,
	        boolean banBidirectional,
                boolean isSequentialDraft,
                boolean isPickBan,
	        Team teamRed,
	        Team teamBlue,
	        Set<Player> unassigned,
	        Timeline timeline,
	        byte currentPhaseTime);
}
