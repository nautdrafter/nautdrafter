/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets.segments;

import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.Team;
import com.monkygames.nautdrafter.network.packets.Packet;
import com.monkygames.nautdrafter.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.network.packets.PacketEncoderException;

/**
 * Represents a team as an array of bytes (composed of PlayerSegment and StringSegment)
 */
public class TeamSegment {
	/**
	 * Encodes a team
	 *
	 * @param team
	 * @return Encoded data
	 * @throws com.monkygames.nautdrafter.network.packets.PacketEncoderException
	 */
	public static byte[] encode(Team team) throws PacketEncoderException {

		byte[] nameBuffer = StringSegment.encode(team.name, 1, true);
		byte bitMap = 0;
		byte[][] playerBuffer = new byte[3][];
		if (team.lockedIn) {
			bitMap |= 0x80;
		}
		if (team.finished) {
			bitMap |= 0x40;
		}
		for (int i = 0; i < 3; i++) {
			if (team.players[i] != null) {
				bitMap |= (1 << i);
				playerBuffer[i] = PlayerSegment.encode(team.players[i]);
			} else {
				playerBuffer[i] = new byte[0];
			}
		}
		// represent player selections by player index for network
		byte[] playerPositions = new byte[3];
		for (int i = 0; i < 3; i++) {
			if (team.nautSelections[i] == null) {
				playerPositions[i] = -1;
			} else if (team.nautSelections[i].equals(team.players[0])) {
				playerPositions[i] = 0;
			} else if (team.nautSelections[i].equals(team.players[1])) {
				playerPositions[i] = 1;
			} else if (team.nautSelections[i].equals(team.players[2])) {
				playerPositions[i] = 2;
			} else {
				playerPositions[i] = -1;
			}
		}
		return Segment.concat(nameBuffer, bitMap, team.pickedNauts, team.bannedNauts, playerBuffer, playerPositions);
	}

	/**
	 * Decodes a player from the packet
	 *
	 * @param packet
	 * @return
	 * @throws PacketDecoderException
	 */
	public static Team decode(Packet packet) throws PacketDecoderException {
		String name = StringSegment.decode(packet, 1, true);
		byte bitMap = packet.data[packet.pos++];
		byte[] picks = Segment.decode(packet, 3, false);
		byte[] bans = Segment.decode(packet, 3, false);
		boolean lockedIn = (bitMap & 0x80) != 0;
		boolean finished = (bitMap & 0x40) != 0;
		Player[] players = new Player[3];
		for (int i = 0; i < 3; i++) {
			if ((bitMap & (1 << i)) != 0) {
				players[i] = PlayerSegment.decode(packet);
			} else {
				players[i] = null;
			}
		}
		Player[] nautSelections = new Player[3];
		byte[] playerPositions = Segment.decode(packet, 3, false);
		for (int i = 0; i < 3; i++) {
			int index = playerPositions[i];
			if (index >= 0 && index < 3) {
				nautSelections[i] = players[index];
			} else {
				nautSelections[i] = null;
			}
		}
		return new Team(name, players, picks, bans, nautSelections, lockedIn, finished);
	}

	/**
	 * Check if we have enough data to decode a Team
	 *
	 * @param packet
	 *            The Packet to decode from (data and offset)
	 * @param advancePos
	 *            Whether to advance the position of the Packet
	 * @return true if we can decode a Team
	 */
	static boolean canDecode(Packet packet, boolean advancePos) {
		int pos = packet.pos;
		boolean can = false;
		canDecode: do {
			if (!StringSegment.canDecode(packet, 1, true, true)) {
				break;
			}
			if (packet.available() < 1 + 3 + 3) {
				break;
			}
			byte bitMap = packet.data[packet.pos++];
			packet.pos += 3 + 3;
			for (int i = 1; i <= 4; i <<= 1) {
				if ((bitMap & i) != 0) {
					if (!PlayerSegment.canDecode(packet, true)) {
						break canDecode;
					}
				}
			}
			if (packet.available() < 3) {
				break;
			}
			can = true;
		} while (false);
		if (!advancePos) {
			// if we're not advancing the position, restore to original position
			packet.pos = pos;
		}
		return can;
	}

	/**
	 * Check if we have enough data to decode a Team
	 *
	 * @param packet
	 *            The Packet to decode from (data and offset)
	 * @return true if we can decode a Team
	 */
	public static boolean canDecode(Packet packet) {
		return TeamSegment.canDecode(packet, false);
	}
}
