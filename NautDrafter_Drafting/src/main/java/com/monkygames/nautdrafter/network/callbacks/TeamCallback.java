/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.callbacks;

import com.monkygames.nautdrafter.network.Player;

/**
 * Team change events (only used by team selection room)
 */
public interface TeamCallback {
	/**
	 * A player has left a team, joined a team, or swapped teams
	 *
	 * @see com.monkygames.nautdrafter.network.Team
	 *
	 * @param player
	 *            The player for which the change happened
	 * @param teamId
	 *            Id of the team which changed
	 * @param position
	 *            Id of the position which changed
	 */
	public void onChangedTeam(Player player, int teamId, int position);

	/**
	 * A player has disputed the Captain position of a team and won, displacing the existing Captain
	 *
	 * @param teamId
	 *            Id of the team
	 * @param newCaptain
	 *            The player who will move into the new Captain position
	 * @param oldCaptain
	 *            The player who was displaced from the Captain position
	 * @param oldCaptainNewPosition
	 *            The position the old Captain will move into
	 */
	public void onCaptainSwap(int teamId, Player newCaptain, Player oldCaptain, int oldCaptainNewPosition);

	/**
	 * A team has changed their name
	 *
	 * @see com.monkygames.nautdrafter.network.Team
	 *
	 * @param teamId
	 *            The team who changed their name
	 * @param name
	 *            The new name for the team
	 */
	public void onChangedTeamName(int teamId, String name);

	/**
	 * A team has been locked (the team is full and the captain clicked "ready")
	 *
	 * @param teamId
	 *            Id of the team
	 */
	public void onLockTeam(int teamId);
}
