/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.callbacks.PlayerIdentifyCallback;
import com.monkygames.nautdrafter.network.callbacks.TeamCallback;
import com.monkygames.nautdrafter.network.packets.segments.PlayerIdentifierSegment;
import com.monkygames.nautdrafter.network.packets.segments.Segment;

/**
 * Special case for Team Selection position changing: <br>
 * A player has disputed the captain's position and won, and is swapping positions with the captain
 */
public class TeamCaptainSwapPacket extends Packet {
	public static final byte       PACKET_ID = Packet.TEAM_CAPTAIN_SWAP_PACKET_ID;

	private PlayerIdentifyCallback playerIdent;
	private TeamCallback           receiver;

	private Integer                teamId, oldCaptainNewPosition;
	private Player                 newCaptain, oldCaptain;

	/**
	 * Creates a new TeamCaptainSwapPacket for decoding
	 *
	 * @param data
	 *            The packet data
	 * @param receiver
	 *            The callback to call once the packet is decoded
	 * @param playerIdent
	 *            The callback to call to get a player by their id
	 * @throws PacketDecoderException
	 */
	public TeamCaptainSwapPacket(byte[] data, TeamCallback receiver, PlayerIdentifyCallback playerIdent)
	        throws PacketDecoderException {
		super(data);

		this.playerIdent = playerIdent;
		this.receiver = receiver;
	}

	/**
	 * Constructs a new TeamCaptainSwapPacket for sending
	 *
	 * @param teamId
	 *            The team the captain was from
	 * @param newCaptain
	 *            The player who is moving into the captain's position
	 * @param oldCaptain
	 *            The player who was kicked out of the captain's position
	 * @param oldCaptainNewPosition
	 *            The position the old captain will move into
	 * @throws PacketEncoderException
	 */
	public TeamCaptainSwapPacket(int teamId, Player newCaptain, Player oldCaptain, int oldCaptainNewPosition)
	        throws PacketEncoderException {

		this.teamId = teamId;
		this.newCaptain = newCaptain;
		this.oldCaptain = oldCaptain;
		this.oldCaptainNewPosition = oldCaptainNewPosition;

		this.data = Segment.concat(PACKET_ID, (byte) ((this.teamId << 4) | this.oldCaptainNewPosition),
		        PlayerIdentifierSegment.encode(this.newCaptain), PlayerIdentifierSegment.encode(this.oldCaptain));
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {

		// teamId & position
		if (this.teamId == null && this.oldCaptainNewPosition == null) {
			if (this.available() < 1) {
				return false;
			}
			int encoded = this.data[this.pos++];

			this.teamId = ((encoded & 0xF0) >> 4);
			this.oldCaptainNewPosition = (encoded & 0x0F);
		}
		// new captain
		if (this.newCaptain == null) {
			if (!PlayerIdentifierSegment.canDecode(this)) {
				return false;
			}
			this.newCaptain = this.playerIdent.getPlayer(PlayerIdentifierSegment.decode(this));
		}
		// old captain
		if (this.oldCaptain == null) {
			if (!PlayerIdentifierSegment.canDecode(this)) {
				return false;
			}
			this.oldCaptain = this.playerIdent.getPlayer(PlayerIdentifierSegment.decode(this));
		}
		return true;
	}

	@Override
	protected void handleImpl() {
		this.receiver.onCaptainSwap(this.teamId, this.newCaptain, this.oldCaptain, this.oldCaptainNewPosition);
	}
}
