/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.callbacks.TeamCallback;
import com.monkygames.nautdrafter.network.packets.segments.Segment;
import com.monkygames.nautdrafter.network.packets.segments.StringSegment;

/**
 * Updates the name of a team
 */
public class TeamNameChangePacket extends Packet {

	private static final StringSegment nameSegment = new StringSegment(1, true);

	protected Integer                  team;
	protected String                   name;

	protected TeamCallback             receiver;

	/**
	 * Creates a new TeamNameChangePacket for decoding
	 *
	 * @param data
	 *            The packet data
	 * @param receiver
	 *            The callback to call once the packet is decoded
	 * @throws PacketDecoderException
	 */
	public TeamNameChangePacket(byte[] data, TeamCallback receiver) throws PacketDecoderException {
		super(data);
		this.receiver = receiver;
	}

	/**
	 * Constructs a new TeamNameChangePacket for sending
	 *
	 * @param team
	 *            The team whose name to change
	 * @param name
	 *            The new name
	 * @throws PacketEncoderException
	 */
	public TeamNameChangePacket(int team, String name) throws PacketEncoderException {
		this.team = team;
		this.name = name;

		this.data = Segment.concat(Packet.TEAM_NAME_CHANGE_PACKET_ID, (byte) (this.team & 0xFF),
		        nameSegment.encode(this.name));
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		// team
		if (this.team == null) {
			if (this.available() < 1) {
				return false;
			}
			this.team = (int) (this.data[this.pos++] & 0xFF);
		}
		// name
		if (this.name == null) {
			if (!nameSegment.canDecode(this)) {
				return false;
			}
			this.name = nameSegment.decode(this);
		}

		return true;
	}

	@Override
	protected void handleImpl() {
		this.receiver.onChangedTeamName(this.team, this.name);
	}
}
