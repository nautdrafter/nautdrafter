/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.callbacks.DraftingCallback;

/**
 * Packet for when a Captain selects a 'naut on their grid
 */
public class CaptainSelectNautPacket extends Packet {

	private Byte             nautId;
	private DraftingCallback receiver;

	/**
	 * Create a new CaptainSelectNautPacket with the following information
	 *
	 * @param nautId
	 *            ID of the 'naut selected by the captain
	 */
	public CaptainSelectNautPacket(byte nautId) {
		this.nautId = nautId;
		this.data = new byte[] { Packet.CAPTAIN_SELECT_NAUT_PACKET_ID, nautId };
	}

	/**
	 * Create a new CaptainSelectNautPacket with the following information
	 *
	 * @param data
	 *            The byte[] of data to be split into the 'naut ID
	 * @param receiver
	 *            The callback to be called
	 */
	public CaptainSelectNautPacket(byte[] data, DraftingCallback receiver) {
		super(data);
		this.receiver = receiver;
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		if (this.nautId == null) {
			if (this.max() < 2) {
				return false;
			}
			this.nautId = this.data[this.pos++];
		}
		return true;
	}

	@Override
	protected void handleImpl() {
		this.receiver.onCaptainSelectNaut(this.nautId);
	}

}
