/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.PlayerIdentifier;
import com.monkygames.nautdrafter.network.callbacks.ChatCallback;
import com.monkygames.nautdrafter.network.callbacks.PlayerIdentifyCallback;
import com.monkygames.nautdrafter.network.packets.segments.PlayerIdentifierSegment;
import com.monkygames.nautdrafter.network.packets.segments.Segment;

/**
 * Chat message packet (sent from Drafting Server to relevant players)
 */
public class ChatPacket extends ChatRequestPacket {

	private PlayerIdentifier       pid = null;
	private PlayerIdentifyCallback playerIdent;

	/**
	 * Create a new ChatPacket for decoding
	 *
	 * @param data
	 *            The packet data
	 * @param receiver
	 *            The callback to call with the decoded chat message
	 * @param playerIdent
	 *            The callback to call to identify the player who sent the message
	 * @throws PacketDecoderException
	 */
	public ChatPacket(byte[] data, ChatCallback receiver, PlayerIdentifyCallback playerIdent)
	        throws PacketDecoderException {
		super(data, receiver);
		this.playerIdent = playerIdent;
	}

	/**
	 * Constructs a new ChatPacket to send
	 *
	 * @param sender
	 *            The id of the sender (us)
	 * @param message
	 *            The chat message
	 * @throws PacketEncoderException
	 */
	public ChatPacket(Player sender, String message) throws PacketEncoderException {
		super(message);

		this.sender = sender;

		this.data = Segment.concat(this.data, PlayerIdentifierSegment.encode(this.sender));
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		// message
		if (!super.decodeImpl()) {
			return false;
		}
		// player
		if (this.pid == null) {
			if (!PlayerIdentifierSegment.canDecode(this)) {
				return false;
			}
			this.pid = PlayerIdentifierSegment.decode(this);
			if (this.sender == null) {
				this.sender = this.playerIdent.getPlayer(this.pid);
			} else if (!this.sender.equals(this.pid)) {
				throw new PacketDecoderException("Invalid ID received!");
			}
		}

		return true;
	}
}
