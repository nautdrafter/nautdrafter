/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.callbacks.AuthenticationCallback;
import com.monkygames.nautdrafter.network.packets.segments.Segment;
import com.monkygames.nautdrafter.network.packets.segments.StringSegment;

/**
 * Used for sending authentication to the server
 */
public class PasswordPacket extends Packet {

	public static final byte           PACKET_ID       = Packet.PASSWORD_PACKET_ID;

	private static final StringSegment passwordSegment = new StringSegment(1, true);

	private String                     password        = null;
	private AuthenticationCallback     receiver;

	/**
	 * Creates a new PasswordPacket for decoding
	 *
	 * @param data
	 *            The packet data
	 * @param receiver
	 *            The callback to call when the packet is decoded
	 * @throws PacketDecoderException
	 */
	public PasswordPacket(byte[] data, AuthenticationCallback receiver) throws PacketDecoderException {
		super(data);
		this.receiver = receiver;
	}

	/**
	 * Constructs a new PasswordPacket for sending
	 *
	 * @param password
	 *            The password to authenticate with
	 * @throws PacketEncoderException
	 */
	public PasswordPacket(String password) throws PacketEncoderException {
		this.password = password;

		this.data = Segment.concat(PACKET_ID, passwordSegment.encode(this.password));
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		// password
		if (this.password == null) {
			if (!passwordSegment.canDecode(this)) {
				return false;
			}
			this.password = passwordSegment.decode(this);
		}

		return true;
	}

	@Override
	protected void handleImpl() {
		this.receiver.onPasswordReceived(this.password);
	}

}
