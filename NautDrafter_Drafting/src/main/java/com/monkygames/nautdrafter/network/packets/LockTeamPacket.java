/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.callbacks.TeamCallback;

/**
 * Generic confirmation from a team captain
 */
public class LockTeamPacket extends Packet {

	public static final byte PACKET_ID = Packet.TEAM_READY_PACKET_ID;
	private TeamCallback     receiver;
	private Integer          teamId;

	/**
	 * Constructs a new LockTeamPacket for sending
	 *
	 * @param teamId
	 *            The team to lock
	 */
	public LockTeamPacket(int teamId) {
		this.teamId = teamId;

		this.data = new byte[2];
		this.data[0] = PACKET_ID;
		this.data[1] = (byte) (teamId & 0xFF);
	}

	/**
	 * Constructs a new LockTeamPacket for sending, with no team id
	 */
	public LockTeamPacket() {
		this(0xFF);
	}

	/**
	 * Creates a new LockTeamPacket for decoding
	 *
	 * @param data
	 *            The packet data
	 * @param receiver
	 *            The callback to call when the packet is decoded
	 */
	public LockTeamPacket(byte[] data, TeamCallback receiver) {
		super(data);
		this.receiver = receiver;
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		if (this.teamId == null) {
			if (this.max() < 2) {
				return false;
			}
			this.teamId = this.data[this.pos++] & 0xFF;
		}
		return true;
	}

	@Override
	protected void handleImpl() {
		this.receiver.onLockTeam(this.teamId);
	}
}
