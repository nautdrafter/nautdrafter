/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.callbacks;

/**
 * Contains callbacks used by the Drafting Server
 */
public interface DraftingCallback {
	/**
	 * Callback called when a captain has picked/banned a naut
	 *
	 * @param nautId
	 *            The naut that was picked/banned
	 * @param bonusTimeRemaining
	 *            The remaining bonus time of the team whose turn it was (given so out timing doesn't get out of sync
	 *            with the server)
	 */
	public void onCaptainChooseNaut(byte nautId, byte bonusTimeRemaining);

	/**
	 * Callback called when a captain has selected a naut (to let their team see what they're looking at)
	 *
	 * @param nautId
	 *            The naut that was selected
	 */
	public void onCaptainSelectNaut(byte nautId);
}
