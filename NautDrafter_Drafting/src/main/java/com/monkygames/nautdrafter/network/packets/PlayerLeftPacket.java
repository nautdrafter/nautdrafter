/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.PlayerIdentifier;
import com.monkygames.nautdrafter.network.callbacks.PlayerIdentifyCallback;
import com.monkygames.nautdrafter.network.callbacks.ServerPlayerConnectionCallback;
import com.monkygames.nautdrafter.network.packets.segments.PlayerIdentifierSegment;
import com.monkygames.nautdrafter.network.packets.segments.Segment;

/**
 * Notify of a player leaving the server
 */
public class PlayerLeftPacket extends Packet {

	public static final byte               PACKET_ID = Packet.PLAYER_LEFT_PACKET_ID;

	private PlayerIdentifier               pid       = null;
	private Player                         player;
	private ServerPlayerConnectionCallback receiver;
	private PlayerIdentifyCallback         playerIdent;

	/**
	 * Creates a new PlayerLeftPacket for decoding
	 *
	 * @param data
	 *            The packet data
	 * @param receiver
	 *            The callback to call once the packet is decoded
	 * @param playerIdent
	 *            The callback to call to identify the player from their id
	 * @throws PacketDecoderException
	 */
	public PlayerLeftPacket(byte[] data, ServerPlayerConnectionCallback receiver, PlayerIdentifyCallback playerIdent)
	        throws PacketDecoderException {
		super(data);
		this.receiver = receiver;
		this.playerIdent = playerIdent;
	}

	/**
	 * Constructs a PlayerLeftPacket for sending
	 *
	 * @param player
	 *            The player who left
	 * @throws PacketEncoderException
	 */
	public PlayerLeftPacket(Player player) throws PacketEncoderException {
		this.data = Segment.concat(PACKET_ID, PlayerIdentifierSegment.encode(player));
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		// player id
		if (this.pid == null) {
			if (!PlayerIdentifierSegment.canDecode(this)) {
				return false;
			}
			this.pid = PlayerIdentifierSegment.decode(this);
			this.player = this.playerIdent.getPlayer(this.pid);
		}

		return true;
	}

	@Override
	protected void handleImpl() {
		this.receiver.onPlayerLeave(this.player);
	}
}
