/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import static com.monkygames.nautdrafter.network.Timeline.TimelineInit.BLUE_BAN;
import static com.monkygames.nautdrafter.network.Timeline.TimelineInit.BLUE_PICK;
import static com.monkygames.nautdrafter.network.Timeline.TimelineInit.RED_BAN;
import static com.monkygames.nautdrafter.network.Timeline.TimelineInit.RED_PICK;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.Stack;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.Team;
import com.monkygames.nautdrafter.network.Timeline;
import com.monkygames.nautdrafter.network.Timeline.TimelineInit;
import com.monkygames.nautdrafter.network.callbacks.RoomInfoCallback;

/**
 * @see com.monkygames.nautdrafter.network.packets.RoomInfoPacket
 */
public class RoomInfoPacketTest implements RoomInfoCallback {

	public RoomInfoPacketTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	private static final Random rng = new Random();

	private static Player getPlayer() {
		return new Player(true, rng.nextLong(), Integer.toHexString(rng.nextInt()), "", rng.nextLong());
	}

	private static byte        _stage            = 0;
	private static byte        _map              = 0;
        private static boolean     _isPickBan        = false;
	private static boolean     _banBidirectional = false;
	private static boolean     _isSequentialMode = true;
	private static Team        _teamRed          = new Team("red team",
	                                                     rng.nextBoolean() ? RoomInfoPacketTest.getPlayer() : null,
	                                                     rng.nextBoolean() ? RoomInfoPacketTest.getPlayer() : null,
	                                                     rng.nextBoolean() ? RoomInfoPacketTest.getPlayer() : null);
	private static Team        _teamBlue         = new Team("blue team",
	                                                     rng.nextBoolean() ? RoomInfoPacketTest.getPlayer() : null,
	                                                     rng.nextBoolean() ? RoomInfoPacketTest.getPlayer() : null,
	                                                     rng.nextBoolean() ? RoomInfoPacketTest.getPlayer() : null);
	private static Timeline    _timeline;
	static {
		int[] timelineSchedule = new int[] { RED_BAN.index(), BLUE_BAN.index(), BLUE_BAN.index(), RED_BAN.index(),
		    BLUE_PICK.index(), RED_PICK.index(), RED_PICK.index(), BLUE_PICK.index(), RED_BAN.index(),
		    BLUE_BAN.index(), RED_PICK.index(), BLUE_PICK.index() };
		TimelineInit[] schedule = new TimelineInit[timelineSchedule.length];
		TimelineInit[] vals = TimelineInit.values();
		int index = 0;
		for (int i : timelineSchedule) {
			schedule[index++] = vals[i];
		}
		_timeline = new Timeline(schedule);
	}
	private static Set<Player> _unassigned       = new HashSet<>();
	static {
		for (int i = 0, max = rng.nextInt(6); i < max; i++) {
			_unassigned.add(RoomInfoPacketTest.getPlayer());
		}
	}

	/**
	 * Test of encoding and decoding, of class ChatPacket
	 */
	@Test
	public void testPacket() throws Exception {
		RoomInfoPacket encodePacket = new RoomInfoPacket(_stage, _map, _isPickBan, _banBidirectional,_isSequentialMode, _teamRed, _teamBlue,
		        _unassigned, _timeline, (byte) 30);
		RoomInfoPacket decodePacket = new RoomInfoPacket(encodePacket.data, this);
		decodePacket.decode();
		decodePacket.handle();
	}

	@Override
	public void onConnected(byte stage,
	        byte mapId,
                boolean isPickBan,
	        boolean banBidirectional,
                boolean isSequentialMode,
	        Team teamRed,
	        Team teamBlue,
	        Set<Player> unassigned,
	        Timeline timeline,
	        byte currentPhaseTime) {
		Assert.assertEquals(_stage, stage);
		Assert.assertEquals(_teamRed.name, teamRed.name);
		Assert.assertEquals(_teamBlue.name, teamBlue.name);

		Assert.assertEquals(_timeline.pos, timeline.pos);
		Assert.assertEquals(_timeline.redBonus, timeline.redBonus);
		Assert.assertEquals(_timeline.blueBonus, timeline.blueBonus);
		Assert.assertArrayEquals(_timeline.items.toArray(), timeline.items.toArray());

		Stack<Player> expected = new Stack<>();
		Stack<Player> actual = new Stack<>();
		Comparator<Player> comp = ((Player p1, Player p2) -> {
			return p1.getId() < p2.getId() ? 1 : -1;
		});
		expected.addAll(_unassigned);
		expected.sort(comp);
		actual.addAll(unassigned);
		actual.sort(comp);

		while (!expected.isEmpty()) {

			Player exp = expected.pop();
			Player act = actual.pop();

			Assert.assertEquals(exp, act);
			Assert.assertEquals(exp.name, act.name);
			Assert.assertEquals(exp.iconUrl, act.iconUrl);
		}

		Assert.assertTrue(actual.isEmpty());
	}

}
