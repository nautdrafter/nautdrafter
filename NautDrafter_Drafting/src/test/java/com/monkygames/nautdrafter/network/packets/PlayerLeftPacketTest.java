/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.PlayerIdentifier;
import com.monkygames.nautdrafter.network.callbacks.PlayerIdentifyCallback;
import com.monkygames.nautdrafter.network.callbacks.ServerPlayerConnectionCallback;

/**
 * @see com.monkygames.nautdrafter.network.packets.PlayerLeftPacket
 */
public class PlayerLeftPacketTest implements PlayerIdentifyCallback, ServerPlayerConnectionCallback {

	public PlayerLeftPacketTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	private Player player;

	/**
	 * Test of encoding and decoding, of class PlayerJoinedPacket
	 */
	@Test
	public void testPacket() throws Exception {
		Random rng = new Random();
		this.player = new Player(rng.nextBoolean(), rng.nextLong(), "Player",
		        "https://gravatar.com/avatar/d56d7460c5b31ce297cfcf6230a6d1a3.jpg", rng.nextInt());
		PlayerLeftPacket encodePacket = new PlayerLeftPacket(this.player);
		PlayerLeftPacket decodePacket = new PlayerLeftPacket(encodePacket.data, null, this);
		decodePacket.decode();
	}

	@Override
	public Player getPlayer(PlayerIdentifier pid) {
		return new Player(pid, "", "", 0);
	}

	@Override
	public void onPlayerJoin(Player player) {
		// unused
	}

	@Override
	public void onPlayerLeave(Player player) {
		Assert.assertEquals(this.player, player);
	}

}
