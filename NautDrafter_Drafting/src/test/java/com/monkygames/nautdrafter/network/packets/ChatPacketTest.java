/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import java.util.Base64;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.PlayerIdentifier;
import com.monkygames.nautdrafter.network.callbacks.ChatCallback;
import com.monkygames.nautdrafter.network.callbacks.PlayerIdentifyCallback;

/**
 * @see com.monkygames.nautdrafter.network.packets.ChatPacket
 */
public class ChatPacketTest implements PlayerIdentifyCallback, ChatCallback {

	public ChatPacketTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	private Player sender;
	private String message;

	/**
	 * Test of encoding and decoding, of class ChatPacket
	 */
	@Test
	public void testPacket() throws Exception {
		Random rng = new Random();
		Base64.Encoder stringGen = Base64.getEncoder();
		this.sender = new Player(rng.nextBoolean(), rng.nextLong(), "Player",
		        "https://gravatar.com/avatar/d56d7460c5b31ce297cfcf6230a6d1a3.jpg", rng.nextInt());
		byte[] buf = new byte[10000];
		rng.nextBytes(buf);
		this.message = stringGen.encodeToString(buf);
		ChatPacket encodePacket = new ChatPacket(this.sender, this.message);
		ChatPacket decodePacket = new ChatPacket(encodePacket.data, null, this);
		Assert.assertTrue(decodePacket.decode());
		decodePacket.handle();
	}

	/**
	 * Test that packets cleanly cancel decoding when running out of data, and continue from where they left off
	 */
	@Test
	public void testBuffered() throws Exception {
		Random rng = new Random();
		Base64.Encoder stringGen = Base64.getEncoder();
		this.sender = new Player(rng.nextBoolean(), rng.nextLong(), "Player",
		        "https://gravatar.com/avatar/d56d7460c5b31ce297cfcf6230a6d1a3.jpg", rng.nextInt());
		byte[] buf = new byte[10000];
		rng.nextBytes(buf);
		this.message = stringGen.encodeToString(buf);
		ChatPacket encodePacket = new ChatPacket(this.sender, this.message);
		ChatPacket decodePacket = new ChatPacket(encodePacket.data, null, this);
		// decode to the first 5 bytes (not enough)
		decodePacket.setAvailable(5);
		Assert.assertFalse(decodePacket.decode());
		// decode the message only
		decodePacket.setAvailable(encodePacket.data.length - 5);
		Assert.assertFalse(decodePacket.decode());
		// decode the whole packet
		decodePacket.setAvailable(Integer.MAX_VALUE);
		Assert.assertTrue(decodePacket.decode());
		decodePacket.handle();
	}

	@Override
	public Player getPlayer(PlayerIdentifier pid) {
		return new Player(pid, this.sender.name, this.sender.iconUrl, this.sender.tieBreak);
	}

	@Override
	public void onChatReceived(Player sender, String message) {
		Assert.assertEquals(this.sender, sender);
		Assert.assertEquals(this.message, message);
	}
}
