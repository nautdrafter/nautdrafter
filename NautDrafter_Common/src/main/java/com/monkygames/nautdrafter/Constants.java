/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter;

import com.monkygames.nautdrafter.network.packets.PacketEncoderException;
import com.monkygames.nautdrafter.network.packets.VersionPacket;

/**
 * General constants used by NautDrafter
 */
public class Constants {

	public static final String LOBBY_ADDRESS      = "server.nautdrafter.monky-games.com";
	public static final int    LOBBY_PORT         = 50000;

	public static final int    KEEPALIVE_INTERVAL = 10000;

	public static final int    VERSION_MAJOR      = 0;
	public static final int    VERSION_MINOR      = 6;
	public static final int    VERSION_PATCH      = 2;
	public static final String VERSION            = VERSION_MAJOR + "." + VERSION_MINOR + "." + VERSION_PATCH;

	private static final byte[] getVersionPacket() {
		try {
			return new VersionPacket(VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH).data;
		} catch (PacketEncoderException e) {
			throw new RuntimeException(e);
		}
	}

	public static final byte[] VERSION_PACKET = Constants.getVersionPacket();
	public static final String HOMEPAGE       = "http://nautdrafter.bitbucket.org/";
	public static final String SUCCESS_PAGE   = HOMEPAGE + "success.html";
	public static final String FAILURE_PAGE   = HOMEPAGE + "failure.html";
	public static final String UPDATE_PAGE    = HOMEPAGE + "nautdrafter/download/";

	private Constants() {
	}
    /**
     * Print the version to be consumed by the installer.
     */
    public static void printVersion(){
        System.out.println(Constants.VERSION);
    }
}
