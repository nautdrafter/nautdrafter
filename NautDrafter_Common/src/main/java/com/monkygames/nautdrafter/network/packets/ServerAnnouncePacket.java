/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.monkygames.nautdrafter.network.packets;

import java.net.InetAddress;

import com.monkygames.nautdrafter.network.ServerInfo;
import com.monkygames.nautdrafter.network.callbacks.ServerAnnounceCallback;
import com.monkygames.nautdrafter.network.packets.segments.Segment;
import com.monkygames.nautdrafter.network.packets.segments.ServerInfoSegment;

/**
 * Lobby Server implementation of ServerAnnouncePacket
 */
public class ServerAnnouncePacket extends Packet {

	private ServerInfo             serverInfo;
	private InetAddress            sender;
	private ServerAnnounceCallback receiver;

	/**
	 * Encodes a ServerInfo to a byte[] to send over the network
	 *
	 * @param serverInfo
	 *            Information about a server
	 * @throws PacketEncoderException
	 */
	public ServerAnnouncePacket(ServerInfo serverInfo) throws PacketEncoderException {
		this.data = Segment.concat(Packet.SERVER_ANNOUNCE_PACKET_ID, ServerInfoSegment.encode(serverInfo, false));
	}

	/**
	 * Creates a new ServerAnnouncePacket
	 *
	 * @param data
	 *            The raw data to decode
	 * @param key
	 *            The connection the announcement came from
	 * @param receiver
	 *            The callback to call to handle this packet
	 * @throws PacketDecoderException
	 */
	public ServerAnnouncePacket(byte[] data, InetAddress sender, ServerAnnounceCallback receiver)
	        throws PacketDecoderException {
		super(data);
		this.sender = sender;
		this.receiver = receiver;
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		if (this.serverInfo == null) {
			if (!ServerInfoSegment.canDecode(this, false)) {
				return false;
			}
			this.serverInfo = ServerInfoSegment.decode(this, false);
			this.serverInfo = new ServerInfo(this.serverInfo.name, this.serverInfo.description, this.sender,
			        this.serverInfo.mapId, this.serverInfo.port, this.serverInfo.playerCount,
			        this.serverInfo.hasPassword, this.serverInfo.dns);
		}
		return true;
	}

	@Override
	protected void handleImpl() {
		this.receiver.onServerAnnounce(this.serverInfo);
	}

}
