/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.callbacks;

/**
 * Callback for version number being received
 */
public interface VersionCallback {

	/**
	 * Callback for version number being received
	 * 
	 * @param major
	 *            The major version number - must be equal for compatibility
	 * @param minor
	 *            The minor version number - must be equal for drafting compatibility
	 * @param patch
	 *            The patch version number - different patch versions are compatible
	 */
	public void onVersionReceived(int major, int minor, int patch);

}
