/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network;

import java.net.InetAddress;

/**
 * Describes a server that we could connect to Note: This means a "game" (drafting) room, NOT a lobby
 */
public class ServerInfo {

	/**
	 * The name and description of the server (first and second row of text in the drafting screen)
	 */
	public final String      name, description;
	/**
	 * The IP address of the server
	 */
	public final InetAddress address;
	/**
	 * The the server is listening on
	 */
	public final int         port;
	/**
	 * The map the server will play on
	 */
	public final int         mapId;
	/**
	 * Whether the server requires a password (lets the client know if they need to send a PasswordPacket)
	 */
	public final boolean     hasPassword;
	/**
	 * Number of players currently in the server
	 */
	public int               playerCount;
        /**
         * The DNS name for the server.
         */
        public String dns;

	/**
	 * Creates a new ServerInfo
	 *
	 * @param name
	 *            The name of the Drafting Server
	 * @param description
	 *            The description of the Drafting Server
	 * @param address
	 *            The IP address of the Drafting Server
	 * @param port
	 *            The port of the Drafting Server
	 * @param mapId
	 *            The map the Drafting Server will play on
	 * @param playerCount
	 *            The number of players on the Drafting Server
	 * @param hasPassword
	 *            Whether the Drafting Server has a password
         * @param dns
         *            The DNS name for this server.
	 */
	public ServerInfo(String name, 
            String description, 
            InetAddress address, 
            int mapId, 
            int port, 
            int playerCount,
	    boolean hasPassword,
            String dns
        ) {
            this.name = name;
            this.description = description;
            this.address     = address;
            this.mapId       = mapId;
            this.port        = port;
            this.playerCount = playerCount;
            this.hasPassword = hasPassword;
            this.dns         = dns;
	}

	@Override
	public int hashCode() {
	    return this.address.hashCode() ^ this.port;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ServerInfo) {
			ServerInfo other = (ServerInfo) obj;
			return this.address.equals(other.address) && this.port == other.port;
		}
		return false;
	}
}
