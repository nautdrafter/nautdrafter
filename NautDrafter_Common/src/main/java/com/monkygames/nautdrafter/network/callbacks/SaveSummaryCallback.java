/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.callbacks;

/**
 * Requesting the Lobby Server to save the summary export.
 */
public interface SaveSummaryCallback {

    /**
     * Saves the summary to disk.
     * Note, if the sever was not setup with a path to a directory for
     * the summary exports, than no file will be saved.
     * @param summary the summary to save to disk.
     * @param redTeamName
     * @param blueTeamName
     * @param mapName
     * @param isRandomMap true if this map was randomly picked by server.
     */
    public void saveSummaryToDisk(String summary, String redTeamName, String blueTeamName, String mapName, boolean isRandomMap);
}