/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets.segments;

import com.monkygames.nautdrafter.network.packets.Packet;
import com.monkygames.nautdrafter.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.network.packets.PacketEncoderException;

/**
 * Represents an integer as an array of bytes
 */
public class IntSegment {

	/**
	 * Number of bytes used to store the data (or to store the length if it is variable)
	 */
	public final int lengthBytes;

	/**
	 * Create an IntSegment of fixed length in bytes
	 *
	 * @param lengthBytes
	 */
	public IntSegment(int lengthBytes) {
		this.lengthBytes = lengthBytes;
	}

	/**
	 * Encodes an integer.
	 *
	 * @param val
	 *            integer to encode
	 * @param length
	 *            Fixed length in number of bytes from the integer to store (max = 8 bytes)
	 * @return Encoded data
	 * @throws PacketEncoderException
	 */
	static byte[] encode(long val, int length) throws PacketEncoderException {
		byte[] data = new byte[length];
		for (int i = 0; i < length; i++) {
			data[i] = (byte) ((val >> ((length - (i + 1)) * 8)) & 0xFF);
		}
		return data;
	}

	/**
	 * Decodes an integer from the packet
	 *
	 * @param packet
	 *            The raw packet
	 * @param length
	 *            Fixed length in number of bytes from the integer to store (max = 8 bytes)
	 * @return The decoded integer
	 * @throws PacketDecoderException
	 */
	static long decode(Packet packet, int length) throws PacketDecoderException {
		int offset = packet.pos;
		byte[] data = packet.data;
		if (data.length - offset < length) {
			throw new PacketDecoderException("Invalid packet length");
		}
		long val = 0;
		for (int i = 0; i < length; i++) {
			val |= (long) (data[i + offset] & 0xFF) << ((length - (i + 1)) * 8);
		}
		packet.pos += length;
		return val;
	}

	/**
	 * Check if we have enough data to decode an integer
	 *
	 * @param packet
	 *            The Packet to decode from (data and offset)
	 * @param advancePos
	 *            Whether to advance the position of the Packet
	 * @return true if we can decode an integer
	 */
	static boolean canDecode(Packet packet, int length, boolean advancePos) {
		if (packet.available() < length) {
			return false;
		}
		if (advancePos) {
			packet.pos += length;
		}
		return true;
	}

	/**
	 * Encodes an integer (1-8 bytes of data depending on the length this segment was constructed with)
	 *
	 * @param val
	 *            The integer to encode
	 * @return The integer encoded as a byte[]
	 * @throws PacketEncoderException
	 */
	public byte[] encode(long val) throws PacketEncoderException {
		return IntSegment.encode(val, this.lengthBytes);
	}

	/**
	 * Decodes an integer (1-8 bytes of data depending on the length this segment was constructed with)
	 *
	 * @param packet
	 *            The Packet to decode from (data and offset)
	 * @return The decoded integer
	 * @throws PacketDecoderException
	 */
	public long decode(Packet packet) throws PacketDecoderException {
		return IntSegment.decode(packet, this.lengthBytes);
	}

	/**
	 * Check if there is enough data available to decode an integer
	 *
	 * @param packet
	 *            The packet to decode from (data and offset)
	 * @return true if we can decode an integer
	 */
	public boolean canDecode(Packet packet) {
		return IntSegment.canDecode(packet, this.lengthBytes, false);
	}
}
