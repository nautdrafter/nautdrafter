/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.monkygames.nautdrafter.network.packets;

import java.nio.charset.Charset;

/**
 * Generic packet class
 */
public abstract class Packet {

	public static final byte    CHAT_PACKET_ID                = 0;
	public static final byte    PLAYER_JOINED_PACKET_ID       = 1;
	public static final byte    PLAYER_LEFT_PACKET_ID         = 2;
	public static final byte    ROOM_INFO_PACKET_ID           = 3;
	public static final byte    TEAM_NAME_CHANGE_PACKET_ID    = 6;
	public static final byte    TEAM_CHANGE_PACKET_ID         = 12;
	public static final byte    TEAM_CAPTAIN_SWAP_PACKET_ID   = 13;
	public static final byte    TEAM_READY_PACKET_ID          = 14;
	public static final byte    STAGE_CHANGE_PACKET_ID        = 15;
	public static final byte    CAPTAIN_CHOOSE_NAUT_PACKET_ID = 16;
	public static final byte    CAPTAIN_SELECT_NAUT_PACKET_ID = 18;
	public static final byte    PASSWORD_PACKET_ID            = 19;
	public static final byte    NAUT_CHOOSE_PACKET_ID         = 20;
	public static final byte    SPECTATOR_JOINED_PACKET_ID    = 21;
	public static final byte    SERVER_MESSAGE_PACKET_ID      = 22;

	// packet constants used for communication with the lobby server
	public static final byte    SERVER_ANNOUNCE_PACKET_ID           = 4;
	public static final byte    SERVER_UPDATE_PACKET_ID             = 5;
	public static final byte    SERVER_LIST_REQUEST_PACKET_ID       = 7;
	public static final byte    SERVER_LIST_PACKET_ID               = 8;
	public static final byte    CLIENTID_PACKET_ID                  = 9;
	public static final byte    NEWAUTH_PACKET_ID                   = 10;
	public static final byte    USERDETAILS_PACKET_ID               = 11;
	public static final byte    VERSION_PACKET_ID                   = 23;
        public static final byte    SAVE_SUMMARY_PACKET_ID              = 24;
        public static final byte    DEDICATED_SERVER_REQUEST_PACKET_ID  = 25;
        public static final byte    DEDICATED_SERVER_RESPONSE_PACKET_ID = 26;

	public static final byte    KEEPALIVE_PACKET_ID           = -128;

	public static final byte    HTTP_GET_REQUEST_ID           = 71;                      // ASCII 'G'

	public static final Charset charset                       = Charset.forName("UTF-8");

	/**
	 * The raw bytes of the packet
	 */
	public byte[]               data;

	/**
	 * The current position in the packet
	 */
	public int                  pos;
	/**
	 * The number of bytes available (used when the packet represents a bytebuffer)
	 */
	private int                 available;
	/**
	 * Whether the packet has already been decoded
	 */
	protected boolean           decoded                       = true;
	/**
	 * Where to start decoding from
	 */
	protected int               decodeFrom                    = 1;
	/**
	 * Response, if any
	 */
	protected Packet            response                      = null;
	/**
	 * Whether to disconnect
	 */
	protected boolean           disconnect                    = false;

	/**
	 * Create a packet from objects and encode it to byte data
	 */
	protected Packet() {
	}

	/**
	 * Create a packet from the network data and decode it to its component Objects
	 *
	 * @param data
	 * @param available
	 */
	protected Packet(byte[] data, int available) {
		this.data = data;
		this.available = available;
		this.decoded = false;
	}

	/**
	 * Create a packet from the network data and decode it to its component Objects
	 *
	 * @param data
	 */
	protected Packet(byte[] data) {
		this(data, Integer.MAX_VALUE);
	}

	/**
	 * @return The total number of bytes available in the packet
	 */
	public int max() {
		return Math.min(this.data.length, this.available);
	}

	/**
	 * @return The number of bytes we can read from our current position in the packet
	 */
	public int available() {
		return this.max() - this.pos;
	}

	/**
	 * Sets the total number of bytes available to the packet
	 *
	 * @param available
	 */
	public void setAvailable(int available) {
		this.available = available;
	}

	/**
	 * Implementation method for decoding a packet<br>
	 * Wrapped by decode(), which ensures the packet is not already decoded, and prepares the packet for decoding
	 *
	 * @return true if the packet was fully decoded
	 * @throws PacketDecoderException
	 */
	protected abstract boolean decodeImpl() throws PacketDecoderException;

	/**
	 * Decodes the packet from byte[] to Objects
	 *
	 * @return true if the packet was fully decoded
	 * @throws com.monkygames.nautdrafter.network.packets.PacketDecoderException
	 */
	public final boolean decode() throws PacketDecoderException {
		if (this.decoded) {
			return true;
		}
		this.pos = this.decodeFrom;
		this.decoded = this.decodeImpl();
		this.decodeFrom = this.pos;
		return this.decoded;
	}

	/**
	 * Implementation method for handling a packet<br>
	 * Wrapped by handle(), which ensures the packet is ready to be handled
	 */
	protected abstract void handleImpl();

	/**
	 * Handles the packet
	 */
	public void handle() {
		// can't handle if not decoded
		if (!this.decoded) {
			return;
		}
		this.handleImpl();
	}

	/**
	 * @return Whether there is a response to send
	 */
	public boolean hasResponse() {
		return (this.response != null);
	}

	/**
	 * @return A packet to send in response, or null
	 */
	public Packet getResponse() {
		return this.response;
	}

	/**
	 * @return Whether the connection should be killed
	 */
	public boolean getDisconnect() {
		return this.disconnect;
	}
}
