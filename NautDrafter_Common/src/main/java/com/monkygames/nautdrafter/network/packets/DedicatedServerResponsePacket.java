/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.callbacks.DedicatedServerResponseCallback;
import com.monkygames.nautdrafter.network.packets.segments.BooleanSegment;
import com.monkygames.nautdrafter.network.packets.segments.IntSegment;
import com.monkygames.nautdrafter.network.packets.segments.Segment;

/**
 */
public class DedicatedServerResponsePacket extends Packet {

    public static final byte           PACKET_ID       = Packet.DEDICATED_SERVER_RESPONSE_PACKET_ID;

    public IntSegment intSegment = new IntSegment(1);
    public int success;

    private DedicatedServerResponseCallback receiver;

    /**
     * Creates a new PasswordPacket for decoding
     *
     * @param data
     *            The packet data
     * @param receiver
     * @throws PacketDecoderException
     */
    public DedicatedServerResponsePacket(byte[] data) throws PacketDecoderException {
        super(data);
    }

    /**
     * Constructs a new PasswordPacket for sending
     *
     * @param success
     * @throws PacketEncoderException
     */
    public DedicatedServerResponsePacket(int success) throws PacketEncoderException {
        this.success = success;
        this.data = Segment.concat(PACKET_ID, 
            this.intSegment.encode(success)
        );
    }

    @Override
    protected boolean decodeImpl() throws PacketDecoderException {
        success = (int)this.intSegment.decode(this);
        return true;
    }

    @Override
    protected void handleImpl() {
        if(receiver != null){
            this.receiver.onDedicatedServerResponseReceived(success);
        }
    }
}