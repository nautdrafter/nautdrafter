/*
 * Copyright (C) 2014 NautDrafter Common
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets.segments;

import java.net.InetAddress;
import java.net.UnknownHostException;

import com.monkygames.nautdrafter.network.ServerInfo;
import com.monkygames.nautdrafter.network.packets.Packet;
import com.monkygames.nautdrafter.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.network.packets.PacketEncoderException;

/**
 * Encodes and Decodes ServerInfo
 */
public class ServerInfoSegment {

	/**
	 * Encodes a ServerInfo
	 *
	 * @param server
	 * @param includeAddr
	 * @return Encoded data
	 * @throws com.monkygames.nautdrafter.network.packets.PacketEncoderException
	 */
	public static byte[] encode(ServerInfo server, boolean includeAddr) throws PacketEncoderException {
		// name, description, ip, map_id, port, player_count
		return Segment.concat(StringSegment.encode(server.name, 1, true),
		        StringSegment.encode(server.description, 1, true),
		        (includeAddr ? Segment.encode(server.address.getAddress(), 1, true) : new byte[0]),
		        (byte) ((server.mapId) & 0xFF),
                        (byte) ((server.port >> 8) & 0xFF),
                        (byte) ((server.port) & 0xFF),
		        (byte) ((server.playerCount) & 0xFF), 
                        (byte) ((server.hasPassword) ? 1 : 0),
                        StringSegment.encode(server.dns, 1, true)
                );
	}

	/**
	 * Encodes a ServerInfo
	 *
	 * @param server
	 * @return Encoded data
	 * @throws com.monkygames.nautdrafter.network.packets.PacketEncoderException
	 */
	public static byte[] encode(ServerInfo server) throws PacketEncoderException {
		return ServerInfoSegment.encode(server, true);
	}

	/**
	 * Decodes a player from the packet
	 *
	 * @param packet
	 * @param includeAddr
	 * @return
	 * @throws PacketDecoderException
	 */
	public static ServerInfo decode(Packet packet, boolean includeAddr) throws PacketDecoderException {
		String name = StringSegment.decode(packet, 1, true);
		String description = StringSegment.decode(packet, 1, true);
		InetAddress addr = null;
		if (includeAddr) {
			try {
				addr = InetAddress.getByAddress(Segment.decode(packet, 1, true));
			} catch (UnknownHostException ex) {
				throw new PacketDecoderException("Failed to decode address");
			}
		}
		int mapId = (int) IntSegment.decode(packet, 1);
		int port = (int) IntSegment.decode(packet, 2);
		int playerCount = (int) IntSegment.decode(packet, 1);
		int hasPassword = (int) IntSegment.decode(packet, 1);
		String dns = StringSegment.decode(packet, 1, true);
		return new ServerInfo(name, 
                    description, 
                    addr, 
                    mapId, 
                    port, 
                    playerCount, 
                    hasPassword != 0,
                    dns
                );
	}

	/**
	 * Decodes a player from the packet
	 *
	 * @param packet
	 * @return
	 * @throws PacketDecoderException
	 */
	public static ServerInfo decode(Packet packet) throws PacketDecoderException {
		return ServerInfoSegment.decode(packet, true);
	}

	/**
	 * Check if we have enough data to decode a ServerInfo
	 *
	 * @param packet
	 *            The Packet to decode from (data and offset)
	 * @param includeAddr
	 *            Whether to include the address of the server
	 * @param advancePos
	 *            Whether to advance the position of the Packet
	 * @return true if we can decode a ServerInfo
	 */
	static boolean canDecode(Packet packet, boolean includeAddr, boolean advancePos) {
		int pos = packet.pos;
		boolean can = (StringSegment.canDecode(packet, 1, true, true) && StringSegment.canDecode(packet, 1, true, true)
		        && (!includeAddr || Segment.canDecode(packet, 1, true, true)) && packet.available() >= 5);
		if (!advancePos) {
			// if we're not advancing the position, restore to original position
			packet.pos = pos;
		}
		return can;
	}

	/**
	 * Checks if there is enough data to decode depending on this segment's settings
	 *
	 * @param packet
	 *            The Packet to decode from (data and offset)
	 * @param includeAddr
	 *            Whether to include the address of the server
	 * @return true if we can decode
	 */
	public static boolean canDecode(Packet packet, boolean includeAddr) {
		return ServerInfoSegment.canDecode(packet, includeAddr, false);
	}
}
