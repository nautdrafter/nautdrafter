/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets.segments;

import java.util.Arrays;

import com.monkygames.nautdrafter.network.packets.Packet;
import com.monkygames.nautdrafter.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.network.packets.PacketEncoderException;

/**
 * Prepends a header containing the length, to an array of bytes
 */
public class Segment {

	/**
	 * Number of bytes used to store the data (or to store the length if it is variable)
	 */
	public final int     lengthBytes;
	/**
	 * Whether the length is variable
	 */
	public final boolean variableLength;

	/**
	 * Create a Segment, optionally of variable length
	 *
	 * @param lengthBytes
	 *            the length in bytes if not variable, or the number of bytes used to store the length
	 * @param variableLength
	 *            whether the length is variable
	 */
	public Segment(int lengthBytes, boolean variableLength) {
		this.lengthBytes = lengthBytes;
		this.variableLength = variableLength;
	}

	/**
	 * Create a Segment of fixed length in bytes
	 *
	 * @param lengthBytes
	 */
	public Segment(int lengthBytes) {
		this(lengthBytes, false);
	}

	/**
	 * Encodes a raw sequence of bytes
	 *
	 * @param raw
	 *            data to encode
	 * @param length
	 *            length in bytes, or number of bytes for storing the length if variable
	 * @param variableLength
	 *            whether the length is variable
	 * @return Encoded data
	 * @throws com.monkygames.nautdrafter.network.packets.PacketEncoderException
	 */
	static byte[] encode(byte[] raw, int length, boolean variableLength) throws PacketEncoderException {
		if (variableLength) {
			if (raw.length >= (2L << (length * 8))) {
				throw new PacketEncoderException("Value was too long");
			}
			byte[] data = new byte[length + raw.length];
			for (int i = 0; i < length; i++) {
				data[i] = (byte) ((raw.length >> ((length - (i + 1)) * 8)) & 0xFF);
			}
			System.arraycopy(raw, 0, data, length, raw.length);
			return data;
		} else {
			return raw;
		}
	}

	/**
	 * Determines the length of a variable-length data segment
	 *
	 * @param data
	 *            The raw data array
	 * @param offset
	 *            Offset into the data array
	 * @param bytes
	 *            Number of bytes used to represent the variable length
	 * @return The length of the segment
	 */
	static int decodeVariableLength(byte[] data, int offset, int bytes) {
		int len = 0;
		for (int i = 0; i < bytes; i++) {
			len |= (data[i + offset] & 0xFF) << ((bytes - (i + 1)) * 8);
		}
		return len;
	}

	/**
	 * Gets some raw data from the packet
	 *
	 * @param packet
	 *            The raw packet
	 * @param length
	 * @param variableLength
	 * @return The extracted data
	 * @throws PacketDecoderException
	 */
	static byte[] decode(Packet packet, int length, boolean variableLength) throws PacketDecoderException {
		int offset = packet.pos;
		byte[] data = packet.data;
		if (variableLength) {
			int len = Segment.decodeVariableLength(data, offset, length);
			offset += length;
			length = len;
		}
		if (data.length - offset < length) {
			throw new PacketDecoderException("Invalid packet length");
		}
		packet.pos = offset + length;
		return Arrays.copyOfRange(data, offset, length + offset);
	}

	/**
	 * Check if we have enough data to decode
	 *
	 * @param packet
	 *            The Packet to decode from (data and offset)
	 * @param length
	 *            number of bytes of data, or number of bytes of the length field (if variableLength)
	 * @param variableLength
	 *            true if the data includes a length header
	 * @param advancePos
	 *            Whether to advance the position of the Packet
	 * @return true if we can decode
	 */
	static boolean canDecode(Packet packet, int length, boolean variableLength, boolean advancePos) {
		// don't try to decode any further if not enough data
		int available = packet.available();
		if (available < length) {
			return false;
		}
		if (variableLength) {
			length += Segment.decodeVariableLength(packet.data, packet.pos, length);
		}
		if (available < length) {
			return false;
		}
		if (advancePos) {
			packet.pos += length;
		}
		return true;
	}

	/**
	 * Encodes some raw data with length depending on this segment's settings
	 *
	 * @param val
	 *            The raw data to encode
	 * @return The raw data with a length header prepended if this packet has variable length
	 * @throws PacketDecoderException
	 */
	public byte[] encode(byte[] val) throws PacketEncoderException {
		return Segment.encode(val, this.lengthBytes, this.variableLength);
	}

	/**
	 * Decodes some raw data with length depending on this segment's settings
	 *
	 * @param packet
	 *            The Packet to decode from (data and offset)
	 * @return The raw data
	 * @throws PacketDecoderException
	 */
	public byte[] decode(Packet packet) throws PacketDecoderException {
		return Segment.decode(packet, this.lengthBytes, this.variableLength);
	}

	/**
	 * Checks if there is enough data to decode with length depending on this segment's settings
	 *
	 * @param packet
	 *            The Packet to decode from (data and offset)
	 * @return true if we can decode
	 */
	public boolean canDecode(Packet packet) {
		return Segment.canDecode(packet, this.lengthBytes, this.variableLength, false);
	}

	/**
	 * Concatenates data into an array
	 *
	 * @param args
	 *            varargs list of byte, byte[], or byte[][]
	 * @return the concatenated data array
	 * @throws PacketEncoderException
	 *             If any of the args is invalid
	 */
	public static final byte[] concat(Object... args) throws PacketEncoderException {
		int length = 0;
		// find the length of the list
		for (Object val : args) {
			if (val instanceof Byte) {
				length++;
			} else if (val instanceof byte[]) {
				length += ((byte[]) val).length;
			} else if (val instanceof byte[][]) {
				for (byte[] arr : (byte[][]) val) {
					length += arr.length;
				}
			} else {
				throw new PacketEncoderException("Invalid parameter!");
			}
		}
		byte[] data = new byte[length];
		int pos = 0;
		// copy the data into the array
		for (Object val : args) {
			if (val instanceof Byte) {
				data[pos++] = (byte) val;
			} else if (val instanceof byte[]) {
				System.arraycopy(val, 0, data, pos, ((byte[]) val).length);
				pos += ((byte[]) val).length;
			} else if (val instanceof byte[][]) {
				for (byte[] arr : (byte[][]) val) {
					System.arraycopy(arr, 0, data, pos, arr.length);
					pos += arr.length;
				}
			}
		}
		return data;
	}
}
