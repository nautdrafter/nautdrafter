/*
 * Copyright (C) 2014 NautDrafter Common
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets.segments;

import com.monkygames.nautdrafter.network.CreateServerInfo;
import com.monkygames.nautdrafter.network.packets.Packet;
import com.monkygames.nautdrafter.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.network.packets.PacketEncoderException;

/**
 * Encodes and Decodes CreateServerInfo
 */
public class CreateServerInfoSegment {

    /**
     * Encodes a ServerInfo
     *
     * @param server
     * @return Encoded data
     * @throws com.monkygames.nautdrafter.network.packets.PacketEncoderException
     */
    public static byte[] encode(CreateServerInfo server) throws PacketEncoderException {
        return Segment.concat(
            StringSegment.encode(server.name, 1, true),
            StringSegment.encode(server.description, 1, true),
            IntSegment.encode(server.mapId, 4),
            BooleanSegment.encode(server.isAutoRandomMap),
            BooleanSegment.encode(server.banBidirectional),
            BooleanSegment.encode(server.isSequentialDraft),
            BooleanSegment.encode(server.isCaptainsMode),
            BooleanSegment.encode(server.isPickBan),
            ArraySegment.encode(server.schedule)
        );
    }

    /**
     * Decodes a player from the packet
     *
     * @param packet
     * @return
     * @throws PacketDecoderException
     */
    public static CreateServerInfo decode(Packet packet) throws PacketDecoderException {
        String name               = StringSegment.decode(packet, 1, true);
        String description        = StringSegment.decode(packet, 1, true);
        int mapId                 = (int) IntSegment.decode(packet, 4);
        boolean isAutoRandomMap   = BooleanSegment.decode(packet);
        boolean banBiDirectional  = BooleanSegment.decode(packet);
        boolean isSequentialDraft = BooleanSegment.decode(packet);
        boolean isCaptainsMode    = BooleanSegment.decode(packet);
        boolean isPickBan         = BooleanSegment.decode(packet);
        int schedule[]            = ArraySegment.decode(packet);

        return new CreateServerInfo(
            name, 
            description, 
            mapId, 
            isAutoRandomMap,
            schedule,
            banBiDirectional,
            isSequentialDraft,
            isCaptainsMode,
            isPickBan
        );
    }

    /**
     * Check if we have enough data to decode a ServerInfo
     *
     * @param packet
     *            The Packet to decode from (data and offset)
     * @param includeAddr
     *            Whether to include the address of the server
     * @param advancePos
     *            Whether to advance the position of the Packet
     * @return true if we can decode a ServerInfo
     */
    public static boolean canDecode(Packet packet, boolean advancePos) {
        /*
        int pos = packet.pos;
        boolean can = (StringSegment.canDecode(packet, 1, true, true) && StringSegment.canDecode(packet, 1, true, true)
                && (!includeAddr || Segment.canDecode(packet, 1, true, true)) && packet.available() >= 5);
        if (!advancePos) {
                // if we're not advancing the position, restore to original position
                packet.pos = pos;
        }
        return can;
        */
        return true;
    }

}
