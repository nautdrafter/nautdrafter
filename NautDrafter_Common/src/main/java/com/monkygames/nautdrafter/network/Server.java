/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.Channel;
import java.nio.channels.ClosedSelectorException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.monkygames.nautdrafter.network.packets.Packet;

/**
 * Base server class
 */
public abstract class Server {

	/**
	 * Size to allocate for send/receive buffers
	 */
	//public static final int             BUFFER_SIZE = 10240;
	public static final int             BUFFER_SIZE = 2*10240; //20KB

	protected final Selector            selector;
	protected final ServerSocketChannel ssc;
	protected final Queue<Runnable>     runLater    = new LinkedList<Runnable>();
	private boolean                     isShutdown;

	/**
	 * Stores the SelectionKey of the currently reading client<br>
	 * Used for getting reference to the connection in callbacks which don't accept a connection reference (and don't
	 * make sense to add it to them... i.e. they are used in other places and it would be messy)<br>
	 * This is not remotely thread-safe, however since it is a private instance variable in a single-threaded server,
	 * there should never be multiple threads accessing it
	 */
	protected SelectionKey              readingKey;

	/**
	 * Creates a new Server listening on addr
	 *
	 * @param addr
	 *            The address to listen on
	 * @throws IOException
	 */
	protected Server(InetSocketAddress addr) throws IOException {
		this.ssc = ServerSocketChannel.open();
		this.selector = Selector.open();
		this.ssc.socket().setReuseAddress(true);
		this.ssc.socket().bind(addr);
		Logger.getLogger(Server.class.getName()).log(Level.INFO, "Server starting on {0}:{1}",
		        new Object[] { this.ssc.socket().getInetAddress().getHostAddress(), this.ssc.socket().getLocalPort() });
		this.ssc.configureBlocking(false);
		this.ssc.register(this.selector, SelectionKey.OP_ACCEPT);
	}

	/**
	 * @return the local port this server is listening on
	 */
	public int getPort() {
		return this.ssc.socket().getLocalPort();
	}

	/**
	 * @return the IP address this server is bound to
	 */
	public InetAddress getAddress() {
		return this.ssc.socket().getInetAddress();
	}

	/**
	 * Closes a connection and performs any relevant cleanup
	 *
	 * @param key
	 *            Reference to the connected client. Contains a ConnectionInfo which has a send and receive buffer
	 */
	protected void kill(SelectionKey key) {
		try {
			if (this.readingKey == key) {
				this.readingKey = null;
			}
			((SocketChannel) key.channel()).socket().close();
			key.channel().close();
		} catch (IOException ex) {
			Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Accepts a connection (either a server or a client looking for a server)
	 *
	 * @param key
	 * @throws IOException
	 */
	protected SelectionKey accept(SelectionKey key) throws IOException {
		SocketChannel sc = ((ServerSocketChannel) key.channel()).accept();
		sc.configureBlocking(false);
		return sc.register(this.selector, SelectionKey.OP_READ, new ConnectionInfo());
	}

	/**
	 * Creates a new packet based on the packet id
	 *
	 * @param key
	 *            Reference to the connected client. Contains a ConnectionInfo which has a send and receive buffer
	 * @param data
	 *            the packet data
	 * @return the new packet, or null if there was an error
	 * @throws java.io.IOException
	 */
	protected abstract Packet createPacket(SelectionKey key, byte[] data) throws IOException;

	/**
	 * Wrapper for handling a packet in case a subclass needs to perform some additional checks
	 *
	 * @param packet
	 *            The packet to handle
	 */
	protected void handlePacket(Packet packet) {
		packet.handle();
	}

	/**
	 * Handles data received from a host
	 *
	 * @param key
	 *            Reference to the connected client. Contains a ConnectionInfo which has a send and receive buffer
	 * @throws Exception
	 */
	private void read(SelectionKey key) throws Exception {
		this.readingKey = key;
		SocketChannel channel = (SocketChannel) key.channel();
		ConnectionInfo info = (ConnectionInfo) key.attachment();
		boolean extraIter = false; // used to force an extra iteration when a packet has been read and data remains
		int read = 0;
		while (!info.kill && (extraIter || (read = channel.read(info.recvBuffer)) > 0)) {
			extraIter = false;

			// create the packet if we're at the start of a new packet
			if (info.decoding == null) {
				info.decoding = this.createPacket(key, info.recvBuffer.array());
				if (info.decoding == null) {
					this.kill(key);
					return;
				}
			}
			// try to decode (or continue decoding) the packet

			// buffer position is the number of bytes in the buffer...
			// aka the number of bytes available to decode
			info.decoding.setAvailable(info.recvBuffer.position());
			// if the packet is fully decoded, handle it
			if (info.decoding.decode()) {
				this.handlePacket(info.decoding);
				// in case the attachment changed
				info = (ConnectionInfo) key.attachment();
				// don't kill immediately
				info.kill = info.decoding.getDisconnect();
				// if there is a response packet to be sent, send it
				if (info.decoding.hasResponse()) {
					// if the buffer overflows, the connection will be killed
					info.sendBuffer.put(info.decoding.getResponse().data);
					this.write(key);
				} else if (info.kill) {
					// can kill immediately since there is no response to send
					this.kill(key);
					return;
				}
				// retain any remaining data in the buffer
				if (info.decoding.pos < info.recvBuffer.position()) {
					byte[] remainder = new byte[info.recvBuffer.position() - info.decoding.pos];
					info.recvBuffer.position(info.decoding.pos);
					info.recvBuffer.get(remainder);
					info.recvBuffer.clear();
					info.recvBuffer.put(remainder);
					extraIter = true;
				} else {
					info.recvBuffer.clear();
				}
				// we're not decoding that packet anymore
				info.decoding = null;
			}
		}
		if (read == -1) {
			this.kill(key);
		}
		this.readingKey = null;
	}

	/**
	 * Tries to write data from the send_buffer of a connection<br>
	 * Will be called again when more data can be written, until the send_buffer is empty
	 *
	 * @param key
	 *            Reference to the connected client. Contains a ConnectionInfo which has a send and receive buffer
	 */
	protected void write(SelectionKey key) {
		try {
			ConnectionInfo info = (ConnectionInfo) key.attachment();
			SocketChannel channel = (SocketChannel) key.channel();
			if (!channel.isConnected()) {
				this.kill(key);
				return;
			}
			if (info.sendBuffer.position() > 0) {
				// go to start of buffer
				info.sendBuffer.flip();
				// write any data
				channel.write(info.sendBuffer);
				// move any unwritten data to the start of the buffer
				info.sendBuffer.compact();
			}
			// check if we have more data to write
			// if so, we need to listen to the OP_WRITE (ready to write) event
			if (info.sendBuffer.position() > 0) {
				key.interestOps(key.interestOps() | SelectionKey.OP_WRITE);
			} else {
				key.interestOps(key.interestOps() & ~SelectionKey.OP_WRITE);
				// once we've written everything, we can kill the client
				if (info.kill) {
					this.kill(key);
				}
			}
		} catch (IOException ex) {
			Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
			this.kill(key);
		}
	}

	/**
	 * The main loop of the server.<br>
	 * Uses non-blocking I/O to find sockets that are ready to accept/read/write, and handles them.
	 */
	public void run() {
		Iterator<SelectionKey> iter;
		SelectionKey key;
		while (this.ssc.isOpen()) {
			try {
				this.selector.select();
				iter = this.selector.selectedKeys().iterator();
				while (iter.hasNext()) {
					key = iter.next();
					iter.remove();
					try {
						if (key.isAcceptable()) {
							this.accept(key);
						}
						if (key.isWritable()) {
							this.write(key);
						}
						if (key.isReadable()) {
							this.read(key);
						}
					} catch (Exception ex) {
						this.kill(key);
					}
				}
				synchronized (this.runLater) {
					Runnable toRun;
					while ((toRun = this.runLater.poll()) != null) {
						toRun.run();
					}
				}
			} catch (IOException ex) {
				Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
			} catch (ClosedSelectorException ex) {
				if (!this.isShutdown) {
					Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
				} else {
					Logger.getLogger(Server.class.getName()).log(Level.INFO, "Server is shutting down");
				}
			}
		}
	}

	/**
	 * Cleanly shuts down this server
	 */
	public void shutdown() {
		try {
			this.isShutdown = true;
			for (SelectionKey key : this.selector.keys()) {
				Channel channel = key.channel();
				if (channel instanceof SocketChannel) {
					((SocketChannel) channel).socket().close();
				}
				channel.close();
			}
			this.ssc.socket().close();
			this.ssc.close();
			this.selector.wakeup();
			this.selector.close();
		} catch (IOException ex) {
			Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
        /**
         * Returns the state of this server.
         * @return true if the server is shutdown and false otherwise.
         */
        public boolean isShutdown(){
            return this.isShutdown;
        }
}
