/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.packets.segments.PlayerSegment;
import com.monkygames.nautdrafter.network.packets.segments.Segment;

/**
 * Encodes a packet that will contain all of the details from Steam
 */
public class UserDetailsPacket extends Packet {

	protected Player player;

	/**
	 * @return The player this packet describes
	 */
	public Player getPlayer() {
		return this.player;
	}

	/**
	 * Creates a new UserDetailsPacket with some data to decode
	 *
	 * @param data
	 *            A player to decode
	 * @throws PacketDecoderException
	 */
	public UserDetailsPacket(byte[] data) throws PacketDecoderException {
		super(data);
	}

	protected UserDetailsPacket() {
	}

	/**
	 * Creates a new UserDetailsPacket with a player to encode
	 *
	 * @param player
	 *            A player to encode
	 * @throws PacketEncoderException
	 */
	public UserDetailsPacket(Player player) throws PacketEncoderException {
		this.player = player;
		this.data = Segment.concat(Packet.USERDETAILS_PACKET_ID, PlayerSegment.encode(this.player));
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		// player
		if (this.player == null) {
			if (!PlayerSegment.canDecode(this)) {
				return false;
			}
			this.player = PlayerSegment.decode(this);
		}
		return true;
	}

	@Override
	protected void handleImpl() {
		throw new UnsupportedOperationException("Not supported");
	}
}
