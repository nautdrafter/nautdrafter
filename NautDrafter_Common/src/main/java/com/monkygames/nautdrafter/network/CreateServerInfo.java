/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network;

import java.util.Arrays;

/**
 * Describes a dedicated server create request.
 */
public class CreateServerInfo {

    /**
     * The name and description of the server (first and second row of text in the drafting screen)
     */
    public final String      name, description;
    /**
     * The map the server will play on
     */
    public final int         mapId;
    /**
     * Is the map automatically randomly chosen by computer.
     */
    public final boolean     isAutoRandomMap;
    /**
     * The drafting configuration.
     */
    public final int[]       schedule;
    /**
     * True if bans are for both teams.
     */
    public final boolean     banBidirectional;
    /**
     * True if its a 1v1 draft or false if 3v3.
     */
    public final boolean     isCaptainsMode;
    public final boolean     isSequentialDraft;
    /**
     * True if a pick is a ban for the opponent's team.
     */
    public final boolean     isPickBan;
    


    /**
     * Creates a new ServerInfo
     *
     * @param name
     *            The name of the Drafting Server
     * @param description
     *            The description of the Drafting Server
     * @param mapId
     * @param isAutoRandomMap
     * @param schedule
     * @param banBidirectional
     * @param isSequentialDraft
     * @param isCaptainsMode
     * @param isPickBan
     */
    public CreateServerInfo(
        String name, 
        String description, 
        int mapId, 
        boolean isAutoRandomMap,
        int[] schedule,
        boolean banBidirectional,
        boolean isSequentialDraft,
        boolean isCaptainsMode,
        boolean isPickBan
    ) {

        this.name               = name;
        this.description        = description;
        this.mapId              = mapId;
        this.isAutoRandomMap    = isAutoRandomMap;
        this.schedule           = schedule;
        this.banBidirectional   = banBidirectional;
        this.isSequentialDraft  = isSequentialDraft;
        this.isCaptainsMode     = isCaptainsMode;
        this.isPickBan          = isPickBan;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CreateServerInfo) {
            CreateServerInfo other = (CreateServerInfo) obj;

            for(int i = 0; i < this.schedule.length; i++){
                System.out.println(this.schedule[i]+" "+other.schedule[i]);
            }
            return 
                this.name.equals(other.name) && 
                this.description.equals(other.description) &&
                this.mapId             == other.mapId &&
                this.isAutoRandomMap   == other.isAutoRandomMap &&
                this.banBidirectional  == other.banBidirectional &&
                this.isSequentialDraft == other.isSequentialDraft &&
                this.isCaptainsMode    == other.isCaptainsMode &&
                this.isPickBan         == other.isPickBan; 
        }
        return false;
    }
}
