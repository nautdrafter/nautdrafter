/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network;

import java.util.ArrayList;
import java.util.List;

/**
 * Describes the structure of the Timeline for a drafting room
 */
public class Timeline {

	public static final byte PICK = 0;
	public static final byte BAN  = 1;

	/**
	 * This enum contains the values for each of phases of the Timeline
	 */
	public static enum TimelineInit {
		RED_PICK, RED_BAN, BLUE_PICK, BLUE_BAN;

		/**
		 * Get the enums index
		 */
		public int index() {

			switch (this) {
			case BLUE_BAN:
				return 3;
			case BLUE_PICK:
				return 2;
			case RED_BAN:
				return 1;
			case RED_PICK:
				return 0;
			}
			return 0;
		}
	}

	/**
	 * A TimelineItem is a phase of the Timeline e.g. Red Pick or Blue Ban
	 */
	public static class TimelineItem {
		public final byte team;
		public final byte type;

		// Used when received by the UI to determine what position the pick/ban is for
		public int        pos;

		// Used to send with the initial data sent to the UI to fill in old picks/bans
		public byte       charId;

		/**
		 * Create a new TimelineItem to represent the phase that the timeline is in</br> This constructor is to be used
		 * when you do not know the Character ID of the character that was picked
		 *
		 * @param team
		 *            The team who turn it is to pick/ban
		 * @param type
		 *            Whether it is a Pick or Ban (using the above constants)
		 */
		public TimelineItem(byte team, byte type) {
			this.team = team;
			this.type = type;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof TimelineItem) {
				TimelineItem other = (TimelineItem) obj;
				return this.charId == other.charId && this.pos == other.pos && this.team == other.team
				        && this.type == other.type;
			}
			return false;
		}
	}

	/**
	 * An ArrayList to represent the entire structure of the Timeline. Contains TimelineItems to represent each of the
	 * phases that the timeline must go through
	 */
	public final ArrayList<TimelineItem> items;

	/**
	 * The position along the timeline that we are. Starting at zero and incrementing as time goes on.
	 */
	public byte                          pos          = 0;

	public static final byte             PHASE_LENGTH = 30;
	public static final byte             BONUS_LENGTH = 120;

	public byte                          redBonus     = BONUS_LENGTH;
	public byte                          blueBonus    = BONUS_LENGTH;

	/**
	 * @return The remaining bonus time for the current team
	 */
	public byte getBonus() {
		if (this.pos < 0 || this.pos >= this.items.size()) {
			return 0;
		}
		switch (this.items.get(this.pos).team) {
		case Team.RED:
			return this.redBonus;
		case Team.BLUE:
			return this.blueBonus;
		default:
			return 0;
		}
	}

	/**
	 * Sets the remaining bonus time for the current team
	 *
	 * @param bonus
	 *            The remaining bonus time to set
	 */
	public void setBonus(byte bonus) {
		if (this.pos < 0 || this.pos >= this.items.size()) {
			return;
		}
		switch (this.items.get(this.pos).team) {
		case Team.RED:
			this.redBonus = bonus;
			break;
		case Team.BLUE:
			this.blueBonus = bonus;
			break;
		}
	}

	/**
	 * @return the current TimelineItem
	 */
	public TimelineItem getCurrent() {
		return this.items.get(this.pos);
	}

	/**
	 * Initialises a timeline with a structure<br>
	 * The structure is checked for sanity<br>
	 * <ul>
	 * <li>must have 3 picks per team
	 * <li>must have at most 3 bans per team
	 * <li>must have equal number of bans for each team
	 * </ul>
	 *
	 * @param init
	 *            varargs list of phase ids
	 */
	public Timeline(TimelineInit... init) {
		if (init.length >= 6 && init.length <= 12 && init.length % 2 == 0) {
			this.items = new ArrayList<TimelineItem>(12);
			int redPicks = 0;
			int redBans = 0;
			int bluePicks = 0;
			int blueBans = 0;
			for (TimelineInit i : init) {
				switch (i) {
				case RED_PICK:
					this.items.add(new TimelineItem(Team.RED, PICK));
					redPicks++;
					break;
				case RED_BAN:
					this.items.add(new TimelineItem(Team.RED, BAN));
					redBans++;
					break;
				case BLUE_PICK:
					this.items.add(new TimelineItem(Team.BLUE, PICK));
					bluePicks++;
					break;
				case BLUE_BAN:
					this.items.add(new TimelineItem(Team.BLUE, BAN));
					blueBans++;
					break;
				}
			}
			if (redPicks == 3 && bluePicks == 3 && redBans == blueBans && redBans <= 3 && blueBans <= 3) {
				return;
			}
		}
		throw new RuntimeException("Invalid timeline settings!");
	}

	/**
	 * Create a new timeline with a pre-existing structure
	 *
	 * @param pos
	 *            The initial phase of the timeline
	 * @param redBonus
	 *            The remaining bonus time of the red team
	 * @param blueBonus
	 *            The remaining bonus time of the blue team
	 * @param items
	 *            The List of phases (TimelineItems) that the timeline needs to go through or has already gone through
	 */
	public Timeline(byte pos, byte redBonus, byte blueBonus, List<TimelineItem> items) {
		this.pos = pos;
		this.redBonus = redBonus;
		this.blueBonus = blueBonus;
		this.items = new ArrayList<>(items);
	}

	/**
	 * Returns the state to what it was when it was first created
	 */
	public void reset() {
		this.pos = 0;
		this.redBonus = BONUS_LENGTH;
		this.blueBonus = BONUS_LENGTH;
		for (TimelineItem item : this.items) {
			item.charId = 0;
			item.pos = 0;
		}
	}
}
