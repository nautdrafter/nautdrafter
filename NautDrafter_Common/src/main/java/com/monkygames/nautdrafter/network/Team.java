/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network;

import com.monkygames.nautdrafter.network.Player;

/**
 * Describes a team
 */
public class Team {

	/**
	 * The name of the team
	 */
	public String            name;
	/**
	 * The players in the team<br>
	 * A team has 3 players, if a position is not taken the player will be null
	 */
	public final Player[]    players        = new Player[3];
	/**
	 * Whether the team has locked in its player selection (will only be false during TeamSelection)
	 */
	public boolean           lockedIn       = false;
	/**
	 * Whether the team has completely finished (ready for the actual match)
	 */
	public boolean           finished       = false;
	/**
	 * The nauts the team has picked<br>
	 * A team has 3 naut picks, initial value (for not picked yet) is -1
	 */
	public final byte[]      pickedNauts    = new byte[] { -1, -1, -1 };
	/**
	 * The nauts the team has banned<br>
	 * A team has at most 3 naut bans, initial value (for not banned yet) is -1
	 */
	public final byte[]      bannedNauts    = new byte[] { -1, -1, -1 };
	/**
	 * The positions the players have chosen
	 */
	public final Player[]    nautSelections = new Player[3];

	/**
	 * Constant for no team
	 */
	public static final byte NONE           = 0;
	/**
	 * Constant for red team
	 */
	public static final byte RED            = 1;
	/**
	 * Constant for blue team
	 */
	public static final byte BLUE           = 2;
	/**
	 * Constant for position captain
	 */
	public static final byte CAPTAIN        = 0;
	/**
	 * Constant for position crew 1
	 */
	public static final byte CREW_1         = 1;
	/**
	 * Constant for position crew 2
	 */
	public static final byte CREW_2         = 2;

	/**
	 * Constructs a new Team with no name, no players, no picks, no bans
	 */
	public Team() {
	}

	/**
	 * Constructs a new Team with a name, but no players, picks, or bans
	 *
	 * @param name
	 *            The team name
	 */
	public Team(String name) {
		this.name = name;
	}

	/**
	 * Constructs a new Team with everything defined (used for decoding a Team from a packet)
	 *
	 * @param name
	 *            The team name
	 * @param players
	 *            The players in the team
	 * @param picks
	 *            The nauts picked by the team
	 * @param bans
	 *            The nauts banned by the team
	 * @param nautSelections
	 *            The indices of the nauts selected by the players
	 * @param lockedIn
	 *            Whether the team has locked in its players
	 * @param finished
	 *            Whether the team has finished all of drafting
	 */
	public Team(String name, Player[] players, byte[] picks, byte[] bans, Player[] nautSelections, boolean lockedIn,
	        boolean finished) {
		this.name = name;
		for (int i = 0; i < 3; i++) {
			this.players[i] = players[i];
			if (picks.length > i && picks[i] != -1) {
				this.addPick(picks[i]);
			}
			if (bans.length > i && bans[i] != -1) {
				this.addBan(bans[i]);
			}
			this.nautSelections[i] = nautSelections[i];
		}
		this.lockedIn = lockedIn;
		this.finished = finished;
	}

	/**
	 * Constructs a new Team with players
	 *
	 * @param name
	 *            The team name
	 * @param captain
	 *            The team captain (if any)
	 * @param player1
	 *            The first crew member (if any)
	 * @param player2
	 *            The second crew member (if any)
	 */
	public Team(String name, Player captain, Player player1, Player player2) {
		this.name = name;
		this.players[0] = captain;
		this.players[1] = player1;
		this.players[2] = player2;
	}

	/**
	 * Removes a player from any number of teams
	 *
	 * @param player
	 *            The player
	 * @param teams
	 *            The teams
	 * @return true if the player was removed
	 */
	public static boolean removePlayer(Player player, Team... teams) {
		boolean ret = false;
		for (int i = 0; i < 3; i++) {
			for (Team team : teams) {
				if (team.players[i] != null && team.players[i].equals(player)) {
					team.players[i] = null;
					ret = true;
				}
			}
		}
		return ret;
	}

	private int picks = 0;
	private int bans  = 0;

	/**
	 * Adds a naut to the picked nauts, and increments the index for the next pick
	 *
	 * @param nautId
	 */
	public void addPick(byte nautId) {
		this.pickedNauts[this.picks++] = nautId;
	}

	/**
	 * Adds a naut to the banned nauts, and increments the index for the next ban
	 *
	 * @param nautId
	 */
	public void addBan(byte nautId) {
		this.bannedNauts[this.bans++] = nautId;
	}

	/**
	 * Check if the team has the given player
	 *
	 * @param player
	 *            The player to look for
	 * @return true if the given player is in the team
	 */
	public boolean hasPlayer(Player player) {
		if (player == null) {
			return false;
		}
		for (Player p : this.players) {
			if (p != null && p.equals(player)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Check if the given player is captain of this team
	 * 
	 * @param player
	 *            The player to check
	 * @return true if the given player is our captain
	 */
	public boolean isCaptain(Player player) {
		return this.players[0] != null && this.players[0].equals(player);
	}

	/**
	 * Attempts to select a naut
	 *
	 * @param player
	 *            Player who selected the naut
	 * @param nautIndex
	 *            Naut that was selected
	 * @return true if the selection is successful
	 */
	public boolean selectNaut(Player player, int nautIndex) {
		// check parameters are valid
		if (!this.hasPlayer(player) || nautIndex < -1 || nautIndex >= 3) {
			return false;
		}

		// -1 means deselect the player so skip this part
		if (nautIndex != -1) {

			// check selection is available
			Player existing = this.nautSelections[nautIndex];
			if (existing != null && !existing.equals(player)) {
				return false;
			}
		}

		// remove player from old position
		for (int i = 0; i < 3; i++) {
			if (this.nautSelections[i] != null && this.nautSelections[i].equals(player)) {
				this.nautSelections[i] = null;
			}
		}

		// Also skip this
		if (nautIndex != -1) {

			// place in new position
			this.nautSelections[nautIndex] = player;
		}
		return true;
	}

	/**
	 * Reset the team to its initial state
	 * 
	 * @param string
	 *            the starting name for the team
	 */
	public void reset(String startName) {
		this.name = startName;
		this.lockedIn = false;
		this.finished = false;
		for (int i = 0; i < 3; i++) {
			this.players[i] = this.nautSelections[i] = null;
			this.pickedNauts[i] = this.bannedNauts[i] = -1;
		}
		this.picks = this.bans = 0;
	}
}
