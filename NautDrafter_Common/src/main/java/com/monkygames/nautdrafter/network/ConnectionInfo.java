/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network;

import java.nio.ByteBuffer;

import com.monkygames.nautdrafter.network.packets.Packet;

/**
 * Stores information about a connected client.
 */
public class ConnectionInfo {
	/**
	 * Buffer storing data that has been received but not yet handled
	 */
	public final ByteBuffer recvBuffer;
	/**
	 * Buffer storing data that has been queued for sending but not yet sent
	 */
	public final ByteBuffer sendBuffer;
	/**
	 * The packet we are currently decoding (will be handled once we receieve enough data to decode it fully)
	 */
	public Packet           decoding = null;
	/**
	 * Whether to kill this connection after sending all the data we have to send
	 */
	public boolean          kill     = false;

	/**
	 * Creates a new ConnectionInfo and allocates buffer space for it
	 */
	public ConnectionInfo() {
		this.recvBuffer = ByteBuffer.allocate(Server.BUFFER_SIZE);
		this.sendBuffer = ByteBuffer.allocate(Server.BUFFER_SIZE);
	}

	/**
	 * Shallow-copies a ConnectionInfo. Used for "upgrading" a ConnectionInfo to a subclass while retaining all its data
	 *
	 * @param shallowCopy
	 *            The ConnectionInfo to copy state from
	 */
	protected ConnectionInfo(ConnectionInfo shallowCopy) {
		this.recvBuffer = shallowCopy.recvBuffer;
		this.sendBuffer = shallowCopy.sendBuffer;
		this.decoding = shallowCopy.decoding;
		this.kill = shallowCopy.kill;
	}
}
