/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.CreateServerInfo;
import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.callbacks.DedicatedServerRequestCallback;
import com.monkygames.nautdrafter.network.packets.segments.CreateServerInfoSegment;
import com.monkygames.nautdrafter.network.packets.segments.PlayerSegment;
import com.monkygames.nautdrafter.network.packets.segments.Segment;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Used by the client to send a request to the lobby server to create a 
 * dedicated server.
 */
public class DedicatedServerRequestPacket extends Packet {

    private CreateServerInfo                createServerInfo;
    private Player                          player;
    private DedicatedServerRequestCallback  receiver;

    /**
     * Encodes a ServerInfo to a byte[] to send over the network
     *
     * @param createServerInfo contains the details of the drafting server to create.
     * @param player the player who is making the request.
     * @throws PacketEncoderException
     */
    public DedicatedServerRequestPacket(
        CreateServerInfo createServerInfo,
        Player player
    ) throws PacketEncoderException {
        this.data = Segment.concat(
            Packet.DEDICATED_SERVER_REQUEST_PACKET_ID, 
            CreateServerInfoSegment.encode(createServerInfo),
            PlayerSegment.encode(player)
        );
    }

    /**
     * Creates a new ServerAnnouncePacket
     *
     * @param data
     *            The raw data to decode
     * @param receiver
     *            The callback to call to handle this packet
     * @throws PacketDecoderException
     */
    public DedicatedServerRequestPacket(byte[] data, DedicatedServerRequestCallback receiver)
        throws PacketDecoderException {
        super(data);
        this.receiver = receiver;
    }

    @Override
    protected boolean decodeImpl() throws PacketDecoderException {
        if (this.createServerInfo == null) {
            if (!CreateServerInfoSegment.canDecode(this, false)) {
                return false;
            }
            this.createServerInfo = CreateServerInfoSegment.decode(this);
            if (!PlayerSegment.canDecode(this)) {
                return false;
            }
            this.player = PlayerSegment.decode(this);
        }
        return true;
    }

    @Override
    protected void handleImpl() {
        int ret = this.receiver.onDedicatedServerRequestReceived(createServerInfo,player);
        try {
            this.response = new DedicatedServerResponsePacket(ret);
        } catch (PacketEncoderException ex) {
            Logger.getLogger(DedicatedServerRequestPacket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
