/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets.segments;

import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.PlayerIdentifier;
import com.monkygames.nautdrafter.network.packets.Packet;
import com.monkygames.nautdrafter.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.network.packets.PacketEncoderException;

/**
 * Represents a player as an array of bytes (composed of IntSegment and StringSegment)
 */
public class PlayerSegment {

	/**
	 * Encodes a player
	 *
	 * @param player
	 * @return Encoded data
	 * @throws com.monkygames.nautdrafter.network.packets.PacketEncoderException
	 */
	public static byte[] encode(Player player) throws PacketEncoderException {
		return Segment.concat(PlayerIdentifierSegment.encode(player), StringSegment.encode(player.name, 1, true),
		        StringSegment.encode(player.iconUrl, 2, true), IntSegment.encode(player.tieBreak, 8));
	}

	/**
	 * Decodes a player from the packet
	 *
	 * @param packet
	 *            The Packet to decode from (data and offset)
	 * @return
	 * @throws PacketDecoderException
	 */
	public static Player decode(Packet packet) throws PacketDecoderException {
		PlayerIdentifier pid = PlayerIdentifierSegment.decode(packet);
		String name = StringSegment.decode(packet, 1, true);
		String iconUrl = StringSegment.decode(packet, 2, true);
		long tieBreak = IntSegment.decode(packet, 8);
		return new Player(pid, name, iconUrl, tieBreak);
	}

	/**
	 * Check if we have enough data to decode a Player
	 *
	 * @param packet
	 *            The Packet to decode from (data and offset)
	 * @param advancePos
	 *            Whether to advance the position of the Packet
	 * @return true if we can decode a Player
	 */
	static boolean canDecode(Packet packet, boolean advancePos) {
		int pos = packet.pos;
		boolean can = (PlayerIdentifierSegment.canDecode(packet, true)
		        && StringSegment.canDecode(packet, 1, true, true) && StringSegment.canDecode(packet, 2, true, true) && IntSegment
		        .canDecode(packet, 8, true));
		if (!advancePos) {
			// if we're not advancing the position, restore to original position
			packet.pos = pos;
		}
		return can;
	}

	/**
	 * Check if we have enough data to decode a Player
	 *
	 * @param packet
	 *            The Packet to decode from (data and offset)
	 * @return true if we can decode a Player
	 */
	public static boolean canDecode(Packet packet) {
		return PlayerSegment.canDecode(packet, false);
	}
}
