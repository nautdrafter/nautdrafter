/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network;

/**
 * Describes a NautDrafter player
 */
public class Player extends PlayerIdentifier {

	/**
	 * The player's display name
	 */
	public final String name;
	/**
	 * The player's display icon
	 */
	public final String iconUrl;
	/**
	 * The player's score (used for captain disputes)
	 */
	public final long   tieBreak;

	/**
	 * @param steam
	 *            Whether the player logged in with Steam
	 * @param id
	 *            ID of the player
	 * @param name
	 *            Display name for the player
	 * @param iconUrl
	 *            URL for the player's avatar
	 * @param tieBreak
	 *            Value used as an indication of the player's AwesomeNauts skill level. A higher value indicates a more
	 *            skilled player.
	 */
	public Player(boolean steam, long id, String name, String iconUrl, long tieBreak) {
		super(steam, id);
		this.name = name;
		this.iconUrl = iconUrl;
		this.tieBreak = tieBreak;
	}

	/**
	 * @param pid
	 *            ID of the player
	 * @param name
	 *            Display name for the player
	 * @param iconUrl
	 *            URL for the player's avatar
	 * @param tieBreak
	 *            Value used as an indication of the player's AwesomeNauts skill level. A higher value indicates a more
	 *            skilled player.
	 */
	public Player(PlayerIdentifier pid, String name, String iconUrl, long tieBreak) {
		super(pid);
		this.name = name;
		this.iconUrl = iconUrl;
		this.tieBreak = tieBreak;
	}

	@Override
	public String toString() {
		return this.name + "[" + super.toString() + "]";
	}
}
