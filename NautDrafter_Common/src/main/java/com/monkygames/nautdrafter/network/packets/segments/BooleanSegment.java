/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets.segments;

import com.monkygames.nautdrafter.network.packets.Packet;
import com.monkygames.nautdrafter.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.network.packets.PacketEncoderException;

/**
 * Represents an integer as an array of bytes
 */
public class BooleanSegment{

    /**
     * Create an BooleanSegment of fixed length of 1 byte
     *
     */
    public BooleanSegment() { }

    /**
     * Encodes an integer.
     *
     * @param val
     *            integer to encode
     * @return Encoded data
     * @throws PacketEncoderException
     */
    public static byte[] encode(boolean val) throws PacketEncoderException {
        return IntSegment.encode((val ? 1:0),1);
    }

    /**
     * Decodes an integer from the packet
     *
     * @param packet
     *            The raw packet
     * @return The decoded boolean
     * @throws PacketDecoderException
     */
    public static boolean decode(Packet packet) throws PacketDecoderException {
        long val = IntSegment.decode(packet, 1);
        return (val == 1);
    }

    /**
     * Check if we have enough data to decode an integer
     *
     * @param packet
     *            The Packet to decode from (data and offset)
     * @param advancePos
     *            Whether to advance the position of the Packet
     * @return true if we can decode an integer
     */
    public static boolean canDecode(Packet packet, int length, boolean advancePos) {
        if (packet.available() < length) {
                return false;
        }
        if (advancePos) {
                packet.pos += length;
        }
        return true;
    }
}
