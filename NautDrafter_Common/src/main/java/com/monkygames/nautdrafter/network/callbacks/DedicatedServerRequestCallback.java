/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.callbacks;

import com.monkygames.nautdrafter.network.CreateServerInfo;
import com.monkygames.nautdrafter.network.Player;

/**
 * Callback to the client indicating if the server request was a success.
 */
public interface DedicatedServerRequestCallback {

    public static final int RESPONSE_SUCCESS                = 0;
    public static final int RESPONSE_FAILED_NO_STEAM        = 1;
    public static final int RESPONSE_FAILED_NO_SERVERS      = 2;
    public static final int RESPONSE_FAILED_EXISTING_SERVER = 3;

    /**
     * Callback for version number being received
     * @param createServerInfo the server information to create a dedicated server.
     * @param player the player information who is requesting the dedicated server.
     * @return the responses of the request which are listed above.
     */
    public int onDedicatedServerRequestReceived(
        CreateServerInfo createServerInfo,
        Player player);
}
