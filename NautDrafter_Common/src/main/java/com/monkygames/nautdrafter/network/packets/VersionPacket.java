/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.callbacks.VersionCallback;
import com.monkygames.nautdrafter.network.packets.segments.IntSegment;
import com.monkygames.nautdrafter.network.packets.segments.Segment;

/**
 * Used for communicating the version number between client/server
 */
public class VersionPacket extends Packet {

	private static final IntSegment versionSegment = new IntSegment(4);

	private VersionCallback         receiver;
	private Integer                 major, minor, patch;

	/**
	 * Constructs a new VersionPacket for sending
	 * 
	 * @param major
	 *            The major version number
	 * @param minor
	 *            The minor version number
	 * @param patch
	 *            The patch version number
	 * @throws PacketEncoderException
	 */
	public VersionPacket(int major, int minor, int patch) throws PacketEncoderException {
		this.data = Segment.concat(Packet.VERSION_PACKET_ID, versionSegment.encode(major),
		        versionSegment.encode(minor), versionSegment.encode(patch));
	}

	/**
	 * Creates a new VersionPacket for decoding
	 * 
	 * @param data
	 *            The data to decode
	 */
	public VersionPacket(byte[] data, VersionCallback receiver) {
		super(data);
		this.receiver = receiver;
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		if (this.major == null) {
			if (!versionSegment.canDecode(this)) {
				return false;
			}
			this.major = (int) versionSegment.decode(this);
		}
		if (this.minor == null) {
			if (!versionSegment.canDecode(this)) {
				return false;
			}
			this.minor = (int) versionSegment.decode(this);
		}
		if (this.patch == null) {
			if (!versionSegment.canDecode(this)) {
				return false;
			}
			this.patch = (int) versionSegment.decode(this);
		}
		return true;
	}

	@Override
	protected void handleImpl() {
		this.receiver.onVersionReceived(this.major, this.minor, this.patch);
	}
}
