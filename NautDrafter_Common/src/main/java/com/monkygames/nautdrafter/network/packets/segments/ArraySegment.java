/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets.segments;

import com.monkygames.nautdrafter.network.packets.Packet;
import com.monkygames.nautdrafter.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.network.packets.PacketEncoderException;
import java.util.ArrayList;

/**
 * Represents an integer array arrays of bytes
 */
public class ArraySegment{

    public ArraySegment() { }

    /**
     * Encodes an integer.
     *
     * @param val
     *            integer to encode
     * @return Encoded data
     * @throws PacketEncoderException
     */
    static byte[] encode(int array[]) throws PacketEncoderException {
        ArrayList<Byte> alist = new ArrayList<>();
        for(int i = 0; i < array.length; i++){
            alist.addAll(encodeInt(array[i]));
        }
        //return IntSegment.encode((val ? 1:0),1);
        byte outlist[] = new byte[alist.size()+1];
        // first element in array is the size
        outlist[0] = (byte)alist.size();
        int i = 1;
        for(Byte data: alist){
            outlist[i++] = data.byteValue();
        }
        return outlist;
    }

    /**
     * Decodes an integer from the packet
     *
     * @param packet
     *            The raw packet
     * @return The decoded boolean
     * @throws PacketDecoderException
     */
    static int[] decode(Packet packet) throws PacketDecoderException {
        int size = (int)IntSegment.decode(packet, 1);
        int arrayI[] = new int[size];
        for(int i = 0; i < size; i++){
            arrayI[i] = (int)IntSegment.decode(packet,1);
        }
        return arrayI;
    }

    /**
     * Check if we have enough data to decode an integer
     *
     * @param packet
     *            The Packet to decode from (data and offset)
     * @param advancePos
     *            Whether to advance the position of the Packet
     * @return true if we can decode an integer
     */
    static boolean canDecode(Packet packet, int length, boolean advancePos) {
        /*
        if (packet.available() < length) {
                return false;
        }
        if (advancePos) {
                packet.pos += length;
        }
        */
        return true;
    }

    /**
     * Encodes an integer to an ArrayList of Bytes.
     */
    static ArrayList<Byte> encodeInt(int data) throws PacketEncoderException{
        ArrayList<Byte> barray = new ArrayList<>();
        byte bb[] = IntSegment.encode(data,1);
        for(int i = 0; i < bb.length; i++){
            barray.add(bb[i]);
        }
        return barray;
    }
}
