/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets.segments;

import com.monkygames.nautdrafter.network.packets.Packet;
import com.monkygames.nautdrafter.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.network.packets.PacketEncoderException;

/**
 * Represents a string as an array of bytes, optionally with a header containing the length
 */
public class StringSegment {

	/**
	 * Number of bytes used to store the data (or to store the length if it is variable)
	 */
	public final int     lengthBytes;
	/**
	 * Whether the length is variable
	 */
	public final boolean variableLength;

	/**
	 * Create a StringSegment, optionally of variable length
	 *
	 * @param lengthBytes
	 *            the length in bytes if not variable, or the number of bytes used to store the length
	 * @param variableLength
	 *            whether the length is variable
	 */
	public StringSegment(int lengthBytes, boolean variableLength) {
		this.lengthBytes = lengthBytes;
		this.variableLength = variableLength;
	}

	/**
	 * Create a StringSegment of fixed length in bytes
	 *
	 * @param lengthBytes
	 */
	public StringSegment(int lengthBytes) {
		this(lengthBytes, false);
	}

	/**
	 * Encodes a string
	 *
	 * @param str
	 *            string to encode
	 * @param length
	 *            length in bytes, or number of bytes for storing the length if variable
	 * @param variableLength
	 *            whether the length is variable
	 * @return Encoded data
	 * @throws PacketEncoderException
	 */
	static byte[] encode(String str, int length, boolean variableLength) throws PacketEncoderException {
		return Segment.encode(str.getBytes(Packet.charset), length, variableLength);
	}

	/**
	 * Decodes a String from the packet
	 *
	 * @param packet
	 *            The raw packet
	 * @param length
	 * @param variableLength
	 * @return The decoded string
	 * @throws PacketDecoderException
	 */
	static String decode(Packet packet, int length, boolean variableLength) throws PacketDecoderException {
		int offset = packet.pos;
		byte[] data = packet.data;
		if (variableLength) {
			int len = Segment.decodeVariableLength(data, offset, length);
			offset += length;
			length = len;
		}
		if (data.length - offset < length) {
			throw new PacketDecoderException("Invalid packet length");
		}
		packet.pos = offset + length;
		return new String(data, offset, length, Packet.charset);
	}

	/**
	 * Check if we have enough data to decode
	 *
	 * @param packet
	 *            The Packet to decode from (data and offset)
	 * @param length
	 *            number of bytes of data, or number of bytes of the length field (if variableLength)
	 * @param variableLength
	 *            true if the data includes a length header
	 * @param advancePos
	 *            Whether to advance the position of the Packet
	 * @return true if we can decode
	 */
	static boolean canDecode(Packet packet, int length, boolean variableLength, boolean advancePos) {
		return Segment.canDecode(packet, length, variableLength, advancePos);
	}

	/**
	 * Encodes a String
	 *
	 * @param val
	 *            The String to encode
	 * @return The encoded data
	 * @throws PacketDecoderException
	 */
	public byte[] encode(String val) throws PacketEncoderException {
		return StringSegment.encode(val, this.lengthBytes, this.variableLength);
	}

	/**
	 * Decodes some raw data with length depending on this segment's settings
	 *
	 * @param packet
	 *            The Packet to decode from (data and offset)
	 * @return The raw data
	 * @throws PacketDecoderException
	 */
	public String decode(Packet packet) throws PacketDecoderException {
		return StringSegment.decode(packet, this.lengthBytes, this.variableLength);
	}

	/**
	 * Checks if there is enough data to decode with length depending on this segment's settings
	 *
	 * @param packet
	 *            The Packet to decode from (data and offset)
	 * @return true if we can decode
	 */
	public boolean canDecode(Packet packet) {
		return StringSegment.canDecode(packet, this.lengthBytes, this.variableLength, false);
	}
}
