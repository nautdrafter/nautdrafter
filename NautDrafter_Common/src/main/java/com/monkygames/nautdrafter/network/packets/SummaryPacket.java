/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.network.callbacks.SaveSummaryCallback;
import com.monkygames.nautdrafter.network.packets.segments.IntSegment;
import com.monkygames.nautdrafter.network.packets.segments.Segment;
import com.monkygames.nautdrafter.network.packets.segments.StringSegment;

/**
 * Used for sending a game summary to the server
 */
public class SummaryPacket extends Packet {

    public static final byte           PACKET_ID       = Packet.SAVE_SUMMARY_PACKET_ID;

    private static final IntSegment isRandomMapSegment     = new IntSegment(1);
    private static final StringSegment summarySegment       = new StringSegment(4, true);
    private static final StringSegment redTeamNameSegment   = new StringSegment(1, true);
    private static final StringSegment blueTeamNameSegment  = new StringSegment(1, true);
    private static final StringSegment mapNameSegment       = new StringSegment(1, true);

    // TODO Add is_random to packet as this is important

    private String                     summary        = null;
    private String                     redTeamName    = null;
    private String                     blueTeamName   = null;
    private String                     mapName        = null;
    private boolean                    isRandomMap    = false;

    private SaveSummaryCallback receiver;

    /**
     * Creates a new PasswordPacket for decoding
     *
     * @param data
     *            The packet data
     * @param receiver
     * @throws PacketDecoderException
     */
    public SummaryPacket(byte[] data, SaveSummaryCallback receiver) throws PacketDecoderException {
        super(data);
        this.receiver = receiver;
    }

    /**
     * Constructs a new PasswordPacket for sending
     *
     * @param summary
     * @param redTeamName
     * @param blueTeamName
     * @param mapName
     * @param isRandomMap
     * @throws PacketEncoderException
     */
    public SummaryPacket(String summary, String redTeamName, String blueTeamName, String mapName, boolean isRandomMap) throws PacketEncoderException {
        this.summary = summary;
        this.redTeamName = redTeamName;
        this.blueTeamName = blueTeamName;
        this.mapName = mapName;
        this.isRandomMap = isRandomMap;
        byte isRandomMapB = (isRandomMap) ? (byte)0b01 : (byte)0b00;
        this.data = Segment.concat(PACKET_ID, 
            isRandomMapB,
            summarySegment.encode(this.summary),
            redTeamNameSegment.encode(this.redTeamName),
            blueTeamNameSegment.encode(this.blueTeamName),
            mapNameSegment.encode(this.mapName));
    }

    @Override
    protected boolean decodeImpl() throws PacketDecoderException {
        if(!isRandomMapSegment.canDecode(this)){
            return false;
        }
        int isRandomMapB = (int)isRandomMapSegment.decode(this);
        this.isRandomMap = (isRandomMapB == 1) ? true : false;


        if (this.summary == null) {
            if (!summarySegment.canDecode(this)) {
                    return false;
            }
            this.summary = summarySegment.decode(this);
        }
        if (this.redTeamName == null){
            if (!redTeamNameSegment.canDecode(this)) {
                    return false;
            }
            this.redTeamName = redTeamNameSegment.decode(this);
        }
        if (this.blueTeamName == null){
            if (!blueTeamNameSegment.canDecode(this)) {
                    return false;
            }
            this.blueTeamName = blueTeamNameSegment.decode(this);
        }
        if (this.mapName == null){
            if (!mapNameSegment.canDecode(this)) {
                    return false;
            }
            this.mapName = mapNameSegment.decode(this);
        }
        return true;
    }

    @Override
    protected void handleImpl() {
        this.receiver.saveSummaryToDisk(summary,redTeamName,blueTeamName,mapName,isRandomMap);
    }
}