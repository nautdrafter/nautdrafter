/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network;

/**
 * The unique data that identifies a player
 */
public class PlayerIdentifier {

	/**
	 * Did this player logged in with their Steam account
	 */
	private final boolean steam;
	/**
	 * The unique id of the player
	 */
	private final long    id;

	/**
	 * Constructs a new PlayerIdentifier
	 * 
	 * @param steam
	 *            Whether the player logged in with Steam
	 * @param id
	 *            ID of the player
	 */
	public PlayerIdentifier(boolean steam, long id) {
		this.steam = steam;
		this.id = id;
	}

	/**
	 * Copies a PlayerIdentifier
	 * 
	 * @param copy
	 *            The PlayerIdentifier to copy
	 */
	public PlayerIdentifier(PlayerIdentifier copy) {
		this.steam = copy.steam;
		this.id = copy.id;
	}

	/**
	 * Check whether two Player instances represent the same Player
	 * 
	 * @return true if both players have the same id, and are both in the same id-space (steam or non-steam)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof PlayerIdentifier) {
			PlayerIdentifier other = (PlayerIdentifier) obj;
			return this.steam == other.steam && this.id == other.id;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Long.hashCode(this.id) ^ Boolean.hashCode(this.steam);
	}

	/**
	 * @return Whether the player logged in using Steam
	 */
	public boolean isSteam() {
		return this.steam;
	}

	/**
	 * Get the player id (should only be used for networking)
	 * 
	 * @return this player's id
	 */
	public long getId() {
		return this.id;
	};

	@Override
	public String toString() {
		return (this.steam ? "steam" : "offline") + "#" + this.id;
	}
}
