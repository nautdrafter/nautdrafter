/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.resource;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Manages language resource which is a Singleton Class.
 */
public class LanguageResource {

    /**
     * Singleton.
     */
    private static LanguageResource languageResource;
    private ResourceBundle resource;


    /**
     * Singleton that sets up resource bundles.
     */
    private LanguageResource(){
	resource = ResourceBundle.getBundle("bundles.Strings");
    }

    // === Public Methods === //

    /**
     * Reload the languages bundles with the specified language code.
     * @param countryCode the country code to use.
     */
    public void reload(String countryCode){
	resource = ResourceBundle.getBundle("bundles.Strings",Locale.forLanguageTag(countryCode));
    }
    /**
     * Return the string from the resource.
     * @param key the key to get the value.
     * @return the value in the resource bundle that matches the key.
     */
    public static String getString(String key){
        return LanguageResource.get().resource.getString(key);
    }

    /**
     * Return the resource bundle.
     * @return the resource bundle.
     */
    public static ResourceBundle getLanguageResource(){
        return LanguageResource.get().resource;
    }

    /**
     * Singleton to ensure that an object is returned.
     */
    public static LanguageResource get(){
	if(LanguageResource.languageResource == null){
	    languageResource = new LanguageResource();
	}
	return languageResource;
    }

    // === private methods === //

}
