/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.resource;

import java.util.HashMap;

import javafx.scene.image.Image;

import com.monkygames.nautdrafter.network.PlayerIdentifier;

/**
 *
 * @author Adam
 */
public class ImageCacher {

	private static HashMap<String, Image> map;

	static {
		ImageCacher.map = new HashMap<>();
		ImageCacher.map.put("default", new Image("assets/images/default.png"));
	}

	/**
	 * Return an Image from either a hard coded id or url
	 *
	 * @param id
	 *            Hard coded id or url of where the image is
	 * @return an Image object or default image if invalid id
	 */
	public static Image get(String id) {
		if (id.equals("empty")) {
			return ImageCacher.getAndPutIfNotThere("assets/images/icons/empty-icon.png");
		}

		// Default to url type id
		return ImageCacher.getAndPutIfNotThere(id);
	}

	private static Image getAndPutIfNotThere(String url) {
		Image i;
		if ((i = ImageCacher.map.get(url)) == null) {
			try {
				i = new Image(url,true);
			} catch (IllegalArgumentException e) {
				return ImageCacher.map.get("default");
			}
			ImageCacher.map.put(url, i);
		}
		return i;
	}

	/**
	 * Try to get the Icon associated with a users steamId Put a mapping into the ImageCacher using
	 * ImageCacher.putSteamIcon(long steamId, String url) before using this
	 *
	 * @param steamId
	 * @return The users icon if valid else default image
	 */
	public static Image get(PlayerIdentifier pid) {
		Image i;
		if ((i = ImageCacher.map.get(pid.toString())) == null) {
			return ImageCacher.map.get("default");
		}
		return i;
	}

	/**
	 * Add a mapping from the Player id to the players icon
	 *
	 * @param pid
	 *            Player id
	 * @param url
	 *            players icon url
	 */
	public static void putPlayerIcon(PlayerIdentifier pid, String url) {
		ImageCacher.map.put(pid.toString(), ImageCacher.getAndPutIfNotThere(url));
	}
}
