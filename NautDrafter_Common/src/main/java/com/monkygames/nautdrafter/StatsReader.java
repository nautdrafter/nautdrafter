/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter;

import com.monkygames.nautdrafter.model.Stats;
import com.monkygames.nautdrafter.model.Summary;
import java.io.File;
import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


/**
 * Reads the summaries from a directory and produces stats.
 */
public class StatsReader extends Application{

    public static String summaryPath;
    public static String titleName;
    public static Stats stats;

    public void start (Stage stage){
        stage.setTitle(titleName);
        displayPicks(stage);

        stage = new Stage();
        stage.setTitle(titleName);
        stage.show();
        displayBans(stage);

        stage = new Stage();
        stage.setTitle(titleName);
        stage.show();
        displayBoth(stage);
    }
    private void displayPicks(Stage stage){

        final NumberAxis xAxis = new NumberAxis();
        final CategoryAxis yAxis = new CategoryAxis();
        final BarChart<Number,String> bc = new BarChart<Number,String>(xAxis,yAxis);
        bc.setStyle("-fx-background-color: rgba(255, 255, 255, 1);");
        xAxis.setTickLabelRotation(90);
        yAxis.setLabel("Nauts");

        XYChart.Series pickSeries = new XYChart.Series();
        pickSeries.setName("Picks");
        XYChart.Series banSeries = new XYChart.Series();
        banSeries.setName("Bans");

        for(Stats.CharacterStats stats: stats.getPickSorted()){
            pickSeries.getData().add(new XYChart.Data(stats.picks,stats.character.name));
            banSeries.getData().add(new XYChart.Data(stats.bans,stats.character.name));
        }
        Scene scene = new Scene(bc,800,600, Color.WHITE);
        bc.getData().addAll(pickSeries);

        for (Node n : bc.lookupAll(".default-color0.chart-bar")) {
            n.setStyle("-fx-bar-fill: green;");
        }
        stage.setScene(scene);
        stage.show();
        for (Node n : bc.lookupAll(".bar-legend-symbol.default-color0")) {
            n.setStyle("-fx-background-color: " + "green" + ";");
        }
    }
    private void displayBans(Stage stage){

        final NumberAxis xAxis = new NumberAxis();
        final CategoryAxis yAxis = new CategoryAxis();
        final BarChart<Number,String> bc = new BarChart<Number,String>(xAxis,yAxis);
        bc.setStyle("-fx-background-color: rgba(255, 255, 255, 1);");
        xAxis.setTickLabelRotation(90);
        yAxis.setLabel("Nauts");

        XYChart.Series pickSeries = new XYChart.Series();
        pickSeries.setName("Picks");
        XYChart.Series banSeries = new XYChart.Series();
        banSeries.setName("Bans");

        for(Stats.CharacterStats stats: stats.getBanSorted()){
            pickSeries.getData().add(new XYChart.Data(stats.picks,stats.character.name));
            banSeries.getData().add(new XYChart.Data(stats.bans,stats.character.name));
        }
        Scene scene = new Scene(bc,800,600, Color.WHITE);
        bc.getData().addAll(banSeries);

        for (Node n : bc.lookupAll(".default-color0.chart-bar")) {
            n.setStyle("-fx-bar-fill: red;");
        }
        stage.setScene(scene);
        stage.show();
        for (Node n : bc.lookupAll(".bar-legend-symbol.default-color0")) {
            n.setStyle("-fx-background-color: " + "red" + ";");
        }
    }
    private void displayBoth(Stage stage){

        // picks
        final NumberAxis xAxis = new NumberAxis();
        final CategoryAxis yAxis = new CategoryAxis();
        final BarChart<Number,String> bc = new BarChart<Number,String>(xAxis,yAxis);
        bc.setStyle("-fx-background-color: rgba(255, 255, 255, 1);");
        xAxis.setTickLabelRotation(90);
        yAxis.setLabel("Nauts");

        XYChart.Series pickSeries = new XYChart.Series();
        pickSeries.setName("Picks");
        XYChart.Series banSeries = new XYChart.Series();
        banSeries.setName("Bans");

        for(Stats.CharacterStats stats: stats.characterStats){
            pickSeries.getData().add(new XYChart.Data(stats.picks,stats.character.name));
            banSeries.getData().add(new XYChart.Data(stats.bans,stats.character.name));
        }
        Scene scene = new Scene(bc,800,600, Color.WHITE);
        bc.getData().addAll(pickSeries,banSeries);

        for (Node n : bc.lookupAll(".default-color0.chart-bar")) {
            n.setStyle("-fx-bar-fill: green;");
        }
        //second bar color
        for (Node n : bc.lookupAll(".default-color1.chart-bar")) {
            n.setStyle("-fx-bar-fill: red;");
        }
        stage.setScene(scene);
        stage.show();
        for (Node n : bc.lookupAll(".bar-legend-symbol.default-color0")) {
            n.setStyle("-fx-background-color: " + "green" + ";");
        }
        for (Node n : bc.lookupAll(".bar-legend-symbol.default-color1")) {
            n.setStyle("-fx-background-color: " + "red" + ";");
        }
    }

    public static void main(String []args){
        /*
        if(args.length != 2){
            System.out.println("Usage: StatsReader <name> <path to summary directory>");
            System.exit(1);
        }
        */
        //args = new String[]{"Awesome Cup - Q1","/home/spethm/Work/MonkyGames/projects/nautdrafter/docs/AwesomeCup-2014-2015/Q1" };
        //args = new String[]{"Awesome Cup - Q2","/home/spethm/Work/MonkyGames/projects/nautdrafter/docs/AwesomeCup-2014-2015/Q2" };
        //args = new String[]{"Awesome Cup - Q3","/home/spethm/Work/MonkyGames/projects/nautdrafter/docs/AwesomeCup-2014-2015/Q3" };
        //args = new String[]{"Awesome Cup - Q4","/home/spethm/Work/MonkyGames/projects/nautdrafter/docs/AwesomeCup-2014-2015/Q4" };
        //args = new String[]{"Awesome Cup - Q1,Q2,Q3,Q4","/home/spethm/Work/MonkyGames/projects/nautdrafter/docs/AwesomeCup-2014-2015/all" };
        args = new String[]{"Awesome Cup - Quarter Finals","/home/spethm/Work/MonkyGames/projects/nautdrafter/docs/AwesomeCup-2014-2015/QFinals" };
        StatsReader.titleName = args[0];
        StatsReader.summaryPath = args[1];
        StatsReader.stats = new Stats();
        File dir = new File(StatsReader.summaryPath);
        for(File file: dir.listFiles()){
            Summary.readSummaryFile(StatsReader.stats, file);
        }
        System.out.println("Number of Games: "+dir.listFiles().length);
        launch(args);
    }
    
}
