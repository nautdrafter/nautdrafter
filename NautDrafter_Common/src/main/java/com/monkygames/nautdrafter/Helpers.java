/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Misc functions
 */
public class Helpers {

	/**
	 * List the contents of a directory on the classpath
	 * Taken from http://www.uofr.net/~greg/java/get-resource-listing.html
	 * 
	 * @param clazz
	 *            A class reference to get the classpath
	 * @param path
	 *            The absolute path to the directory to look for
	 * @return The contents of the directory
	 * @throws IOException If we fail to read the resources
	 */
	public static String[] getResources(Class<?> clazz, String path) throws IOException {
		URL dirURL = clazz.getResource(path);
		if (dirURL != null) {
			if (dirURL.getProtocol().equals("file")) {
				return new File(dirURL.getPath()).list();
			}
		} else {
			// In case of a jar file, we can't actually find a directory. Have to assume the same jar as clazz.
			String me = clazz.getName().replace(".", "/") + ".class";
			dirURL = clazz.getClassLoader().getResource(me);
		}
		if (dirURL.getProtocol().equals("jar")) {
			// strip out the jar file
			String jarPath = dirURL.getPath().substring(5, dirURL.getPath().indexOf("!"));
			try (JarFile jar = new JarFile(URLDecoder.decode(jarPath, "UTF-8"))) {
				// gives ALL entries in jar
				Enumeration<JarEntry> entries = jar.entries();
				// avoid duplicates in case it is a subdirectory
				Set<String> result = new HashSet<String>();
				// remove the leading '/' from the path
				path = path.replaceAll("^\\/", "");
				while (entries.hasMoreElements()) {
					String name = entries.nextElement().getName();
					// filter according to the path
					if (name.startsWith(path)) {
						String entry = name.substring(path.length());
						if (entry.length() > 0) {
							int checkSubdir = entry.indexOf("/");
							if (checkSubdir >= 0) {
								// if it is a subdirectory, we just return the directory name
								entry = entry.substring(0, checkSubdir);
							}
							result.add(entry);
						}
					}
				}
				return result.toArray(new String[result.size()]);
			}
		}

		throw new IOException("Cannot list files for URL " + dirURL);
	}
}
