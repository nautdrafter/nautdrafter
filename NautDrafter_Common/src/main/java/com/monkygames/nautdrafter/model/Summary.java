/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.model;

import com.monkygames.nautdrafter.network.Team;
import com.monkygames.nautdrafter.network.Timeline;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;

/**
 * Manages exporting a summary of the game.
 */
public class Summary {
    /**
     * Generate the summary output from the draft in the specified format. Includes team summaries and drafting order
     * 
     * @param toClipboard
     *            True if the summary should be copied to the clipboard
     * @param formatter
     *            The formatter object carrying the information about the format you want to export for.
     * @param redTeam
     * @param blueTeam
     * @param mapId
     * @param timeline
     * @return the string representation of the data
     */
    public static String exportSummary(boolean toClipboard, Formatter formatter, 
        Team redTeam, Team blueTeam, int mapId, Timeline timeline) {

        String output = formatter.format(redTeam, blueTeam,
                Planet.getById(mapId), timeline);

        if(toClipboard){
            final Clipboard clipboard = Clipboard.getSystemClipboard();
            clipboard.clear();
            final ClipboardContent content = new ClipboardContent();
                content.putString(output);
                clipboard.setContent(content);
        }
        System.out.println(output);
        return output;
    }

    /**
     * Generates a file name based on the team names, map, date.
     * @param summary The complete summary content.
     * @param redTeamName the red team name
     * @param blueTeamName the blue team name
     * @param mapName the map name
     * @param isRandomMap true if the map was randomly
     * @return A name that is suitable to be used as the file name.
     */
    public static String getExportFileName(String summary, String redTeamName, 
        String blueTeamName, String mapName, boolean isRandomMap){
        redTeamName = redTeamName.replace(" ", "_");
        blueTeamName = blueTeamName.replace(" ", "_");
        mapName = mapName.replace(" ", "_");
        String randomMap = (isRandomMap) ? "RAND_" : "";

        Date dNow = new Date();
        SimpleDateFormat ft
            = new SimpleDateFormat("yyyy.MM.dd'-'HH:mm:ss-zzz");

        String filename = redTeamName+"-vs-"+blueTeamName+"-"+randomMap+mapName+"-"+ft.format(dNow)+".txt";
        System.out.println("[Summary:getExportFileName] filename = "+filename);
        return filename;
    }

    /**
     * Writes the summary to the disk.
     * @param summary the summary to write.
     * @param file the file to write.
     */
    public static boolean writeSummaryFile(String summary, File file){
        if(file == null){
            return false;
        }
        try {
            PrintWriter pw = new PrintWriter(file);
            pw.write(summary);
            pw.flush();
            pw.close();
        } catch (Exception e1) {
            e1.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Reads the summary and populates the stats.
     * Note, assumes txt file format.
     */
    public static void readSummaryFile(Stats stats, File file){
        try {
            List<String> linesL = Files.readAllLines(file.toPath());
            String tmpS;
            for(int i = 0; i < linesL.size(); i++){
               tmpS = linesL.get(i);
               if(tmpS != null && tmpS.equals("Picks (in order):")){
                   // progress for the next 3 as there will always just be 3
                   stats.addPick(linesL.get(++i).trim());
                   stats.addPick(linesL.get(++i).trim());
                   stats.addPick(linesL.get(++i).trim());
               }
               // get the bans (could be an indeterminate number of bans
               if(tmpS != null && tmpS.equals("Bans (in order):")){
                   for(int j = i+1; j < linesL.size(); j++){
                       tmpS = linesL.get(j);
                       if(tmpS != null && tmpS.equals("\n")){
                           // reached the end
                           i = j;
                           break;
                       }
                       stats.addBan(linesL.get(j).trim());
                   }
               }
            }
        } catch (IOException ex) {
            Logger.getLogger(Summary.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}