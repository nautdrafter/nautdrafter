/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.model;

import javafx.scene.image.Image;

import com.monkygames.nautdrafter.resource.ImageCacher;

/**
 * An enum to represent a 'naut or Character. It contains all of the details that are needed about a charcater and
 * methods to access some of the data.
 */
public enum Character {

	//private Character(int id, String name, String shortName, int health, float maxHealth, float movementSpeed,
	//        AttackType attack, Mobility mobility, Role... roles) {
	/**
	 * == For Maintainers (as private javadoc comments are not included in generated javadoc) ==
	 * id 		- a unique identifier for the character (generally in order if the level on which they unlock)
	 * name 	- the name of the character
	 * shortName 	- a short human-friendly name given to access resources (needs to be consistent across multiple folders)
	 * health 	- the health of the character
	 * maxHealth 	- the maximum health of the character
	 * movementSpeed - the movement speed of the character
	 * attack 	- the attack type of the character
	 * mobility 	- the mobility type of the character
	 * roles 	- the roles of the character (can be multiple)
	 */

	LEON(	0, 			// ID
	     	"Leon Chameleon",  	// name
	     	"leon_chameleon",  	// short name
		1250, 			// health
		1625, 			// max health
		7.8f, 			// movement speed
		AttackType.MELEE, 	// attack type
		Mobility.SWIFT, 	// mobility
		Role.ASSASSIN, Role.INITIATOR, Role.DAMAGE_DEALER), // roles

	FROGGY_G(1, 			 // ID
		"Froggy G",		 // name          
		"froggy_g",              // short name
		1150,                    // health
		1495f,                   // max health
		8.4f,                    // movement speed
		AttackType.SHORT_RANGE,  // attack type
		Mobility.SWIFT,          // mobility
		Role.ASSASSIN, Role.INITIATOR, Role.NUKER), // roles

	SHERRIF_LONESTAR(2,			 // ID            
			"Sheriff Lonestar",      // name          
			"sheriff_lonestar",      // short name
			1300,                    // health
			1690,                    // max health
			7.4f,                    // movement speed
			AttackType.MEDIUM_RANGE, // attack type
	        	Mobility.TACTICAL,       // mobility
			Role.FIGHTER, Role.PUSHER), // roles

	CLUNK(	3, 				// ID            
		"Clunk",                        // name          
		"clunk",                        // short name
		1900,                           // health
		2470,                           // max health
		7.6f,                           // movement speed
		AttackType.MEDIUM,              // attack type
		Mobility.TACTICAL,              // mobility
		Role.TANK, Role.CROWD_CONTROL, Role.NUKER), // roles

	VOLTAR(	4, 				// ID            
		"Voltar the Omniscient",        // name          
		"voltar_the_omniscient",        // short name
		1200,                           // health
		1560f,                          // max health
		7.6f,                           // movement speed
		AttackType.SHORT_RANGE,         // attack type
	        Mobility.TACTICAL,              // mobility
		Role.SUPPORT, Role.PUSHER),	// roles

	GNAW(	5, 			// ID            
		"Gnaw",                 // name          
		"gnaw",                 // short name
		1350,                   // health
		1755,                   // max health
		7.8f,                   // movement speed
		AttackType.MELEE,       // attack type
		Mobility.BALANCED,      // mobility
		Role.HARASSER, Role.AREA_CONTROL, Role.DAMAGE_DEALER),

	COCO(	6, 			// ID            
		"Coco Nebulon",         // name          
		"coco_nebulon",         // short name
		1250,                   // health
		1625,                   // max health
		9.4f,                   // movement speed
		AttackType.MELEE,       // attack type
		Mobility.SWIFT,         // mobility
		Role.FIGHTER, Role.AREA_CONTROL, Role.HARASSER),

	SKOLLDIR(7, 			// ID            
		"Sk\u00F8lldir",        // name          
		"skolldir",             // short name
		1600,                   // health
		2080,                   // max health
		7.4f,                   // movement speed
		AttackType.MELEE,       // attack type
		Mobility.BALANCED,      // mobility
		Role.FIGHTER, Role.INITIATOR, Role.CROWD_CONTROL),

	YURI(	8, 			// ID            
		"Yuri",                 // name          
		"yuri",                 // short name
		1350,                   // health
		1755,                   // max health
		8.2f,                   // movement speed
		AttackType.LONG_RANGE,  // attack type
		Mobility.AERIAL,        // mobility
		Role.SUPPORT, Role.PUSHER, Role.AREA_CONTROL),

	RAELYNN(9, 			// ID            
		"Raelynn",              // name          
		"raelynn",              // short name
		1300,                   // health
		1690,                   // max health
		7.4f,                   // movement speed
		AttackType.LONG_RANGE,  // attack type
		Mobility.TACTICAL,      // mobility
		Role.HARASSER, Role.PUSHER, Role.AREA_CONTROL),

	DERPL(	10, 				// ID            
		"Derpl Zork",                   // name          
		"derpl_zork",                   // short name
		1800,                           // health
		2340,                           // max health
		7.2f,                           // movement speed
		AttackType.MEDIUM_RANGE,        // attack type
		Mobility.TACTICAL,              // mobility
		Role.TANK, Role.AREA_CONTROL, Role.NUKER),

	VINNIE_SPIKE(	11, 				// ID            
			"Vinnie & Spike",               // name          
			"vinnie_spike",                 // short name
			1250,                           // health
			1625,                           // max health
			6.8f,                           // movement speed
			AttackType.MEDIUM_RANGE,        // attack type
			Mobility.AERIAL,                // mobility
	        	Role.ASSASSIN, Role.CROWD_CONTROL, Role.NUKER),

	GENJI(	12, 				// ID            
		"Genji the Pollen Prophet",     // name          
		"genji_the_pollen_prophet",     // short name
		1250,                           // health
		1625f,                          // max health
		8.4f,                           // movement speed
		AttackType.MEDIUM_RANGE,        // attack type
	        Mobility.TACTICAL,              // mobility
		Role.SUPPORT, Role.INITIATOR, Role.CROWD_CONTROL),

	AYLA(	13, 			// ID            
		"Ayla",                 // name          
		"ayla",                 // short name
		1300,                   // health
		1690,                   // max health
		7.4f,                   // movement speed
		AttackType.MELEE,       // attack type
		Mobility.BALANCED,      // mobility
		Role.FIGHTER, Role.DAMAGE_DEALER),

	ADMIRAL_SWIGGINS(14,			 // ID            
			"Admiral Swiggins",      // name          
			"admiral_swiggins",      // short name
			1500,                    // health
			1950,                    // max health
			7.2f,                    // movement speed
			AttackType.COMBINED,     // attack type
	        	Mobility.TACTICAL,       // mobility
			Role.INITIATOR, Role.FIGHTER, Role.CROWD_CONTROL),

	PENNY(	15, 			// ID            
		"Penny Fox",            // name          
		"penny_fox",            // short name
		1250,                   // health
		1625,                   // max health
		8.8f,                   // movement speed
		AttackType.COMBINED,    // attack type
		Mobility.SWIFT,         // mobility
		Role.ASSASSIN, Role.HARASSER, Role.PUSHER),

	SENTRY(	16, 				// ID            
		"Sentry X-58",                  // name          
		"sentry_x58",                   // short name
		1550,                           // health
		2015,                           // max health
		6.8f,                           // movement speed
		AttackType.MEDIUM_RANGE,        // attack type
		Mobility.TACTICAL,              // mobility
		Role.TANK, Role.CROWD_CONTROL, Role.AREA_CONTROL),

	SKREE(	17, 			// ID            
		"Skree",                // name          
		"skree",                // short name
		1350,                   // health
		1755,                   // max health
		8.2f,                   // movement speed
		AttackType.LONG_RANGE,  // attack type
		Mobility.SWIFT,         // mobility
		Role.AREA_CONTROL, Role.HARASSER, Role.DAMAGE_DEALER),

	TED(	18, 				// ID            
		"Ted McPain",                   // name          
		"ted_mcpain",                   // short name
		1400,                           // health
		1820,                           // max health
		7.6f,                           // movement speed
		AttackType.MEDIUM_RANGE,        // attack type
		Mobility.TACTICAL,              // mobility
	        Role.DAMAGE_DEALER, Role.FIGHTER),

	SCOOP(	19, 			// ID            
		"Scoop",                // name          
		"scoop",                // short name
		1600,                   // health
		2080,                   // max health
		7.2f,                   // movement speed
		AttackType.MELEE,       // attack type
		Mobility.BALANCED,      // mobility
		Role.TANK, Role.CROWD_CONTROL),

	NIBBS(	20, 			// ID            
		"Nibbs",                // name          
		"nibbs",                // short name
		1250,                   // health
		1625,                   // max health
		7f,                   // movement speed
		AttackType.MELEE,       // attack type
		Mobility.TACTICAL,      // mobility
		Role.ASSASSIN, Role.NUKER, Role.DAMAGE_DEALER),

	ROCCO(	21, 			// ID            
		"Rocco",                // name          
		"rocco",                // short name
		1250,                   // health
		1625,                   // max health
		7f,                     // movement speed
		AttackType.LONG_RANGE,  // attack type
		Mobility.TACTICAL,      // mobility
		Role.FIGHTER, Role.NUKER, Role.AREA_CONTROL),

	KSENIA(	22, 			// ID            
		"Ksenia",               // name          
		"ksenia",               // short name
		1150,                   // health
		1495,                   // max health
		7f,                     // movement speed
		AttackType.LONG_RANGE,  // attack type
		Mobility.SWIFT,         // mobility
		Role.ASSASSIN, Role.DAMAGE_DEALER),

	UNKNOWN(-1, "Unknown", "unknown", 0, 0, 0, AttackType.UNKNOWN, Mobility.UNKNOWN, Role.UNKNOWN),

	NO_BAN(-2, "No Ban", "no_ban", 0, 0, 0, AttackType.UNKNOWN, Mobility.UNKNOWN, Role.UNKNOWN);

	static String IMAGE_DIR = "assets/ronimo/images/";

	/**
	 * Represents the types of image to retrieve.
	 */
	public enum IconType {

		/**
		 * A small square icon of the character.
		 */
		ICON,

		/**
		 * A detailed larger render of the character
		 */
		RENDER,
	}

	/**
	 * Represents the types of attacks of the characters
	 */
	public enum AttackType {
		MELEE("Melee"), MEDIUM("Medium"), MEDIUM_RANGE("Medium Range"), SHORT_RANGE("Short Range"), LONG_RANGE(
		        "Long Range"), COMBINED("Melee/Ranged"), UNKNOWN("?");

		public final String displayName;

		private AttackType(String displayName) {
			this.displayName = displayName;
		}

		@Override
		public String toString() {
			return this.displayName;
		}
	}

	/**
	 * Represents the types of mobility of the characters
	 */
	public enum Mobility {
		SWIFT("Swift"), BALANCED("Balanced"), AERIAL("Aerial"), TACTICAL("Tactical"), UNKNOWN("?");

		public final String displayName;

		private Mobility(String displayName) {
			this.displayName = displayName;
		}

		@Override
		public String toString() {
			return this.displayName;
		}
	}

	/**
	 * Represents the types of role of the characters
	 */
	public enum Role {
		AREA_CONTROL("Area Control"),
		ASSASSIN("Assassin"),
		CROWD_CONTROL("Crowd Control"),
		DAMAGE_DEALER("Damage Dealer"),
		FIGHTER("Fighter"),
		HARASSER("Harasser"),
		INITIATOR("Initiator"),
		NUKER("Nuker"),
		PUSHER("Pusher"),
		SUPPORT("Support"),
		TANK("Tank"),
		UNKNOWN("?");
		public final String displayName;

		private Role(String displayName) {
			this.displayName = displayName;
		}

		@Override
		public String toString() {
			return this.displayName;
		}

	}

	/**
	 * @param id
	 *            a unique identifier for the character (generally in order if the level on which they unlock)
	 * @param name
	 *            the name of the character
	 * @param shortName
	 *            a short human-friendly name given to access resources (needs to be consistent across multiple folders)
	 * @param health
	 *            the health of the character
	 * @param maxHealth
	 * 	      the maximum health of the character
	 * @param movementSpeed
	 *            the movement speed of the character
	 * @param attack
	 *            the attack type of the character
	 * @param mobility
	 *            the mobility type of the character
	 * @param roles
	 *            the roles of the character (can be multiple)
	 */
	private Character(int id, String name, String shortName, int health, float maxHealth, float movementSpeed,
	        AttackType attack, Mobility mobility, Role... roles) {
		this.name = name;
		this.id = (byte) id;
		this.shortName = shortName;
		this.health = health;
		this.maxHealth = maxHealth;
		this.movementSpeed = movementSpeed;
		this.attack = attack;
		this.mobility = mobility;
		this.roles = roles;
	}

	public final String     name;
	public final byte       id;
	public final String     shortName;
	public final int        health;
	public final Number     maxHealth;
	public final float      movementSpeed;
	public final AttackType attack;
	public final Mobility   mobility;
	public final Role[]     roles;

	/**
	 * Get the character that corresponds to the given id
	 *
	 * @param id
	 *            the id of the character rquested
	 * @return the character that corresponds to the id if it exist, otherwise Character.UNKNOWN will be returned
	 */
	public static Character getById(int id) {
		for (Character c : Character.values()) {
			if (c.id == id) {
				return c;
			}
		}

		return Character.UNKNOWN;
	}

	/**
	 * Fetches an image associated with the character in the requested format.
	 *
	 * @param type
	 *            The type of image to return, as defined in {@link IconType}
	 * @return An image object containing the image for the character
	 */
	public Image getImage(IconType type) {
		String middle = "";

		switch (type) {
		case ICON:
			middle = "icons/";
			break;
		case RENDER:
			middle = "renders/";
		}

		return ImageCacher.get(IMAGE_DIR + middle + this.shortName + ".png");
	}

	/**
	 * User friendly display name
	 */
	@Override
	public String toString() {
		return this.name;
	}

	/**
	 * Convenience method to display health stats. Display health like: "base (max)" health. Cuts off any extra 0's
	 *
	 * @return the string representation of the character health stats in the form of: health (maxHealth)
	 */
	public String getDisplayHealth() {
		return this.health
		        + " ("
		        + (this.maxHealth.floatValue() == this.maxHealth.intValue() ? this.maxHealth.intValue()
		                : this.maxHealth) + ")";
	}
}
