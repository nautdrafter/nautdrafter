/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.model;

import java.util.ArrayList;

/**
 * A list of planet ids that can be used selected for use by players.
 */
public class PlanetPool {
    /**
     * The name of the planet pool.
     */
    private String name;
    /**
     * The ids of the planets that can be used in this pool.
     */
    private ArrayList <Integer> planetIds;

    /**
     * Creates a new Planet Pool with the specified planet ids.
     * @param name the name of this planet pool.
     * @param ids a list of integers that represent the planets to use.
     */
    public PlanetPool(String name, int ... ids){
	this.name = name;
	planetIds = new ArrayList<Integer>();
	for (int i = 0; i < ids.length; i++){
	    planetIds.add(ids[i]);
	}
    }

    /**
     * Returns the name of this Planet Pool.
     * @return the name of this pool.
     */
    public String getName(){
	return name;
    }

    /**
     * Returns a list of valid ids.
     * @return a list of planet ids to be used.
     */

    public ArrayList<Integer> getPlanets(){
	return planetIds;
    }

    /**
     * Returns the number of planets are in this pool.
     * @return the size of the pool.
     */
    public int size(){
	return planetIds.size();
    }
}