/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.model;

import javafx.scene.image.Image;

import com.monkygames.nautdrafter.resource.ImageCacher;

/**
 * Used to represent a single, particular AwesomeNauts map or an unknown/undecided map.
 */
public enum Planet {
	UNKNOWN(0, "Random", "unknown"),
	RIBBIT_IV(1, "Ribbit IV", "ribbit"),
	AI_STATION_404(2, "AI Station 404", "ai-station-404"),
	SORONA(3, "Sorona", "sorona"),
	AIGUILLON(4, "Aiguillon", "aiguillon"),
	AI_STATION_205(5, "AI Station 205", "ai-station-205");

	static String IMAGE_DIR = "assets/ronimo/images/maps/";

	/**
	 * Represents the types of map images that may (not guaranteed) be available for a given map.
	 */
	public enum ImageType {
		/**
		 * A small, square thumbnail image.
		 */
		THUMB,
		/**
		 * A detailed, close-up view of the map
		 */
		DETAILED,
		/**
		 * Mini map as displayed in AwesomeNauts
		 */
		MAP;
	}

	private byte   id;
	private String name;
	private String shortName;

	/**
	 * @param id
	 *            A unique number that can be used to represent the map
	 * @param name
	 *            User-friendly name or title of the map
	 * @param shortName
	 *            A short name for the map that will be used to represent the map in all file structures. Will be
	 *            prepended to a value based on {@link ImageType}. See code for {@link Planet#getImage(ImageType)}
	 */
	private Planet(int id, String name, String shortName) {
		this.id = (byte) id;
		this.name = name;
		this.shortName = shortName;
	}

	/**
	 * Gets the Map object that corresponds to the given ID.
	 *
	 * @param id
	 *            ID number of the map being requested
	 * @return the Map object corresponding to the provided ID, or Map.UNKNOWN if the given ID does not correspond to a
	 *         map.
	 */
	public static Planet getById(int id) {
		for (Planet m : Planet.values()) {
			if (m.id == id) {
				return m;
			}
		}

		return Planet.UNKNOWN;
	}

	/**
	 * Gets an ID number that can be used to represent this map. {@link Planet#getById(int)} can be used to get the map
	 * object from the returned ID.
	 *
	 * @return The ID that can be used to represent this map
	 */
	public byte getId() {
		return this.id;
	}

	/**
	 * Fetches an image associated with the map in the requested format.
	 *
	 * @param type
	 *            The type of image to return, as defined in {@link ImageType}
	 * @return An image object containing the image for the map
	 */
	public Image getImage(ImageType type) {
		String suffix = "";

		switch (type) {
		case THUMB:
			suffix = "_thumb";
			break;
		case DETAILED:
			suffix = "";
			break;
		case MAP:
			suffix = "_map";
			break;
		default:
			break;
		}

		return ImageCacher.get(IMAGE_DIR + this.shortName + suffix + ".png");
	}

	/**
	 * Return the user-friendly name associated with the map. "Random" will be returned for {@link Planet#UNKNOWN}
	 *
	 * @return The name associated with the map
	 */
	public String getName() {
		return this.name;
	}

	@Override
	public String toString() {
		return this.getName();
	}
}
