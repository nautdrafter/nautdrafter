/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.model;

import com.monkygames.nautdrafter.resource.LanguageResource;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.util.Pair;

import com.monkygames.nautdrafter.network.Team;
import com.monkygames.nautdrafter.network.Timeline;
import com.monkygames.nautdrafter.network.Timeline.TimelineItem;

/**
 * Contains the necessary strings to be able to format a block of text into a certain format e.g. HTML or BBCode.
 */
public enum Formatter {

	@SuppressWarnings("serial")
	HTML("versus.save.html", "<b>", "</b>", "<ul>", "</ul>", "<li>", "</li>", "<ol>", "</ol>", "<li>", "</li>", "<h1>", "</h1>",
	        "<h2>", "</h2>", "<i>", "</i>", "<hr>", new CharacterEncoder(new HashMap<String, String>() {
		        {
			        this.put("<", "&lt;");
			        this.put(">", "&gt;");
			        this.put("&", "&amp;");
		        }
	        })),

	@SuppressWarnings("serial")
	Markdown("versus.save.markdown", "**", "**", "", "", "+ ", "", "", "", "1. ", "", "## ", " ##", "#### ", " ####", "_", "_", "\n----\n",
	        new CharacterEncoder(new HashMap<String, String>() {
		        {
			        this.put("*", "\\*");
			        this.put("_", "\\_");
			        this.put("+", "\\+");
			        this.put("-", "\\-");
			        this.put("~", "\\~");
			        this.put("^", "\\^");
			        this.put("#", "\\#");
			        this.put("=", "\\=");
			        this.put(".", "\\.");
			        this.put("`", "\\`");
			        this.put("_", "\\_");
			        this.put("[", "\\[");
			        this.put("]", "\\]");
			        this.put("(", "\\(");
			        this.put(")", "\\)");
			        this.put(">", "\\>");
			        this.put(":", "\\:");
			        this.put("|", "\\|");
		        }
	        })),

	@SuppressWarnings("serial")
	BBCode("versus.save.bbcode", "[b]", "[/b]", "[list]", "[/list]", "[*]", "", "[list=1]", "[/list]", "[*]", "", "[size=130]", "[/size]",
	        "[b]", "[/b]", "[i]", "[/i]", "\n", new CharacterEncoder(new HashMap<String, String>() {
		        {
			        this.put("[", "{");
			        this.put("]", "}");
		        }
	        })),

	PlainText("versus.save.plain", "", "", "", "", "\t", "", "", "", "\t", "", "", "", "", "", "", "", "", null);

	/**
	 * A encoder to remove special characters for different formats
	 */
	public static class CharacterEncoder {

		private static final Pattern      regexRegex = Pattern.compile("[-\\[\\]{}()*+?.,\\\\^$|#\\s]");
		private final Pattern             regex;
		private final Map<String, String> replacements;

		/**
		 * Construct an encoder with a String mapping
		 * 
		 * @param replacements
		 *            The replacements that need to be made mapping the substrings to the substring it should be
		 *            replaced with
		 */
		public CharacterEncoder(Map<String, String> replacements) {
			this.replacements = replacements;
			StringBuilder find = new StringBuilder();
			boolean start = true;
			for (String match : this.replacements.keySet()) {
				if (start) {
					start = false;
				} else {
					find.append("|");
				}
				Matcher matcher = regexRegex.matcher(match);
				StringBuilder escapedMatch = new StringBuilder();
				int i = 0;
				while (matcher.find()) {
					escapedMatch.append(match.substring(i, matcher.start())).append("\\").append(matcher.group());
					i = matcher.end();
				}
				escapedMatch.append(match.substring(i));
				find.append(escapedMatch);
			}
			this.regex = Pattern.compile(find.toString());
		}

		/**
		 * Encode the string given by following the replacement map given in the constructor
		 * 
		 * @param input
		 *            The String to encode
		 * @return
		 */
		public String encode(String input) {
			Matcher matcher = this.regex.matcher(input);
			StringBuilder output = new StringBuilder();
			int i = 0;
			while (matcher.find()) {
				output.append(input.substring(i, matcher.start()));
				String replacement = this.replacements.get(matcher.group());
				output.append(replacement == null ? matcher.group() : replacement);
				i = matcher.end();
			}
			output.append(input.substring(i));
			return output.toString();
		}
	}

	/**
	 * Key for the name of the format in the resource bundle
	 */
	public final String            nameKey;

	/**
	 * The series of characters that begins a block of bold text
	 */
	private final String           boldStart;
	/**
	 * The series of characters that ends a block of bold text
	 */
	private final String           boldEnd;
	/**
	 * The series of characters that begins a list items
	 */
	private final String           listStart;
	/**
	 * The series of characters that ends a list items
	 */
	private final String           listEnd;
	/**
	 * The series of characters that begins an item that should be in a list or tabbed out
	 */
	private final String           tabStart;
	/**
	 * The series of characters that ends an item that should be in a list or tabbed out
	 */
	private final String           tabEnd;

	/**
	 * The series of characters that begins an ordered list of items
	 */
	private final String           olistStart;
	/**
	 * The series of characters that ends an ordered list of items
	 */
	private final String           olistEnd;
	/**
	 * The series of characters that begins an item that should be in an ordered list or tabbed out
	 */
	private final String           otabStart;
	/**
	 * The series of characters that ends an item that should be in an ordered list or tabbed out
	 */
	private final String           otabEnd;
	/**
	 * The series of characters that begins a level 1 heading
	 */
	private final String           h1Start;
	/**
	 * The series of characters that ends a level 1 heading
	 */
	private final String           h1End;
	/**
	 * The series of characters that begins a level 2 heading
	 */
	private final String           h2Start;
	/**
	 * The series of characters that ends a level 1 heading
	 */
	private final String           h2End;
	/**
	 * The series of characters that begins a block of text that should be italic
	 */
	private final String           italicStart;
	/**
	 * The series of characters that ends a block of text that should be italic
	 */
	private final String           italicEnd;
	/**
	 * The series of characters that indicates a horizontal line
	 */
	private final String           horizonalRule;
	/**
	 * The CharacterEncoder for the Format
	 */
	private final CharacterEncoder encoder;

	/**
	 * Construct a new formatter object to be applied to a block of text
	 * 
	 * @param nameKey
	 *            Key for the name of the format in the resource bundle
	 * @param boldStart
	 *            The series of characters that begins a block of bold text
	 * @param boldEnd
	 *            The series of characters that ends a block of bold text
	 * @param listStart
	 *            The series of characters that begins a list items
	 * @param listEnd
	 *            The series of characters that ends a list items
	 * @param tabStart
	 *            The series of characters that begins an item that should be in a list or tabbed out
	 * @param tabEnd
	 *            The series of characters that ends an item that should be in a list or tabbed out
	 * @param newLine
	 *            The series of characters that indicates a new line
	 * @param olistStart
	 *            The series of characters that ends an ordered list of items
	 * @param olistEnd
	 *            The series of characters that ends an ordered list of items
	 * @param otabStart
	 *            The series of characters that begins an item that should be in an ordered list or tabbed out
	 * @param otabEnd
	 *            The series of characters that ends an item that should be in an ordered list or tabbed out
	 * @param h1Start
	 *            The series of characters that begins a level 1 heading
	 * @param h1End
	 *            The series of characters that ends a level 1 heading
	 * @param h2Start
	 *            The series of characters that begins a level 2 heading
	 * @param h2End
	 *            The series of characters that ends a level 1 heading
	 * @param italicStart
	 *            The series of characters that begins a block of text that should be italic
	 * @param italicEnd
	 *            The series of characters that ends a block of text that should be italic
	 * @param horizonalRule
	 *            The series of characters that indicates a horizontal line
	 */
	private Formatter(String nameKey, String boldStart, String boldEnd, String listStart, String listEnd, String tabStart,
	        String tabEnd, String olistStart, String olistEnd, String otabStart, String otabEnd,
	        String h1Start, String h1End, String h2Start, String h2End, String italicStart, String italicEnd,
	        String horizonalRule, CharacterEncoder encoder) {
		this.nameKey = nameKey;
		
		this.boldStart = boldStart;
		this.boldEnd = boldEnd;
		this.listStart = listStart;
		this.listEnd = listEnd;
		this.tabStart = tabStart;
		this.tabEnd = tabEnd;
		this.olistStart = olistStart;
		this.olistEnd = olistEnd;
		this.otabStart = otabStart;
		this.otabEnd = otabEnd;
		this.h1Start = h1Start;
		this.h1End = h1End;
		this.h2Start = h2Start;
		this.h2End = h2End;
		this.italicStart = italicStart;
		this.italicEnd = italicEnd;
		this.horizonalRule = horizonalRule;
		this.encoder = encoder;
	}

	private String encode(String input) {
		return (this.encoder == null) ? input : this.encoder.encode(input);
	}

	@SuppressWarnings("unchecked")
	public String format(Team red, Team blue, Planet map, Timeline timeline) {
		int banCount = red.bannedNauts[0] == -1 ? 0 : red.bannedNauts[1] == -1 ? 1 : red.bannedNauts[2] == -1 ? 2 : 3;
		StringBuilder s = new StringBuilder();
		s.append(this.h1Start)
			.append(LanguageResource.getString("formatter.game_info"))
			.append(":")
		    .append(this.h1End)
		    .append("\n")
		    .append(this.boldStart)
		    .append(LanguageResource.getString("formatter.map"))
		    .append(":")
		    .append(this.boldEnd)
		    .append(" ")
		    .append(this.encode(map.getName()))
		    .append("\n")
		    .append(this.horizonalRule)
		    .append("\n");
		for (Pair<String, Team> p : new Pair[] { new Pair<String, Team>(LanguageResource.getString("formatter.red_team")+"):", red),
		        new Pair<String, Team>(LanguageResource.getString("formatter.blue_team")+"):", blue) }) {
			Team team = p.getValue();
			// team name
			s.append(this.h1Start)
				.append(this.encode(team.name))
				.append(" (")
				.append(p.getKey())
				.append(this.h1End)
			    .append("\n")
			    .append(this.h2Start)
			    .append(LanguageResource.getString("formatter.players"))
			    .append(":")
			    .append(this.h2End)
			    .append("\n")
			    .append(this.listStart);
			// players
			for (int i = 0; i < 3; i++) {
				s.append(this.tabStart)
					.append(this.encode(team.players[i].name));
				if (i == 0) {
					s.append(" ")
						.append(this.italicStart)
						.append("(")
						.append(LanguageResource.getString("formatter.captain"))
						.append(")")
						.append(this.italicEnd);
				}
				s.append(this.tabEnd)
					.append("\n");
			}
			// picks
			s.append(this.listEnd)
				.append("\n")
				.append(this.h2Start)
				.append(LanguageResource.getString("formatter.picks"))
				.append(":")
				.append(this.h2End)
			    .append("\n")
			    .append(this.olistStart);
			for (int i = 0; i < 3; i++) {
				Character pick = Character.getById(team.pickedNauts[i]);
				s.append(this.otabStart)
					.append(this.encode(pick.name))
					.append(this.otabEnd)
					.append("\n");
			}
			// bans
			s.append(this.olistEnd)
				.append("\n")
				.append(this.h2Start)
				.append(LanguageResource.getString("formatter.bans"))
				.append(":")
				.append(this.h2End)
			    .append("\n")
			    .append(this.olistStart);
			for (int i = 0; i < banCount; i++) {
				Character ban = Character.getById(team.bannedNauts[i]);
				s.append(this.tabStart)
					.append(this.encode(ban.name))
					.append(this.tabEnd)
					.append("\n");
			}
			s.append(this.olistEnd)
				.append(this.horizonalRule)
				.append("\n");
			
		}
		// drafting sequence
		s.append(this.h1Start)
			.append(LanguageResource.getString("formatter.drafting_sequence"))
			.append(":")
			.append(this.h1End)
			.append("\n");
		if (timeline == null) {
			s.append(LanguageResource.getString("formatter.could_not_get"))
				.append("\n");
		} else {
			s.append(this.h2Start)
				.append(LanguageResource.getString("formatter.full"))
				.append(":")
				.append(this.h2End)
				.append("\n");
			StringBuilder shortened = new StringBuilder(this.h2Start)
				.append(LanguageResource.getString("formatter.order_only"))
				.append(":")
			    .append(this.h2End)
			    .append("\n");
			int[] pick = new int[2], ban = new int[2];
			boolean first = true;
			s.append(this.listStart);
			for (TimelineItem i : timeline.items) {
				Team t = i.team == Team.RED ? red : blue;
				if (i.type == Timeline.PICK) {
					i.pos = pick[i.team - 1]++;
					i.charId = t.pickedNauts[i.pos];
				} else {
					i.pos = ban[i.team - 1]++;
					i.charId = t.bannedNauts[i.pos];
				}

				s.append(this.tabStart)
					.append(this.boldStart)
					.append(i.team == Team.RED ? LanguageResource.getString("formatter.red") : LanguageResource.getString("formatter.blue"))
					.append(" ")
				    .append(i.type == Timeline.PICK ? LanguageResource.getString("formatter.pick") : LanguageResource.getString("formatter.ban"))
				    .append(" #").append(i.pos + 1)
				    .append(this.boldEnd)
				    .append(": ")
				    .append(this.encode(Character.getById(i.charId).name))
				    .append(this.tabEnd)
				    .append("\n");
				if (first) {
					first = false;
				} else {
					shortened.append(", ");
				}
				shortened.append(i.team == Team.RED ? "R-" : "B-")
					.append(i.type == Timeline.PICK ? "P" : "B");
			}
			s.append(this.listEnd).append("\n")
				.append(shortened);
		}
		return s.toString();
	}
}
