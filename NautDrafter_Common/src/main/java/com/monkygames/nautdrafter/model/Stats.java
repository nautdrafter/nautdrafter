/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.model;

import java.util.ArrayList;

/**
 * Contains stats for a series of matches like the number of times a naut was picked or banned.
 */
public class Stats {

    public ArrayList<CharacterStats> characterStats;

    public Stats(){
        characterStats = new ArrayList<>();
        // populate t
        for(Character character: Character.values()){
            characterStats.add(new CharacterStats(character));
        }
    }

    public ArrayList<CharacterStats> getPickSorted(){
        ArrayList<CharacterStats> sorted = new ArrayList<>();

        // insertion sort
        for(CharacterStats stat: characterStats){
            boolean inserted = false;
            //for(CharacterStats comparStat: sorted){
            for(int i = 0; i < sorted.size(); i++){
                CharacterStats comparStat = sorted.get(i);
                if(comparStat.picks > stat.picks){
                    sorted.add(i, stat);
                    inserted = true;
                    break;
                }
            }
            if(!inserted){
                sorted.add(stat);
            }
        }
        return sorted;
    }

    public ArrayList<CharacterStats> getBanSorted(){
        ArrayList<CharacterStats> sorted = new ArrayList<>();

        // insertion sort
        for(CharacterStats stat: characterStats){
            boolean inserted = false;
            //for(CharacterStats comparStat: sorted){
            for(int i = 0; i < sorted.size(); i++){
                CharacterStats comparStat = sorted.get(i);
                if(comparStat.bans > stat.bans){
                    sorted.add(i, stat);
                    inserted = true;
                    break;
                }
            }
            if(!inserted){
                sorted.add(stat);
            }
        }

        return sorted;
    }

    /**
     * Adds a pick to the character stats.
     * @param name the character name of the stats to increase.
     */
    public void addPick(String name){
        for(CharacterStats stat: characterStats){
           if(stat.character.name.equals(name) || 
               stat.character.shortName.equals(name)){
               stat.picks++;
               break;
           } 
        }
    }

    /**
     * Adds a pick to the character stats.
     * @param name the character name of the stats to increase.
     */
    public void addBan(String name){
        for(CharacterStats stat: characterStats){
           if(stat.character.name.equals(name) || 
               stat.character.shortName.equals(name)){
               stat.bans++;
               break;
           } 
        }
    }
    
    // internal class that holds character stats
    public class CharacterStats {
        public Character character;
        public int picks = 0;
        public int bans = 0;
        private CharacterStats(Character character){
            this.character = character;
        }
    }

}
