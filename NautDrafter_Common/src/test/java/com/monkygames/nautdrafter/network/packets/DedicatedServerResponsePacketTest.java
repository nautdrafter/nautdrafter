/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.monkygames.nautdrafter.network.Player;
import com.monkygames.nautdrafter.network.PlayerIdentifier;
import com.monkygames.nautdrafter.network.callbacks.DedicatedServerRequestCallback;

/**
 * @see com.monkygames.nautdrafter.network.packets.PlayerLeftPacket
 */
public class DedicatedServerResponsePacketTest {

	public DedicatedServerResponsePacketTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of encoding and decoding, of class PlayerJoinedPacket
	 */
	@Test
	public void testPacket() throws Exception {
            DedicatedServerResponsePacket encodePacket = new DedicatedServerResponsePacket(DedicatedServerRequestCallback.RESPONSE_SUCCESS);
            DedicatedServerResponsePacket decodePacket = new DedicatedServerResponsePacket(encodePacket.data);
            decodePacket.decode();
            Assert.assertEquals(encodePacket.success,decodePacket.success);

            encodePacket = new DedicatedServerResponsePacket(DedicatedServerRequestCallback.RESPONSE_FAILED_EXISTING_SERVER);
            decodePacket = new DedicatedServerResponsePacket(encodePacket.data);
            decodePacket.decode();
            Assert.assertEquals(encodePacket.success,decodePacket.success);

            encodePacket = new DedicatedServerResponsePacket(DedicatedServerRequestCallback.RESPONSE_FAILED_NO_SERVERS);
            decodePacket = new DedicatedServerResponsePacket(encodePacket.data);
            decodePacket.decode();
            Assert.assertEquals(encodePacket.success,decodePacket.success);

            encodePacket = new DedicatedServerResponsePacket(DedicatedServerRequestCallback.RESPONSE_FAILED_NO_STEAM);
            decodePacket = new DedicatedServerResponsePacket(encodePacket.data);
            decodePacket.decode();
            Assert.assertEquals(encodePacket.success,decodePacket.success);
	}

}
