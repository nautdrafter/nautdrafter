/*
 * Copyright (C) 2014 NautDrafter Common
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.monkygames.nautdrafter.network.packets.Packet;
import com.monkygames.nautdrafter.network.packets.TestPacket;

/**
 * Server implementation for testing
 */
public class TestServer extends Server {

	public TestServer(InetSocketAddress addr) throws IOException {
		super(addr);
	}

	@Override
	protected void write(SelectionKey key) {
		try {
			ConnectionInfo info = (ConnectionInfo) key.attachment();
			if (info.sendBuffer.position() > 0) {
				System.out.println("TestServer sending: " + (101 - info.sendBuffer.position()) + "/" + 100);
				// go to start of buffer
				info.sendBuffer.flip();
				// backup limit
				int limit = info.sendBuffer.limit();
				// write one byte at a time
				info.sendBuffer.limit(1);
				// write any data
				((SocketChannel) key.channel()).write(info.sendBuffer);
				// restore limit
				info.sendBuffer.limit(limit);
				// move any unwritten data to the start of the buffer
				info.sendBuffer.compact();
			}
			// check if we have more data to write
			// if so, we need to listen to the OP_WRITE (ready to write) event
			if (info.sendBuffer.position() > 0) {
				key.interestOps(key.interestOps() | SelectionKey.OP_WRITE);
			} else {
				key.interestOps(key.interestOps() & ~SelectionKey.OP_WRITE);
				// once we've written everything, we can kill the client
				if (info.kill) {
					this.kill(key);
				}
			}
		} catch (IOException ex) {
			Logger.getLogger(TestServer.class.getName()).log(Level.SEVERE, null, ex);
			this.kill(key);
		}
	}

	@Override
	protected Packet createPacket(SelectionKey key, byte[] data) throws IOException {
		// create a packet that expects 100 bytes
		return new TestPacket(data, 100);
	}

}
