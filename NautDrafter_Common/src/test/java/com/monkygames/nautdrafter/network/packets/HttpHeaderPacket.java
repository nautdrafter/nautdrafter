/*
 * Copyright (C) 2014 NautDrafter Common
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import java.util.Arrays;

import com.monkygames.nautdrafter.Constants;

/**
 * Uses some HTTP headers to get through Openshift's virtual host routing
 */
public class HttpHeaderPacket extends Packet {

	public static final byte[] LobbyConnect = ("GET / HTTP/1.1\r\nHost: " + Constants.LOBBY_ADDRESS + "\r\n\r\n")
	                                                .getBytes(Packet.charset);

	public HttpHeaderPacket(byte[] data) {
		super(data);
		this.decodeFrom = 0;
	}

	@Override
	protected boolean decodeImpl() throws PacketDecoderException {
		int max = this.max();
		for (; this.decodeFrom < max; this.decodeFrom++) {
			if (this.decodeFrom < 4) {
				continue;
			}
			// read until the end of the HTTP header (\n\n or \r\n\r\n)
			if (this.data[this.decodeFrom] == 10
			        && ((this.data[this.decodeFrom - 1] == 10) || (this.data[this.decodeFrom - 1] == 13
			                && this.data[this.decodeFrom - 2] == 10 && this.data[this.decodeFrom - 3] == 13))) {
				this.pos = this.decodeFrom;
				return true;
			}
			// if this is not a HTTP header,
			// then the input buffer will fill up and cause an exception,
			// and we will disconnect
		}
		this.pos = this.decodeFrom;
		return false;
	}

	@Override
	protected void handleImpl() {
		System.out.println(new String(this.data, 0, this.pos, Packet.charset));
		this.response = new HttpHeaderPacket(Arrays.copyOf(this.data, this.pos));
	}

}
