/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets.segments;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.monkygames.nautdrafter.network.packets.TestPacket;

/**
 * @see com.monkygames.nautdrafter.network.packets.segments.BooleanSegment
 */
public class BooleanSegmentTest {

    public BooleanSegmentTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of encoding and decoding, of class IntSegment
     */
    @Test
    public void testSegment() throws Exception {
        // test for false value
        boolean val = false;
        BooleanSegment instance = new BooleanSegment();
        byte[] encoded = instance.encode(val);
        boolean decoded = instance.decode(new TestPacket(encoded));
        Assert.assertEquals(val, decoded);

        // test for true value
        val = true;
        encoded = instance.encode(val);
        decoded = instance.decode(new TestPacket(encoded));
        Assert.assertEquals(val, decoded);
    }
}
