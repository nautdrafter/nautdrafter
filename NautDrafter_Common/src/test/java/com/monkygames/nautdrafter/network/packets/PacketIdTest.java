/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network.packets;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PacketIdTest {

	public PacketIdTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Make sure no two packet IDs are the same
	 */
	@Test
	public void checkPacketIds() throws Exception {
		Map<Byte, Field> ids = new HashMap<>();
		for (Field f : Packet.class.getDeclaredFields()) {
			if (Modifier.isStatic(f.getModifiers()) && Modifier.isPublic(f.getModifiers())
			        && (f.getType().equals(byte.class) || f.getType().equals(Byte.class))
			        && f.getName().endsWith("_ID")) {
				byte value = f.getByte(null);
				System.err.println("Checking ID for " + f.getName() + " (" + value + ")");
				Field collision = ids.put(value, f);
				if (collision != null) {
					Assert.fail(f.getName() + " has the same ID as " + collision.getName());
				}
			}
		}
	}
}
